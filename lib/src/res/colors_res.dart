import 'package:flutter/material.dart';

class ColorsRes {
  static const purple_color = Color.fromRGBO(74, 17, 146, 1);
  static const purple_color_ligth = Color.fromRGBO(74, 17, 146, 0.45);
  static const light_blue_color = Color.fromRGBO(42, 208, 207, 1);
}
