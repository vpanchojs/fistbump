import 'package:flutter/material.dart';

import 'colors_res.dart';

class StyleRes {
  static const TextStyle text_title_intro = TextStyle(
    fontSize: 48,
    fontFamily: "Viga",
    color: ColorsRes.light_blue_color,
  );

  static const TextStyle text_description_intro = TextStyle(
    fontSize: 18,
    fontFamily: "montserrat",
    fontWeight: FontWeight.w100,
    color: ColorsRes.purple_color,
  );

  static const TextStyle text_appbar =
      TextStyle(fontFamily: 'montserrat', fontWeight: FontWeight.w600,color: Colors.black);

  static const TextStyle text_appbar_white = TextStyle(
      fontFamily: 'montserrat',
      fontWeight: FontWeight.w600,
      color: Colors.white);

  static const TextStyle text_populate_name_user = TextStyle(
      fontSize: 10,
      fontFamily: 'montserrat',
      fontWeight: FontWeight.w600,
      color: ColorsRes.purple_color);

  static const TextStyle text_name_user = TextStyle(
      fontSize: 22,
      fontFamily: 'montserrat',
      fontWeight: FontWeight.w600,
      color: ColorsRes.light_blue_color);

  static const TextStyle text_button_chip = TextStyle(
      fontSize: 12,
      fontFamily: 'montserrat',
      fontWeight: FontWeight.w600,
      color: ColorsRes.purple_color);

  static const TextStyle text_button_chip_white = TextStyle(
      fontSize: 12,
      fontFamily: 'montserrat',
      fontWeight: FontWeight.w600,
      color: Colors.white);
}
