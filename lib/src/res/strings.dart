const String string_tooltip_fab_challenge = "Crear nuevo reto";

//ERRORS
const String string_error_generic = "Algo salió mal, inténtalo más tarde";
const String string_error_ups = "Algo salió mal,";
const String string_error_undefid = "Problemas al procesar su solicitud";
const string_error_network = "Verifique la conexión a internet";

const string_error_challenge_delete =
    "No se pudo recuperar la información, es posible que el reto haya sido eliminado";

const String string_scroll_update = "$string_error_ups desliza para actualizar";

const String string_retry = "REINTENTAR";

///label o hints
const string_label_name_challenge = "Nombre";
const string_label_name_person = "Nombre";
const string_hint_name_person = "Ingrese su nombre";

const string_label_name_user = "Nombre Usuario";
const string_hint_name_user = "Ingrese su nombre de usuario";

const string_label_phone = 'Número de celular';
const string_hint_phone = "Ingrese su número de celular";

const string_label_birthdate = "Fecha de Nacimiento (opcional)";

const string_label_select_date = "Seleccionar Fecha";

const string_label_select_gender = "Seleccionar género";

const string_label_men = "Hombre";
const string_label_women = "Mujer";

const string_tooltip_edit_user = "Crear/Actualizar Usuario";
const string_tooltip_create_challenge= "Crear Reto";

const string_confirm_create = "Confirmar Creación";

const string_hint_name_challenge = "Ingrese el nombre del reto";
const string_label_description_challenge = "Descripción";
const string_hint_description_challenge =
    "Describa lo que el participante debe realizar para cumplir el reto";
const string_label_finish_challenge = "Finaliza el";
const string_label_winner_by_challenge = "Gana por";
const string_label_participant_with_challenge = "Participan con";
const string_hint_search_users_or_challenges = "Buscar usuarios o retos";
const string_hint_search_user="Buscar usuarios";
const string_label_report_participation = "Reportar Participación";
const string_label_report_challenge = "Reportar Reto";
const string_label_button_sing_in = "INGRESAR";
const string_label_button_validate = "VALIDAR";
const string_hint_in_code = "Ingrese código";
const string_label_button_reintentar = "Intentar de nuevo";
const string_label_button_send_sms = "ENVIARME SMS";
const string_hint_in_number_phone = "Ingrese su número de celular";
const string_title_page_in_number = "Ingresar Número";
const string_label_followers = "Seguidores";
const string_label_followings = "Seguidos";
const string_label_challenges = "Retos";

const string_label_button_edit_profile = "Editar Perfil";
const string_label_button_follower = "Seguir";
const string_label_button_unfollower = "Dejar de Seguir";

const string_message_information_phone_validation =
    "Te enviaremos un SMS que contiene un código para verificar tu número de teléfono";

const string_message_load_info_user = "Consultando Información del perfil";

const string_message_post_participation = 'Publicando Participación';

const string_message_post_winner = "Participación Ganadora";

const string_message_none_participants_winner =
    "Sin Ganador, Es posible que no existieron participantes";

///Opciones
const String string_option_camera = "Cámara";
const String string_option_photo = "Foto";
const String string_option_text = "Texto";
const String string_option_video = "Video";
const String string_option_random = "Aleatorio";
const String string_option_support = "Apoyo";
const String string_option_gallery_image = "Galería de Imagenes";
const String string_option_gallery_video = "Galería de Videos";
const String string_option_report_content_inappropriate =
    "Contenido Inapropiado";
const String string_option_report_not_delivery = "No entregó el premio";
const String string_option_report_challenge_false = "No cumplio lo prometido";
const String string_option_report_breach_request = "Incumple lo Requerido";
const String string_option_report_copy_participation =
    "Copia de otra Participacion";

///Errores para multimedia
const String string_error_video_exceed_size =
    "El video es muy pesado";
const String string_error_video_path_read =
    "No se puede leer el video desde la ruta especificada";
const String string_error_play_video =
    "No se puede reproducir video, formato incompatible";
const String string_error_image_path_read =
    "No se puede leer la imagen desde la ruta especificada";
const String string_error_image_exceed_size =
    "La image es muy pesada";
const String string_error_microphone_permission_required =
    "El permiso de microfono es requerido";
const String string_error_camera_permission_required =
    "El permiso de camara es requerido";
const String string_error_read_permission_required =
    "El permiso de lectura es requerido";

///Mensajes de exito
const String string_success_update_data_profile =
    "Información de perfil actualizada";
const String string_success_report_participation = "Participación Reportada";
const String string_success_report_challenge = "Reto Reportado";
const string_success_create_challenge="Reto Creado";

//Divisores de secciones
const String string_section_participant_challenges = "Participaciones en retos";
const String string_section_challenges_actives = "Retos Activos";
const string_section_participantions = "Participaciones";
const string_section_participation_select = "Participación Seleccionada";

//Pages
const string_message_none_newsfeed =
    "Sin noticias de tus amigos animate y retalos";
const string_title_page_newsfeed = "Inicio";
const string_title_page_populate = "Populares";
const string_message_none_challenges_populate =
    "Sin retos populares, que extraño :(";
const string_message_none_users_populate = "Sin usuarios populares";
const string_title_page_validate_code = "Validar Código";
const string_message_none_challenges_active = "Sin Retos que Mostrar";
const string_message_none_participations = "Sin participaciones";
const string_title_edit_profile = "Editar Perfil";
const string_message_none_participants = "Anímate a ser el primero";

//Slides init App
const String string_title_accept_challenge = "ACEPTA EL RETO";
const String string_title_create_challenge = 'LANZA TU DESAFIO';
const String string_title_win_prize = "GANA PREMIOS";
const String string_description_accept_challenge =
    "Sé parte de esta gran comunidad";
const String string_description_create_challenge =
    'Atrévete a retar a tus amigos a realizar cosas muy divertidas';
const String string_description_win_prize =
    'Sé el ganador de fabulosos premios de tus marcas favoritas ';

//default text

const string_default_name_user = "NombreUsuario...";
const string_default_name_complete = "Nombre Completo";

const string_dialog_delete_participation = "¿Eliminar participación?";
const string_label_button_cancel = "CANCELAR";
const string_label_button_delete = "ELIMINAR";
const string_label_button_create_challenge = "CREAR RETO";
const string_label_button_post_challenge="PUBLICAR";
