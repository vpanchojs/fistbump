const double dimen_small_progressbar = 15.0;
const double dimen_medium_progressbar = 30.0;
const double dimen_big_progressbar = 50.0;

const double dimen_radius_pb = 45.0;