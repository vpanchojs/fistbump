import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_news.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action_list.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:rxdart/rxdart.dart';

class BNewsFeed extends BlocBase
    with MixActionViewStream, ProgressMore, ProgressActionList<String> {
  List<MNews> _myNewsList;
  int limitNews = 6;
  MUser user;

  final _myNewsCtrl = BehaviorSubject<List<MNews>>();

  Function(List<MNews>) get _inNews => _myNewsCtrl.sink.add;

  Stream<List<MNews>> get outNews => _myNewsCtrl.stream;

  BNewsFeed(this.user) {
    _myNewsList = List();
    getNews();
  }

  getNews() async {
    await api.firestoreApi.getMyNewsFeed(user.idUser, limitNews).then((data) {
      _myNewsList.clear();
      if (data.documents.length > 0) {
        api.firestoreApi.lastMyNewsFeed = data.documents.last;
        data.documents.forEach((it) {
          if (it.data['type'] == 1) //Participacion
            _myNewsList.add(MNews.isParticipation(it.data, it.documentID));
          else // Nuevo Reto
            _myNewsList.add(MNews.newChallenge(it.data, it.documentID));
        });
      }
      _inNews(_myNewsList);
    }).catchError((e) {
      print("No se puede obtener mis noticias $e");
      _myNewsCtrl.sink.addError(string_error_generic);
    });
  }

  getMoreNews() async {
    if (!gettingMore) {
      print("se esta consultando mas noticias");
      gettingMore = true;
      inProgressMore(false);
      //comienzo la busqueda
      api.firestoreApi.getMoreMyNewsFeed(user.idUser, limitNews).then((data) {
        inProgressMore(true);
        if (data.documents.length > 0) {
          api.firestoreApi.lastMyNewsFeed = data.documents.last;
          data.documents.forEach((it) {
            if (it.data['type'] == 1) //Participacion
              _myNewsList.add(MNews.isParticipation(it.data, it.documentID));
            else // Nuevo Reto
              _myNewsList.add(MNews.newChallenge(it.data, it.documentID));
          });
          gettingMore = false;
          _inNews(_myNewsList);
        } else {
          gettingMore = true;
        }
      }).catchError((e) {
        print("error to load more challenges $e");
        gettingMore = false;
        inProgressMore(true);
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }

  shareChallengePlatforms(shareOtherPlatforms, MNews news) {
    inProgressActionList(idList..add(news.idNews));
    api.dynamicLinkApi
        .createDynamicLinkShareChallenge(
            descriptionPost: news.challenge.description,
            imageUrlPost: news.challenge.urlThumbnail,
            titlePost: news.challenge.name,
            idChallenge: news.challenge.idChallenge)
        .then((data) {
      inProgressActionList(idList..remove(news.idNews));
      if (data is ShortDynamicLink) {
        shareOtherPlatforms(data.shortUrl.toString());
        print(data.shortUrl.toString());
      } else {
        shareOtherPlatforms(data.toString());
        print(data.toString());
      }
    }).catchError((error) {
      inProgressActionList(idList..remove(news.idNews));
      print("Error al crear el dynamicLinks $error");
      inView(MActionView.error(message: string_error_generic));
    });
  }

  shareParticipationPlatforms(share, MNews news) {
    inProgressActionList(idList..add(news.idNews));
    api.dynamicLinkApi
        .createDynamicLinkShareParticipationInChallenge(
            descriptionPost: (news.participation.type == "text")
                ? news.participation.response
                : " ",
            imageUrlPost: (news.participation.type != "text")
                ? news.participation.response
                : news.participation.challenge.urlThumbnail,
            titlePost: "Participando en ${news.participation.challenge.name}",
            idChallenge: news.participation.challenge.idChallenge,
            idParticipation: news.participation.idParticipation)
        .then((data) {
      inProgressActionList(idList..remove(news.idNews));
      if (data is ShortDynamicLink) {
        share(data.shortUrl.toString());
        print(data.shortUrl.toString());
      } else {
        share(data.toString());
        print(data.toString());
      }
    }).catchError((error) {
      inProgressActionList(idList..remove(news.idNews));
      print("Error al crear el dynamicLinks $error");
      inView(MActionView.error(message: string_error_generic));
    });
  }

  @override
  void dispose() {
    _myNewsCtrl.close();
    closeProgressMoreList();
    closeProgressActionList();
    closeDataView();
    closeProgressActionList();
  }
}
