import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/newsfeed/b_newsfeed.dart';
import 'package:Darhu/src/ui/pages/newsfeed/v_newsfeed.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PNewsFeed extends StatefulWidget {
  @override
  PNewsFeedState createState() {
    return new PNewsFeedState();
  }
}

class PNewsFeedState extends State<PNewsFeed> {
  BNewsFeed _newsFeedBloc;
  ApplicationBloc _applicationBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      child: VNewsFeed(),
       blocBuilder: () => _newsFeedBloc,
    );
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _newsFeedBloc = BNewsFeed(_applicationBloc.user);
    super.initState();
  }
}
