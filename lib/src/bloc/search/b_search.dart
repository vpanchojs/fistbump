import 'dart:async';

import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:rxdart/rxdart.dart';

class BSearch extends BlocBase with MixActionViewStream, ProgressMore {
  List<MUser> _userList = List();

  //List<Challenge> _challengeList;
  bool request = true;

  int limitUsersSearch = 10;

  BSearch(this.query);

  //int limitChallengueSearch = 16;
  String query;

  /*Strems para lista de usuarios*/
  final usersCtrl = BehaviorSubject<List<MUser>>();

  Function(List<MUser>) get _inUsersList => usersCtrl.sink.add;

  Stream<List<MUser>> get outUsersList => usersCtrl.stream;

  /*Strems para lista de retos*/ /*
  final challengeCtrl = PublishSubject<List<Challenge>>();

  Sink<List<Challenge>> get _inChallengeList => challengeCtrl.sink;

  Stream<List<Challenge>> get outChallengeList => challengeCtrl.stream;
*/

  getUsersByQuery() async {
    print("Se ejecuto $query");
    _userList.clear();
    api.firestoreApi
        .findPeopleQuery(query.toLowerCase().trim(), limitUsersSearch)
        .then((query) {
      query.documents.forEach((doc) {
        _userList.add(MUser.minId(doc.data, doc.documentID));
      });
      _inUsersList(_userList);
    }).catchError((error) {
      print("error al buscar  usuarios $error");
      usersCtrl.sink.addError(string_error_generic);
    });
  }

  @override
  void dispose() {
    print("cerrando user search stream");
    usersCtrl.close();
    //  challengeCtrl.close();
    closeDataView();
  }

//  void _visibleProgress(data) {
//    _inProgress.add(data);
//  }

/*_getChallengesbyQuery() {
    print("buscando retos por query $query");
    _challengeList.clear();
    api.firestoreApi
        .findChallengesQuery(query, limitChallengueSearch)
        .then((query) {
      query.documents.forEach((doc) {
        // _challengeList.add(Challenge().setDateSnapshot(doc));
      });

      _inChallengeList.add(_challengeList);
    });
  }*/
}
