import 'dart:async';

import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';
import '../../ui/pages/search/v_search.dart';
import 'b_search.dart';

class PSearch extends SearchDelegate {
  String queryTemp = "";
  Timer timerSearch;

  PSearch({String hinText})
      : super(
            searchFieldLabel: hinText,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.search);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
          }),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Back',
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  /* Se utiliza un timer, debido al que el metodo se llama 2 veces consecutivas
  * se necesita que la consulta se ejecute una sola vez
  * Siendo tbm necesario controlar para futuras busquedas al ir escribiendo*/
  @override
  Widget buildResults(BuildContext context) {
    print("Se generarn las busquedas");
    return BlocProvider<BSearch>(
      blocBuilder: () => BSearch(query.trim().toLowerCase()),
      child: SearchResultsPage(),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text("Busca a tu amigo mediante su nombre de usuario",
            textAlign: TextAlign.center),
      ),
    );
  }
}
