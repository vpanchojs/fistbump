import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:Darhu/utils/mixins/show_widget.dart';
import 'package:Darhu/utils/validations/validators.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:rxdart/rxdart.dart';

class BManageProfile extends BlocBase
    with Validators, MixActionViewStream, ProgressAction, ShowWidget {
  MUser user;
  bool exists = false;
  var first = DateTime.now().subtract(Duration(days: (365 * 60)));
  var last = DateTime.now().subtract(Duration(days: (365 * 13)));

  BManageProfile(this.user) {
    getInfoProfile();
  }

  final name = BehaviorSubject<String>();
  final nameUser = BehaviorSubject<String>();
  final gender = BehaviorSubject<int>();
  final cellPhone = BehaviorSubject<String>();
  final birthday = BehaviorSubject<DateTime>();

  //Entradas
  Function(String) get inName => name.sink.add;

  Function(String) get inNameUser => nameUser.sink.add;

  Function(String) get inCellPhone => cellPhone.sink.add;

  Function(int) get inGender => gender.sink.add;

  Function(DateTime) get inBirthday => birthday.sink.add;

  //Salidas
  Stream<String> get outName => name.stream.transform(validName);

  Stream<String> get outNameUser => nameUser.stream.transform(validNameUser);

  Stream<String> get outCellPhone => cellPhone.stream;

  Stream<int> get outGender => gender.stream;

  Stream<DateTime> get outBirthday => birthday.stream;

  Stream<bool> get active =>
      Rx.combineLatest2(outName, outNameUser, (o, u) => true);

  @override
  void dispose() {
    closeShowWidget();
    nameUser.close();
    name.close();
    gender.close();
    cellPhone.close();
    birthday.close();
    closeProgressAction();
    closeDataView();
  }

  updateInfoProfile() {
    var userTemp = MUser()
      ..name = name.value
      ..gender = gender.value
      ..cellPhone = cellPhone.value
      ..birthday = birthday.value
      ..nameUser = nameUser.value;

    inProgressAction(true);

    api.firestoreApi
        .updateProfile(userTemp, exists, user.idUser)
        .then((v) async {
      //Se actualiza el usuario
      user
        ..name = userTemp.name
        ..nameUser = userTemp.nameUser
        ..birthday = userTemp.birthday
        ..gender = userTemp.gender;

      await api.authApi.updateFirebaseUser(userTemp.nameUser);

      if (exists) {
        inView(MActionView.popData(arguments: exists));
      } else {
        inView(MActionView.push(routeName: ""));
        //inView(DataView()..action = ActionEnum.PUSH);
      }
      //inView(exists);
      inProgressAction(false);
    }).catchError((e) {
      inProgressAction(false);
      if (e.toString() == "nameUser") {
        nameUser.sink.addError("Ya esta en uso, cambielo e intente nuevamente");
      }
      inView(MActionView.error(message: string_error_generic));
    });
  }

  getInfoProfile() {
    api.firestoreApi.getInfoUser(user.idUser).then((doc) {
      print("termine de consultar");
      inShow(true);
      MUser userInfo;

      exists = doc.exists;

      if (exists) {
        userInfo = MUser.completeId(doc.data, doc.documentID);
      }

      //Ya existe el usuario
      if (doc.exists && userInfo.nameUser != null) {
        print("existe");
        var user = MUser.completeId(doc.data, doc.documentID);
        inGender(user.gender);
        inBirthday(user.birthday);
        inCellPhone(user.cellPhone);
        inName(user.name);
        inNameUser(user.nameUser);
      } else {
        inCellPhone(user.cellPhone);
        inBirthday(last);
        inGender(1);
      }
    }).catchError((e) {
      print("No se puede obtener la informacion del usuario $e");
      inShow(false);
      inView(MActionView.error(
          message: "No se puede obtener la informacion del usuario"));
    });
  }
}
