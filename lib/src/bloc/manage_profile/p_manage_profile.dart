import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/manage_profile/b_manage_profile.dart';
import 'package:Darhu/src/ui/pages/manage_profile/v_manage_profile.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PManageProfile extends StatefulWidget {
  @override
  PManageProfileState createState() {
    return new PManageProfileState();
  }
}

class PManageProfileState extends State<PManageProfile> {
  BManageProfile _editProfileBloc;

  @override
  void initState() {
    var _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _editProfileBloc = BManageProfile(_applicationBloc.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BManageProfile>(
       blocBuilder: () => _editProfileBloc,
      child: VManageProfile(),
    );
  }
}
