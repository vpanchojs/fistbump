
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

class BMyParticipation extends BlocBase {
  MChallenge challenge;
  MUser user;

  BMyParticipation(this.challenge, this.user){
    getMyParticipation();
  }

  ///Stream para mi participacion en el reto
  final myParticipation = BehaviorSubject<MParticipation>();
  Function(MParticipation) get inMyParticipation => myParticipation.sink.add;
  Stream<MParticipation> get outMyParticipation => myParticipation.stream;


  /*
  * Metodo para obtener mi participacion en el reto.
  * */
  getMyParticipation(){
    api.firestoreApi.getMyParticipation(challenge.idChallenge,user.idUser).then((doc){
      if(doc.exists){
        var myParticipation= MParticipation.myParticipation(doc.data, doc.documentID);
        inMyParticipation(myParticipation);
      }else{
        inMyParticipation(MParticipation());
      }
    }).catchError((error){
      myParticipation.addError(error.toString());
      print("Error al obtener mi participación en reto $error");
    });
  }

  updateDataView(){
    inMyParticipation(MParticipation());
  }

  @override
  void dispose() {
    myParticipation.close();
  }
}
