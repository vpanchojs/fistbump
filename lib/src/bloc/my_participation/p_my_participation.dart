import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

import '../../../application_bloc.dart';
import '../../ui/pages/my_participation/v_my_participation.dart';
import 'b_my_participation.dart';

class PMyParticipation extends StatefulWidget {
  final MChallenge challenge;

  const PMyParticipation({Key key, this.challenge}) : super(key: key);
  @override
  _PMyParticipationState createState() => _PMyParticipationState();
}

class _PMyParticipationState extends State<PMyParticipation> {
  BMyParticipation _myParticipationBloc;
  ApplicationBloc _applicationBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BMyParticipation>(
       blocBuilder: () => _myParticipationBloc,
      child: VMyParticipation(),
    );
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _myParticipationBloc = BMyParticipation(widget.challenge,_applicationBloc.user);
    super.initState();
  }


}


