import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/menu/b_menu.dart';
import 'package:Darhu/src/ui/pages/menu/v_menu.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PMenu extends StatefulWidget {
  @override
  PMenuState createState() {
    return new PMenuState();
  }
}

class PMenuState extends State<PMenu> {
  BMenu bloc;
  ApplicationBloc _applicationBloc;

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    bloc = BMenu(user: _applicationBloc.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BMenu>(
      blocBuilder: () => bloc,
      child: Menu(),
    );
  }
}
