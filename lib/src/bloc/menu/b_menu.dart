import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_device_user.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/devices/device_info.dart';
import 'package:Darhu/src/res/strings.dart';

class BMenu extends BlocBase {
  final MUser user;

  BMenu({this.user});

  @override
  void dispose() {}

  Future closeSession() async {
    try {
      await api.preferencesApi.resetTokenNotification();
      MDeviceUser deviceUser=await DeviceInfo.getDevicesInfo();
      await api.firestoreApi.removeTokenNotification(user.idUser, deviceUser);
      return api.authApi.closeSession();
    } catch (ex) {
      print("No se pudo cerrar session $ex");
      return Future.error(string_error_generic);
    }
  }
}
