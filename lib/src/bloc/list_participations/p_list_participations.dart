import 'package:Darhu/src/ui/pages/list_participations/v_list_participations.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

import '../../../application_bloc.dart';
import 'b_list_participations.dart';

class PListParticipations extends StatefulWidget {
  final MChallenge challenge;
  final ScrollController scrollController;

  const PListParticipations({Key key, this.challenge,this.scrollController}) : super(key: key);

  @override
  _PListParticipationsState createState() => _PListParticipationsState();
}

class _PListParticipationsState extends State<PListParticipations> {
  BListParticipations _bparticipations;
  ApplicationBloc _applicationBloc;

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _bparticipations = BListParticipations(widget.challenge, _applicationBloc.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BListParticipations>(
       blocBuilder: () => _bparticipations,
      child: VListParticipations(scrollController: widget.scrollController),
    );
  }
}
