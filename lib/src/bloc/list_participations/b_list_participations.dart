import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:rxdart/rxdart.dart';

class BListParticipations extends BlocBase with MixActionViewStream, ProgressMore {
  MChallenge challenge;
  MUser user;
  List<MParticipation> listParticipation;

  BListParticipations(this.challenge, this.user) {
    listParticipation = List();
    getParticipations();
  }

/*Strems para lista de retos*/
  final participationController = BehaviorSubject<List<MParticipation>>();

  Function(List<MParticipation>) get _inParticipationList =>
      participationController.sink.add;

  Stream<List<MParticipation>> get outParticipationList =>
      participationController.stream;

  getParticipations() async {
    await api.firestoreApi
        .getInitialParticipations(challenge.idChallenge, user.idUser)
        .then((list) {
      listParticipation.clear();
      listParticipation.addAll(list);
      ommitMyParticipation();
      _inParticipationList(listParticipation);
      print("todo bien  ${list.length}");
    }).catchError((e) {
      print("error al obtener las participaciones $e");
      participationController.sink.addError(string_error_generic);
    });
  }

  getMoreParticipations() async {
    if (!gettingMore) {
      gettingMore = true;
      api.firestoreApi
          .getMoreParticipations(challenge.idChallenge, user.idUser)
          .then((lista) {
        if (lista.length > 0) {
          listParticipation.addAll(lista);
          ommitMyParticipation();
          gettingMore = false;
        } else {
          gettingMore = true;
        }
        _inParticipationList(listParticipation);
      }).catchError((e) {
        gettingMore = false;
        print("error al cargar mas participaciones $e");
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }

  //1 eliminacion , 0 actualizacion
  updateDataListParticipation(int action, [String idParticipation]) {
    if (action == 1) {
      listParticipation
          .removeWhere((item) => item.idParticipation == idParticipation);
    }
    _inParticipationList(listParticipation);
  }

  @override
  void dispose() {
    participationController.close();
    closeDataView();
  }

  void ommitMyParticipation() {
    listParticipation.removeWhere(
        (item) => item.idParticipation == user.idUser + challenge.idChallenge);
  }
}
