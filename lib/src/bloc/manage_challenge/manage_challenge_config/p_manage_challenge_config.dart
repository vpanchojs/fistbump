import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_config/b_manage_challenge_config.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_config/v_manage_challenge_config.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PManageChallengeConfig extends StatefulWidget {
  PManageChallengeConfig({Key key}) : super(key: key);

  @override
  _PManageChallengeConfigState createState() => _PManageChallengeConfigState();
}

class _PManageChallengeConfigState extends State<PManageChallengeConfig> {
  ApplicationBloc _applicationBloc;
  BManageChallengeConfig bloc;
  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    bloc = BManageChallengeConfig(
        _applicationBloc.challenge, _applicationBloc.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        child: VManageChallengeConfig(), blocBuilder: () => bloc);
  }
}
