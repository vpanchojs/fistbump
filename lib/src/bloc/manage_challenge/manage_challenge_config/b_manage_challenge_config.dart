import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_resume/v_manage_challenge_resume.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class BManageChallengeConfig extends BlocBase with MixActionViewStream {
  MChallenge challenge;
  MUser userSession;
  final DateTime firstDate = DateTime.now().add(Duration(days: 1));
  DateTime fromDate = DateTime.now().add(Duration(days: 1));
  TimeOfDay fromTime;
  final participationsWithCtrl = BehaviorSubject<String>();
  final winnerByCtrl = BehaviorSubject<String>();

  BManageChallengeConfig(this.challenge, this.userSession) {
    fromTime = TimeOfDay.fromDateTime(fromDate);
    print("tiempo ${fromTime.minute}");
    fromTime = fromTime.replacing(minute: 0);
    print("tiempo remplazado ${fromTime.minute}");
    inWinerBy("like");
    inParticipations('image');
  }

  Function(String) get inWinerBy => winnerByCtrl.sink.add;
  Stream<String> get outWinnerBy => winnerByCtrl.stream;

  Function(String) get inParticipations => participationsWithCtrl.sink.add;
  Stream<String> get outParticipants => participationsWithCtrl.stream;

  @override
  void dispose() {
    participationsWithCtrl.close();
    winnerByCtrl.close();
    closeDataView();
  }

  nextPage() {
    challenge.dateFinish = getDateFinish();
    challenge.winnerBy = winnerByCtrl.value;
    challenge.typeParticipation = participationsWithCtrl.value;
    inView(MActionView.push(routeName: VManageChallengeResume.routeName));
  }

  DateTime getDateFinish() {
    DateTime date = DateTime(fromDate.year, fromDate.month, fromDate.day,
        fromTime.hour, fromTime.minute);
    return date;
  }
}
