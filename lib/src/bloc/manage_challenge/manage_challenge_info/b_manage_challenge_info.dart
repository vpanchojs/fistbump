import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_config/v_manage_challenge_config.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/validations/validators.dart';
import 'package:rxdart/rxdart.dart';

class BManageChallengeInfo extends BlocBase
    with Validators, MixActionViewStream {
  MChallenge challenge;
  MUser userSession;

  BManageChallengeInfo(this.challenge, this.userSession);

  @override
  void dispose() {
    nameCtrl.close();
    descriptionCtrl.close();
    multimediaCtrl.close();
  }

  final multimediaCtrl = BehaviorSubject<MMultimedia>();
  Function(MMultimedia) get inMultimedia => multimediaCtrl.sink.add;
  Stream<MMultimedia> get outMultimedia => multimediaCtrl.stream;

  final nameCtrl = BehaviorSubject<String>();
  Function(String) get inName => nameCtrl.sink.add;
  Stream<String> get outName =>
      nameCtrl.stream.transform(validateNameChallenge);

  final descriptionCtrl = BehaviorSubject<String>();
  Function(String) get inDescription => descriptionCtrl.sink.add;
  Stream<String> get outDescription =>
      descriptionCtrl.stream.transform(validDescription);

  Stream<bool> get enableNextButton => Rx.combineLatest3(outName, outDescription,
      outMultimedia, (name, deescription, multimeda) => true);

  nextPage() {
    challenge.name = nameCtrl.value;
    challenge.description = descriptionCtrl.value;
    challenge.multimedia = multimediaCtrl.value;
    inView(MActionView.push(routeName: VManageChallengeConfig.routeName));
  }
}
