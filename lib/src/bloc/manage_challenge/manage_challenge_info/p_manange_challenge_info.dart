import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_info/b_manage_challenge_info.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_info/v_manage_challenge_info.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PManageChallengeInfo extends StatefulWidget {
  final String idChallenge;
  PManageChallengeInfo({Key key, this.idChallenge}) : super(key: key);

  @override
  _PManageChallengeInfoState createState() => _PManageChallengeInfoState();
}

class _PManageChallengeInfoState extends State<PManageChallengeInfo> {
  ApplicationBloc _applicationBloc;
  BManageChallengeInfo bloc;

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _applicationBloc.challenge = MChallenge();
    bloc =
        BManageChallengeInfo(_applicationBloc.challenge, _applicationBloc.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(child: VManageChallengeInfo(), blocBuilder: () => bloc);
  }
}
