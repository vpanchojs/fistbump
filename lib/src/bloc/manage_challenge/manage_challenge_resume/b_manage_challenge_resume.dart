import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_prize.dart';
import 'package:Darhu/src/repository/r_new_challenge.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/ui/pages/init_section/v_init_section.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:rxdart/rxdart.dart';

class BManageChallengeResume extends BlocBase
    with MixActionViewStream, ProgressAction {
  MChallenge challenge;
  MUser userSession;
  RChallenge repository;
  final prizeCtrl = BehaviorSubject<Prize>();
  final withPrizeCtrl = BehaviorSubject<bool>();
  final canDeliveryPrizeCtrl = BehaviorSubject<bool>();
  final challengeCtrl = BehaviorSubject<MChallenge>();

  BManageChallengeResume(this.challenge, this.userSession, this.repository) {
    inChallenge(challenge);
    isUserVerified();
  }

  Function(MChallenge) get inChallenge => challengeCtrl.sink.add;
  Stream<MChallenge> get outChallenge => challengeCtrl.stream;

  Function(Prize) get inPrize => prizeCtrl.sink.add;
  Stream<Prize> get outPrize => prizeCtrl.stream;

  Function(bool) get inWithPrize => withPrizeCtrl.sink.add;
  Stream<bool> get outWithPrize => withPrizeCtrl.stream;

  Function(bool) get inCanDeliveryPrize => canDeliveryPrizeCtrl.sink.add;
  Stream<bool> get outCanDeliveryPrize => canDeliveryPrizeCtrl.stream;

  @override
  void dispose() {
    prizeCtrl.close();
    withPrizeCtrl.close();
    canDeliveryPrizeCtrl.close();
    challengeCtrl.close();
    closeProgressAction();
  }

  void createOrUpdateChallenge() {
    inProgressAction(true);
    challenge.available = true;
    challenge.numParticipantions = 0;
    challenge.withPrize = withPrizeCtrl.value ?? false;
    if (challenge.withPrize) {
      challenge.prize = prizeCtrl.value;
    }
    challenge.user = userSession;

    repository.createChallenge(challenge, userSession).then((res) {
      print("se creo el reto ");
      //myListChallenge.add(Challenge().getMinFromDateSnapshotJson(doc.data, doc.data["idChallenge"]));
      //inView(MActionView.error(message: string_success_create_challenge));
      //inView(MActionView.popData(arguments: challenge));
      inView(MActionView.popUntil(routeName: VInitSection.routeName));
      inProgressAction(false);
    }).catchError((error) {
      print(error.toString());
      inProgressAction(false);
      inView(MActionView.error(message: error.toString()));
    });
  }

  isUserVerified() {
    repository.isVerifiedUser(idUser: userSession.idUser).then((res) {
      if (res.success) {
        inCanDeliveryPrize(res.data);
      } else {
        canDeliveryPrizeCtrl.sink.addError(
            "No encontramos su información, es posible que su usuario este eliminado o bloqueado");
      }
    }).catchError((e) {
      canDeliveryPrizeCtrl.sink
          .addError("Problemas al consuntar su información");
    });
  }

  removePrize() {
    inPrize(Prize());
    inWithPrize(false);
  }
}
