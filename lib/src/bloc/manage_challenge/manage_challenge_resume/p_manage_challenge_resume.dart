import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_resume/b_manage_challenge_resume.dart';
import 'package:Darhu/src/repository/r_new_challenge.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_resume/v_manage_challenge_resume.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PManageChalleResume extends StatefulWidget {
  PManageChalleResume({Key key}) : super(key: key);

  @override
  _PManageChalleResumeState createState() => _PManageChalleResumeState();
}

class _PManageChalleResumeState extends State<PManageChalleResume> {
  ApplicationBloc _applicationBloc;
  BManageChallengeResume bloc;
  RChallenge _repository;

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _repository = RChallenge();
    bloc = BManageChallengeResume(
        _applicationBloc.challenge, _applicationBloc.user, _repository);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        child: VManageChallengeResume(), blocBuilder: () => bloc);
  }
}
