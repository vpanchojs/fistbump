import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/bloc/view_profile/b_view_profile.dart';
import 'package:Darhu/src/ui/pages/view_profile/v_view_profile.dart';
import 'package:Darhu/src/repository/r_profile.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PViewProfile extends StatefulWidget {
  final MUser user;

  const PViewProfile({Key key, this.user}) : super(key: key);

  @override
  PViewProfileState createState() {
    return new PViewProfileState();
  }
}

class PViewProfileState extends State<PViewProfile> {
  BViewProfile _bloc;
  ApplicationBloc _applicationBloc;
  RProfile _repository;


  @override
  void initState() {
    _applicationBloc= BlocProvider.of<ApplicationBloc>(context);
    _repository= RProfile();
    _bloc = BViewProfile(widget.user,_applicationBloc.user,_repository);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BViewProfile>(
      blocBuilder: () => _bloc,
      child: VViewProfile(),
    );
  }
}
