import 'dart:async';
import 'package:Darhu/src/repository/r_profile.dart';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:Darhu/utils/mixins/progress_action_aux.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/utils/mixins/progress_share.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:rxdart/rxdart.dart';

class BViewProfile extends BlocBase
    with
        MixActionViewStream,
        ProgressAction,
        ProgressMore,
        ProgressActionAux,
        ProgressShare {
  MUser user;
  MUser userSession;
  List<MParticipation> myParticipations = List();
  int limit = 6;
  RProfile _repository;

  final urlPhoto = BehaviorSubject<String>();
  final _userController = BehaviorSubject<MUser>();
  final _participationsController = BehaviorSubject<List<MParticipation>>();

  Function(String) get inUrlPhoto => urlPhoto.sink.add;

  Stream<String> get outUrlPhoto => urlPhoto.stream;

  Function(MUser) get _inUsers => _userController.sink.add;

  Stream<MUser> get outUsers => _userController.stream;

  Function(List<MParticipation>) get _inParticipations =>
      _participationsController.sink.add;

  Stream<List<MParticipation>> get outParticipations =>
      _participationsController.stream;

  BViewProfile(this.user, this.userSession,this._repository) {
    getInfoUser();
  }

  getInfoUser() {
    api.firestoreApi.getInfoUser(user.idUser).then((userInfo) async {
      if (userInfo.exists) {
        user.updateDataProfile(userInfo.data);

        if (user.idUser != userSession.idUser) {
          print("estoy viendo el perfil  de otra usuario");
          //estoy viendo el perfil de otro usuario
          var isContact =
              await api.firestoreApi.isContact(user.idUser, userSession.idUser);

          if (isContact.exists) {
            print("ya es mi contacto ${isContact.data["type"]}");
            //es mi contacto
            user.type = isContact.data["type"];
          } else {
            print("no es mi contacto");
            //no es mi contacto
            user.type = -1;
          }
        } else {
          //estoy viendo mi perfi
          print("estoy viendo mi perfl");
        }
        inUrlPhoto(user.urlPhoto);
        inProgressActionAux(true);
        _inUsers(user);
        getMyParticipations();
      } else {
        print("el usuario no existe");
        _userController.sink.addError("El usuario no exite");
      }
    }).catchError((e) {
      print("Error, no se pudo obtener la informacion del usuario $e");
      _userController.sink.addError(string_error_generic);
    });
  }

  followingUser() {
    inProgressAction(true);
    api.functionsApi
        .followingUser(user.idUser, userSession.idUser)
        .then((value) {
      inProgressAction(false);
      print("Seguir correctamente  type $value");
      user.type = value.data;
      user.numFollowers = user.numFollowers + 1;
      _inUsers(user);
      //api.notificationsApi.subscribeTopic(user.idUser);
    }).catchError((error) {
      inProgressAction(false);
      print("Error al seguir un usuario $error");
      inView(MActionView.error(message: string_error_generic));
    });
  }

  unFollowingUser() {
    inProgressAction(true);
    api.functionsApi
        .unfollowingUser(user.idUser, userSession.idUser)
        .then((value) {
      print("Se dejo de seguir al usuario type $value");
      inProgressAction(false);
      user.type = value.data;
      user.numFollowers = user.numFollowers - 1;
      _inUsers(user);
      //api.notificationsApi.unSubscribeTopic(user.idUser);
    }).catchError((error) {
      inProgressAction(false);
      inView(MActionView.error(message: string_error_generic));
      print("Error al dejar de seguir un usuario $error");
    });
  }

  Future getMyParticipations() async {
    await api.firestoreApi.getMyParticipations(user.idUser, limit).then((docs) {
      myParticipations.clear();
      if (docs.documents.length > 0) {
        api.firestoreApi.lastMyPartition = docs.documents.last;
        docs.documents.forEach((doc) {
          print(doc.data);
          myParticipations
              .add(MParticipation.inProfile(doc.data, doc.documentID));
        });
      } else {
        gettingMore = true;
      }
      _inParticipations(myParticipations);
    }).catchError((error) {
      _participationsController.sink.addError(
          "Tuvimos probleas al obtener la información, intentelo nuevamente");
      print("mis paricipaciones error $error");
    });
  }

  Future getMoreMyParticipations() async {
    if (!gettingMore) {
      gettingMore = true;
      inProgressMore(true);
      await api.firestoreApi
          .getMyMoreParticipations(user.idUser, limit)
          .then((docs) {
        if (docs.documents.length > 0) {
          api.firestoreApi.lastMyPartition = docs.documents.last;
          docs.documents.forEach((doc) {
            print(doc.data);
            myParticipations
                .add(MParticipation.inProfile(doc.data, doc.documentID));
          });
          gettingMore = false;
        } else {
          gettingMore = true;
        }
        inProgressMore(false);
        _inParticipations(myParticipations);
      }).catchError((error) {
        gettingMore = false;
        inProgressMore(false);
        inView(MActionView.error(message: string_error_generic));
        print("mis paricipaciones error $error");
      });
    }
  }

  //metodo migrado a functions api
  updatePhotoProfile(MMultimedia multimedia) {
    inProgressActionAux(false);
    _repository.updatePhotoProfile(multimedia: multimedia, idUser:userSession.idUser).then((response) {
      inProgressActionAux(true);
      if(response.success){
        inUrlPhoto(response.data);
        print("se subio la foto $response.data");
        user.urlPhoto = response.data;
        userSession.urlPhoto = response.data;
        _inUsers(user);
        inView(MActionView.succes(message: "Se actualizo la foto de perfil"));
      }else{
       inView(MActionView.error(message: response.error));
      }
    }).catchError((e) {
      inProgressActionAux(true);
      inView(MActionView.error(message: string_error_generic));
      print("error al cambiar la foto $e");
    });
  }

  @override
  void dispose() {
    _userController.close();
    _participationsController.close();
    closeProgressAction();
    closeProgressMoreList();
    closeProgressActionAux();
    closeProgressShare();
    urlPhoto.close();
    closeDataView();
  }

  shareProfilePlatforms(Function share) {
    inProgressShare(true);
    api.dynamicLinkApi
        .createDynamicLinkShareUser(
            descriptionPost: user.name,
            imageUrlPost: user.urlPhoto,
            titlePost: user.nameUser,
            idUser: user.idUser)
        .then((data) {
      inProgressShare(false);
      if (data is ShortDynamicLink) {
        share(data.shortUrl.toString());
        print(data.shortUrl.toString());
      } else {
        share(data.toString());
        print(data.toString());
      }
    }).catchError((error) {
      inProgressShare(false);
      inView(MActionView.error(message: string_error_generic));
    });
  }

  void updateInfoBasicUser() {
    user.name = userSession.name;
    _inUsers(user);
  }
}
