import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:Darhu/utils/validations/validators.dart';
import 'package:rxdart/rxdart.dart';

class BPrizeDeliver extends BlocBase
    with MixActionViewStream, ProgressAction, Validators {
  final MUser userSession;
  final String idChallenge;
  final dynamic refParticipation;
  final String idPrize;

  final code = BehaviorSubject<String>();
  final multimediaCtrl = BehaviorSubject<MMultimedia>();

  BPrizeDeliver(
      this.userSession, this.idChallenge, this.refParticipation, this.idPrize);

  Function(MMultimedia) get inMultimedia => multimediaCtrl.sink.add;

  Stream<MMultimedia> get outMultimedia => multimediaCtrl.stream;

  Function(String) get inCode => code.sink.add;

  Stream<String> get outCode => code.stream.transform(validCodePrize);

  Stream<bool> get submitValid =>
      Rx.combineLatest2(outMultimedia, outCode, (url, code) => true);

  confirmDeliveryPrize() {
    try {
      /*
      inProgressAction(true);
      api.firestoreApi
          .verificateCode(refParticipation, code.value)
          .then((data) async {
        if (data.exists) {
          //codigo correcto
          print("Codigo correcto");
          List<String> photos = List();
          if (multimediaCtrl.value != null &&
              multimediaCtrl.value.data.item != TypeMultimedia.EMPTY) {
            final data = await api.storageApi.uploadMultimedia(
                multimediaCtrl.value.data.item,

                api.storageApi.getStorageReferenceDeliveryPrize(
                    idChallenge, userSession.idUser, idPrize)
            );

            if (data != null) {
              photos.add(data[0]);
            }
          }
          api.firestoreApi
              .confirmDelivery(idChallenge, idPrize, code.value, photos)
              .then((data) {
            print("Se confirmo correctamente la entrega");
            inView(ActionView.popData(arguments: "Se entrego correctamente"));
          }).catchError((error) {
            inView(
                ActionView.error(message: "No se pudo confirmar la entrega"));
            print("Problemas al confirmar la entrega $error");
          });
        } else {
          print("Codigo incorrecto");
          inView(ActionView.error(message: "Código Incorrecto"));
        }
        inProgressAction(false);
      }).catchError((error) {
        inProgressAction(false);
        inView(ActionView.error(message: "Problemas al verificar el código"));
        print("Problemas al verificar el codigo $error");
      });
       */
    } catch (e) {
      inProgressAction(false);
      code.addError("El código es incorrecto");
    }


  }

  @override
  void dispose() {
    code.close();
    multimediaCtrl.close();
  }
}
