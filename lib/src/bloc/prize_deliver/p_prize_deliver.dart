import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/ui/pages/prize_deliver/util/arg_deliver_prize.dart';
import 'package:Darhu/src/ui/pages/prize_deliver/v_deliver_prize.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/cupertino.dart';

import 'b_prize_deliver.dart';

class PPrizeDeliver extends StatefulWidget {
  final ArgDeliverPrize argDeliverPrize;

  const PPrizeDeliver(
      {Key key, this.argDeliverPrize})
      : super(key: key);
  @override
  _PPrizeDeliverState createState() => _PPrizeDeliverState();
}

class _PPrizeDeliverState extends State<PPrizeDeliver> {
  ApplicationBloc _applicationBloc;
  BPrizeDeliver _bPrizeDelivery;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        child: VDeliverPrize(), blocBuilder: () => _bPrizeDelivery);
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _bPrizeDelivery = BPrizeDeliver(_applicationBloc.user, widget.argDeliverPrize.idChallenge,
        widget.argDeliverPrize.refParticipation, widget.argDeliverPrize.idPrize);
    super.initState();
  }
}
