import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/notifications/b_notifications.dart';
import 'package:Darhu/src/ui/pages/notifications/v_notifications.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PNotifications extends StatefulWidget {
  @override
  PNotificationsState createState() {
    return new PNotificationsState();
  }
}

class PNotificationsState extends State<PNotifications> {
  ApplicationBloc _applicationBloc;
  BNotifications _notificationsBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocBuilder: () => _notificationsBloc,
      child: VNotifications(),
    );
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _notificationsBloc = BNotifications(_applicationBloc.user);
    super.initState();
  }
}
