import 'dart:async';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_notification.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:rxdart/rxdart.dart';

class BNotifications extends BlocBase with MixActionViewStream, ProgressMore {
  MUser user;
  List<MNotification> _myNotificationsList;
  int limitNotifications = 14;

  /*Strems para lista de notificaciones*/
  final _myNotificationCtrl = BehaviorSubject<List<MNotification>>();

  Function(List<MNotification>) get _inNotifications =>
      _myNotificationCtrl.sink.add;

  Stream<List<MNotification>> get outNotifications =>
      _myNotificationCtrl.stream;

  BNotifications(this.user) {
    _myNotificationsList = List();
    getNotifications();
  }

  @override
  void dispose() {
    _myNotificationCtrl.close();
    closeProgressMoreList();
    closeDataView();
  }

  getNotifications() async {
    await api.firestoreApi
        .getMyNotifications(user.idUser, limitNotifications)
        .then((data) {
      _myNotificationsList.clear();
      if (data.documents.length > 0) {
        api.firestoreApi.lastMyNotification = data.documents.last;

        data.documents.forEach((it) {
          switch (it.data["type"]) {
            case 0: //Nuevo reto
              _myNotificationsList
                  .add(MNotification.isNewChallenge(it.data, it.documentID));
              break;
            case 1: //Participacion
              _myNotificationsList.add(
                  MNotification.isNewParticipation(it.data, it.documentID));
              break;
          }
        });
      }
      _inNotifications(_myNotificationsList);
    }).catchError((error) {
      _myNotificationCtrl.sink.addError(string_error_generic);
    });
  }

  getMoreNotifications() async {
    if (!gettingMore) {
      print("Traer mas datos de notificaciones");
      gettingMore = true;
      inProgressMore(false);

      api.firestoreApi
          .getMoreMyNotifications(user.idUser, limitNotifications)
          .then((data) {
            print("la data traida ${data.documents.length}");
        if (data.documents.length > 0) {
          print("existe mas elementos");
          api.firestoreApi.lastMyNotification = data.documents.last;
          data.documents.forEach((it) {

            switch (it.data["type"]) {
              case 0: //Nuevo reto
                _myNotificationsList
                    .add(MNotification.isNewChallenge(it.data, it.documentID));
                break;
              case 1: //Participacion
                _myNotificationsList.add(
                    MNotification.isNewParticipation(it.data, it.documentID));
                break;
            }
          });
          gettingMore = false;
        } else {
          print("no existen mas elementos");
          gettingMore = true;
        }
        inProgressMore(true);
        _inNotifications(_myNotificationsList);
      }).catchError((e) {
        print("error al consultar las notificaciones $e");
        gettingMore = true;
        inProgressMore(true);
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }
}
