import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/my_challenges/b_my_challenges.dart';
import 'package:Darhu/src/ui/pages/my_challenges/v_my_challenges.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PMyChallenges extends StatefulWidget {
  @override
  PMyChallengesState createState() {
    return new PMyChallengesState();
  }
}

class PMyChallengesState extends State<PMyChallenges> {
  BMyChallenges _challengeBloc;
  ApplicationBloc _applicationBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BMyChallenges>(
      child: VMyChallenges(),
       blocBuilder: () => _challengeBloc,
    );
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _challengeBloc = BMyChallenges(_applicationBloc.myListChallenge,_applicationBloc.user, _applicationBloc.getChallenge,_applicationBloc.resetChallenge);
    super.initState();
  }
}
