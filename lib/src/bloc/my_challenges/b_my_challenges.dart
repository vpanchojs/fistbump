import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:rxdart/rxdart.dart';

class BMyChallenges extends BlocBase with MixActionViewStream, ProgressMore {
  List<MChallenge> myListChallenge;
  MUser user;
  int limitChallenges = 10;
  Function() getChallenge;
  Function() resetChallenge;

  /*Strems para lista de retos*/
  final myChallengesCtrl = BehaviorSubject<List<MChallenge>>();

  Function(List<MChallenge>) get _inMyChallenge => myChallengesCtrl.sink.add;

  Stream<List<MChallenge>> get outMyChallenge => myChallengesCtrl.stream;

  BMyChallenges(
      this.myListChallenge, this.user, this.getChallenge, this.resetChallenge) {
    if (myListChallenge.length <= 0) {
      getMyChallenges();
    } else {
      _inMyChallenge(myListChallenge);
    }
  }

  getMoreMyChallenges() async {
    if (!gettingMore) {
      gettingMore = true;

      //para que se visualize el progress
      inProgressMore(false);
      //comienzo la busqueda

      await api.firestoreApi
          .getSomeChallenges(user.idUser, limitChallenges)
          .then((list) {
        inProgressMore(true);
        if (list.isNotEmpty) {
          print("Se encontraron resultados");
          myListChallenge.addAll(list);
          _inMyChallenge(myListChallenge);
          gettingMore = false;
        } else {
          print("no se encontraon mas resultados");
          gettingMore = true;
        }
      }).catchError((e) {
        print("error to load more challenges $e");
        gettingMore = false;
        inProgressMore(true);
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }

  Future getMyChallenges() async {
    //false, para indicar que se puede traer mas resultados
    gettingMore = false;
    await api.firestoreApi
        .getInitialMyChallenges(user.idUser, limitChallenges)
        .then((list) {
      myListChallenge.clear();
      myListChallenge.addAll(list);
      _inMyChallenge(myListChallenge);
    }).catchError((e) {
      myChallengesCtrl.sink.addError(string_error_generic);
      print("error consultando mis resto $e");
    });
  }

  deleteChallenge(MChallenge challenge) async {
    print("idUser ${user.idUser} idReto ${challenge.idChallenge}");
    myListChallenge
        .removeWhere((item) => item.idChallenge == challenge.idChallenge);
    _inMyChallenge(myListChallenge);

    await api.functionsApi
        .deleteChallengeMinAndFull(user.idUser, challenge.idChallenge)
        .then((value) {
      print("Se elimino el reto");
    }).catchError((error) {
      print("no se elimin por $error");
      inView(MActionView.error(message: string_error_generic));
      myListChallenge.add(challenge);
      _inMyChallenge(myListChallenge);
    });
  }

  shareChallenge(MChallenge challenge, Function share) {
    api.dynamicLinkApi
        .createDynamicLinkShareChallenge(
            descriptionPost: challenge.description,
            imageUrlPost: challenge.urlThumbnail,
            titlePost: challenge.name,
            idChallenge: challenge.idChallenge)
        .then((data) {
      if (data is ShortDynamicLink) {
        print(data.shortUrl.toString());
        share(data.shortUrl.toString());
      } else {
        print(data.toString());
        share(data.toString());
      }
    }).catchError((error) {
      share("");
      print("Error al crear el dynamicLinks $error");
      inView(MActionView.error(message: string_error_generic));
    });
  }

  @override
  void dispose() {
    myChallengesCtrl.close();
    closeProgressMoreList();
    closeDataView();
  }

  addNewChallenge(MChallenge challenge) {
    myListChallenge.add(challenge);
    myListChallenge.sort((b, a) => a.datePost.compareTo(b.datePost));    
    _inMyChallenge(myListChallenge);
    inView(MActionView.succes(message: string_success_create_challenge));
  }

  verifiedNewChallenge() {
    MChallenge newChallenge = getChallenge();
    if (newChallenge != null && newChallenge.idChallenge != null) {
      //Existe un nuevo reto, hay que agregarlo a la lista
      addNewChallenge(newChallenge);
    } else {
      //no existen ningun nuevo reto
    }
  }
}
