import 'dart:async';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action_list.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

//Toda la logica de negocio
class BContacts extends BlocBase
    with MixActionViewStream, ProgressActionList<String>, ProgressMore {
  List<MUser> _userList;
  MUser userSession;
  final MUser user;

  ///Indica el type de contacto que es (Seguidor, seguido)
  final int type;
  int limitContacts = 14;

  //stream para la lista de usuarios
  final _usersController = BehaviorSubject<List<MUser>>();

  Function(List<MUser>) get _inUsersList => _usersController.sink.add;

  Stream<List<MUser>> get outUsersList => _usersController.stream;

  BContacts(this.user, this.type, this.userSession) {
    _userList = List();
    getContacts();
  }

  getFollowMore() {
    if (!gettingMore) {
      inProgressMore(false);
      gettingMore = true;
      _getFutureFollowMore().then((data) {
        inProgressMore(true);
        if (data.documents.length > 0) {
          api.firestoreApi.lastContact = data.documents.last;
          data.documents.forEach((doc) {
            _userList.add(MUser.minId(doc.data, doc.documentID));
          });
          gettingMore = false;
          _inUsersList(_userList);
        } else {
          gettingMore = true;
        }
      }).catchError((e) {
        inProgressMore(true);
        gettingMore = false;
        print("Error al obtener mas seguidores $e");
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }

  Future<QuerySnapshot> _getFutureFollow() {
    if (type == 0) {
      //seguidores
      return api.firestoreApi.getFollowers(user.idUser, limitContacts);
    } else {
      //Seguidos
      return api.firestoreApi.getFolloweds(user.idUser, limitContacts);
    }
  }

  Future<QuerySnapshot> _getFutureFollowMore() {
    if (type == 0) {
      //seguidores
      return api.firestoreApi.getMoreFollowers(user.idUser, limitContacts);
    } else {
      //seguidos
      return api.firestoreApi.getMoreFolloweds(user.idUser, limitContacts);
    }
  }

  @override
  void dispose() {
    _usersController.close();
    closeDataView();
    closeProgressMoreList();
    closeProgressActionList();
  }

  Future getContacts() async {
    await _getFutureFollow().then((data) {
      if (data.documents.length > 0) {
        gettingMore = false;
        _userList.clear();
        api.firestoreApi.lastContact = data.documents.last;
        data.documents.forEach((it) {
          _userList.add(MUser.minId(it.data, it.documentID));
        });
      }
      _inUsersList(_userList);
    }).catchError((e) {
      _usersController.sink.addError(string_error_generic);
    });
  }

  addOrRemoveContactsProgress(String idUser, bool add) {
    if (add) {
      idList.add(idUser);
      inProgressActionList(idList);
    } else {
      idList.remove(idUser);
      inProgressActionList(idList);
    }
  }

  void removeContact(idUser) {
    addOrRemoveContactsProgress(idUser, true);

    if (type == 0) {
      //Seguidores eliminar (eliminar un seguidor)
      api.functionsApi.unfollowerUser(idUser, userSession.idUser).then((val) {
        print("Se elimino seguidor");
        _userList.removeWhere((u) => u.idUser == idUser);
        _inUsersList(_userList);
        addOrRemoveContactsProgress(idUser, false);
        //api.notificationsApi.unSubscribeTopic(idUser);
        user.numFollowers -= 1;
      }).catchError((error) {
        addOrRemoveContactsProgress(idUser, false);
        print("Error al eliminar seguidor $error");
        inView(MActionView.error(message: string_error_generic));
      });
    } else {
      //seguidos eliminar(dejar de seguir)
      api.functionsApi.unfollowingUser(idUser, userSession.idUser).then((val) {
        print("Se dejo de seguir");
        _userList.removeWhere((u) => u.idUser == idUser);
        _inUsersList(_userList);
        addOrRemoveContactsProgress(idUser, false);
        user.numFolloweds -= 1;
      }).catchError((error) {
        addOrRemoveContactsProgress(idUser, false);
        print("Error al dejar de seguir $error");
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }

  bool isDelete() {
    return user.idUser == userSession.idUser;
  }
}
