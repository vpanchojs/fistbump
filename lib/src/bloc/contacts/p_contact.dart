import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/contacts/b_contacts.dart';
import 'package:Darhu/src/ui/pages/contacts/util/arg_contact.dart';
import 'package:Darhu/src/ui/pages/contacts/v_contacts.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PContact extends StatefulWidget {
  final ArgContact argContact;
  //final MUser user;
  //final int type;// Indica el tipo de contacto a cargar (seguidores/seguidos)

  const PContact({Key key, this.argContact}) : super(key: key);

  @override
  PContactState createState() => new PContactState();
}

class PContactState extends State<PContact> {
  BContacts _contactsBloc;
  ApplicationBloc _applicationBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BContacts>(
        blocBuilder: () => _contactsBloc, child: VContacts());
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _contactsBloc =
        BContacts(widget.argContact.user, widget.argContact.type, _applicationBloc.user);
    super.initState();
  }
}
