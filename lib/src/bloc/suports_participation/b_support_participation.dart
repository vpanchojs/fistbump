import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:rxdart/rxdart.dart';

class BSupportParticipation extends BlocBase with ProgressMore, MixActionViewStream {
  String participationId;
  int numSupports;
  MUser userSession;
  String userCreatorId;
  List<MUser> usersSupports;
  int limitUserSupports = 10;

  BSupportParticipation(this.participationId, this.userSession, this.userCreatorId,
      this.numSupports) {
    usersSupports = List();
    getSupportParticipations();
  }

  //stream para la lista de usuarios
  final _usersController = BehaviorSubject<List<MUser>>();

  Function(List<MUser>) get _inUsersList => _usersController.sink.add;

  Stream<List<MUser>> get outUsersList => _usersController.stream;

  @override
  void dispose() {
    closeProgressMoreList();
    closeDataView();
    _usersController.close();
  }

  getSupportParticipations() {
    api.firestoreApi
        .getSupportParticipations(
            this.participationId, this.userCreatorId, limitUserSupports)
        .then((data) {
      print("apoyos obtenidos");
      if (data.documents.length > 0) {
        gettingMore = false;
        print("apoyos obtenidos total ${data.documents.length}");
        usersSupports.clear();
        api.firestoreApi.lastSupportUser = data.documents.last;
        data.documents.forEach((it) {
          usersSupports.add(MUser.minId(it.data, it.documentID));
        });
      } else {
        print("no se encontro apoyos");
      }
      _inUsersList(usersSupports);
    }).catchError((error) {
      print("error apoyos ${error.toString()}");
      _usersController.sink.addError(string_error_generic);
    });
  }

  getMoreSupportParticipations() {
    if (!gettingMore) {
      inProgressMore(false);
      gettingMore = true;

      api.firestoreApi
          .getMoreSupportParticipations(
              this.participationId, this.userCreatorId, limitUserSupports)
          .then((data) {
        inProgressMore(true);
        if (data.documents.length > 0) {
          api.firestoreApi.lastSupportUser = data.documents.last;
          data.documents.forEach((doc) {
            usersSupports.add(MUser.minId(doc.data, doc.documentID));
          });
          gettingMore = false;
          _inUsersList(usersSupports);
        } else {
          gettingMore = true;
        }
      }).catchError((error) {
        inProgressMore(true);
        gettingMore = false;
        print("Error al obtener mas usuarios que apoyaron $error");
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }
}
