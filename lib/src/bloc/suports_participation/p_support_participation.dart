import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/suports_participation/b_support_participation.dart';
import 'package:Darhu/src/ui/pages/supports_participation/util/arg_support_participation.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/cupertino.dart';

import '../../ui/pages/supports_participation/v_support_participation.dart';

class PSupportParticipation extends StatefulWidget {
  final ArgSupportParticipation argSupportParticipation;
  /*
  final String participationId;
  final String userCreatorId;
  final int numSupports;

   */
  PSupportParticipation({this.argSupportParticipation});

  @override
  _PSupportParticipationState createState() => _PSupportParticipationState();
}

class _PSupportParticipationState extends State<PSupportParticipation> {
  ApplicationBloc _applicationBloc;
  BSupportParticipation _bSupportUser;

  @override
  void initState() {
    super.initState();
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _bSupportUser = BSupportParticipation(widget.argSupportParticipation.participationId, _applicationBloc.user,
        widget.argSupportParticipation.userCreatorId, widget.argSupportParticipation.numSupports);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        child: VSupportParticipation(), blocBuilder: () => _bSupportUser);
  }
}
