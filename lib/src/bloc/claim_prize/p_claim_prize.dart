import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/ui/pages/claim_prize/util/arg_claim_prize.dart';
import 'package:Darhu/src/ui/pages/claim_prize/v_claim_prize.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'b_claim_prize.dart';

class PClaimPrize extends StatefulWidget {
  final ArgClaimPrize argClaimPrize;

  const PClaimPrize(
      {Key key, this.argClaimPrize})
      : super(key: key);

  @override
  _PClaimPrizeState createState() => _PClaimPrizeState();
}

class _PClaimPrizeState extends State<PClaimPrize> {
  BClaimPrize _bGetPrize;
  ApplicationBloc _applicationBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(child: VClaimPrize(), blocBuilder: () => _bGetPrize);
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _bGetPrize = BClaimPrize(
        _applicationBloc.user, widget.argClaimPrize.idChallenge, widget.argClaimPrize.refParticipation);
    super.initState();
  }
}
