import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

class BClaimPrize extends BlocBase{
  final MUser userSession;
  final String idChallenge;
  final dynamic refParticipation;
  BClaimPrize(this.userSession, this.idChallenge, this.refParticipation){
    getCodeVerificated();
  }

  final code= BehaviorSubject<String>();

  Function(String) get inCode => code.sink.add;
  Stream<String> get outCode => code.stream;



  @override
  void dispose() {
    code.close();
  }

  ///Metodo encargado de obtener el codigo de verificacion para la entrega del premio
  getCodeVerificated(){
    api.firestoreApi.getCodeVerificated(userSession.idUser,refParticipation).then((data){
      if(data.documents.isNotEmpty){
        inCode(data.documents[0].documentID);
      }else{
        print("no se dispone de un codigo");
        code.sink.addError("No dispone de un codigo");
      }
    }).catchError((error){
      print("error al obtener el codigo ${error.toString()}");
      code.sink.addError("No se pudo obtener el codigo");
    });
  }
}