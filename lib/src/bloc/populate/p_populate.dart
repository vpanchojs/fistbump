import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/ui/pages/populate/v_populate.dart';
import 'package:Darhu/src/bloc/populate/b_populate.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PPopulate extends StatefulWidget {
  @override
  PPopulateState createState() {
    return new PPopulateState();
  }
}

class PPopulateState extends State<PPopulate> {
  BPopulate _populateBloc;
  ApplicationBloc _applicationBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BPopulate>(
       blocBuilder: () => _populateBloc,
      child: VPopulate(),
    );
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _populateBloc = BPopulate(
        _applicationBloc.usersPopulate, _applicationBloc.challengesPopulate);
    super.initState();
  }
}
