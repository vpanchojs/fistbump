import 'dart:async';

import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:rxdart/rxdart.dart';

class BPopulate extends BlocBase with MixActionViewStream, ProgressMore {
  List<MUser> _populateUsers;
  List<MChallenge> _populateChallenges;

  int limitUsersPopulate = 8;
  int limitChallengePopulate = 12;

  /*Strems para lista de usuarios*/
  final _populateUsersCtrl = BehaviorSubject<List<MUser>>();

  Function(List<MUser>) get _inUsers => _populateUsersCtrl.sink.add;

  Stream<List<MUser>> get outUsers => _populateUsersCtrl.stream;

  /*Strems para lista de retos*/
  final _populateChallengeCtrl = BehaviorSubject<List<MChallenge>>();

  Function(List<MChallenge>) get _inChallenge => _populateChallengeCtrl.sink.add;

  Stream<List<MChallenge>> get outChallenge => _populateChallengeCtrl.stream;

  BPopulate(List<MUser> usersPopulate, List<MChallenge> challengesPopulate) {
    _populateUsers = usersPopulate;
    _populateChallenges = challengesPopulate;
    if (_populateUsers.length <= 0) {
      getUserPopulate();
    } else {
      _inUsers(_populateUsers);
    }

    if (_populateChallenges.length <= 0) {
      getChallengePopulate();
    } else {
      _inChallenge(_populateChallenges);
    }

  }

  void getUserPopulate() {
    _populateUsers.clear();
    api.firestoreApi.getUsersPopulate(limitUsersPopulate).then((query) {
      query.documents.forEach((doc) {
        _populateUsers.add(MUser.minId(doc.data, doc.documentID));
      });

      _inUsers(_populateUsers);
    }).catchError((e) {
      _populateUsersCtrl.sink
          .addError("Problemas al obtener los retadores populares");
    });
  }

  Future getChallengePopulate() async {
    await api.firestoreApi
        .getChallengesPopulate(limitChallengePopulate)
        .then((query) {
      _populateChallenges.clear();
      if (query.documents.length > 0) {
        api.firestoreApi.lastPopulateChallenge = query.documents.last;
        query.documents.forEach((doc) {
          _populateChallenges.add(
              MChallenge().getFullFromDateSnapshot(doc.data, doc.documentID));
        });
      }
      _inChallenge(_populateChallenges);
    }).catchError((e) {
      _populateChallengeCtrl.sink
          .addError("Problemas al obtener retos populares");
      print("error al consultar los retos $e");
    });
  }

  getMoreChallengePopulate() {
    if (!gettingMore) {
      gettingMore = true;
      inProgressMore(true);
      api.firestoreApi
          .getMoreChallengePopulate(limitChallengePopulate)
          .then((query) {
        if (query.documents.length > 0) {
          api.firestoreApi.lastPopulateChallenge = query.documents.last;
          query.documents.forEach((doc) {
            _populateChallenges.add(
                MChallenge().getFullFromDateSnapshot(doc.data, doc.documentID));
          });
          gettingMore = false;
        } else {
          gettingMore = true;
        }

        inProgressMore(false);
        _inChallenge(_populateChallenges);
      }).catchError((e) {
        gettingMore = false;
        inProgressMore(false);
        print("error al consultar los retos $e");
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }

  @override
  void dispose() {
    _populateUsersCtrl.close();
    _populateChallengeCtrl.close();
    closeProgressMoreList();
    closeDataView();
  }
}
