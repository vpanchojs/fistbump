import 'dart:async';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/enums/navigation_login.dart';
import 'package:quiver/async.dart';
import 'package:rxdart/rxdart.dart';

class BLogin extends BlocBase with MixActionViewStream, ProgressAction {
  MUser user;
  String verificationId = "";
  String phone;

  /// Indica la duracion del codigo de verificaciones enviado en sms
  int duration = 60;

  CountdownTimer _countdownTimer;

  final _countCtrl = PublishSubject<int>();

  Sink get inCount => _countCtrl.sink;

  Stream<int> get outCount => _countCtrl.stream;

  final _navigationCtrl = BehaviorSubject<StateLogin>();

  //Para controlar la navegacion de la ventana de ingresar codigo, ingresar numero de celular
  Function(StateLogin) get inNavegation => _navigationCtrl.sink.add;

  Stream<StateLogin> get outNavegation => _navigationCtrl.stream;

  final _sessionCtrl = BehaviorSubject<StateSession>();

  //Para controlar la navegacion de la ventana de ingresar codigo, ingresar nuemero de celular
  Function(StateSession) get inSession => _sessionCtrl.sink.add;

  Stream<StateSession> get outSession => _sessionCtrl.stream;

  BLogin(this.user) {
    _verificationSession();
  }

  @override
  void dispose() {
    _navigationCtrl.close();
    _countCtrl.close();
    _countdownTimer?.cancel();
    _sessionCtrl.close();
    closeProgressAction();
    closeDataView();
  }

  /// Verifica si ya termino el tiempo disponible para el codigo enviando por sms
  isTimeOut() {
    api.preferencesApi.getTimeOutAuthPhone().then((time) {
      if (time != null) {
        //viewLoginCtrl.sink.add(false);
        var timeout = getTimeOutActuality(time);
        if (timeout < 0) {
          //navegar hacia la venta de ingresar el codigo de verificacion
          inSession(StateSession.VALIDATE_CODE);
          api.preferencesApi.getVerificatedId().then((id) {
            verificationId = id;
          });
          cuentaregresiva(timeout * -1);
        } else {
          inSession(StateSession.INIT);
        }
      } else {
        inSession(StateSession.INIT);
      }
    }).catchError((error) {
      print("no se pudo obtener el tiempo restante $error");
      inSession(StateSession.INIT);
    });
  }

  /// Obtener el tiempo restante en segundos, entre la hora actual y la hora en la que debe caducar el codigo
  int getTimeOutActuality(String after) {
    print("cuando se terminara el codigo" + after);
    DateTime timeAfter = DateTime.parse(after);
    Duration dif = DateTime.now().difference(timeAfter);
    print("faltan tantos segundos" + dif.inSeconds.toString());
    return dif.inSeconds;
  }

  /// Se encarga de llevar la cuenta regresiva del tiempo restante en segundos
  cuentaregresiva(int seconds) {
    _countdownTimer = CountdownTimer(
        Duration(seconds: seconds), Duration(seconds: 1),
        stopwatch: null);
    _countdownTimer.listen((data) {
      print(data.remaining.inSeconds);
      inCount.add(data.remaining.inSeconds);
    });
  }

  verificateUserComplete(MUser u) async {
    //Almacenamos el usuario en el bloc padre
    user
      ..nameUser = u.nameUser
      ..cellPhone = u.cellPhone
      ..idUser = u.idUser
      ..urlPhoto = u.urlPhoto;

    _countdownTimer?.cancel();

    if (user.nameUser == null || user.nameUser.isEmpty) {
      inNavegation(StateLogin.EDIT_USER);
    } else {
      inNavegation(StateLogin.HOME);
    }
    api.preferencesApi.saveTimeOutAuthPhone(0);
  }

  // Recibe el número de celular
  void enviar(phone) async {
    this.phone = phone;

    inProgressAction(true);
    await api.internetConnection().then((connection) {
      if (!connection) {
        inProgressAction(false);
        inView(MActionView.error(message: string_error_network));
        return;
      }

      final PhoneVerificationCompleted verificationCompleted =
          (AuthCredential credential) async {
        print("========verificado========");
        AuthResult user = await api.authApi.loginCredential(credential);
        api.authApi.firebaseUser = user.user;
        verificateUserComplete(MUser.firebaseUser(user.user));
      };

      final PhoneVerificationFailed verificationFailed =
          (AuthException authException) {
        print("========error========");
        print(authException.message);
        //inViewError.add("Ocurrió un error al verificar su número");
        inView(MActionView.error(
            message: "Ocurrió un error al verificar su número"));
        inProgressAction(false);
      };

      final PhoneCodeSent codeSent =
          (String v, [int forceResendingToken]) async {
        //inViewErrorCode.add("Se enviará código al número de celular $phone");
        verificationId = v;
        api.preferencesApi.saveVerificatedId(verificationId);
        print("========codigo telefono========" + this.verificationId);
        api.preferencesApi.saveTimeOutAuthPhone(duration);
        cuentaregresiva(duration);
        inProgressAction(false);
        inSession(StateSession.VALIDATE_CODE);
      };
      final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
          (String verificationId) {
        print("========ERROR codigo de telifono con tiempo========");
        inProgressAction(false);
        inView(MActionView.error(message: "El código ha caducado"));
      };
//phone
      api.authApi
          .verifyPhoneNumber(phone, verificationCompleted, verificationFailed,
              codeSent, codeAutoRetrievalTimeout, duration)
          .then((data) {})
          .catchError((e) {
        print("Existe un error");
        inProgressAction(false);
        //inViewError.add(string_error_generic);
        inView(MActionView.error(message: string_error_generic));
      });
    }).catchError((e) {
      inProgressAction(false);
      inView(MActionView.error(message: string_error_undefid));
    });
  }

  verificarCodigo(code) {
    inProgressAction(true);
    api.authApi.signIn(verificationId, code.toString()).then((user) {
      api.authApi.firebaseUser = user.user;
      inProgressAction(false);
      verificateUserComplete(MUser.firebaseUser(user.user));
    }).catchError((e) {
      inProgressAction(false);
      //inViewErrorCode.add(string_error_generic);
      inView(MActionView.error(message: string_error_undefid));
    });
  }

  _verificationSession() {
    print("voy a verificar la sesion");
    api.authApi.inSession().then((firebaseUser) async {
      if (firebaseUser != null) {
        print("si existe session");
        verificateUserComplete(MUser.firebaseUser(firebaseUser));
      } else {
        print(" sin session voy a verificar el tiempo");
        isTimeOut();
      }
    });
  }
}
