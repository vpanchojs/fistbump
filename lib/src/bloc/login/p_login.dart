import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/login/b_login.dart';
import 'package:Darhu/src/ui/pages/login/v_login.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PLogin extends StatefulWidget {
  PLogin();

  @override
  PLoginState createState() {
    return new PLoginState();
  }
}

class PLoginState extends State<PLogin> {
  BLogin signInBloc;

  @override
  void initState() {
    var _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    signInBloc = BLogin(_applicationBloc.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BLogin>(
      blocBuilder: () => signInBloc,
      child: VLogin(),
    );
  }
}
