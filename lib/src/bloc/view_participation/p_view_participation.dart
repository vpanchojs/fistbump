import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/ui/pages/view_participation/v_item_participation.dart';
import 'package:Darhu/src/bloc/view_participation/b_view_participation.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class ViewParticipationProvider extends StatefulWidget {
  final MParticipation participation;
  final MChallenge challenge; // Reto al que pertenece la participacion
  final dynamic updateDateview; //Metodo para notificar a la vista padre de su actualizacion

  const ViewParticipationProvider({Key key, this.participation, this.challenge,this.updateDateview})
      : super(key: key);

  @override
  _ViewParticipationProviderState createState() =>
      _ViewParticipationProviderState();
}

class _ViewParticipationProviderState extends State<ViewParticipationProvider> {
  ApplicationBloc _applicationBloc;
  BViewParticipation _viewParticipationBloc;

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _viewParticipationBloc = BViewParticipation(
        _applicationBloc.user, widget.challenge, widget.participation,widget.updateDateview);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        child: VItemParticipation(),  blocBuilder: () => _viewParticipationBloc);
  }
}
