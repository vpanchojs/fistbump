import 'dart:async';

import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:Darhu/utils/mixins/progress_share.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

class BViewParticipation extends BlocBase
    with MixActionViewStream, ProgressAction, ProgressShare {
  MUser userSession;
  MChallenge challenge;
  MParticipation participation;
  bool myParticipation = false;
  bool myChallenge = false;
  dynamic updateDataView;
  Timer supportOrUnSupport;
  bool stateLike;
  int secondsSpam = -1;

  //Stream para progress de eliminacion y reportar participacion.
  //true => se muestra el progress
  //false => se oculta

  BViewParticipation(this.userSession, this.challenge, this.participation,
      this.updateDataView) {
    myParticipation = participation.user.idUser == userSession.idUser;
    myChallenge = challenge.user.idUser == userSession.idUser;
    stateLike = participation.liked;
    print(
        "participation id view particiption ${participation.idParticipation}");
    //getParticipationComplete(challenge.idChallenge, participation.idParticipation, user.idUser);
  }

  bool isItem(String idUser) {
    //return (idUser == api.authApi.firebaseUser.uid);
    return (idUser == userSession.idUser);
  }

  likeParticipation() {
    print("voy a dar like");
    if (participation.liked) {
      setLikedOrDisliked(
          participation.idParticipation, false, participation.numLikes - 1);
    } else {
      setLikedOrDisliked(
          participation.idParticipation, true, participation.numLikes + 1);
    }
    supportOrUnSupport?.cancel();
    //Controlamos que la peticio se enviada 2 segundos despues de la emision para controlar el spam
    supportOrUnSupport = Timer(Duration(seconds: secondsSpam++), () {
      //Se verifica que la peticion no sea para marca el mismo estado (Apoyo, Sin apoyo)
      if (participation.liked != stateLike) {
        api.functionsApi
            .likeOrDislikeParticipation(participation.idParticipation,
                userSession, participation.user.idUser, challenge.idChallenge)
            .then((value) {
          print("numlikes ${value.data}");
          stateLike = participation.liked;
        }).catchError((error) {
          print("error al dar like" + error);
          setLikedOrDisliked(participation.idParticipation, participation.liked,
              participation.numLikes);
        });
      }
    });
  }

  setLikedOrDisliked(String idParticipation, bool isLiked, int numLikes) {
    participation
      ..liked = isLiked
      ..numLikes = numLikes;
    updateDataView(0);
  }

  deleteParticipation() {
    inProgressAction(true);
    api.functionsApi
        .deleteParticipation(participation.idParticipation,
            participation.user.idUser, challenge.idChallenge)
        .then((value) {
      print("Se elimino la participancion ${value.data}");
      updateDataView(1, participation.idParticipation);
      //api.notificationsApi.unSubscribeTopic(challenge.idChallenge);
      inProgressAction(false);
    }).catchError((error) {
      inProgressAction(false);
      if (error is CloudFunctionsException) {
        print("codigo ${error.code}");
        print("message ${error.message}");
        print("codigo ${error.details}");
        inView(MActionView.error(message: string_error_generic));
      } else {
        print("eliminar reto error => $error");
        inView(MActionView.error(message: string_error_undefid));
      }
    });
  }

  reportParticipation(String code) {
    inProgressAction(true);
    api.firestoreApi
        .reportParticipation(
            participation.idParticipation,
            participation.user.idUser,
            challenge.idChallenge,
            userSession.idUser,
            code)
        .then((value) {
      inProgressAction(false);
      inView(MActionView.succes(message: string_success_report_participation));
    }).catchError((error) {
      print("Error al reportar $error");
      inView(MActionView.error(message: string_error_generic));
      inProgressAction(false);
    });
  }

  @override
  void dispose() {
    closeProgressAction();
    supportOrUnSupport?.cancel();
    closeDataView();
    closeProgressShare();
  }

  shareParticipationPlatforms(share) {
    inProgressShare(true);
    api.dynamicLinkApi
        .createDynamicLinkShareParticipationInChallenge(
            descriptionPost:
                (participation.type == "text") ? participation.response : " ",
            imageUrlPost: (participation.type != "text")
                ? participation.response
                : challenge.urlThumbnail,
            titlePost: "Participando en ${challenge.name}",
            idChallenge: challenge.idChallenge,
            idParticipation: participation.idParticipation)
        .then((data) {
      inProgressShare(false);
      if (data is ShortDynamicLink) {
        share(data.shortUrl.toString());
        print(data.shortUrl.toString());
      } else {
        share(data.toString());
        print(data.toString());
      }
    }).catchError((error) {
      inProgressShare(false);
      inView(MActionView.error(message: string_error_generic));
      print("Error al crear el dynamicLinks $error");
    });
  }
}
