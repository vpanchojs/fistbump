import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/creation_participation/b_create_participation.dart';
import 'package:Darhu/src/repository/r_create_participation.dart';
import 'package:Darhu/src/ui/pages/create_participation/v_create_participation.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'util/arg_creation_participation.dart';

class PCreateParticipation extends StatefulWidget {
  final ArgCreationParticipation argCreationParticipation;
  PCreateParticipation({Key key, this.argCreationParticipation})
      : super(key: key);

  @override
  _PCreateParticipationState createState() => _PCreateParticipationState();
}

class _PCreateParticipationState extends State<PCreateParticipation> {
  ApplicationBloc _applicationBloc;
  BCreateParticipation bloc;
  RCreateParticipation _repository;

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _repository = RCreateParticipation();
    bloc = BCreateParticipation(
        _applicationBloc.user,
        _repository,
        widget.argCreationParticipation.challenge,
        widget.argCreationParticipation.typeParticipation);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      child: VCreateParticipation(),
      blocBuilder: () => bloc,
    );
  }
}
