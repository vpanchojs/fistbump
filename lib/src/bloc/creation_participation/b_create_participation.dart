import 'dart:async';

import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/repository/r_create_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:rxdart/rxdart.dart';

class BCreateParticipation extends BlocBase
    with MixActionViewStream, ProgressAction {
  MUser userSession;
  RCreateParticipation repository;
  MChallenge challenge;
  TypeParticipation typeParticipation;
  StreamSubscription controlListen;

  BCreateParticipation(this.userSession, this.repository, this.challenge,
      this.typeParticipation) {
    if (typeParticipation == TypeParticipation.TEXT) {
      controlText();
    } else {
      controlMultimedia();
    }
  }

  final _multimediaCtrl = BehaviorSubject<MMultimedia>();

  Function(MMultimedia) get inMultimedia => _multimediaCtrl.sink.add;

  Stream<MMultimedia> get outMultimedia => _multimediaCtrl.stream;

  final _textCtrl = BehaviorSubject<String>();

  Function(String) get inText => _textCtrl.sink.add;

  Stream<String> get outText => _textCtrl.stream;

  final _activeCtrl = BehaviorSubject<bool>();

  Function(bool) get inActive => _activeCtrl.sink.add;

  Stream<bool> get outActive => _activeCtrl.stream;


  @override
  void dispose() {
    _multimediaCtrl.close();
    _textCtrl.close();
    _activeCtrl.close();
    closeDataView();
    closeProgressAction();
    controlListen.cancel();
  }

  createParticipation() {
    final participation = MParticipation();
    participation.typeParticipation = typeParticipation;
    switch (typeParticipation) {
      case TypeParticipation.IMAGE:
        participation.multimedia = _multimediaCtrl.value;
        break;
      case TypeParticipation.VIDEO:
        participation.multimedia = _multimediaCtrl.value;
        break;
      case TypeParticipation.TEXT:
        participation.description = _textCtrl.value;
        break;
    }
    inProgressAction(true);
    repository
        .createParticipation(participation, challenge, userSession)
        .then((res) {
      inProgressAction(false);
      if (res.success) {
        inView(MActionView.popData(arguments: res.data));
      } else {
        inView(MActionView.error(message: res.error));
      }
    }).catchError((e) {
      inProgressAction(false);
      inView(MActionView.error(message: e.toString()));
    });
  }

  controlMultimedia() {
    controlListen = outMultimedia.listen((multimedia) {
      inActive(multimedia != null);
    });
  }

  controlText() {
    controlListen = outText.listen((text) {
      inActive(text.isNotEmpty && text.length > 3);
    });
  }
}
