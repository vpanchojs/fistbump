import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/prize_detail/b_prize_detail.dart';
import 'package:Darhu/src/ui/pages/prize_detail/util/arg_prize_detail.dart';
import 'package:Darhu/src/ui/pages/prize_detail/v_prize_detail.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PPrizeDetail extends StatefulWidget {
  final ArgPrizeDetail argPrizeDetail;
  /*
  final USER_CHALLENGE userChallenge;
  final String idChallenge;
  final String stateChallenge;
   */

  const PPrizeDetail({Key key, this.argPrizeDetail}) : super(key: key);

  @override
  _PPrizeDetailState createState() => _PPrizeDetailState();
}

class _PPrizeDetailState extends State<PPrizeDetail> {
  BPrizeDetail _bprizeDetail;
  ApplicationBloc _applicationBloc;


  @override
  Widget build(BuildContext context) {
    return BlocProvider(child: VPrizeDetail(),  blocBuilder: () => _bprizeDetail);
  }

  @override
  void initState() {
    super.initState();
    _applicationBloc= BlocProvider.of<ApplicationBloc>(context);
    _bprizeDetail= BPrizeDetail(_applicationBloc.user,widget.argPrizeDetail.idChallenge,widget.argPrizeDetail.userChallenge,widget.argPrizeDetail.stateChallenge);
  }

}