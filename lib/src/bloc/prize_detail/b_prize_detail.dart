import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_prize.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/enums/enums.dart';
import 'package:rxdart/rxdart.dart';

class BPrizeDetail extends BlocBase{
  MUser userSession;
  String idChallenge;
  USER_CHALLENGE userChallenge;
  String stateChallenge;

  BPrizeDetail(this.userSession,this.idChallenge, this.userChallenge, this.stateChallenge){
    getPrizesChallenge();
  }


  final prizesCtrl= BehaviorSubject<Prize>();

  Function(Prize) get inPrizes => prizesCtrl.sink.add;
  Stream<Prize> get outPrizes => prizesCtrl.stream;

  final userWinnerCtrl= BehaviorSubject<MUser>();

  Function(MUser) get inUser => userWinnerCtrl.sink.add;
  Stream<MUser> get outUser => userWinnerCtrl.stream;


  @override
  void dispose() {
    prizesCtrl.close();
  }

  /// Se encarga de obtener los premios de un reto
  /// actualmente solo se estaba obteniendo uno, en futuras versiones considere
  /// actualizar el metodo para soportar la lista de premios.
  getPrizesChallenge() {
    api.firestoreApi.getPrizesChallenge(idChallenge).then((data){
      if(data.documents.isNotEmpty){
        print("se encontro premio");
        inPrizes(Prize.complete(data.documents[0].data,data.documents[0].documentID));
      }else{
        prizesCtrl.sink.addError("No se encontro ningun premio");
      }
    }).catchError((error){
        print("Error al traer los premios del reto" + error.toString());
        prizesCtrl.sink.addError("Problemas al consultar los premios");
    });
  }

  ///Metodo para obtener la informacion del usuario ganador del premio.
  ///
  getWinnerUser(Prize prize){
    api.firestoreApi.getInfoUserWinner(prize.winnerCode.refParticipation).then((data){
      inUser(MUser.completeId(data.data, data.documentID));
    }).catchError((error){
      userWinnerCtrl.addError("Problemas al obtener al usuario ganador");
    });
  }

}