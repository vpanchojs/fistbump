import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_prize.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:Darhu/utils/validations/validators.dart';
import 'package:rxdart/rxdart.dart';

class BAddPrize extends BlocBase
    with Validators, MixActionViewStream, ProgressAction {
  final namePrize = BehaviorSubject<String>();
  final lotPrize = BehaviorSubject<int>();
  final termsConditions = BehaviorSubject<String>();
  final photoPrize = BehaviorSubject<MMultimedia>();

  BAddPrize() {
    inLotPrize(1);
  }

  Function(String) get inNamePrize => namePrize.sink.add;

  Function(int) get inLotPrize => lotPrize.sink.add;

  Stream<String> get outNamePrize => namePrize.stream.transform(validNamePrize);

  Stream<int> get outLotPrize => lotPrize.stream;

  Function(String) get inTermsConditions => termsConditions.sink.add;

  Function(MMultimedia) get inMultimediaPrize => photoPrize.sink.add;

  Stream<String> get outTermsConditions =>
      termsConditions.stream.transform(validDescription);

  Stream<MMultimedia> get outMultimediaPrize => photoPrize.stream;

  Stream<bool> get submitValidWithPrize => Rx.combineLatest2(
      outNamePrize, outTermsConditions, (napri, terms) => true);

  @override
  void dispose() {
    namePrize.close();
    lotPrize.close();
    termsConditions.close();
    photoPrize.close();
  }

  //Se conforma el objeto para retornarlo al proceso de creacion del reto.
  saveTempPrize() {
    var prize = Prize()
      ..multimedia = photoPrize.value
      ..name = namePrize.value
      ..lot = lotPrize.value
      ..termsConditions = termsConditions.value;

    inView(MActionView.popData(arguments: prize));
    /*
    inView(DataView()
      ..action = ActionEnum.POPDATA
      ..data = prize);
    */
  }
}
