import 'package:Darhu/src/bloc/add_prize/b_add_prize.dart';
import 'package:Darhu/src/ui/pages/add_prize/v_add_prize.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PAddPrize extends StatefulWidget {
  @override
  _PAddPrizeState createState() => _PAddPrizeState();
}

class _PAddPrizeState extends State<PAddPrize> {
  BAddPrize _bAddPrize;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(child: VAddPrize(),  blocBuilder: () => _bAddPrize);
  }

  @override
  void initState() {
    super.initState();
    _bAddPrize= BAddPrize();
  }

}