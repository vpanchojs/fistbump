import 'dart:async';

import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/enums/enums.dart';
import 'package:Darhu/utils/mixins/mix_action_view_stream.dart';
import 'package:Darhu/utils/mixins/progress_action.dart';
import 'package:Darhu/utils/mixins/progress_info_action.dart';
import 'package:Darhu/utils/mixins/progress_more.dart';
import 'package:Darhu/utils/mixins/progress_share.dart';
import 'package:Darhu/utils/mixins/show_widget.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:rxdart/rxdart.dart';

class BViewChallenge extends BlocBase
    with
        MixActionViewStream,
        ProgressAction,
        ProgressMore,
        ProgressInfoAction,
        ProgressShare,
        ShowWidget {
  MChallenge challenge;
  List<MParticipation> listParticipation;
  MUser userSession;
  bool myChallenge = false;
  MParticipation participationSelect;
  String labelMyParticipation = "Mi participación";

  /// CONSTRUCTOR
  /// @Param Challenge,  entidad reto, debe contener al menos (identificador, nombre, thumbail y usuario creador)
  /// @Param userSession, entidad Usuario de sesion actual, debe contener al menos (identificador)
  /// @Param participationSelect, entidad de participacion seleccionada para ser visualizada en el reto, debe contener al menos  (identificador)
  BViewChallenge(this.challenge, this.userSession, this.participationSelect) {
    listParticipation = List();
    getFulChallenge();
  }

  @override
  void dispose() {
    challengeCtl.close();
    progressInfo.close();
    myParticipationCtrl.close();
    selectParticipationCtrl.close();
    closeProgressMoreList();
    closeProgressAction();
    participationCtrl.close();
    closeProgressShare();
    closeShowWidget();
    closeDataView();
  }

  ///INICIO STREAMS

  final challengeCtl = BehaviorSubject<MChallenge>();

  Function(MChallenge) get inChallenge => challengeCtl.sink.add;

  Stream<MChallenge> get outChallenge => challengeCtl.stream;

  final myParticipationCtrl = BehaviorSubject<MParticipation>();

  Function(MParticipation) get inMyParticipation =>
      myParticipationCtrl.sink.add;

  Stream<MParticipation> get outMyParticipation => myParticipationCtrl.stream;

  final selectParticipationCtrl = BehaviorSubject<MParticipation>();

  Function(MParticipation) get inSelectParticipation =>
      selectParticipationCtrl.sink.add;

  Stream<MParticipation> get outSelectParticipation =>
      selectParticipationCtrl.stream;

  final participationCtrl = BehaviorSubject<List<MParticipation>>();

  Function(List<MParticipation>) get _inParticipationList =>
      participationCtrl.sink.add;

  Stream<List<MParticipation>> get outParticipationList =>
      participationCtrl.stream;

  /*
  * Método para decirdir si obtengo  mi participacion o la participación previamente seleccionada de una notificacion, seccion noticias, etc.
  * */
  getParticipation() {
    if (participationSelect != null) {
      if (participationSelect.idParticipation ==
          userSession.idUser + challenge.idChallenge) {
        //Si la participacion selecciona es mi participacion, solo se obtiene mi participacion.
        labelMyParticipation = "Mi participación / Seleccionada";
        getMyParticipation();
      } else {
        //Si la participacion seleccionada es diferente a mi participacion, se debe traer las dos participaciones.
        getMyParticipation();
        getParticipationSelect(participationSelect.idParticipation);
      }
    } else {
      getMyParticipation();
    }
  }

  /*
  * Metodo para obtener mi participacion en el reto.
  * */
  getMyParticipation() {
    api.firestoreApi
        .getMyParticipation(
            userSession.idUser + challenge.idChallenge, userSession.idUser)
        .then((doc) {
      if (doc.exists) {
        var myParticipation =
            MParticipation.myParticipation(doc.data, doc.documentID);
        inMyParticipation(myParticipation);
      } else {
        inMyParticipation(MParticipation());
      }
      isWithParticipants(challenge.state, challenge.user.idUser);
    }).catchError((error) {
      isWithParticipants(challenge.state, challenge.user.idUser);
      myParticipationCtrl.addError(string_error_generic);
      print("Error al obtener mi participacion en reto $error");
    });
  }

  /*
  * Metodo para obtener la participacion seleccionada
  * */
  getParticipationSelect(String idParticipation) {
    api.firestoreApi
        .getMyParticipation(idParticipation,
            idParticipation.replaceAll(challenge.idChallenge, ""))
        .then((doc) async {
      if (doc.exists) {
        DocumentSnapshot liked =
            await api.firestoreApi.isLiked(doc.reference, userSession.idUser);
        var myParticipation = MParticipation.completeIsLiked(
            doc.data, doc.documentID, liked.exists);
        /*var myParticipation =
        Participation.myParticipation(doc.data, doc.documentID);*/

        inSelectParticipation(myParticipation);
      } else {
        inSelectParticipation(MParticipation());
      }
      isWithParticipants(challenge.state, challenge.user.idUser);
    }).catchError((error) {
      isWithParticipants(challenge.state, challenge.user.idUser);
      selectParticipationCtrl.addError(string_error_generic);
      print("Error al obtener la  participacion selecionada  $error");
    });
  }

  //1 eliminacion , 0 actualizacion
  updateDataMyParticipationView(int action, String idParticipation) {
    print("debo actualizar la vista");
    inMyParticipation(MParticipation());
    isWithParticipants(challenge.state, challenge.user.idUser);
  }

  updateDateParticipationSelect(action) {
    print("se actualiza la participacion seleccionada");
    inSelectParticipation(selectParticipationCtrl.value);
  }

  getFulChallenge() async {
    await api.firestoreApi.getFullChallenge(challenge.idChallenge).then((c) {
      print("vamos a obtener el reto");
      challenge.updateData(c);

      if (challenge.state == "a") {
        getParticipation();
        getParticipations();
      }
      inChallenge(challenge);
      isWithParticipants(challenge.state, challenge.user.idUser);
    }).catchError((error) {
      //isWithParticipants(challenge.state, challenge.user.idUser);
      print("Error al obtener el reto completo : $error");
      challengeCtl.sink.addError(string_error_generic);
    });
  }

  bool isMyPost(String idUser) {
    if (idUser != null)
      return (idUser == userSession.idUser);
    else
      return true;
  }

  uploadParticipation(result) {
    print("estoy por subir la participacion $result");
    inShow(false);
    //inWithParticipants(false);
    final participation = MParticipation()
      ..type = result[0]
      ..response = (result[0] != "text")
          ? result[1].resolveSymbolicLinksSync()
          : result[1];
    inProgressInfo(true);
    api.functionsApi
        .createParticipation(participation, challenge, userSession)
        .then((participation) {
      inProgressInfo(false);
      inMyParticipation(MParticipation.myParticipation(
          participation.data, participation.data["idParticipation"]));
      // print("Se creo la participancion ${participation.data}");
      //api.notificationsApi.subscribeTopic(challenge.idChallenge);
      isWithParticipants(challenge.state, challenge.user.idUser);
    }).catchError((error) {
      isWithParticipants(challenge.state, challenge.user.idUser);
      inProgressInfo(false);
      print("error crear participancion $error");
      if (error is CloudFunctionsException) {
        inView(MActionView.error(message: error.message));
      } else {
        inView(MActionView.error(message: string_error_generic));
      }
    });
  }

  bool isParticipant() {
    if (myParticipationCtrl.value != null) {
      return (myParticipationCtrl.value.idParticipation != null) ? true : false;
    }
    return false;
  }

  /*
  * Se encarga de controlar cuando se muestran las opciones de participar
  * true => se muestran
  * false => se oculta
  * @Param state, indica el estado del reto.
  * @Param idUser, indica el identificador del usuario propietario del reto.
  * */
  isWithParticipants(String state, String idUser) {
    (state == 'a' && !isMyPost(idUser) && !isParticipant())
        ? inShow(true)
        : inShow(false);
  }

  shareChallengePlatforms(shareOtherPlatforms) {
    inProgressShare(true);
    api.dynamicLinkApi
        .createDynamicLinkShareChallenge(
            descriptionPost: challenge.description,
            imageUrlPost: challenge.urlThumbnail,
            titlePost: challenge.name,
            idChallenge: challenge.idChallenge)
        .then((data) {
      inProgressShare(false);
      if (data is ShortDynamicLink) {
        shareOtherPlatforms(data.shortUrl.toString());
        print(data.shortUrl.toString());
      } else {
        shareOtherPlatforms(data.toString());
        print(data.toString());
      }
    }).catchError((error) {
      inProgressShare(false);
      inView(MActionView.error(message: string_error_generic));
    });
  }

  reportChallenge(String code) {
    api.firestoreApi
        .reportChallenge(challenge.idChallenge, userSession.idUser, code)
        .then((data) {
      inView(MActionView.error(message: string_success_report_challenge));
    }).catchError((error) {
      print("Error al reportar un reto $error");
      inView(MActionView.error(message: string_error_generic));
    });
  }

  getParticipations() async {
    await api.firestoreApi
        .getInitialParticipations(challenge.idChallenge, userSession.idUser)
        .then((list) {
      listParticipation.clear();
      listParticipation.addAll(list);
      ommitMyParticipation();
      ommitSelectParticipation();
      gettingMore = list.length == 0 ? true : false;
      _inParticipationList(listParticipation);
      print("todo bien  ${list.length}");
    }).catchError((e) {
      print("error al obtener las participaciones $e");
      participationCtrl.sink.addError(string_error_generic);
    });
  }

  getMoreParticipations() async {
    //si es falso se puede buscar
    if (!gettingMore) {
      gettingMore = true;
      inProgressMore(true);
      api.firestoreApi
          .getMoreParticipations(challenge.idChallenge, userSession.idUser)
          .then((lista) {
        inProgressMore(false);
        if (lista.length > 0) {
          listParticipation.addAll(lista);
          ommitMyParticipation();
          ommitSelectParticipation();
          gettingMore = false; //se vuelve a buscar
        } else {
          gettingMore = true; //no se vuelve a buscar
        }
        _inParticipationList(listParticipation);
      }).catchError((e) {
        inProgressMore(false);
        gettingMore = false; //se vuelven a buscar
        print("error al cargar mas participaciones $e");
        inView(MActionView.error(message: string_error_generic));
      });
    }
  }

  void ommitMyParticipation() {
    listParticipation.removeWhere((item) =>
        item.idParticipation == userSession.idUser + challenge.idChallenge);
  }

  void ommitSelectParticipation() {
    if (participationSelect != null) {
      listParticipation.removeWhere((item) =>
          item.idParticipation == participationSelect.idParticipation);
    }
  }

  //1 eliminacion , 0 actualizacion
  updateDataListParticipation(int action, [String idParticipation]) {
    print("se actualiza la participacions");
    if (action == 1) {
      listParticipation
          .removeWhere((item) => item.idParticipation == idParticipation);
    }
    _inParticipationList(listParticipation);
  }

  USER_CHALLENGE getRolUserInChallenge() {
    if (userSession.idUser == challenge.user.idUser) {
      return USER_CHALLENGE.CREATOR;
    } else {
      if (challenge.winner.user != null &&
          challenge.winner.user.idUser == userSession.idUser) {
        return USER_CHALLENGE.WINNER;
      }
      return USER_CHALLENGE.PARTICIPANT;
    }
  }

  myParticipation(MParticipation participation) {
    inMyParticipation(participation);
  }
}
