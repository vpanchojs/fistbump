import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/view_challenge/b_view_challenge.dart';
import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class PViewChallenge extends StatefulWidget {
  final ArgViewChallenge arguments;

  PViewChallenge({Key key, this.arguments})
      : super(key: key);

  @override
  PViewChallengeState createState() {
    return new PViewChallengeState();
  }
}

class PViewChallengeState extends State<PViewChallenge> {
  BViewChallenge _viewChallengeBloc;
  ApplicationBloc _applicationBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BViewChallenge>(
       blocBuilder: () => _viewChallengeBloc,
      child: VViewChallenge(),
    );
  }

  @override
  void initState() {
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    _viewChallengeBloc = BViewChallenge(
        widget.arguments.challenge, _applicationBloc.user, widget.arguments.participation);
    super.initState();
  }
}
