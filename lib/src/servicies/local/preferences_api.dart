import 'package:shared_preferences/shared_preferences.dart';

class PreferencesApi {
  static const String KEY_TIMEOUT = "timeout";
  static const String KEY_VERIFICATED_ID = "verficicatedid";
  static const String TOKEN_NOTIFICATION = "tokenNotification";


  //Guarda el tiempo en el que caduca el codigo de verificacion en el celular
  saveTimeOutAuthPhone(int duration) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(KEY_TIMEOUT,
        new DateTime.now().add(Duration(seconds: duration)).toString());
  }

  //Obtiene el tiempo en que que caduca el codigo de verificacion del celular
  Future<String> getTimeOutAuthPhone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(KEY_TIMEOUT);
  }

  saveTokenNotification(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(TOKEN_NOTIFICATION, token);
  }

  Future<bool> isTokenExists(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return token == prefs.getString(TOKEN_NOTIFICATION);
  }

  Future resetTokenNotification() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(TOKEN_NOTIFICATION, "");
  }

  saveVerificatedId(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(KEY_VERIFICATED_ID, id);
  }

  Future<String> getVerificatedId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(KEY_VERIFICATED_ID);
  }


}
