
import 'dart:async';
import 'dart:ui';

import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

class QrApi{
  Timer timerSearch;
  int timeWait=1000;


  readCameraImage(CameraImage image,setCode) async {
    //print("leyenado foto");
    if(timerSearch==null){
      startReadCode(image,setCode);
    }else{
      if(!timerSearch.isActive){
        startReadCode(image,setCode);
      }
     }
  }

  startReadCode(CameraImage image,setCode){
    final FirebaseVisionImageMetadata metadata = FirebaseVisionImageMetadata(
        rawFormat: image.format.raw,
        size: Size(image.width.toDouble(),image.height.toDouble()),
        planeData: image.planes.map((currentPlane) => FirebaseVisionImagePlaneMetadata(
            bytesPerRow: currentPlane.bytesPerRow,
            height: currentPlane.height,
            width: currentPlane.width
        )).toList(),
        rotation: ImageRotation.rotation90
    );

    final FirebaseVisionImage visionImage = FirebaseVisionImage.fromBytes(image.planes[0].bytes,metadata);
      timerSearch = Timer(Duration(milliseconds: timeWait), () async {
        print("procesare la imagen");
        setCode(await getInfoQr(visionImage));
      });
    }

  Future getInfoQr(FirebaseVisionImage visionImage) async {
    String code="";

    final BarcodeDetector barcodeDetector = FirebaseVision.instance.barcodeDetector();
    final List<Barcode> barcodes = await barcodeDetector.detectInImage(visionImage);

    for (Barcode barcode in barcodes) {
      if(BarcodeValueType.text==barcode.valueType){
        print("el ${barcode.rawValue}");
        code=barcode.rawValue;
      }
      barcodeDetector.close();
    }
    return code;
  }
}


