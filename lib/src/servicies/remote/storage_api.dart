import 'dart:io';
import 'dart:math';
import 'package:path/path.dart' as p;

import 'package:Darhu/src/models/m_item_multimedia.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/utils/responses/response_api.dart';
import 'package:Darhu/utils/share_widgets/multimedia/encoding_provider.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:path_provider/path_provider.dart';

class StorageApi {
  final FirebaseStorage _firebaseStorage = FirebaseStorage.instance;
  static const quality = 50;
  static const limitSizeVideo = 75000000;
  static const limitSizeImage = 10000000;
  static const ommitSizeImage = 200000;
  static const ommitSizeVideo = 1000000;

  final Api ap;

  StorageApi(this.ap);

  /// Funcion se encarga de subir la miniatura de un archivo multimedia
  /// @Param Multimedia
  /// Como es archivo thumbnail no se realiza compresion antes de subir
  Future<ResponseApi<MItemMultimedia>> uploadThumbnail(
      MItemMultimedia multimedia, StorageReference ref) async {
    try {
      var name = DateTime.now().millisecondsSinceEpoch.toString() +
          "." +
          multimedia.item.path.split(".").last;
      var uploadTask = ref.child(name).putFile(multimedia.item);
      var refUploadFileFull = (await uploadTask.onComplete).ref;
      String pathFull = refUploadFileFull.path;
      var urlMultimedia =
          await (await uploadTask.onComplete).ref.getDownloadURL();

      return ResponseApi.success(
          MItemMultimedia.imageUrl(item: urlMultimedia, pathOrigin: pathFull));
    } catch (e) {
      return ResponseApi.error(e.toString());
    }
  }

  StorageReference getStorageReferencePhotoUser({String idUser}) {
    return _firebaseStorage.ref().child(Api.path_users).child(idUser);
  }

  ///Referencia para almacenar los multimedia de  premios de los retos.
  StorageReference getStorageReferencePrize(
      {String idChallenge, String idUser}) {
    return _firebaseStorage
        .ref()
        .child(Api.path_challenges)
        .child(idChallenge)
        .child(Api.path_prize_challenges);
  }

  ///Referencia para almacenar los   multimedia de la entrega del premio de un reto
  StorageReference getStorageReferenceDeliveryPrize(
      {String idChallenge, String idUser, String idPrize}) {
    return _firebaseStorage
        .ref()
        .child("challenges")
        .child(idChallenge)
        .child(idPrize)
        .child("winner");
  }

  /// Referencia para almacenar los multimedia de un reto.
  StorageReference getStorageReferenceChallenge(
      {String idChallenge, String idUser}) {
    return _firebaseStorage.ref().child(Api.path_challenges).child(idChallenge);
  }

  ///Referencia para almacernar los multimedia de una participacion
  StorageReference getStorageReferenceParticipation(
      {String idParticipation, String idUser}) {
    return _firebaseStorage
        .ref()
        .child(Api.path_users)
        .child(idUser)
        .child(Api.path_participations)
        .child(idParticipation);
  }

  Future<ResponseApi<MItemMultimedia>> uploadMultimedia(
      MItemMultimedia multimedia, StorageReference ref) async {
    try {
      File file = multimedia.item as File;
      if (multimedia.type == TypeMultimedia.VIDEO) {
        return await uploadVideo(file, ref);
      } else {
        return await uploadImage(file, ref);
      }
    } catch (e) {
      return ResponseApi.error(e.toString());
    }
  }

  uploadImage(File file, StorageReference ref) async {
    var sizeTemp = file.lengthSync();
    if (sizeTemp > ommitSizeImage) {
      file = await compressImage(file);
      var size = file.lengthSync();
      print("El tamano $size");
      if (size > limitSizeImage) {
        return ResponseApi.error(string_error_image_exceed_size);
      }
    }

    var name = DateTime.now().millisecondsSinceEpoch.toString() +
        "." +
        file.path.split(".").last;
    var uploadTask = ref.child(name).putFile(file);
    var refUploadFileFull = (await uploadTask.onComplete).ref;
    String pathFull = refUploadFileFull.path;
    var urlMultimedia = await refUploadFileFull.getDownloadURL();

    return ResponseApi.success(
        MItemMultimedia.imageUrl(item: urlMultimedia, pathOrigin: pathFull));
  }

  Future<ResponseApi<MItemMultimedia>> uploadVideo(
      File file, StorageReference ref) async {
    var sizeTemp = file.lengthSync();
    if (sizeTemp > ommitSizeVideo) {
      //file = await compressVideo(file);
      var size = file.lengthSync();
      if (size > limitSizeVideo) {
        return ResponseApi.error(string_error_video_exceed_size);
      }
    }
    final String rand = '${new Random().nextInt(10000)}';
    final videoName = 'video$rand';

    //String directoryVideoFiles = await encodeVideo(file, videoName);
    ResponseApi responseEncode = await encodeVideo(file, videoName);

    if (responseEncode.success) {
      ResponseApi urlFileMaster =
          await _uploadHLSFiles(responseEncode.data, videoName, ref);
      return urlFileMaster;
    } else {
      return responseEncode;
    }
  }

  //Metodo aun falta por probar
  deleteFile(String path) {
    _firebaseStorage.ref().child(path).delete().then((value) {
      print("Eliminado");
    }).catchError((e) {
      print("No se pudo eliminar el archivo ${e.toString()}");
    });
  }

  compressImage(File file) async {
    var name = DateTime.now().millisecondsSinceEpoch.toString() +
        "." +
        file.path.split(".").last;
    final FlutterFFmpeg _flutterFFmpeg = new FlutterFFmpeg();
    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path + "/$name";
    var arguments = ["-i", file.path, "-q:v", "18", path];
    int rc = await _flutterFFmpeg.executeWithArguments(arguments);
    if (rc == 0) {
      print("Se comprio");
      return file = File(path);
    } else {
      print("No se comprimio");
      Future.error("Problemas al comprimir la imagen");
    }
  }

  compressVideo(File file) async {
    print("path absoluto sin coma ${file.path}");
    var name = DateTime.now().millisecondsSinceEpoch.toString() +
        "." +
        file.path.split(".").last;
    final FlutterFFmpeg _flutterFFmpeg = new FlutterFFmpeg();
    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path + "/$name";
    var arguments = [
      "-i",
      file.path,
      "-vcodec",
      "h264",
      "-crf",
      "30",
      "-preset",
      "veryfast",
      "-c:a",
      "copy",
      path
    ];
    int rc = await _flutterFFmpeg.executeWithArguments(arguments);
    print("rc $rc");
    if (rc == 0) {
      print("Se comprio");
      return file = File(path);
    } else {
      print("No se comprimio");
      Future.error("Problemas al comprimir la imagen");
    }
  }

  Future<ResponseApi> encodeVideo(File rawVideoFile, String videoName) async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    final outDirPath = '${extDir.path}/Videos/$videoName';
    final videosDir = new Directory(outDirPath);
    videosDir.createSync(recursive: true);

    final rawVideoPath = rawVideoFile.path;
    //final info = await EncodingProvider.getMediaInformation(rawVideoPath);
    //final aspectRatio = EncodingProvider.getAspectRatio(info);

    try {
      return ResponseApi.success(
          await EncodingProvider.encodeHLS(rawVideoPath, outDirPath));
    } catch (e) {
      return ResponseApi.error(e.toString());
    }
  }

  Future<ResponseApi<MItemMultimedia>> _uploadHLSFiles(
      dirPath, videoName, StorageReference ref) async {
    final videosDir = Directory(dirPath);
    ResponseApi responseApi;
    var pathStorageParts = ref.child(videoName).path.replaceAll("/", "%2F");

    final files = videosDir.listSync();
    for (FileSystemEntity file in files) {
      final fileName = p.basename(file.path);
      final fileExtension = getFileExtension(fileName);
      if (fileExtension == 'm3u8') {
        _updatePlaylistUrls(file, pathStorageParts);
      }

      /*
      setState(() {
        _processPhase = 'Uploading video file $i out of ${files.length}';
        _progress = 0.0;
      });
      */

      final downloadUrl = await _uploadFile(file.path, videoName, ref);

      if (fileName == 'master.m3u8') {
        responseApi = downloadUrl;
      }
    }

    return responseApi;
  }

  void _updatePlaylistUrls(File file, String path) {
    final lines = file.readAsLinesSync();
    var updatedLines = List<String>();
    //https://firebasestorage.googleapis.com/v0/b/fistbump-690bd.appspot.com/o/
    for (final String line in lines) {
      var updatedLine = line;
      if (line.contains('.ts') || line.contains('.m3u8')) {
        updatedLine = '$path%2F$line?alt=media';
      }
      updatedLines.add(updatedLine);
    }
    final updatedContents =
        updatedLines.reduce((value, element) => value + '\n' + element);

    file.writeAsStringSync(updatedContents);
  }

  String getFileExtension(String fileName) {
    final exploded = fileName.split('.');
    return exploded[exploded.length - 1];
  }

  Future<ResponseApi<MItemMultimedia>> _uploadFile(
      filePath, folderName, StorageReference ref) async {
    try {
      final file = new File(filePath);
      final basename = p.basename(filePath);
      StorageUploadTask uploadTask =
          ref.child(folderName).child(basename).putFile(file);
      StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
      String pathFull = taskSnapshot.ref.path;
      String videoUrl = await taskSnapshot.ref.getDownloadURL();

      return ResponseApi.success(
          MItemMultimedia.imageUrl(item: videoUrl, pathOrigin: pathFull));
    } catch (e) {
      return ResponseApi.error("Error al subir el archivo");
    }
  }
}
