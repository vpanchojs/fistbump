import 'package:Darhu/src/models/m_device_user.dart';
import 'package:Darhu/src/models/m_item_multimedia.dart';
import 'package:Darhu/src/models/m_video_info.dart';
import 'package:Darhu/utils/responses/response_api.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';

class FirestoreApi {
  final Firestore firestore = Firestore.instance;
  final Api ap;
  DocumentSnapshot lastMyChallenge;
  DocumentSnapshot lastPopulateChallenge;
  DocumentSnapshot lastParticipationChallenge;
  DocumentSnapshot lastMyNewsFeed;
  DocumentSnapshot lastMyNotification;
  DocumentSnapshot lastMyPartition;
  DocumentSnapshot lastContact;
  DocumentSnapshot lastSupportUser;

  FirestoreApi(this.ap);

  //==================== USUARIOS ==========================

  Future<void> updateTokenNotification(
      String idUser, MDeviceUser device) async {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_devices_user)
        .document(device.idDevice)
        .setData(device.deviceUserMap(), merge: true);

/*
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .updateData({"tokenNotification": token});*/
  }

  Future<void> removeTokenNotification(
      String idUser, MDeviceUser device) async {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_devices_user)
        .document(device.idDevice)
        .delete();

/*
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .updateData({"tokenNotification": token});*/
  }

  Future<DocumentSnapshot> getInfoUser(String idUser) async {
    return firestore.collection(Api.path_users).document(idUser).get();
  }

  Future updateProfile(MUser user, bool exist, String idUser) async {
    if (!exist) {
      //el usuario aun no existe
      var doc = await firestore
          .collection(Api.path_users)
          .where("nameUser", isEqualTo: user.nameUser)
          .limit(1)
          .getDocuments();
      if (doc.documents.length >= 1) {
        //ya existe otro usuario con el name user
        return Future.error("nameUser");
      }
    }

    var batch = firestore.batch();

    var refUser = firestore.collection(Api.path_users).document(idUser);

    var refUserSearch =
        firestore.collection(Api.path_users_search).document(idUser);

    batch.setData(refUser, user.userMapUpdate(), merge: true);

    batch.setData(refUserSearch, user.userMapSearch(), merge: true);

    return batch.commit();
  }

  Future<QuerySnapshot> findPeopleQuery(String query, int limit) {
    return firestore
        .collection(Api.path_users_search)
        .where("keywords", arrayContainsAny: query.split(" "))
        .limit(limit)
        .getDocuments();
  }

  //Obtener seguidores
  Future<QuerySnapshot> getFollowers(String idUser, int limitContacts) {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_contacts)
        .where("type", isLessThanOrEqualTo: 2)
        .orderBy("type")
        .limit(limitContacts)
        .getDocuments();
  }

  //Obtener seguidos
  Future<QuerySnapshot> getFolloweds(String idUser, int limitContacts) {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_contacts)
        .where("type", isGreaterThanOrEqualTo: 2)
        .orderBy("type")
        .limit(limitContacts)
        .getDocuments();
  }

  ///Obtener mas Seguidores
  Future<QuerySnapshot> getMoreFollowers(idUser, int limitContacts) {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_contacts)
        .where("type", isLessThanOrEqualTo: 2)
        .orderBy("type")
        .startAfterDocument(lastContact)
        .limit(limitContacts)
        .getDocuments();
  }

  ///Obtener mas Seguidos
  Future<QuerySnapshot> getMoreFolloweds(String idUser, int limitContacts) {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_contacts)
        .where("type", isGreaterThanOrEqualTo: 2)
        .orderBy("type")
        .startAfterDocument(lastContact)
        .limit(limitContacts)
        .getDocuments();
  }

  Future<QuerySnapshot> getUsersPopulate(int numUsersPopulate) {
    return firestore
        .collection(Api.path_users_search)
        .orderBy("numChallenges", descending: true)
        .limit(numUsersPopulate)
        .getDocuments();
  }

  /*
  Future<DocumentSnapshot> getInfoUserProfile(String idUser) async {
    return firestore.collection(Api.path_users).document(idUser).get();
  }
   */

  Future<DocumentSnapshot> isContact(String idUser, String myId) {
    return firestore
        .collection(Api.path_users)
        .document(myId)
        .collection(Api.path_contacts)
        .document(idUser)
        .get();
  }

//==================== RETOS ==========================

  Future<QuerySnapshot> getChallengesPopulate(int limitChallenguePopulate) {
    return firestore
        .collection(Api.path_challenges)
        // .where("state", isEqualTo: "a")
        .where("numParticipantions", isGreaterThanOrEqualTo: 0)
        .orderBy("numParticipantions", descending: true)
        .getDocuments();
  }

  Future<QuerySnapshot> findChallengesQuery(query, int limitChallengueSearch) {
    return firestore
        .collection(Api.path_challenges)
        .where("keywords", arrayContains: query)
        .limit(limitChallengueSearch)
        .getDocuments();
  }

  // RETOS
  Future<List<MChallenge>> getInitialMyChallenges(
      String idUser, int limitChallenges) async {
    try {
      QuerySnapshot doc = await firestore
          .collection(Api.path_challenges)
          //.where("state", isEqualTo: "a")
          .where("user.idUser", isEqualTo: idUser)
          .orderBy('datePost', descending: true)
          .limit(limitChallenges)
          .getDocuments();

      var list = List<MChallenge>();
      if (doc.documents != null) {
        if (doc.documents.isNotEmpty) lastMyChallenge = doc.documents.last;
        for (final q in doc.documents) {
          list.add(MChallenge().getFullFromDateSnapshot(q.data, q.documentID));
        }
      }
      return list;
    } catch (e) {
      print("getInitialMyChallenges $e");
      return Future.error(e);
    }
  }

  Future<List<MChallenge>> getSomeChallenges(String idUser, int value) async {
    try {
      QuerySnapshot doc;
      doc = await firestore
          .collection(Api.path_challenges)
          .where("user.idUser", isEqualTo: idUser)
          .orderBy('datePost', descending: true)
          .startAfterDocument(lastMyChallenge)
          .limit(value)
          .getDocuments();

      var list = List<MChallenge>();
      if (doc.documents != null) {
        if (doc.documents.isNotEmpty) lastMyChallenge = doc.documents.last;
        for (final q in doc.documents) {
          list.add(MChallenge().getFullFromDateSnapshot(q.data, q.documentID));
        }
      }
      return list;
    } catch (e) {
      print(e.toString());
      return Future.error(e);
    }
  }

  Future<MChallenge> getFullChallenge(String idChallenge) async {
    print("el idChallenge a consulta $idChallenge");
    try {
      DocumentSnapshot doc = await firestore
          .collection(Api.path_challenges)
          .document(idChallenge)
          .get();

      print("el reto obtenido es ${doc.data}");
      return MChallenge.complete(doc.data, doc.documentID);
    } catch (e) {
      print("Error al obtener el reto $e");
      return Future.error(e);
    }
  }

  /*
  * Se obtiene las participaciones del reto, para el usuario que esta solicitando
  * */

  Future<List<MParticipation>> getInitialParticipations(
      String idChallenge, String idUser) async {
    print(" participaciones el id usuario $idUser");
    try {
      final doc = await firestore
          .collectionGroup(Api.path_participations)
          .where("challenge.idChallenge", isEqualTo: idChallenge)
          .orderBy('datePost')
          .limit(5)
          .getDocuments();

      var list = List<MParticipation>();
      if (doc.documents != null) {
        if (doc.documents.isNotEmpty)
          lastParticipationChallenge = doc.documents.last;
        for (final q in doc.documents) {
          DocumentSnapshot liked = await isLiked(q.reference, idUser);

          list.add(MParticipation.completeIsLiked(
              q.data, q.documentID, liked.exists));
        }
      }
      return list;
    } catch (e) {
      print("get  $e");
      return Future.error(e);
    }
  }

  Future isLiked(DocumentReference ref, String idUser) {
    return ref.collection(Api.path_users_like).document(idUser).get();
  }

//========== NEWSFEED ==============
  Future<QuerySnapshot> getMyNewsFeed(String idUser, int limitNews) {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_my_news)
        .orderBy("datePost", descending: true)
        .limit(limitNews)
        .getDocuments();
  }

  //============ NOTIFICACIONES=============

  Future<QuerySnapshot> getMyNotifications(
      String idUser, int limitNotifications) {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_my_notifications)
        .orderBy("datePost", descending: true)
        .limit(limitNotifications)
        .getDocuments();
  }

  Future<QuerySnapshot> getMoreMyNotifications(
      String idUser, int limitNotifications) {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_my_notifications)
        .orderBy("datePost", descending: true)
        .limit(limitNotifications)
        .startAfterDocument(lastMyNotification)
        .getDocuments();
  }

  Future<QuerySnapshot> getMyParticipations(String idUser, int limit) {
    return firestore
        .collectionGroup(Api.path_participations)
        .where("user.idUser", isEqualTo: idUser)
        .orderBy("datePost", descending: true)
        .limit(limit)
        .getDocuments();
  }

  Future<QuerySnapshot> getMyMoreParticipations(String idUser, int limit) {
    return firestore
        .collectionGroup(Api.path_participations)
        .where("user.idUser", isEqualTo: idUser)
        .orderBy("datePost", descending: true)
        .startAfterDocument(lastMyPartition)
        .limit(limit)
        .getDocuments();
  }

  Future<QuerySnapshot> getMoreChallengePopulate(
      int limitChallengePopulate) async {
    return firestore
        .collection(Api.path_search_challenges)
        .where("state", isEqualTo: "a")
        .where("numParticipantions", isGreaterThanOrEqualTo: 0)
        .orderBy("numParticipantions", descending: true)
        .startAfterDocument(lastPopulateChallenge)
        .getDocuments();
  }

  /*
  * Metodo para obtener mi participacion en un reto
  * @param idChallenge, identificador del reto
  * @param idUser, identificador del usuario
  * @return Future DocumentSnapshot
  * */
  Future<DocumentSnapshot> getMyParticipation(
      String idParticipant, String idUser) {
    /*
    return firestore
        .collection(Api.path_challenges)
        .document(idChallenge)
        .collection(Api.path_participations)
        .document(idUser)
        .get();
     */

    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_participations)
        .document(idParticipant)
        .get();
  }

  Future<List<MParticipation>> getMoreParticipations(
      String idChallenge, String idUser) async {
    try {
      /*
      final doc = await firestore
          .collection(Api.path_challenges)
          .document(idChallenge)
          .collection(Api.path_participations)
          .orderBy('datePost')
          .startAfterDocument(lastParticipationChallenge)
          .limit(5)
          .getDocuments();
       */

      final doc = await firestore
          .collectionGroup(Api.path_participations)
          .where("challenge.idChallenge", isEqualTo: idChallenge)
          .orderBy('datePost')
          .startAfterDocument(lastParticipationChallenge)
          .limit(5)
          .getDocuments();

      var list = List<MParticipation>();
      if (doc.documents != null) {
        if (doc.documents.isNotEmpty)
          lastParticipationChallenge = doc.documents.last;
        for (final q in doc.documents) {
          DocumentSnapshot liked = await isLiked(q.reference, idUser);
          list.add(MParticipation.completeIsLiked(
              q.data, q.documentID, liked.exists));
        }
      }
      return list;
    } catch (e) {
      print("getInitialMyChallenges $e");
      return Future.error(e);
    }
  }

  getMoreMyNewsFeed(String idUser, int limitNews) {
    return firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_my_news)
        .orderBy("datePost", descending: true)
        .startAfterDocument(lastMyNewsFeed)
        .limit(limitNews)
        .getDocuments();
  }

  /*
  * Metodo para guardar en la bd el reporte de una participacion.
  * @Param idParticipation, identificador de la participacion.
  * @Param ideUser, identificador del usuario propietario de la participacion.
  * @Param idChallenge, identificador del reto, a donde pertenece la participacion.
  * @Param idUserReport, identificador del usuario que reporta.
  * @Param code, identificador de la razon por la cual se reporta
  * */
  Future reportParticipation(String idParticipation, String idUser,
      String idChallenge, String idUserReport, String code) {
    final refParticipation = firestore
        .collection(Api.path_users)
        .document(idUser)
        .collection(Api.path_participations)
        .document(idParticipation);

    final refReportParticipation = refParticipation
        .collection(Api.path_reports_participation)
        .document(idUserReport);

    var batch = firestore.batch();

    //Se incrementa en 1 los reportes hacia la participacion
    batch.updateData(refParticipation, {
      "numReports": FieldValue.increment(1),
    });

    //Se almacena el reporte o denuncia
    batch.setData(refReportParticipation, {
      "code": code,
      "date": FieldValue.serverTimestamp(),
    });

    return batch.commit();
  }

  /*
  * Metodo para guardar en la bd el reporte o denuncia  hacia un reto.
  * @Param idChallenge, identificador del reto, a donde pertenece la participacion.
  * @Param idUserReport, identificador del usuario que reporta.
  * @Param code, identificador de la razon por la cual se reporta
  * */
  Future reportChallenge(String idChallenge, String idUserReport, String code) {
    final refChallenge =
        firestore.collection(Api.path_challenges).document(idChallenge);

    final refReportChallenge = refChallenge
        .collection(Api.path_reports_challenge)
        .document(idUserReport);

    var batch = firestore.batch();

    //Se incrementa en 1 los reportes hacia la participacion
    batch.updateData(refChallenge, {
      "numReports": FieldValue.increment(1),
    });

    //Se almacena el reporte o denuncia
    batch.setData(refReportChallenge, {
      "code": code,
      "date": FieldValue.serverTimestamp(),
    });

    return batch.commit();
  }

  Future<QuerySnapshot> getCodeVerificated(
      String idUser, DocumentReference refParticipation) {
    return refParticipation.collection(Api.path_code_win_prize).getDocuments();
    //return firestore.collection(Api.path_challenges).document(idChallenge).collection(Api.path_prize_challenges).getDocuments();
  }

  Future deliveryPrize(String url, String codigo, String idChallenge,
      String idParticipation, String idUserWinner) async {
    DocumentSnapshot codePrizeUserRef = await firestore
        .collection(Api.path_users)
        .document(idUserWinner)
        .collection(Api.path_participations)
        .document(idParticipation)
        .collection(Api.path_code_win_prize)
        .document(codigo)
        .get();

    if (codePrizeUserRef.exists) {
      //Si existe el documento con este codigo, se entregara el premio

      return firestore
          .collection(Api.path_challenges)
          .document(idChallenge)
          .updateData({"winner.delivery": true});
    } else {
      return Future.error("Codigo Ingresado en Incorrecto");
    }
  }

  Future<QuerySnapshot> getPrizesChallenge(String idChallenge) {
    return firestore
        .collection(Api.path_challenges)
        .document(idChallenge)
        .collection(Api.path_prize_challenges)
        .getDocuments();
  }

  Future<DocumentSnapshot> verificateCode(
      DocumentReference refParticipation, String code) {
    return refParticipation
        .collection(Api.path_code_win_prize)
        .document(code)
        .get();
  }

  Future<void> confirmDelivery(String idChallenge, String idPrize, String value,
      List<String> photoDelivery) {
    DocumentReference prizeRef = firestore
        .collection(Api.path_challenges)
        .document(idChallenge)
        .collection(Api.path_prize_challenges)
        .document(idPrize);
    return prizeRef
        .updateData({"delivery": true, "photosDelivery": photoDelivery});
  }

  Future<ResponseApi> getMinUser(String idUser) async {
    try {
      DocumentSnapshot document = await firestore
          .collection(Api.path_users_search)
          .document(idUser)
          .get();
      return ResponseApi.success(document);
    } catch (e) {
      return ResponseApi.error(e.toString());
    }
  }

  Future<DocumentSnapshot> getInfoUserWinner(
      DocumentReference refParticipationWinner) {
    DocumentReference refUserWinner = refParticipationWinner.parent().parent();
    return refUserWinner.get();
  }

  Future<QuerySnapshot> getSupportParticipations(
      String participationId, String userCreatorId, int limitUserSupports) {
    print("useriD $userCreatorId participationId $participationId");
    return firestore
        .collection(Api.path_users)
        .document(userCreatorId)
        .collection(Api.path_participations)
        .document(participationId)
        .collection(Api.path_users_like)
        .orderBy("nameUser")
        .limit(limitUserSupports)
        .getDocuments();
  }

  Future<QuerySnapshot> getMoreSupportParticipations(
      String participationId, String userCreatorId, int limitUserSupports) {
    return firestore
        .collection(Api.path_users)
        .document(userCreatorId)
        .collection(Api.path_participations)
        .document(participationId)
        .collection(Api.path_users_like)
        .orderBy("nameUser")
        .startAfterDocument(lastSupportUser)
        .limit(limitUserSupports)
        .getDocuments();
  }

  Future<ResponseApi> updatePhotoPerfil(
      String idUser, MItemMultimedia multimedia) async {
    try {
      var batch = firestore.batch();

      batch.setData(firestore.collection(Api.path_users).document(idUser),
          {'urlPhoto': multimedia.item, 'pathThumbail': multimedia.pathOrigin},
          merge: true);
      batch.setData(
          firestore.collection(Api.path_users_search).document(idUser),
          {
            'urlPhoto': multimedia.item,
          },
          merge: true);

      await batch.commit();
      return ResponseApi.success("ok");
    } catch (e) {
      return ResponseApi.error(e.toString());
    }
  }

  Future<ResponseApi> createChallenge(MChallenge challenge) async {
    try {
      var refChallengeFull = firestore
          .collection(Api.path_challenges)
          .document(challenge.idChallenge);
      var refPrizeChallenge =
          refChallengeFull.collection(Api.path_prize_challenges).document();
      var batch = firestore.batch();

      if (challenge.withPrize) {
        batch.setData(refPrizeChallenge, challenge.prize.getPrizeMap());
      }

      batch.setData(refChallengeFull, challenge.getFullMap());
      await batch.commit();
      return ResponseApi.success("ok");
    } catch (e) {
      return ResponseApi.error(e.toString());
    }
  }

  Future<ResponseApi> createVideo(MVideoInfo video) async {
    try {
      await firestore.collection('videos').document().setData({
        'videoUrl': video.videoUrl,
        'thumbUrl': video.thumbUrl,
        'coverUrl': video.coverUrl,
        'aspectRatio': video.aspectRatio,
        'uploadedAt': video.uploadedAt,
        'videoName': video.videoName,
      });
      return ResponseApi.success("ok");
    } catch (e) {
      return ResponseApi.error(e.toString());
    }
  }

  listenToVideos(callback) async {
    Firestore.instance.collection('videos').snapshots().listen((qs) {
      final videos = mapQueryToVideoInfo(qs);
      callback(videos);
    });
  }

  static mapQueryToVideoInfo(QuerySnapshot qs) {
    return qs.documents.map((DocumentSnapshot ds) {
      return MVideoInfo(
        videoUrl: ds.data['videoUrl'],
        thumbUrl: ds.data['thumbUrl'],
        coverUrl: ds.data['coverUrl'],
        aspectRatio: ds.data['aspectRatio'],
        videoName: ds.data['videoName'],
        uploadedAt: ds.data['uploadedAt'],
      );
    }).toList();
  }
}
