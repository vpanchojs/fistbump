import 'package:Darhu/utils/FlavorFlutter.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

import 'api.dart';

class DynamicLinkApi {
  final Api ap;
  final dylinks = FirebaseDynamicLinks.instance;
  static String _link;
  static String _linkPrefix;
  static String _packageName;
  static String _bundleId;
  static int _minVersionAndroid = 4;
  static String _minVersionIos = '0';

  DynamicLinkApi(this.ap) {
    if (FlavorFlutter.debug) {
      _packageName = "site.darhu.challenge.dev";
      _bundleId = "site.darhu.challenge.dev";
      _link="https://darhu.site/";
      _linkPrefix="https://fistbump.page.link";
    } else {
      _packageName = "site.darhu.challenge";
      _bundleId = "site.darhu.challenge";
      _link="https://darhu.site/";
      _linkPrefix="https://darhu.page.link/";
    }
  }

  /*
  Metodo encargado de crear un enlance dinamico para compartir un reto.
  @Param short, indica si se debe acortar el link, default=true 
  @Param descriptionPost, indica la descripcion que se mostrara en las otras plataformas (Facebook, instamgram,twiter etc)
  @Param imageUrlPost, indica la minuatura a visualizar en las otras plataformas(Facebook, instamgram,twiter etc)
  @Param titlePost, indica el titulo a visualizar en las otras plataformas(Facebook, instamgram,twiter etc)
  @Param idChallenge, identificador del reto a compartir
  @return Uri o ShortDynamicLink, segun corresponda
   */
  Future<dynamic> createDynamicLinkShareChallenge(
      {bool short = true,
      String descriptionPost,
      String imageUrlPost,
      String titlePost,
      String idChallenge}) {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: _linkPrefix,
      link: Uri.parse('$_link?challenge=$idChallenge'),
      androidParameters: AndroidParameters(
        packageName: _packageName,
        minimumVersion: _minVersionAndroid,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
          description: descriptionPost,
          imageUrl: Uri.parse(imageUrlPost),
          title: titlePost),
      iosParameters: IosParameters(
        bundleId: _bundleId,
        minimumVersion: _minVersionIos,
      ),
    );

    if (short) {
      return parameters.buildShortLink();
      // url = shortLink.shortUrl;
    } else {
      return parameters.buildUrl();
    }
  }

  /*
  Metodo encargado de crear un enlance dinamico para compartir un Usuario.
  @Param short, indica si se debe acortar el link, default=true 
  @Param descriptionPost, indica la descripcion que se mostrara en las otras plataformas (Facebook, instamgram,twiter etc)
  @Param imageUrlPost, indica la minuatura a visualizar en las otras plataformas(Facebook, instamgram,twiter etc)
  @Param titlePost, indica el titulo a visualizar en las otras plataformas(Facebook, instamgram,twiter etc)
  @Param idUser, identificador del usuario a compartir.
  @return Uri o ShortDynamicLink, segun corresponda
   */
  Future<dynamic> createDynamicLinkShareUser(
      {bool short = true,
      String descriptionPost,
      String imageUrlPost,
      String titlePost,
      String idUser}) {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: _linkPrefix,
      link: Uri.parse('$_link?user=$idUser'),
      androidParameters: AndroidParameters(
        packageName: _packageName,
        minimumVersion: _minVersionAndroid,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
          description: descriptionPost,
          imageUrl: Uri.parse(imageUrlPost),
          title: titlePost),
      iosParameters: IosParameters(
        bundleId: _bundleId,
        minimumVersion: _minVersionIos,
      ),
    );

    if (short) {
      return parameters.buildShortLink();
      // url = shortLink.shortUrl;
    } else {
      return parameters.buildUrl();
    }
  }

/*
  Metodo encargado de crear un enlance dinamico para compartir una participacion en un reto.
  @Param short, indica si se debe acortar el link, default=true 
  @Param descriptionPost, indica la descripcion que se mostrara en las otras plataformas (Facebook, instamgram,twiter etc)
  @Param imageUrlPost, indica la minuatura a visualizar en las otras plataformas(Facebook, instamgram,twiter etc)
  @Param titlePost, indica el titulo a visualizar en las otras plataformas(Facebook, instamgram,twiter etc)
  @Param idChallenge, identificador del reto a compartir.
  @Param idParticipation, identificador de la participacion a compartir.
  @return Uri o ShortDynamicLink, segun corresponda
   */
  Future<dynamic> createDynamicLinkShareParticipationInChallenge(
      {bool short = true,
      String descriptionPost,
      String imageUrlPost,
      String titlePost,
      String idParticipation,
      String idChallenge}) {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: _linkPrefix,
      link: Uri.parse(
          '$_link?participation=$idParticipation&challenge=$idChallenge'),
      androidParameters: AndroidParameters(
        packageName: _packageName,
        minimumVersion: _minVersionAndroid,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
          description: descriptionPost,
          imageUrl: Uri.parse(imageUrlPost),
          title: titlePost),
      iosParameters: IosParameters(
        bundleId: _bundleId,
        minimumVersion: _minVersionIos,
      ),
    );

    if (short) {
      return parameters.buildShortLink();
      // url = shortLink.shortUrl;
    } else {
      return parameters.buildUrl();
    }
  }
}
