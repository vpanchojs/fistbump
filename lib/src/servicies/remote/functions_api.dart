import 'package:Darhu/src/res/strings.dart';
import 'package:Darhu/utils/responses/response_api.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';

class FunctionsApi {
  final Api ap;
  final CloudFunctions _cloudFunctions = CloudFunctions.instance;

  static const String function_update_photo_profile = "updatePhotoProfile";
  static const String function_following_user = "followingUser";
  static const String function_unfollowing_user = "unfollowingUser";
  static const String function_unfollower_user = "unfollowerUser";
  static const String function_create_challenge = "createChallengeFull";
  static const String function_create_participation = "createParticipacion";
  static const String function_delete_participation = "deleteParticipacion";
  static const String function_like_dislike_participation =
      "likeOrDislikeParticipation";

  static const String function_delete_challenge = "deleteChallenge";

  FunctionsApi(this.ap);

  Future followingUser(String idUser, String userSessionId) async {
    //print("seguir $idUser  mi id ${ap.authApi.firebaseUser.uid}");
    Map<String, dynamic> params = Map();
    params["idUser"] = idUser;
    //params["myId"] = ap.authApi.firebaseUser.uid;
    params["myId"] = userSessionId;

    return _cloudFunctions.getHttpsCallable(
        functionName: function_following_user)
      ..call(params);
  }

  Future unfollowingUser(String idUser, String userSessionId) {
    Map<String, dynamic> params = Map();
    params["idUser"] = idUser;
    //params["myId"] = ap.authApi.firebaseUser.uid;
    params["myId"] = userSessionId;

    return _cloudFunctions
        .getHttpsCallable(functionName: function_unfollowing_user)
        .call(params);
  }

  Future unfollowerUser(String idUser, String userSessionId) {
    Map<String, dynamic> params = Map();
    params["idUser"] = idUser;
    params["myId"] = userSessionId;

    return _cloudFunctions
        .getHttpsCallable(functionName: function_unfollower_user)
        .call(params);
  }

  Future<ResponseApi> createParticipation(
      MParticipation p, MChallenge c, MUser u) async {
    try {
      Map<String, dynamic> params = Map();
      params["user"] = u.userMin();
      params["challenge"] = c.minChallengeMap();
      params["participation"] = p.minParticipationMap();

      HttpsCallableResult response = await _cloudFunctions
          .getHttpsCallable(functionName: function_create_participation)
          .call(params);

      if (response.data != null) {
        return ResponseApi.success(response.data);
      } else {
        return ResponseApi.error(string_error_generic);
      }
    } catch (e) {
      if (e.code == 'functionsError') {
        //final String code = e.details['code'];
        final String message = e.details['message'];
        //final dynamic details = e.details['details'];
        return ResponseApi.error(message);
      } else {
        return ResponseApi.error(string_error_generic);
      }
    }
  }

  Future<HttpsCallableResult> deleteParticipation(
      String idParticipation, String idUser, String idChallenge) {
    Map<String, dynamic> params = Map();
    params["idParticipation"] = idParticipation;
    params["idChallenge"] = idChallenge;
    params["idUser"] = idUser;

    return _cloudFunctions
        .getHttpsCallable(functionName: function_delete_participation)
        .call(params);
  }

  Future<HttpsCallableResult> likeOrDislikeParticipation(
      String idParticipation, MUser user, String idUser, String idChallenge) {
    Map<String, dynamic> params = Map();
    params["idParticipation"] = idParticipation;
    params["idUser"] = idUser;
    params["idChallenge"] = idChallenge;
    params["user"] = user.userMin();

    return _cloudFunctions
        .getHttpsCallable(functionName: function_like_dislike_participation)
        .call(params);
  }

  Future deleteChallengeMinAndFull(String idUser, String idChallenge) {
    Map<String, dynamic> params = Map();
    params["idUser"] = idUser;
    params["idChallenge"] = idChallenge;

    return _cloudFunctions
        .getHttpsCallable(functionName: function_delete_challenge)
        .call(params);
  }

  Future<HttpsCallableResult> updatePhotoProfile(String idUser,
      String pathMultimedia, String fileName, String urlMultimedia) async {
    Map<String, dynamic> params = Map();
    params["idUser"] = idUser;
    params["pathMultimedia"] = pathMultimedia;
    params["fileName"] = fileName;
    params["urlMultimedia"] = urlMultimedia;

    return _cloudFunctions
        .getHttpsCallable(functionName: function_update_photo_profile)
        .call(params);
  }
}
