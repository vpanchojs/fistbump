import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:Darhu/src/servicies/remote/api.dart';

class NotificationsApi {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final Api ap;

  NotificationsApi(this.ap);

  Future subscribeTopic(String topic) {
    return _firebaseMessaging.subscribeToTopic(topic);
  }

  Future unSubscribeTopic(String topic) {
    return _firebaseMessaging.unsubscribeFromTopic(topic);
  }

  //Obtiene el token para enviar la notificacion
  Future<String> getToken() {
    print("Entre a obtener Token");
    return _firebaseMessaging.getToken();
  }
}
