import 'dart:io';


import 'package:Darhu/src/servicies/remote/database_api.dart';
import 'package:Darhu/src/servicies/remote/firestore_api.dart';
import 'package:Darhu/src/servicies/remote/notifications_api.dart';
import 'package:Darhu/src/servicies/local/preferences_api.dart';
import 'package:Darhu/src/servicies/remote/remote_config_api.dart';
import 'package:Darhu/src/servicies/remote/storage_api.dart';

import 'auth_api.dart';
import 'dynamic_link_api.dart';
import 'functions_api.dart';

class Api {
  FunctionsApi functionsApi;
  AuthApi authApi;
  StorageApi storageApi;
  NotificationsApi notificationsApi;
  FirestoreApi firestoreApi;
  PreferencesApi preferencesApi;
  DynamicLinkApi dynamicLinkApi;
  RemoteConfigApi remoteConfigApi;
  DatabaseApi databaseApi;

  static const path_users = "users";
  static const path_users_search = "users_search";
  static const path_my_challenge = "challenges";
  static const path_contacts = "contacts";
  static const path_challenges = "challenges";
  static const path_search_challenges = "search_challenges";
  static const path_reports_participation = "participation_reports";
  static const path_reports_challenge = "challenge_reports";

  static const path_participations = "participations_challenge";
  static const path_users_like = "likes";
  static const path_my_notifications = "notifications";
  static const path_my_news = "newsfeed";
  static const path_code_win_prize="prize_code";
  static const path_prize_challenges="prize_challenges";
  static const path_devices_user="devices_user";

  Future<bool> internetConnection() async {
    print("entre a verificar conexion");
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  Api() {
    notificationsApi = NotificationsApi(this);
    functionsApi = FunctionsApi(this);
    authApi = AuthApi(this);
    storageApi = StorageApi(this);
    firestoreApi = FirestoreApi(this);
    preferencesApi = PreferencesApi();
    dynamicLinkApi = DynamicLinkApi(this);
    remoteConfigApi = RemoteConfigApi();
    databaseApi = DatabaseApi();
  }
}

final api = Api();
