import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/utils/responses/response_api.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthApi {
  final Api ap;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser firebaseUser;

  AuthApi(this.ap);

  //Para verificar el numero de telefono
  Future<void> verifyPhoneNumber(
      String phone,
      PhoneVerificationCompleted verificationCompleted,
      PhoneVerificationFailed verificationFailed,
      PhoneCodeSent codeSent,
      PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout,
      int duration) async {
    return _auth.verifyPhoneNumber(
        phoneNumber: phone,
        timeout: Duration(seconds: duration),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future updateFirebaseUser(String nameUser) async {
    var userUpdateInfo = UserUpdateInfo();
    userUpdateInfo.displayName = nameUser;
    FirebaseUser firebaseUser = await _auth.currentUser();
    return firebaseUser.updateProfile(userUpdateInfo);
  }

  //Ingresar a la aplicacion con el codigo enviado al celular
  Future<AuthResult> signIn(String verificacionId, String code) {
    AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: verificacionId, smsCode: code);

    return _auth.signInWithCredential(credential);
  }

  Future<AuthResult> loginCredential(AuthCredential credential) {
    return _auth.signInWithCredential(credential);
  }

  Future<FirebaseUser> inSession() async {
    firebaseUser = await _auth.currentUser();
    return firebaseUser;
  }

  //Cerra sesion
  Future<void> closeSession() {
    firebaseUser = null;
    return _auth.signOut();
  }

  //Actualizar la foto de perfil
  Future<ResponseApi> updatePhotoProfile({String url}) async {
    try{
      var userUpdateInfo = UserUpdateInfo();
      userUpdateInfo.photoUrl = url;
      await firebaseUser.updateProfile(userUpdateInfo);
      return ResponseApi.success("ok");
    }catch(e){
      return ResponseApi.error(e.toString());
    }
  }
    Stream isSession() {
      return _auth.onAuthStateChanged;
    }

}
