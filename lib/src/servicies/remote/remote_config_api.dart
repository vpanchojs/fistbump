import 'package:firebase_remote_config/firebase_remote_config.dart';

class RemoteConfigApi {
  final defaults = <String, dynamic>{'welcome': 'default welcome'};

  RemoteConfigApi() {
    initRemoteConfig();
  }

  initRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(defaults);
    await remoteConfig.fetch(expiration: const Duration(hours: 5));
    await remoteConfig.activateFetched();
    print('welcome message: ' + remoteConfig.getString('welcome'));
  }
}
