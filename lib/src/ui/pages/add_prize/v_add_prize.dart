import 'dart:async';
import 'package:Darhu/src/bloc/add_prize/b_add_prize.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_item_multimedia.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/multimedia/get_multimedia.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class VAddPrize extends StatefulWidget {
  static const routeName = "VAddPrize";
  const VAddPrize({Key key}) : super(key: key);

  @override
  _VAddPrizeState createState() => _VAddPrizeState();
}

class _VAddPrizeState extends State<VAddPrize>
    with AutomaticKeepAliveClientMixin {
  BAddPrize _bAddPrize;
  Timer timerSearch;
  bool isSelectLot = false;
  List<int> optionsLot = new List<int>.generate(10, (int index) => index + 1);
  TextEditingController namePrize = TextEditingController();
  TextEditingController termAndCond = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: appBar(),
      body: ListView(children: <Widget>[
        multimediaChallenge(),
        SizedBox(height: 16),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            children: <Widget>[
              Expanded(child: textLotPrize(), flex: 1),
              SizedBox(width: 8),
              Expanded(child: textNamePrize(), flex: 3)
            ],
          ),
        ),
        SizedBox(height: 8),
        textTermsAndConditionsPrize()
      ]),
    );
  }

  Widget appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: new Text('Premio', style: TextStyle(color: Colors.black)),
      actions: <Widget>[actionButton()],
    );
  }

  Widget actionButton() {
    return StreamBuilder(
      stream: _bAddPrize.outProgressAction,
      initialData: false,
      builder: (context, snapshot) {
        if (snapshot.data) {
          return Align(
            alignment: FractionalOffset.center,
            child: Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.purple),
              ),
            ),
          );
        }
        return button();
      },
    );
  }

  Widget button() {
    return StreamBuilder(
        stream: _bAddPrize.submitValidWithPrize,
        builder: (context, snapshot) {
          print("data name ${snapshot.data}  ${snapshot.hasData}");
          return FlatButton(
            textColor: ColorsRes.light_blue_color,
            child: Text(
              "Guardar",
              style: GoogleFonts.montserrat(
                  fontSize: 16, fontWeight: FontWeight.bold),
            ),
            onPressed: snapshot.hasData ? _bAddPrize.saveTempPrize : null,
          );
        });
  }

  @override
  void initState() {
    super.initState();
    _bAddPrize = BlocProvider.of<BAddPrize>(context);
    _bAddPrize.outView.listen((view) {
      view.run(context: context, key: _scaffoldKey);
    });
  }

  Widget multimediaChallenge() {
    return InkWell(
      child: Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          height: 250,
          decoration: BoxDecoration(
              color: Colors.black38,
              borderRadius: BorderRadius.all(Radius.circular(12.0))),
          child: getContainerMultimedia()),
      onTap: mainBottomSheet,
    );
  }

  Widget textNamePrize() {
    return StreamBuilder(
        stream: _bAddPrize.outNamePrize,
        builder: (context, snapshot) {
          return TextField(
            onChanged: _bAddPrize.inNamePrize,
            keyboardType: TextInputType.text,
            controller: namePrize,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Premio',
              hintText: "Ej: Entradas al cine",
              errorText: snapshot.error,
            ),
            maxLines: 1,
          );
        });
  }

  Widget textLotPrize() {
    return StreamBuilder(
        stream: _bAddPrize.outLotPrize,
        initialData: 1,
        builder: (context, snapshot) {
          return Stack(
            children: <Widget>[
              TextField(
                keyboardType: TextInputType.text,
                controller: TextEditingController()..text = " ",
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Cant.',
                ),
                maxLines: 1,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 3),
                child: FlatButton(
                    onPressed: () {
                      isSelectLot = true;
                      showModalBottomSheet(
                          context: context,
                          builder: (context) => Container(
                                height: 200,
                                child: ListWheelScrollView(
                                    controller: FixedExtentScrollController(
                                        initialItem: snapshot.data - 1),
                                    itemExtent: 40,
                                    useMagnifier: true,
                                    magnification: 1.5,
                                    onSelectedItemChanged: (pos) =>
                                        _onSelectValue(pos),
                                    physics: FixedExtentScrollPhysics(),
                                    children: optionsLot
                                        .map((item) => Text(item.toString(),
                                            textAlign: TextAlign.center))
                                        .toList()),
                              )).whenComplete(() {
                        print("se cerror el modal");
                        isSelectLot = false;
                      });
                    },
                    child: Container(
                        height: 50,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text(snapshot.data.toString(),
                                    textAlign: TextAlign.center)),
                            Icon(Icons.arrow_drop_down)
                          ],
                        ))),
              )
            ],
          );
        });
  }

  Widget textTermsAndConditionsPrize() {
    return StreamBuilder(
        stream: _bAddPrize.outTermsConditions,
        builder: (context, snapshot) {
          return Padding(
            padding: EdgeInsets.only(
              left: 16.0,
              right: 16.0,
            ),
            child: TextField(
              controller: termAndCond,
              onChanged: _bAddPrize.inTermsConditions,
              keyboardType: TextInputType.text,
              inputFormatters: [
                LengthLimitingTextInputFormatter(800),
              ],
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Términos y Condiciones',
                hintText:
                    "Ej: El premio sera entregado el día de la función, 30 minutos antes, llevar cédula...",
                errorText: snapshot.error,
              ),
              maxLines: 5,
            ),
          );
        });
  }

  Widget getContainerMultimedia() {
    return StreamBuilder<MMultimedia>(
        stream: _bAddPrize.outMultimediaPrize,
        initialData: MMultimedia.empty(),
        builder: (context, snapshot) {
          if (snapshot.hasData &&
              snapshot.data.origin != OriginMultimedia.EMPTY) {
            return showMultimediaImage(snapshot.data.thumbnail);
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.camera_alt, color: Colors.white30, size: 100.0),
                Text("Foto del Premio (Opcional)", textAlign: TextAlign.center)
              ],
            );
          }
        });
  }

  showMultimediaImage(MItemMultimedia multimedia) {
    return ClipRRect(
        borderRadius: new BorderRadius.circular(12.0),
        child: Image.file(
          multimedia.item,
          fit: BoxFit.cover,
          width: double.maxFinite,
        ));
  }

  mainBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _createTitle(
                context,
                string_option_camera,
                Icons.photo_camera,
                () =>
                    GetMultimedia.openCameraView(TypeMultimedia.IMAGE, context)
                        .then((response) {
                  if (response.success) {
                    setMultimedia(response.multimedia);
                  } else {
                    _bAddPrize
                        .inView(MActionView.error(message: response.error));
                  }
                }).catchError((error) {
                  _bAddPrize
                      .inView(MActionView.error(message: error.toString()));
                }),
              ),
              _createTitle(
                context,
                string_option_gallery_image,
                Icons.image,
                () => GetMultimedia.actionGetGalleryPhoto().then((response) {
                  if (response.success) {
                    setMultimedia(response.multimedia);
                  } else {
                    _bAddPrize
                        .inView(MActionView.error(message: response.error));
                  }
                }).catchError((error) {
                  _bAddPrize
                      .inView(MActionView.error(message: error.toString()));
                }),
              ),
              _createTitle(context, "Remover foto", Icons.image,
                  () => _bAddPrize.inMultimediaPrize(MMultimedia.empty())),
            ],
          );
        });
  }

  ListTile _createTitle(
      BuildContext context, String name, IconData icon, action) {
    return ListTile(
      leading: Icon(icon),
      title: Text(name),
      onTap: () async {
        Navigator.pop(context);
        action();
      },
    );
  }

  void setMultimedia(MMultimedia multimedia) async {
    _bAddPrize.inMultimediaPrize(multimedia);
  }

  @override
  bool get wantKeepAlive => true;

  void _onSelectValue(int value) {
    _bAddPrize.inLotPrize(value + 1);
    timerSearch?.cancel();
    timerSearch = Timer(Duration(milliseconds: 1000), () {
      if (isSelectLot) Navigator.pop(context);
    });
  }
}
