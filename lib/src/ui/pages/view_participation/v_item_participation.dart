import 'package:Darhu/src/bloc/view_participation/b_view_participation.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/ui/pages/supports_participation/util/arg_support_participation.dart';
import 'package:Darhu/src/ui/pages/supports_participation/v_support_participation.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/date_format.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/utils/share_widgets/share_platforms.dart';
import 'package:Darhu/utils/share_widgets/multimedia/image/thumbnail_action.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class VItemParticipation extends StatefulWidget {
  @override
  VItemParticipationState createState() => new VItemParticipationState();
}

class VItemParticipationState extends State<VItemParticipation> {
  BViewParticipation _viewParticipationBloc;

  @override
  void initState() {
    _viewParticipationBloc = BlocProvider.of<BViewParticipation>(context);
    _viewParticipationBloc.outView.listen((data) {
      data.run(context: context, key: null);
    });
    super.initState();
  }

  void _showDialogDelete() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(string_dialog_delete_participation),
          content: null,
          contentPadding: EdgeInsets.all(0),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(
                string_label_button_cancel,
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(
                string_label_button_delete,
                style: TextStyle(fontSize: 16.0),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                _viewParticipationBloc.deleteParticipation();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _thumbnailWidget() {
    return ThumbnailAction(
      actionTap: null,
      typeMultimedia: _viewParticipationBloc.participation.type,
      urlMultimedia: _viewParticipationBloc.participation.response,
      urlThumbnail: _viewParticipationBloc.participation.thumbnail,
    );
  }

  getContainerText() {
    return Container(
      child: Column(
        children: <Widget>[
          Divider(color: Colors.black38),
          SizedBox(height: 8),
          Text(_viewParticipationBloc.participation.response ?? " es nula",
              style: GoogleFonts.montserrat(fontSize: 16)),
          Divider(color: Colors.black38),
          SizedBox(
            height: 8,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          getContainerUser(),
          _viewParticipationBloc.participation.type == 'text'
              ? getContainerText()
              : _thumbnailWidget(),
          getContainerFooter()
        ],
      ),
    );
  }

  getContainerUser() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListTile(
        onTap: () {},
        contentPadding: EdgeInsets.symmetric(horizontal: 0),
        leading:
            PhotoThumbnail(user: _viewParticipationBloc.participation.user),
        title: Text(
          _viewParticipationBloc.participation.user.nameUser,
          style:
              GoogleFonts.montserrat(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        subtitle: DatePostFull(
            datePost: _viewParticipationBloc.participation.datePost),
        //trailing: canDelete(),
        trailing: actionOptions(),
      ),
    );
  }

  actionShareOtherPlatforms() {
    return StreamBuilder<bool>(
        stream: _viewParticipationBloc.outProgressShare,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return Align(
              alignment: FractionalOffset.center,
              child: Container(
                height: 24,
                margin: EdgeInsets.only(right: 8),
                width: 24,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor:
                      AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
                ),
              ),
            );
          } else {
            return IconButton(
                icon: Icon(Icons.share),
                color: Colors.black,
                onPressed: () => _viewParticipationBloc
                    .shareParticipationPlatforms(_shareOtherPlatforms));
          }
        });
  }

  _shareOtherPlatforms(String url) {
    SharePlatforms.shareOtherPlatformsDynamicLinks(context, url);
  }

  actionOptions() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[canDelete(), actionShareOtherPlatforms()],
    );
  }

  canDelete() {
    return StreamBuilder<bool>(
        stream: _viewParticipationBloc.outProgressAction,
        initialData: false, //Inicia ocultado el progress
        builder: (context, snapshot) {
          if (snapshot.data) {
            return Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.purple),
              ),
            );
          }

          if (_viewParticipationBloc.myParticipation) {
            if (_viewParticipationBloc.challenge.state == 'e') {
              return Icon(
                FontAwesomeIcons.medal,
                color: Colors.black,
              );
            }
            return IconButton(
                icon: Icon(
                  Icons.delete,
                  color: Colors.black,
                ),
                onPressed: _showDialogDelete);
          } else {
            return IconButton(
                icon: Icon(
                  Icons.report,
                  color: Colors.black,
                ),
                onPressed: _showDialogReport);
          }
        });
  }

  getContainerFooter() {
    if (_viewParticipationBloc.challenge.winnerBy == 'random') {
      return Container();
    }
    return Container(
      height: 40,
      margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      alignment: FractionalOffset.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Expanded(
            child: FlatButton(
              onPressed: () {
                _navigationToListSupport(
                    _viewParticipationBloc.participation.idParticipation,
                    _viewParticipationBloc.participation.user.idUser,
                    _viewParticipationBloc.participation.numLikes);
              },
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "${_viewParticipationBloc.participation.numLikes} Apoyos",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: GoogleFonts.montserrat(
                      fontSize: 14, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
          ),
          getActionButton()
        ],
      ),
    );
  }

  _navigationToListSupport(
      String participationId, String userCreatorId, int numSupports) {
    print("participation id $participationId");

    Navigator.pushNamed(context, VSupportParticipation.routeName,
        arguments: ArgSupportParticipation()
          ..numSupports = numSupports
          ..participationId = participationId
          ..userCreatorId = userCreatorId);
    /*
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => PSupportParticipation(participationId,userCreatorId,numSupports)));

     */
  }

  getActionButton() {
    if (_viewParticipationBloc.challenge.state == 'e') {
      return Container();
    }

    if (_viewParticipationBloc.myParticipation ||
        _viewParticipationBloc.myChallenge) {
      return Container();
    } else {
      return buttonSupport();
    }
  }

  buttonSupport() {
    final Widget svg = SvgPicture.asset(
      'images/apoyo_solid_ic.svg',
      height: 24,
      width: 24,
      color: getColor(_viewParticipationBloc.participation.liked),
    );
    return FlatButton.icon(
        onPressed: () => _viewParticipationBloc.likeParticipation(),
        label: Text(
          string_option_support,
          style:
              GoogleFonts.montserrat(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        icon: svg);
  }

  getColor(bool liked) => liked ? ColorsRes.purple_color : Colors.black38;

  void _showDialogReport() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: new Text(string_label_report_participation),
          children: <Widget>[
            SimpleDialogOption(
              child: Text(string_option_report_content_inappropriate),
              onPressed: () => reportParticipation("ci"),
            ),
            SimpleDialogOption(
              child: Text(string_option_report_breach_request),
              onPressed: () => reportParticipation("is"),
            ),
            SimpleDialogOption(
              child: Text(string_option_report_copy_participation),
              onPressed: () => reportParticipation("cp"),
            ),
          ],
        );
      },
    );
  }

  reportParticipation(String code) {
    _viewParticipationBloc.reportParticipation(code);
    Navigator.pop(context);
  }
}
