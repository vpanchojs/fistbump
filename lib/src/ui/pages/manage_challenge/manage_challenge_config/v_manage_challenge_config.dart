import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_config/b_manage_challenge_config.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/selectors/DatePiker.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'widgets/option_item.dart';

class VManageChallengeConfig extends StatefulWidget {
  static const routeName = "vmanagechallengeconfig";
  VManageChallengeConfig({Key key}) : super(key: key);

  @override
  _VManageChallengeConfigState createState() => _VManageChallengeConfigState();
}

class _VManageChallengeConfigState extends State<VManageChallengeConfig> {
  final _scaffKey = new GlobalKey<ScaffoldState>();
  BManageChallengeConfig bloc;
  @override
  void initState() {
    bloc = BlocProvider.of<BManageChallengeConfig>(context);
    bloc.outView.listen((view) {
      view.run(context: context, key: _scaffKey);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffKey,
      appBar: AppBar(
        title: Text("Crear Reto", style: StyleRes.text_appbar),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        actions: <Widget>[
          buttomNextPage(),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: ListView(children: <Widget>[
          SizedBox(height: 8),
          selectWinnerBy(),
          SizedBox(height: 8.0),
          selectParticipationWith(),
          SizedBox(height: 8.0),
          selectDateTime(),
        ]),
      ),
    );
  }

  Widget selectParticipationWith() {
    return StreamBuilder<String>(
        stream: bloc.outParticipants,
        builder: (context, snapshot) {
          return Stack(children: <Widget>[
            TextField(
              enabled: false,
              controller: TextEditingController(text: " "),
              maxLines: 2,
              decoration: InputDecoration(
                  errorText: snapshot.error,
                  border: OutlineInputBorder(),
                  labelText: string_label_participant_with_challenge),
            ),
            Container(
              height: 84,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  OptionItem(
                    assetName: 'images/image_ic.svg',
                    texto: string_option_photo,
                    select: ('image' == snapshot.data ?? ""),
                    onSelected: (val) {
                      bloc.inParticipations('image');
                    },
                  ),
                  OptionItem(
                    assetName: 'images/texto_ic.svg',
                    texto: string_option_text,
                    select: ('text' == snapshot.data ?? ""),
                    onSelected: (val) {
                      bloc.inParticipations('text');
                    },
                  ),
                  OptionItem(
                    assetName: 'images/video_ic.svg',
                    texto: string_option_video,
                    select: ('video' == snapshot.data ?? ""),
                    onSelected: (val) {
                      bloc.inParticipations('video');
                    },
                  ),
                ],
              ),
            ),
          ]);
        });
  }

  Widget selectWinnerBy() {
    return StreamBuilder<String>(
        stream: bloc.outWinnerBy,
        builder: (context, snapshot) {
          return Stack(children: <Widget>[
            TextField(
              enabled: false,
              controller: TextEditingController(text: " "),
              maxLines: 2,
              decoration: InputDecoration(
                  errorText: snapshot.error,
                  border: OutlineInputBorder(),
                  labelText: string_label_winner_by_challenge),
            ),
            Container(
              height: 84,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  OptionItem(
                    assetName: 'images/aleatorio_line.svg',
                    value: 'random',
                    texto: string_option_random,
                    select: ('random' == snapshot.data ?? ""),
                    onSelected: (val) {
                      bloc.inWinerBy('random');
                    },
                  ),
                  OptionItem(
                    assetName: 'images/apoyos_line.svg',
                    select: ('like' == snapshot.data ?? ""),
                    texto: string_option_support,
                    value: 'like',
                    onSelected: (val) {
                      bloc.inWinerBy('like');
                    },
                  ),
                ],
              ),
            ),
          ]);
        });
  }

  Widget selectDateTime() {
    return Stack(children: <Widget>[
      TextField(
        enabled: false,
        maxLines: 1,
        controller: TextEditingController(text: " "),
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: string_label_finish_challenge),
      ),
      Container(
        height: 54,
        margin: EdgeInsets.only(bottom: 16.0),
        padding: EdgeInsets.only(left: 13.0, right: 13.0),
        child: DateTimePicker(
          labelText: ' ',
          //FROM
          firstDate: bloc.firstDate,
          selectedDate: bloc.fromDate,
          selectedTime: bloc.fromTime,
          selectDate: (DateTime date) {
            setState(() {
              bloc.fromDate = date;
            });
          },
          selectTime: (TimeOfDay time) {
            setState(() {
              bloc.fromTime = time;
            });
          },
        ),
      ),
    ]);
  }

  //button
  Widget buttomNextPage() {
    return FlatButton(
      textColor: ColorsRes.light_blue_color,
      child: Text("Siguiente",
          style: GoogleFonts.montserrat(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          )),
      onPressed: bloc.nextPage,
    );
  }
}
