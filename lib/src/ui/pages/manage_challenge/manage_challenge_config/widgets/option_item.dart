import 'package:Darhu/src/res/colors_res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OptionItem extends StatelessWidget {
  final String assetName;
  final double altura;
  final bool select;
  final Color colorSelect;
  final Color colorUnSelect;
  final String texto;
  final dynamic value;
  final ValueChanged<bool> onSelected;

  const OptionItem(
      {Key key,
      @required this.assetName,
      this.altura = 24.0,
      this.select = false,
      this.colorSelect = ColorsRes.purple_color,
      this.colorUnSelect = Colors.black38,
      this.texto,
      this.value,
      this.onSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Widget svg = SvgPicture.asset(
      assetName,
      height: altura,
      width: altura,
      color: getColor(),
    );
    return InkWell(
      borderRadius: BorderRadius.circular(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[svg, getText()],
      ),
      onTap: () {
        onSelected(!select);
      },
    );
  }

  getColor() => select ? colorSelect : colorUnSelect;

  getText() => texto == null
      ? Container()
      : Text(texto, style: TextStyle(color: getColor()));
}
