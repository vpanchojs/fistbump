import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_info/b_manage_challenge_info.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'dart:io';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/utils/share_widgets/multimedia/get_multimedia.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class VManageChallengeInfo extends StatefulWidget {
  static const routeName = "VManageChallengeInfo";

  const VManageChallengeInfo({Key key}) : super(key: key);

  @override
  _VManageChallengeInfoState createState() => _VManageChallengeInfoState();
}

class _VManageChallengeInfoState extends State<VManageChallengeInfo> {
  final _scaffKey = new GlobalKey<ScaffoldState>();
  BManageChallengeInfo bloc;
  @override
  void initState() {
    bloc = BlocProvider.of<BManageChallengeInfo>(context);
    bloc.outView.listen((view) {
      view.run(context: context, key: _scaffKey);
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffKey,
      appBar: AppBar(
        title: Text("Crear Reto", style: StyleRes.text_appbar),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        actions: <Widget>[
          buttomNextPage(),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 8.0),
            multimediaChallenge(),
            SizedBox(height: 16.0),
            textName(),
            SizedBox(height: 8.0),
            textDescription()
          ],
        ),
      ),
    );
  }

  Widget multimediaChallenge() {
    return InkWell(
      child: Container(
          height: 300,
          decoration: BoxDecoration(
              color: Colors.black38,
              borderRadius: BorderRadius.all(Radius.circular(12.0))),
          child: showThumbnailMultimedia()),
      onTap: mainBottomSheet,
    );
  }

  Widget showThumbnailMultimedia() {
    return StreamBuilder<MMultimedia>(
        stream: bloc.outMultimedia,
        initialData: MMultimedia.empty(),
        builder: (context, snapshot) {
          switch (snapshot.data.origin) {
            case OriginMultimedia.LOCAL:
              return showMultimediaImage(snapshot.data.thumbnail.item);
            case OriginMultimedia.REMOTE:
              return showMultimediaImage(snapshot.data.thumbnail.item);
            case OriginMultimedia.EMPTY: // esta opcion esta marcada por defecto
              return Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(
                      Icons.camera_alt,
                      color: Colors.white70,
                      size: 100.0,
                    ),
                    Text(
                        "Foto o Video que ilustre lo que el participante debe realizar",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.montserrat(
                            color: Colors.white, fontSize: 14)),
                  ],
                ),
              );
            default:
              return Container();
          }
        });
  }

  Widget getContainerMultimedia() {
    return StreamBuilder<MMultimedia>(
        stream: bloc.outMultimedia,
        initialData: MMultimedia.empty(),
        builder: (context, snapshot) {
          switch (snapshot.data.origin) {
            case OriginMultimedia.EMPTY:
              return Icon(
                Icons.camera_alt,
                color: Colors.white30,
                size: 100.0,
              );
            case OriginMultimedia.LOCAL:
              return showMultimediaImage(snapshot.data.thumbnail.item);
            case OriginMultimedia.REMOTE:
              return showMultimediaImage(snapshot.data.thumbnail.item);
            default:
              return Container();
          }
        });
  }

  showMultimediaImage(dynamic image) {
    if (image is File) {
      return ClipRRect(
          borderRadius: new BorderRadius.circular(12.0),
          child: Image.file(
            image,
            fit: BoxFit.cover,
            width: double.maxFinite,
          ));
    } else {
      return ClipRRect(
          borderRadius: new BorderRadius.circular(12.0),
          child: Image.file(
            image,
            fit: BoxFit.cover,
            width: double.maxFinite,
          ));
    }
  }

  Widget textName() {
    return StreamBuilder(
        stream: bloc.outName,
        builder: (context, snapshot) {
          return TextField(
            onChanged: bloc.inName,
            keyboardType: TextInputType.text,
            inputFormatters: [
              LengthLimitingTextInputFormatter(50),
            ],
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: string_label_name_challenge,
              hintText: string_hint_name_challenge,
              errorText: snapshot.error,
            ),
            maxLines: 1,
          );
        });
  }

  Widget textDescription() {
    return StreamBuilder(
        stream: bloc.outDescription,
        builder: (context, snapshot) {
          return TextField(
            onChanged: bloc.inDescription,
            keyboardType: TextInputType.text,
            inputFormatters: [
              LengthLimitingTextInputFormatter(500),
            ],
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: string_label_description_challenge,
              hintText: string_hint_description_challenge,
              errorText: snapshot.error,
            ),
            maxLines: 4,
          );
        });
  }

  mainBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _createTitle(
                context,
                string_option_camera,
                Icons.photo_camera,
                () => {
                  GetMultimedia.openCameraView(TypeMultimedia.ALL, context)
                      .then((response) {
                    if (response.success) {
                      bloc.inMultimedia(response.multimedia);
                    } else {
                      bloc.inView(MActionView.error(message: response.error));
                    }
                  }).catchError((e) {
                    bloc.inView(MActionView.error(message: e.toString()));
                  })
                },
              ),
              _createTitle(
                  context,
                  string_option_gallery_image,
                  Icons.image,
                  () => GetMultimedia.actionGetGalleryPhoto().then((response) {
                        if (response.success) {
                          bloc.inMultimedia(response.multimedia);
                        } else {
                          bloc.inView(
                              MActionView.error(message: response.error));
                        }
                      }).catchError((e) {
                        bloc.inView(MActionView.error(message: e.toString()));
                      })),
              _createTitle(
                context,
                string_option_gallery_video,
                Icons.video_library,
                () => GetMultimedia.actionGetGalleryVideo().then((response) {
                  if (response.success) {
                    bloc.inMultimedia(response.multimedia);
                  } else {
                    bloc.inView(MActionView.error(message: response.error));
                  }
                }).catchError((e) {
                  bloc.inView(MActionView.error(message: e.toString()));
                }),
              ),
            ],
          );
        });
  }

  ListTile _createTitle(
      BuildContext context, String name, IconData icon, action) {
    return ListTile(
      leading: Icon(icon),
      title: Text(name),
      onTap: () async {
        Navigator.pop(context);
        action();
      },
    );
  }

  //button
  Widget buttomNextPage() {
    return StreamBuilder<bool>(
        stream: bloc.enableNextButton,
        builder: (context, snapshot) {
          print("Controles de premio activos ${snapshot.data}");
          return FlatButton(
            textColor: ColorsRes.light_blue_color,
            child: Text("Siguiente",
                style: GoogleFonts.montserrat(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                )),
            onPressed: snapshot.hasData ? bloc.nextPage : null,
          );
        });
  }
}
