import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_resume/b_manage_challenge_resume.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_prize.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/src/ui/pages/add_prize/v_add_prize.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/date_format.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class VManageChallengeResume extends StatefulWidget {
  static const routeName = "VManageChallengeResume";
  VManageChallengeResume({Key key}) : super(key: key);

  @override
  _VManageChallengeResumeState createState() => _VManageChallengeResumeState();
}

class _VManageChallengeResumeState extends State<VManageChallengeResume> {
  final _scaffKey = new GlobalKey<ScaffoldState>();
  BManageChallengeResume bloc;

  @override
  void initState() {
    bloc = BlocProvider.of<BManageChallengeResume>(context);
    bloc.outView.listen((view) {
      view.run(context: context, key: _scaffKey);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffKey,
      appBar: AppBar(
        title: Text("Crear Reto", style: StyleRes.text_appbar),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        actions: <Widget>[
          actionButton(),
        ],
      ),
      body: body(),
    );
  }

  body() {
    return StreamBuilder(
      stream: bloc.outProgressAction,
      initialData: false,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data) {
          return containerProgressCreateChallenge();
        } else {
          return containerResumeChallenge();
          //return containerProgressCreateChallenge();
        }
      },
    );
  }

  containerResumeChallenge() {
    return ListView(children: <Widget>[
      SizedBox(height: 8.0),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: infoChallenge()),
      SizedBox(height: 8.0),
      dividerSection("Agregar Premio"),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: canDeliveryPrize()),
    ]);
  }

  dividerSection(String title) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      color: Colors.black12,
      height: 40,
      child: Align(child: Text(title), alignment: Alignment.centerLeft),
    );
  }

  infoChallenge() {
    return StreamBuilder<MChallenge>(
        stream: bloc.outChallenge,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    containerImage(
                        snapshot.data.multimedia.thumbnail.item, 100),
                    SizedBox(width: 8),
                    Expanded(
                      child: Container(
                        height: 100,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Text(snapshot.data.name,
                                style: GoogleFonts.montserrat(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                            Text(
                              snapshot.data.description,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 5,
                              style: GoogleFonts.montserrat(fontSize: 14),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 16),
                Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            getTitleSection("Participa con"),
                            SizedBox(height: 8),
                            iconParticipant(snapshot.data.typeParticipation),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            getTitleSection("Gana por"),
                            SizedBox(height: 8),
                            iconWinnerBy(snapshot.data.winnerBy),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 10),
                getTitleSection("Finaliza el"),
                SizedBox(height: 8),
                getSubtitle(snapshot.data.dateFinish),
              ],
            );
          } else {
            return Container();
          }
        });
  }

  containerImage(dynamic image, double size) {
    if (image is String) {
      return ClipRRect(
        borderRadius: new BorderRadius.circular(12.0),
        child: CachedNetworkImage(
          imageUrl: image,
          placeholder: (context, url) {
            return Container(
              height: size,
              width: size,
            );
          },
          width: size,
          height: size,
          fit: BoxFit.cover,
          alignment: FractionalOffset.center,
          errorWidget: (context, url, error) {
            return Container(
              height: size,
              width: size,
              child: Center(
                  child: Icon(
                Icons.error,
                color: Colors.black38,
              )),
            );
          },
        ),
      );
    }
    return Container(
      height: size,
      width: size,
      child: ClipRRect(
          borderRadius: new BorderRadius.circular(12.0),
          child: Image.file(
            image,
            fit: BoxFit.cover,
            width: double.maxFinite,
          )),
    );
  }

  /// Se encarga de mostrar u ocultar los controles para activar o desactivar la
  /// entrega de premios con el reto.
  Widget canDeliveryPrize() {
    return StreamBuilder<bool>(
        initialData: false,
        stream: bloc.outCanDeliveryPrize,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return activePrize();
          } else {
            return ListTile(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 0, vertical: 4),
              title: Text("Entregar Premio"),
              leading: Icon(Icons.verified_user, size: 50, color: Colors.green),
              subtitle:
                  Text("Solo usuarios verificados pueden configurar un premio"),
            );
          }
        });
  }

  Widget activePrize() {
    return StreamBuilder<Prize>(
        stream: bloc.outPrize,
        initialData: Prize(),
        builder: (context, snapshot) {
          if (snapshot.data.name == null) {
            return ListTile(
              contentPadding: const EdgeInsets.symmetric(horizontal: 0),
              leading: Icon(Icons.card_membership, size: 50),
              title: Text("Premio"),
              subtitle: Text("Presione para agregar un premio"),
              onTap: navigationAddPrize,
            );
          }
          return ListTile(
            contentPadding: const EdgeInsets.symmetric(horizontal: 0),
            leading: (snapshot.data.multimedia != null &&
                    snapshot.data.multimedia.origin != OriginMultimedia.EMPTY)
                ? containerImage(snapshot.data.multimedia.thumbnail.item, 50)
                : Icon(Icons.card_giftcard, size: 50),
            title: Text(snapshot.data.name),
            subtitle: Text(snapshot.data.termsConditions,
                overflow: TextOverflow.ellipsis, maxLines: 2),
            onTap: navigationAddPrize,
            trailing: IconButton(
                icon: Icon(Icons.delete, color: Colors.black),
                onPressed: bloc.removePrize),
          );
        });
  }

  navigationAddPrize() async {
    var result = await Navigator.pushNamed(context, VAddPrize.routeName);
    if (result != null) {
      if (result is Prize) {
        print("la foto que viene es ${result.urlMultimedia}");
        bloc.inPrize(result);
        bloc.inWithPrize(true);
      }
    }
  }

  Widget iconParticipant(String typeParticipation) {
    String assetName = "";
    String descripcion = "";

    switch (typeParticipation) {
      case 'image':
        assetName = 'images/image_ic.svg';
        descripcion = "Foto";
        break;
      case 'video':
        assetName = 'images/video_ic.svg';
        descripcion = "Video";
        break;
      case 'text':
        assetName = 'images/texto_ic.svg';
        descripcion = "Texto";
        break;
    }

    return Row(
      children: <Widget>[
        SvgPicture.asset(
          assetName,
          height: 24,
          color: ColorsRes.purple_color,
        ),
        SizedBox(width: 8),
        Text(descripcion, style: GoogleFonts.montserrat(fontSize: 14))
      ],
    );
  }

  Widget iconWinnerBy(String typeParticipation) {
    String assetName = "";
    String description = "";

    switch (typeParticipation) {
      case 'like':
        assetName = 'images/apoyos_line.svg';
        description = "Apoyos";
        break;
      case 'random':
        assetName = 'images/aleatorio_line.svg';
        description = "Aleatorio";
        break;
    }

    return Row(
      children: <Widget>[
        SvgPicture.asset(
          assetName,
          height: 24,
          color: ColorsRes.purple_color,
        ),
        SizedBox(width: 8),
        Text(description, style: GoogleFonts.montserrat(fontSize: 14))
      ],
    );
  }

  getSubtitle(DateTime data) {
    return DatePostFull(datePost: data, style: TextStyle(fontSize: 14));
  }

  getTitleSection(String data) {
    return Text(
      data,
      style: GoogleFonts.montserrat(fontSize: 14, fontWeight: FontWeight.bold),
      textAlign: TextAlign.start,
    );
  }

  Widget actionButton() {
    return StreamBuilder(
      stream: bloc.outProgressAction,
      initialData: false,
      builder: (context, snapshot) {
        if (snapshot.data) {
          FocusScope.of(context).requestFocus(FocusNode());
          return Align(
            alignment: FractionalOffset.center,
            child: Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor:
                    AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
              ),
            ),
          );
        }
        return buttonDone();
      },
    );
  }

  Widget buttonDone() {
    return FlatButton(
        textColor: ColorsRes.light_blue_color,
        child: Text(
          "Publicar",
          style:
              GoogleFonts.montserrat(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        onPressed: bloc.createOrUpdateChallenge);
  }

  containerProgressCreateChallenge() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(24),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
          SizedBox(height: 16),
          Text(
            "Publicando Reto",
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
                fontSize: 32, fontWeight: FontWeight.bold),
          ),
          Text(
            "Este proceso puede tardar unos minutos",
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
                fontSize: 18, fontWeight: FontWeight.normal),
          )
        ],
      ),
    );
  }
}
