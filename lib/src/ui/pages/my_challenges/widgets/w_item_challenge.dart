import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/utils/share_widgets/share_platforms.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class WItemChallenge extends StatefulWidget {
  final MChallenge challenge;
  final dynamic delete;
  final dynamic share;

  const WItemChallenge(this.challenge, this.delete, this.share);

  @override
  WItemChallengeState createState() => new WItemChallengeState();
}

class WItemChallengeState extends State<WItemChallenge> {
  //ApplicationBloc applicationBloc;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _goViewChallengePage,
      onLongPress: mainBottomSheet,
      child: Container(
          height: 125,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  color: (widget.challenge.state == "a")
                      ? Colors.green[900]
                      : Colors.red[900],
                  width: 15,
                ),
                SizedBox(width: 8),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 4),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(widget.challenge.name,
                            overflow: TextOverflow.ellipsis,
                            softWrap: false,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                                fontSize: 18)),
                        Text(widget.challenge.description,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            softWrap: false,
                            textAlign: TextAlign.left),
                        SizedBox(height: 16),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            iconParticipant(widget.challenge.typeParticipation),
                            SizedBox(width: 8),
                            iconWinnerBy(widget.challenge.winnerBy),
                            SizedBox(width: 8),
                            iconTime(widget.challenge.state),
                            SizedBox(width: 8),
                            iconWithPrize(widget.challenge.withPrize),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 16),
                Card(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    elevation: 0,
                    child: CachedNetworkImage(
                      imageUrl: widget.challenge.urlThumbnail,
                      placeholder: (context, url) {
                        return Container(
                          height: 125,
                          width: 125,
                        );
                      },
                      width: 125,
                      height: 125,
                      fit: BoxFit.cover,
                      alignment: FractionalOffset.center,
                      errorWidget: (context, url, error) {
                        return Container(
                          height: 125,
                          width: 125,
                          child: Center(
                              child: Icon(
                            Icons.error,
                            color: Colors.black38,
                          )),
                        );
                      },
                    ))
              ],
            ),
          )),
    );
  }

  Widget iconParticipant(String typeParticipation) {
    String assetName = "";

    switch (typeParticipation) {
      case 'image':
        assetName = 'images/image_ic.svg';
        break;
      case 'video':
        assetName = 'images/video_ic.svg';
        break;
      case 'text':
        assetName = 'images/texto_ic.svg';
        break;
    }

    return SvgPicture.asset(
      assetName,
      height: 20,
      color: Colors.black38,
    );
  }

  Widget iconWinnerBy(String typeParticipation) {
    String assetName = "";

    switch (typeParticipation) {
      case 'like':
        assetName = 'images/apoyos_line.svg';
        break;
      case 'random':
        assetName = 'images/aleatorio_line.svg';
        break;
    }

    return SvgPicture.asset(
      assetName,
      height: 20,
      color: Colors.black38,
    );
  }

  mainBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _createTitle(
                  context, "Eliminar Reto", Icons.delete, _confirmationDelete),
              _createTitle(
                  context, "Compartir Reto", Icons.share, shareOtherPlatforms),
            ],
          );
        });
  }

  ListTile _createTitle(
      BuildContext context, String name, IconData icon, Function action) {
    return ListTile(
      leading: Icon(icon),
      title: Text(name),
      onTap: () {
        Navigator.pop(context);
        action();
      },
    );
  }

  void _goViewChallengePage() async {
    Navigator.pushNamed(context, VViewChallenge.routeName,
        arguments: ArgViewChallenge()..challenge = widget.challenge);

    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new PViewChallenge(
            challenge: widget.challenge,
          ),
        ));

     */
  }

  Future<void> _confirmationDelete() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Eliminar Reto'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[Text('Esta seguro de eliminar el reto')],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Eliminar'),
              onPressed: () {
                Navigator.of(context).pop();
                widget.delete(widget.challenge);
              },
            ),
          ],
        );
      },
    );
  }

  shareOtherPlatforms() {
    widget.share(widget.challenge, share);
  }

  share(String url) {
    SharePlatforms.shareOtherPlatformsDynamicLinks(context, url);
  }

  @override
  void initState() {
    // applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();
  }

  iconWithPrize(bool withPrize) {
    if (withPrize) {
      return Icon(Icons.card_membership, size: 22, color: Colors.black38);
    } else {
      return Container();
    }
  }

  iconTime(String state) {
    if (state == 'a') {
      return Icon(Icons.timelapse, size: 22, color: Colors.black38);
    } else {
      if (state == 'e')
        return Icon(Icons.timer_off, size: 22, color: Colors.black38);
    }
  }
}
