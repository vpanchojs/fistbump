import 'package:Darhu/src/bloc/my_challenges/b_my_challenges.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_info/v_manage_challenge_info.dart';
import 'package:Darhu/src/ui/pages/my_challenges/widgets/w_item_challenge.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/list/ListGenericWidget.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class VMyChallenges extends StatefulWidget {
  @override
  MyChallengesPageState createState() => MyChallengesPageState();
}

class MyChallengesPageState extends State<VMyChallenges>
    with AutomaticKeepAliveClientMixin<VMyChallenges> {
  BMyChallenges _challengeBloc;
  final scaffKey = new GlobalKey<ScaffoldState>();
  final scrollController = new ScrollController();
  final Widget svg = SvgPicture.asset(
    'images/info_none.svg',
    height: 200,
  );

  @override
  void initState() {
    _challengeBloc = BlocProvider.of<BMyChallenges>(context);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        _challengeBloc.getMoreMyChallenges();
      }
    });

    _challengeBloc.viewCtrl.listen((view) {
      view.run(context: context, key: scaffKey);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      key: scaffKey,
      appBar: AppBar(
        title: Text("Mis Retos", style: StyleRes.text_appbar),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        /*actions: <Widget>[
          IconButton(
            icon: Icon(Icons.history),
            onPressed: () {},
          )
        ],*/
      ),
      body: getContainerMyDares(),
      floatingActionButton: Container(
          height: 40,
          child: FloatingActionButton.extended(
            label: Text("Nuevo reto"),
            onPressed: _goToNewChallengeScreen,
            tooltip: string_tooltip_fab_challenge,
            icon: Icon(Icons.add),
          )),
    );
  }

  Future<void> _refresh() async {
    return _challengeBloc.getMyChallenges();
  }

  setDataStream(MChallenge challenge) {
    return WItemChallenge(challenge, _challengeBloc.deleteChallenge,
        _challengeBloc.shareChallenge);
  }

  getContainerMyDares() {
    return Container(
      child: ListGenericWidget(
        refresh: _refresh,
        stream: _challengeBloc.outMyChallenge,
        progress: _challengeBloc.outProgressMore,
        scrollController: scrollController,
        noneData: """Sin retos 
Animate a retar a tus amigos""",
        itemWidget: setDataStream,
      ),
    );
  }

  _goToNewChallengeScreen() {
    Navigator.pushNamed(context, VManageChallengeInfo.routeName).then((res) {
      _challengeBloc.verifiedNewChallenge();
    });
  }

  @override
  bool get wantKeepAlive => true;
}
