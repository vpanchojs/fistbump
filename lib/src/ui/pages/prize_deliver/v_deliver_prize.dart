import 'package:Darhu/src/bloc/prize_deliver/b_prize_deliver.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/multimedia/get_multimedia.dart';
import 'package:Darhu/utils/utils.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class VDeliverPrize extends StatefulWidget {
  static const routeName="VDeliverPrize";
  @override
  _VDeliverPrizeState createState() => _VDeliverPrizeState();
}

class _VDeliverPrizeState extends State<VDeliverPrize> {
  BPrizeDeliver _bPrizeDelivery;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _bPrizeDelivery = BlocProvider.of<BPrizeDeliver>(context);
    _bPrizeDelivery.outView.listen((view) {
      view.run(context: context, key: _scaffoldKey);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: appBar(),
        body: ListView(
          children: <Widget>[
            multimediaChallenge(context),
            SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                children: <Widget>[
                  Expanded(child: buttonLotPrize(), flex: 1),
                  SizedBox(width: 8),
                  Expanded(child: textCodePrize(), flex: 6)
                ],
              ),
            )
          ],
        ));
  }

  Widget actionButton() {
    return StreamBuilder(
      stream: _bPrizeDelivery.outProgressAction,
      initialData: false,
      builder: (context, snapshot) {
        if (snapshot.data) {
          return Align(
            alignment: FractionalOffset.center,
            child: Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor:
                    AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
              ),
            ),
          );
        }
        return button();
      },
    );
  }

  Widget button() {
    return StreamBuilder(
        stream: _bPrizeDelivery.submitValid,
        builder: (context, snapshot) {
          return FlatButton(
            child: Text("Entregar"),
            onPressed: (snapshot.hasData)
                ? _bPrizeDelivery.confirmDeliveryPrize
                : null,
          );
        });
  }

  Widget appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: new Text("Entregar Premio", style: TextStyle(color: Colors.black)),
      actions: <Widget>[actionButton()],
    );
  }

  Widget multimediaChallenge(BuildContext context) {
    return InkWell(
      child: Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          height: 250,
          decoration: BoxDecoration(
              color: Colors.black38,
              borderRadius: BorderRadius.all(Radius.circular(12.0))),
          child: getContainerMultimedia()),
      onTap: () => mainBottomSheet(context),
    );
  }

  Widget textCodePrize() {
    return StreamBuilder(
        stream: _bPrizeDelivery.outCode,
        initialData: "",
        builder: (context, snapshot) {
          return TextField(
            onChanged: _bPrizeDelivery.inCode,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Codigo',
              hintText: "Ej: AC4D8834",
              errorText: snapshot.error,
            ),
            maxLines: 1,
          );
        });
  }

  Widget buttonLotPrize() {
    return IconButton(
        icon: Icon(FontAwesomeIcons.qrcode),
        onPressed: () => GetMultimedia.actionReadQr(context).then((code) {
              print("el codigo obtenido es el siguiente $code");
              _bPrizeDelivery.inCode(code);
            }).catchError((error) {
              print("Error al leer el codigo $error");
            }));
  }

  Widget getContainerMultimedia() {
    return StreamBuilder<MMultimedia>(
        stream: _bPrizeDelivery.outMultimedia,
        initialData: MMultimedia.empty(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data.data.item != TypeMultimedia.EMPTY) {
            return showMultimediaImage(snapshot.data);
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.camera_alt, color: Colors.white30, size: 100.0),
                Text("Foto de entrega del premio (Opcional)",
                    textAlign: TextAlign.center)
              ],
            );
          }
        });
  }

  showMultimediaImage(MMultimedia multimedia) {
    return ClipRRect(
        borderRadius: new BorderRadius.circular(12.0),
        child: Image.file(
          multimedia.data.item,
          fit: BoxFit.cover,
          width: double.maxFinite,
        ));
  }

  mainBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _createTitle(
                context,
                string_option_camera,
                Icons.photo_camera,
                () =>
                    GetMultimedia.openCameraView(TypeMultimedia.IMAGE, context)
                        .then((response) {
                  if (response.success) {
                    setMultimedia(response.multimedia);
                  } else {
                    _bPrizeDelivery
                        .inView(MActionView.error(message: response.error));
                  }
                }).catchError((error) {
                  _bPrizeDelivery
                      .inView(MActionView.error(message: error.toString()));
                }),
              ),
              _createTitle(
                context,
                string_option_gallery_image,
                Icons.image,
                () => GetMultimedia.actionGetGalleryPhoto().then((values) {
                  //setMultimedia(values[0], values[1]);
                  print(values);
                  //print("path ${values[1]}");
                }).catchError((error) {
                  if (error != "")
                    Utils.showSnackBar(error.toString(), _scaffoldKey);
                }),
              ),
              _createTitle(context, "Remover foto", Icons.image,
                  () => setMultimedia(MMultimedia.empty())),
            ],
          );
        });
  }

  ListTile _createTitle(
      BuildContext context, String name, IconData icon, action) {
    return ListTile(
      leading: Icon(icon),
      title: Text(name),
      onTap: () async {
        Navigator.pop(context);
        action();
      },
    );
  }

  void setMultimedia(MMultimedia multimedia) async {
    _bPrizeDelivery.inMultimedia(multimedia);
  }
}
