import 'package:Darhu/src/models/m_notification.dart';
import 'package:Darhu/src/bloc/notifications/b_notifications.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/share_widgets/list/ListGenericWidget.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'widgets/w_notificacion_new_challenge.dart';
import 'widgets/w_notificacion_participation.dart';

class VNotifications extends StatefulWidget {
  static const routeName="vnotifications";
  @override
  VNotificationsState createState() {
    return new VNotificationsState();
  }
}

class VNotificationsState extends State<VNotifications>
    with AutomaticKeepAliveClientMixin<VNotifications> {
  final scrollController = new ScrollController();
  BNotifications _notificationsBloc;

  @override
  void initState() {
    _notificationsBloc = BlocProvider.of<BNotifications>(context);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        print("se llego al final debo traer mas datos");
        _notificationsBloc.getMoreNotifications();
      }
    });

    _notificationsBloc.outView.listen((view) {
      view.run(context: context, key: null);      
    });

    super.initState();
  }

  Future<void> refreshData() {
    return _notificationsBloc.getNotifications();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    setDataNotificationsStream(MNotification item) {
      switch (item.type) {
        case 0: //Nuevo reto
          return WNotificationNewChallenge(
              notification: item, meId: _notificationsBloc.user.idUser);
        case 1: //Participacion
          return WNotificationParticipation(
              notification: item, meId: _notificationsBloc.user.idUser);
      }
      return Container();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Notificaciones", style: StyleRes.text_appbar),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
      ),
      body: ListGenericWidget(
        refresh: refreshData,
        stream: _notificationsBloc.outNotifications,
        progress: _notificationsBloc.outProgressMore,
        scrollController: scrollController,
        itemWidget: setDataNotificationsStream,
        noneData: "Sin notificaciones",
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
