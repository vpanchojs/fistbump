import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/models/m_notification.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/utils/share_widgets/item_notification/description_notification.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:flutter/material.dart';

class WNotificationParticipation extends StatelessWidget {
  final MNotification notification;
  final String meId;

  WNotificationParticipation({Key key, this.notification, this.meId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: InkWell(
          child: ListTile(
        title: getDescripcion(),
        subtitle:
            DatePostNotification(datePost: notification.datePost),
        leading: PhotoThumbnail(user: notification.user),
        onTap: () => _navigationParticipationInChallenge(context),
      )),
    );
  }

  getDescripcion() {
    if (notification.challenge.state == "e") {
      if (notification.participation.winner &&
          notification.user.idUser == meId) {
        return DescriptionNotification(
            nameUser: notification.user.nameUser,
            action: notification.challenge.name,
            description: 'eres el ganador del reto ');
      } else {
        return DescriptionNotification(
            nameUser: notification.user.nameUser,
            action: notification.challenge.name,
            description: 'es el ganador del reto ');
      }
    } else {
      return DescriptionNotification(
          nameUser: notification.user.nameUser,
          action: notification.challenge.name,
          description: 'esta participando en el reto ');
    }
  }

  void _navigationParticipationInChallenge(BuildContext context) {
    Navigator.pushNamed(context, VViewChallenge.routeName,arguments: ArgViewChallenge()..challenge= notification.challenge..participation=notification.participation);

    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PViewChallenge(
            challenge: notification.challenge,
            participation: notification.participation,
          ),
        ));

     */
  }
}
