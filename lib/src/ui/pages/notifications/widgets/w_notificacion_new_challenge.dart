import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/models/m_notification.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/utils/share_widgets/item_notification/description_notification.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:flutter/material.dart';

class WNotificationNewChallenge extends StatelessWidget {
  final MNotification notification;
  final String meId;

  WNotificationNewChallenge({Key key, this.notification, this.meId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: InkWell(
          child: ListTile(
        title: getDescription(),
        subtitle: DatePostNotification(datePost: notification.datePost),
        leading: PhotoThumbnail(user: notification.user),
        onTap: () => _navigationChallenge(context),
      )),
    );
  }

  getDescription() {
    if (notification.challenge.state == "r") {
     return DescriptionNotification(
          nameUser: notification.user.nameUser,
          action: notification.challenge.name,
          description: 'ha eliminado el reto');
    }
    return DescriptionNotification(
        nameUser: notification.user.nameUser,
        action: notification.challenge.name,
        description: 'ha creado el reto');
  }

  void _navigationChallenge(BuildContext context) {
    Navigator.pushNamed(context, VViewChallenge.routeName,arguments: ArgViewChallenge()..challenge=notification.challenge);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PViewChallenge(
            challenge: notification.challenge,
          ),
        ));

     */
  }
}
