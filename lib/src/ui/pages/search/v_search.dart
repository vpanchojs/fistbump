import 'package:Darhu/src/bloc/search/b_search.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

import 'widgets/w_list_users.dart';

class SearchResultsPage extends StatefulWidget {
  @override
  _SearchResultsPageState createState() => _SearchResultsPageState();
}

class _SearchResultsPageState extends State<SearchResultsPage> {
  BSearch searchResultBloc;

  @override
  void initState() {
    searchResultBloc= BlocProvider.of<BSearch>(context);
    searchResultBloc.getUsersByQuery();
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return WListUsers(searchResultBloc: searchResultBloc);

    /*return DefaultTabController(
      length: 2,
      child: Column(
        children: <Widget>[
          TabBar(tabs: [
            Tab(icon: Icon(Icons.people)),
            Tab(icon: Icon(Icons.event_note)),
          ]),
          Expanded(
              child: TabBarView(children: [
            PageUsers(searchResultBloc: searchResultBloc),
            PageChallenges(searchResultBloc: searchResultBloc),
          ])),
        ],
      ),
      initialIndex: 0,
    );*/
  }

}
