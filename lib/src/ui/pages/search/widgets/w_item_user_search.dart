import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/ui/pages/view_profile/v_view_profile.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:flutter/material.dart';

class WItemUserSearch extends StatelessWidget {
  final MUser user;

  const WItemUserSearch({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.blue,
      child: _contact(context),
    );
  }

  Widget _contact(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.all(0),
      title: Text(
        user.nameUser??"Nombre de Usuario",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Text(
        user.name ?? "Nombre Completo",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      leading: PhotoThumbnail(user: user, size: 45),
      onTap: () {
        Navigator.pushNamed(context, VViewProfile.routeName,arguments: user);
        /*
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => PViewProfile(
                      user: user,
                    )));

         */
      },
    );
  }
}
