import 'package:Darhu/utils/share_widgets/list/ListGenericWidget.dart';
import 'package:Darhu/src/bloc/search/b_search.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:flutter/material.dart';

import 'w_item_user_search.dart';

class WListUsers extends StatefulWidget {
  final BSearch searchResultBloc;

  const WListUsers({Key key, this.searchResultBloc}) : super(key: key);

  @override
  WListUsersState createState() => WListUsersState();
}

class WListUsersState extends State<WListUsers> {

  Future<void> _refresData() async {
    return widget.searchResultBloc.getUsersByQuery();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(16.0),
      child: ListGenericWidget(
        progress: widget.searchResultBloc.outProgressMore,
        stream: widget.searchResultBloc.outUsersList,
        refresh:_refresData,
        itemWidget: setItems,
        noneData: "Sin Resultados",
        scrollController: ScrollController(),
      ),
    );
  }

  setItems(MUser user) {
    return WItemUserSearch(user: user);
  }
}
