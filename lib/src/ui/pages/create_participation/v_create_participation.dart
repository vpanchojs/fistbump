import 'dart:io';

import 'package:Darhu/src/bloc/creation_participation/b_create_participation.dart';
import 'package:Darhu/src/models/m_action_view.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:Darhu/src/ui/share_widgets/w_input_text_stream.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/multimedia/get_multimedia.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class VCreateParticipation extends StatefulWidget {
  static const routeName = "VCreateParticipation";
  VCreateParticipation({Key key}) : super(key: key);

  @override
  _VCreateParticipationState createState() => _VCreateParticipationState();
}

class _VCreateParticipationState extends State<VCreateParticipation> {
  final GlobalKey<ScaffoldState> _scaffKey = new GlobalKey<ScaffoldState>();
  BCreateParticipation bloc;

  @override
  void initState() {
    bloc = BlocProvider.of<BCreateParticipation>(context);
    bloc.initActionView(context: context, key: _scaffKey);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffKey,
      appBar: appBar(),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: streamBody(),
      ),
    );
  }

  streamBody() {
    return StreamBuilder(
      stream: bloc.outProgressAction,
      initialData: false,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data) {
          return containerProgressCreateParticipation();
        }
        return body();
      },
    );
  }

  body() {
    switch (bloc.typeParticipation) {
      case TypeParticipation.IMAGE:
        return containerImage();
      case TypeParticipation.VIDEO:
        return containerVideo();
      case TypeParticipation.TEXT:
        return containerText();
    }
  }

  containerText() {
    return WInputTextStream(
      inData: bloc.inText,
      outData: bloc.outText,
      label: "Participacion",
      hint: "Escribe lo solicitado en el reto",
    );
  }

  containerVideo() {
    return multimediaParticipation(
        "Video realizando lo solicitado por el retador");
  }

  containerImage() {
    return multimediaParticipation(
        "Imagen realizando lo solicitado por el retador");
  }

  Widget appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: new Text('Participar', style: TextStyle(color: Colors.black)),
      actions: <Widget>[actionButton()],
    );
  }

  Widget multimediaParticipation(String instruction) {
    return InkWell(
      child: Container(
          height: 300,
          width: double.maxFinite,
          decoration: BoxDecoration(
              color: Colors.black38,
              borderRadius: BorderRadius.all(Radius.circular(12.0))),
          child: showThumbnailMultimedia(instruction)),
      onTap: mainBottomSheet,
    );
  }

  Widget showThumbnailMultimedia(String instruccion) {
    return StreamBuilder<MMultimedia>(
        stream: bloc.outMultimedia,
        initialData: MMultimedia.empty(),
        builder: (context, snapshot) {
          switch (snapshot.data.origin) {
            case OriginMultimedia.LOCAL:
              return showMultimediaImage(snapshot.data.thumbnail.item);
            case OriginMultimedia.REMOTE:
              return showMultimediaImage(snapshot.data.thumbnail.item);
            case OriginMultimedia.EMPTY: // esta opcion esta marcada por defecto
              return Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(
                      Icons.camera_alt,
                      color: Colors.white70,
                      size: 100.0,
                    ),
                    Text(instruccion,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.montserrat(
                            color: Colors.white, fontSize: 14)),
                  ],
                ),
              );
            default:
              return Container();
          }
        });
  }

  Widget getContainerMultimedia() {
    return StreamBuilder<MMultimedia>(
        stream: bloc.outMultimedia,
        initialData: MMultimedia.empty(),
        builder: (context, snapshot) {
          switch (snapshot.data.origin) {
            case OriginMultimedia.EMPTY:
              return Icon(
                Icons.camera_alt,
                color: Colors.white30,
                size: 100.0,
              );
            case OriginMultimedia.LOCAL:
              return showMultimediaImage(snapshot.data.thumbnail.item);
            case OriginMultimedia.REMOTE:
              return showMultimediaImage(snapshot.data.thumbnail.item);
            default:
              return Container();
          }
        });
  }

  showMultimediaImage(dynamic image) {
    if (image is File) {
      return ClipRRect(
          borderRadius: new BorderRadius.circular(12.0),
          child: Image.file(
            image,
            fit: BoxFit.cover,
            width: double.maxFinite,
          ));
    } else {
      return ClipRRect(
          borderRadius: new BorderRadius.circular(12.0),
          child: Image.file(
            image,
            fit: BoxFit.cover,
            width: double.maxFinite,
          ));
    }
  }

  Widget actionButton() {
    return StreamBuilder(
      stream: bloc.outProgressAction,
      initialData: false,
      builder: (context, snapshot) {
        if (snapshot.data) {
          return Align(
            alignment: FractionalOffset.center,
            child: Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.purple),
              ),
            ),
          );
        }
        return button();
      },
    );
  }

  Widget button() {
    return StreamBuilder(
        stream: bloc.outActive,
        initialData: false,
        builder: (context, snapshot) {
          return FlatButton(
            textColor: ColorsRes.light_blue_color,
            child: Text(
              "Publicar",
              style: GoogleFonts.montserrat(
                  fontSize: 16, fontWeight: FontWeight.bold),
            ),
            onPressed: snapshot.data ? bloc.createParticipation : null,
          );
        });
  }

  mainBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _createTitle(
                context,
                string_option_camera,
                Icons.photo_camera,
                () => {
                  GetMultimedia.openCameraView(
                          bloc.typeParticipation == TypeParticipation.VIDEO
                              ? TypeMultimedia.VIDEO
                              : TypeMultimedia.IMAGE,
                          context)
                      .then((response) {
                    if (response.success) {
                      bloc.inMultimedia(response.multimedia);
                    } else {
                      bloc.inView(MActionView.error(message: response.error));
                    }
                  }).catchError((e) {
                    bloc.inView(MActionView.error(message: e.toString()));
                  })
                },
              ),
              bloc.typeParticipation == TypeParticipation.IMAGE
                  ? _createTitle(
                      context,
                      string_option_gallery_image,
                      Icons.image,
                      () => GetMultimedia.actionGetGalleryPhoto()
                              .then((response) {
                            if (response.success) {
                              bloc.inMultimedia(response.multimedia);
                            } else {
                              bloc.inView(
                                  MActionView.error(message: response.error));
                            }
                          }).catchError((e) {
                            bloc.inView(
                                MActionView.error(message: e.toString()));
                          }))
                  : Container(),
              bloc.typeParticipation == TypeParticipation.VIDEO
                  ? _createTitle(
                      context,
                      string_option_gallery_video,
                      Icons.video_library,
                      () => GetMultimedia.actionGetGalleryVideo()
                          .then((response) {
                        if (response.success) {
                          bloc.inMultimedia(response.multimedia);
                        } else {
                          bloc.inView(
                              MActionView.error(message: response.error));
                        }
                      }).catchError((e) {
                        bloc.inView(MActionView.error(message: e.toString()));
                      }),
                    )
                  : Container(),
            ],
          );
        });
  }

  ListTile _createTitle(
      BuildContext context, String name, IconData icon, action) {
    return ListTile(
      leading: Icon(icon),
      title: Text(name),
      onTap: () async {
        Navigator.pop(context);
        action();
      },
    );
  }

  containerProgressCreateParticipation() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(24),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
          SizedBox(height: 16),
          Text(
            "Publicando tu participación",
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
                fontSize: 32, fontWeight: FontWeight.bold),
          ),
          Text(
            "Este proceso puede tardar unos minutos",
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
                fontSize: 18, fontWeight: FontWeight.normal),
          )
        ],
      ),
    );
  }
}
