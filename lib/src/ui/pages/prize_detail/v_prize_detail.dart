import 'package:Darhu/src/bloc/prize_detail/b_prize_detail.dart';
import 'package:Darhu/src/models/m_prize.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/ui/pages/claim_prize/util/arg_claim_prize.dart';
import 'package:Darhu/src/ui/pages/claim_prize/v_claim_prize.dart';
import 'package:Darhu/src/ui/pages/prize_deliver/util/arg_deliver_prize.dart';
import 'package:Darhu/src/ui/pages/prize_deliver/v_deliver_prize.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/enums/enums.dart';
import 'package:Darhu/utils/share_widgets/button/button_reload.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/utils/share_widgets/multimedia/image/thumbnail_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import 'widgets/w_user_winner_dialog.dart';

class VPrizeDetail extends StatefulWidget {
  static const routeName = "VPrizeDetail";

  @override
  _VPrizeDetailState createState() => _VPrizeDetailState();
}

class _VPrizeDetailState extends State<VPrizeDetail> {
  BPrizeDetail _bprizeDetail;

  @override
  void initState() {
    super.initState();
    _bprizeDetail = BlocProvider.of<BPrizeDetail>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: appBar(), body: bodyStream());
  }

  Widget appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: new Text("Premio", style: TextStyle(color: Colors.black)),
    );
  }

  Widget bodyStream() {
    return StreamBuilder<Prize>(
        stream: _bprizeDetail.outPrizes,
        builder: (context, snapshot) {
          if (snapshot.hasError) return simpleHasError();

          if (snapshot.connectionState == ConnectionState.waiting)
            return simpleWaiting();

          if (!snapshot.hasData) return simpleNoneData();

          return Stack(children: <Widget>[
            ListView(
              children: <Widget>[
                ThumbnailAction(
                    urlMultimedia: snapshot.data.urlMultimedia,
                    typeMultimedia: "image",
                    urlThumbnail: snapshot.data.urlThumbnail),
                SizedBox(height: 8),
                infoWinnerUser(snapshot.data),
                ListTile(
                  title: Text(
                    "${snapshot.data.lot} ${snapshot.data.name}",
                    style: GoogleFonts.montserrat(
                        fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(
                    snapshot.data.termsConditions,
                    style: GoogleFonts.montserrat(fontSize: 16),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                  child: getButtonActionTypeUser(
                      context,
                      snapshot.data.winnerCode.refParticipation,
                      snapshot.data.idPrize,
                      snapshot.data.delivery),
                  alignment: Alignment.bottomCenter),
            )
          ]);
        });
  }

  infoWinnerUser(Prize prize) {
    if (_bprizeDetail.stateChallenge == "e" &&
        prize.winnerCode.refParticipation != null) {
      return ListTile(
          title: Text("Ver usuario ganador"),
          onTap: () {
            _bprizeDetail.getWinnerUser(prize);
            showDialog(
              context: context,
              builder: (BuildContext context) => UserWinnerDialog(
                bloc: _bprizeDetail,
                title: "Ganador",
                buttonText: "Listo",
              ),
            );
          },
          trailing: Icon(Icons.chevron_right));
    } else {
      return Container();
    }
  }

  simpleHasError() {
    return Container(
        height: 200,
        width: double.infinity,
        child: ButtonReload(refresh: () {}));
  }

  simpleWaiting() {
    return Container(
      height: 200,
      width: double.infinity,
      child: Align(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  simpleNoneData() {
    final Widget svg = SvgPicture.asset(
      'images/info_none.svg',
      height: 150,
    );
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        svg,
        SizedBox(height: 4.0),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("Sin Premios",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black26, fontSize: 16.0)),
        )
      ],
    );
  }

  getButtonActionTypeUser(BuildContext context, dynamic refParticipation,
      String idPrize, bool delivery) {
    if (_bprizeDetail.stateChallenge == 'e') {
      if (delivery) {
        return FlatButton(
            onPressed: () {},
            child: Text("PREMIO ENTREGADO",
                style: TextStyle(
                    color: Colors.blue[900],
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.center));
      }
      switch (_bprizeDetail.userChallenge) {
        case USER_CHALLENGE
            .CREATOR: //Se muestra el boton al creador del reto, "Entregar premio"
          return FloatingActionButton.extended(
            onPressed: () =>
                navigationToDeliveryPrize(context, refParticipation, idPrize),
            label: Text("Entregar Premio"),
          );
        case USER_CHALLENGE
            .PARTICIPANT: //No se muestra el boton, a los usuarios participantes que no sean ganadores ni creadores.
          return Container();
        case USER_CHALLENGE
            .WINNER: //Se muestra el boton al ganador del reto, "Reclamar Premio"
          return FloatingActionButton.extended(
            onPressed: () =>
                navigationToGetPrize(context, refParticipation, idPrize),
            label: Text("Reclamar Premio"),
          );
      }
    } else {
      return Container();
    }

    //Existen 3 escenarios
    //1. Se muestra el boton al ganador del reto, "Reclamar Premio"
    //2. Se muestra el boton al creador del reto, "Entregar premio"
    //3. No se muestra el boton, a los usuarios participantes que no sean ganadores ni creadores.
  }

  navigationToDeliveryPrize(
      BuildContext context, dynamic refParticipation, String idPrize) async {
    /*
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => PPrizeDeliver(idChallenge: _bprizeDetail.idChallenge,refParticipation: refParticipation,idPrize: idPrize),
      ),
    );

     */
    Navigator.pushNamed(context, VDeliverPrize.routeName,
        arguments: ArgDeliverPrize()
          ..idChallenge = _bprizeDetail.idChallenge
          ..refParticipation = refParticipation
          ..idPrize = idPrize);
  }

  navigationToGetPrize(
      BuildContext context, dynamic refParticipation, String idPrize) {
    /*
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => PClaimPrize(idChallenge: _bprizeDetail.idChallenge,refParticipation: refParticipation,idPrize: idPrize),
      ),
    );
     */
    Navigator.pushNamed(context, VClaimPrize.routeName,
        arguments: ArgClaimPrize()
          ..idChallenge = _bprizeDetail.idChallenge
          ..refParticipation = refParticipation
          ..idPrize = idPrize);
  }

  ///Se visualizara la informacion del usuario ganador
  winnerUser(MUser user) {
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 0),
      leading: PhotoThumbnail(user: user, size: 50),
      title: Text(user.nameUser ?? "Nombre Usuario.."),
      subtitle: Text(user.name ?? "Nombre Real..."),
      trailing: moreInfoUser(user),
    );
  }

  moreInfoUser(MUser user) {
    if (_bprizeDetail.userChallenge == USER_CHALLENGE.CREATOR) {
      return IconButton(
        icon: Icon(Icons.info),
        onPressed: () {},
      );
    } else
      return Icon(Icons.info_outline);
  }
}
