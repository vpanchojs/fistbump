import 'package:Darhu/src/bloc/prize_detail/b_prize_detail.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/utils/share_widgets/button/button_reload.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class UserWinnerDialog extends StatelessWidget {
  final String title,buttonText;
  final BPrizeDetail bloc;

  final Image image;

  UserWinnerDialog({
    @required this.title,    
    @required this.buttonText,
    this.image, this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return StreamBuilder<Object>(
      stream: bloc.outUser,
      builder: (context, snapshot) {
        if (snapshot.hasError) return simpleHasError();

        if (snapshot.connectionState == ConnectionState.waiting)
          return simpleWaiting();

        if (!snapshot.hasData)
          return simpleNoneData();
        return bodyStream(context,snapshot.data);
      }
    );
  }


  bodyStream(BuildContext context,MUser user){
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: Consts.avatarRadius + Consts.padding,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          margin: EdgeInsets.only(top: Consts.avatarRadius),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Consts.padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              Text(
                user.nameUser,
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                title,
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(height: 16.0),
              FlatButton.icon(onPressed: (){
                Clipboard.setData(ClipboardData(text: user.cellPhone));
                Scaffold.of(context).showSnackBar(SnackBar(content: Text("Número copiado")));
              }, icon: Icon(Icons.phone), label: Text(user.cellPhone)),
              SizedBox(height: 24.0),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(); // To close the dialog
                  },
                  child: Text(buttonText),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: Consts.padding,
          right: Consts.padding,
          child: CircleAvatar(
            backgroundColor: ColorsRes.purple_color,
            radius: Consts.avatarRadius,
            child: FaIcon(FontAwesomeIcons.award,size: 60,color: Colors.white,),
          ),
        ),
      ],
    );
  }


  simpleHasError() {
    return Container(
        height: 200,
        color: Colors.white,
        width: double.infinity,
        child: ButtonReload(refresh: (){}));
  }

  simpleWaiting() {
    return Container(
      height: 200,
      color: Colors.white,
      width: double.infinity,
      child: Align(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  simpleNoneData(){
    final Widget svg = SvgPicture.asset(
      'images/info_none.svg',
      height: 150,
    );
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          svg,
          SizedBox(height: 4.0),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Sin Premios",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black26, fontSize: 16.0)),
          )
        ],
      ),
    );
  }


}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}