import 'dart:async';

import 'package:Darhu/src/ui/pages/init_section/v_init_section.dart';
import 'package:Darhu/src/bloc/login/b_login.dart';
import 'package:Darhu/src/ui/pages/login/widgets/w_enter_number_phone.dart';
import 'package:Darhu/src/ui/pages/login/widgets/w_validate_code.dart';
import 'package:Darhu/src/ui/pages/manage_profile/v_manage_profile.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/enums/navigation_login.dart';
import 'package:flutter/material.dart';

import 'widgets/w_init.dart';
import 'widgets/w_splash_screen.dart';

class VLogin extends StatefulWidget {
  static const routeName="vlogin";
  @override
  VLoginState createState() {
    return new VLoginState();
  }
}

class VLoginState extends State<VLogin> with SingleTickerProviderStateMixin {
  BLogin signInBloc;
  StreamSubscription streamSubscription;

  @override
  void initState() {
    signInBloc = BlocProvider.of<BLogin>(context);
    navigationToScreen();
    super.initState();
  }

  @override
  void dispose() {
    streamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: signInBloc.outSession,
        builder: (context, snapshot) {
          print("login stream ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            print("entro al splash");
            return SplashScreen();
          }

          if (snapshot.hasData) {
            switch (snapshot.data) {
              case StateSession.INIT:
                return InitPage(signInBloc: signInBloc);
              case StateSession.VALIDATE_CODE:
                return ValidateCode(signInBloc: signInBloc);
              case StateSession.ENTER_NUMBER:
                return EnterNumberPhone(signInBloc: signInBloc);
              default:
                return InitPage(signInBloc: signInBloc);
            }
          }else{
            return InitPage(signInBloc: signInBloc);
          }

        });
  }



  void navigationToScreen() {
    streamSubscription = signInBloc.outNavegation.listen((data) {
      switch (data) {
        case StateLogin.HOME:
          Navigator.pushNamedAndRemoveUntil(context, VInitSection.routeName, ModalRoute.withName(""));
          /*
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => VInitSection()),
              ModalRoute.withName(""));*/
          break;
        case StateLogin.EDIT_USER:
          Navigator.pushNamedAndRemoveUntil(context, VManageProfile.routeName, ModalRoute.withName(""));
          /*
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => PManageProfile()),
              ModalRoute.withName(""));*/
          break;
      }
    });
  }
}
