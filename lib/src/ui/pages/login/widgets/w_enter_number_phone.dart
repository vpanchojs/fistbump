import 'package:Darhu/src/bloc/login/b_login.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/utils/validations/validate_fields.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class EnterNumberPhone extends StatefulWidget {
  final BLogin signInBloc;

  const EnterNumberPhone({Key key, this.signInBloc}) : super(key: key);

  @override
  EnterNumberPhoneState createState() {
    return new EnterNumberPhoneState();
  }
}

class EnterNumberPhoneState extends State<EnterNumberPhone> {
  static final GlobalKey<ScaffoldState> _scaffKey =
      new GlobalKey<ScaffoldState>();

  final formEnterNumber = new GlobalKey<FormState>();
  final _celularController = TextEditingController();

//=============================================== MÉTODOS ========================

  @override
  void initState() {
    super.initState();
    widget.signInBloc.outView.listen((view) {
      view.run(context: context, key: _scaffKey);
    });
  }

  void enviarNumeroCelular() {
    final form = formEnterNumber.currentState;
    if (form.validate()) {
      widget.signInBloc.enviar("+593" + _celularController.text);
    }
  }

  //****************** VALIDACIONES **************************
  String validatePhone(String phone) {
    return ValidateFields.validatePhone(phone);
  }

//================================================================= VISTA ===============================
  Widget actionButton() {
    return StreamBuilder(
      stream: widget.signInBloc.outProgressAction,
      initialData: false,
      builder: (context, snapshot) {
        if (snapshot.data) {
          return Align(
            alignment: FractionalOffset.center,
            child: Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor:
                    AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
              ),
            ),
          );
        }
        return Container(width: double.infinity, height: 45, child: button());
      },
    );
  }

  Widget button() {
    return StreamBuilder(builder: (context, snapshot) {
      return RaisedButton(
        onPressed: enviarNumeroCelular,
        child: Text(
          string_label_button_send_sms,
          // style: TextStyle(color: Colors.white),
          style: GoogleFonts.montserrat(
              color: Colors.white, fontWeight: FontWeight.w600),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        color: ColorsRes.purple_color,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        return true;
      },
      child: Scaffold(
          key: _scaffKey,
          appBar: AppBar(
            title: Text(
              string_title_page_in_number,
              style: GoogleFonts.montserrat(
                color: Colors.black,
                fontWeight: FontWeight.w600,
              ),
            ),
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            leading: Container(),
          ),
          body: Form(
            key: formEnterNumber,
            child: Container(
              margin: EdgeInsets.all(16),
              child: Column(children: <Widget>[
                Row(
                  children: <Widget>[
                    RaisedButton(onPressed: null, child: Text("+593")),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                      child: Container(
                        child: TextFormField(
                          keyboardType: TextInputType.phone,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.all(12),
                            hintText: string_hint_in_number_phone,
                          ),
                          controller: _celularController,
                          validator: validatePhone,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Text(string_message_information_phone_validation,
                      textAlign: TextAlign.justify,                     
                      style: GoogleFonts.montserrat(fontSize: 16)),
                ),
                actionButton(),
              ]),
            ),
          )),
    );
  }
}
