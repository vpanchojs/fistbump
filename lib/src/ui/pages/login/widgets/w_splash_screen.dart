import 'package:flutter/material.dart';

/// Se encargan de mostrar un splashScreen, ademas verifica la sesion de usuario
class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image(image: AssetImage("images/ic_splash.png"),height: 120,width: 120,),
          Align(alignment: Alignment.center, child: SizedBox( height:24, width:24,child: CircularProgressIndicator(strokeWidth: 2,)))
        ],
      ),
    );
  }
}
