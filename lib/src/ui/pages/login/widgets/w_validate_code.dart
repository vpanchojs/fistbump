import 'package:Darhu/src/bloc/login/b_login.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/utils/enums/navigation_login.dart';
import 'package:Darhu/utils/validations/validate_fields.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ValidateCode extends StatefulWidget {
  final BLogin signInBloc;

  const ValidateCode({Key key, this.signInBloc}) : super(key: key);

  @override
  ValidateCodeState createState() {
    return new ValidateCodeState();
  }
}

class ValidateCodeState extends State<ValidateCode> {
  final _codigoController = TextEditingController();

  //Para mostrar mensajes de error
  static final GlobalKey<ScaffoldState> _sKey = new GlobalKey<ScaffoldState>();
  final formEnterCode = new GlobalKey<FormState>();

  //=============================================== MÉTODOS ========================
  @override
  void initState() {
    widget.signInBloc.outView.listen((view) {
      view.run(context: context, key: _sKey);
    });

    /*widget.signInBloc.outViewErrorCode.listen((data) {
      Utils.showSnackBar(data, _sKey);
    });*/
    super.initState();
  }

  validarCodigo() {
    final form = formEnterCode.currentState;
    if (form.validate()) {
      widget.signInBloc.verificarCodigo(_codigoController.text);
    }
  }

  @override
  void dispose() {
    super.dispose();
  } //****************** VALIDACIONES **************************

//Valida el codigo enviado al msj
  String validateCode(String code) {
    return ValidateFields.validateCode(code);
  }

  //================================================================= VISTA ===============================

  Widget actionButton() {
    return StreamBuilder(
      stream: widget.signInBloc.outProgressAction,
      initialData: false,
      builder: (context, snapshot) {
        //print("se ejecuta ${snapshot.data}");
        if (snapshot.data) {
          return Align(
            alignment: FractionalOffset.center,
            child: Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
            ),
          );
        }
        return Container(width: double.infinity, height: 45, child: button());
      },
    );
  }

  Widget button() {
    return StreamBuilder(builder: (context, snapshot) {
      return RaisedButton(
        onPressed: validarCodigo,
        child: Text(
          string_label_button_validate,
          style: GoogleFonts.montserrat(
              color: Colors.white, fontWeight: FontWeight.w600),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        color: ColorsRes.purple_color,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _sKey,
        appBar: AppBar(
          title: Text(
            string_title_page_validate_code,
            style: GoogleFonts.montserrat(
              color: Colors.black,
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          leading: Container(),
        ),
        body: Form(
          key: formEnterCode,
          child: Container(
              margin: EdgeInsets.all(16),
              child: Column(children: <Widget>[
                new TextFormField(
                  keyboardType: TextInputType.phone,
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.all(12),
                    hintText: string_hint_in_code,
                  ),
                  controller: _codigoController,
                  validator: validateCode,
                  //maxLength: 10,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Text(
                    "El código de verificación fue enviado al buzon de mensajes del número ${widget.signInBloc.phone}",
                    textAlign: TextAlign.justify,
                    style: GoogleFonts.montserrat(fontSize: 16)
                  ),
                ),
                StreamBuilder<int>(
                    stream: widget.signInBloc.outCount,
                    initialData: widget.signInBloc.duration,
                    // a Stream<int> or null
                    builder:
                        (BuildContext context, AsyncSnapshot<int> snapshot) {
                      if (snapshot.data > 0) {
                        return Text(
                            "${snapshot.data} / ${widget.signInBloc.duration}");
                      } else {
                        return FlatButton(
                            onPressed: () {
                              widget.signInBloc
                                  .inSession(StateSession.ENTER_NUMBER);
                            },
                            child: Text(string_label_button_reintentar));
                      }
                    }),
                SizedBox(height: 8),
                actionButton(),
              ])),
        ));
  }
}
