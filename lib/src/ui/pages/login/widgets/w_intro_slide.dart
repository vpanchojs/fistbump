import 'package:Darhu/src/res/colors_res.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class IntroSlide extends StatelessWidget {
  final Widget assetName;
  final String title;
  final String description;

  const IntroSlide({Key key, this.title, this.description, this.assetName})
      : super(key: key);

  //= 'images/starts_slide.svg';
  @override
  Widget build(BuildContext context) {
    /*  final Widget svg = SvgPicture.asset(
      assetName,
      height: 200,
    );*/
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          assetName,
          SizedBox(height: 24),
          Container(
            child: Text(title,
                textAlign: TextAlign.center,
                style: GoogleFonts.viga(
                    fontSize: 48, color: ColorsRes.light_blue_color)),
          ),
          SizedBox(height: 12),
          Text(description,
              textAlign: TextAlign.center,              
              style: GoogleFonts.montserrat(
                  fontSize: 20,
                  color: ColorsRes.purple_color,
                  fontWeight: FontWeight.w300))
        ],
      ),
    );
  }
}
