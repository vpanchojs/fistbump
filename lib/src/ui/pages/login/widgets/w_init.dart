import 'package:Darhu/src/bloc/login/b_login.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/utils/enums/navigation_login.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'w_intro_slide.dart';

class InitPage extends StatefulWidget {
  final BLogin signInBloc;

  const InitPage({Key key, this.signInBloc}) : super(key: key);

  @override
  _InitPageState createState() => _InitPageState();
}

class _InitPageState extends State<InitPage>
    with
        SingleTickerProviderStateMixin,
        AutomaticKeepAliveClientMixin<InitPage> {
  TabController _tabController;
  Widget doneImage;
  Widget arrowImage;
  Widget startsImage;
  double heightImage=200;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: pageViewSlide(),
      bottomNavigationBar: footer(),
    );
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
    doneImage = SvgPicture.asset(
      'images/done_slide.svg',
      height: heightImage,
    );
    arrowImage = SvgPicture.asset(
      'images/arrow_slide.svg',
      height: heightImage,
    );
    startsImage = SvgPicture.asset(
      'images/starts_slide.svg',
      height: heightImage,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _tabController?.dispose();
  }

  Widget pageViewSlide() {
    return TabBarView(
      controller: _tabController,
      children: <Widget>[
        IntroSlide(
          assetName: doneImage,
          title: string_title_accept_challenge,
          description: string_description_accept_challenge,
        ),
        IntroSlide(
          assetName: arrowImage,
          title: string_title_create_challenge,
          description: string_description_create_challenge,
        ),
        IntroSlide(
          assetName: startsImage,
          title: string_title_win_prize,
          description: string_description_win_prize,
        ),
      ],
    );
  }

  footer() {
    return Container(
      decoration: BoxDecoration(
          border: Border.fromBorderSide(BorderSide(color: Colors.black12))),
      height: 50,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
              child:
                  Center(child: TabPageSelector(controller: _tabController))),
          Expanded(
              child: Container(
                  margin: const EdgeInsets.symmetric(vertical: 8),
                  child: actionButton()))
        ],
      ),
    );
  }

  actionButton() {
    return FlatButton(
      onPressed: () {
        widget.signInBloc.inSession(StateSession.ENTER_NUMBER);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text(
            string_label_button_sing_in,
            style: TextStyle(color: ColorsRes.purple_color),
          ),
          Icon(
            Icons.arrow_forward_ios,
            color: ColorsRes.purple_color,
            size: 16,
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
