import 'package:Darhu/src/bloc/claim_prize/b_claim_prize.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class VClaimPrize extends StatefulWidget {
  static const routeName="VClaimPrize";

  @override
  _VClaimPrizeState createState() => _VClaimPrizeState();
}

class _VClaimPrizeState extends State<VClaimPrize> {
  BClaimPrize _getPrize;

  @override
  void initState() {
    _getPrize = BlocProvider.of<BClaimPrize>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body:body()
    );
  }

  Widget body() {
    return StreamBuilder<Object>(
        stream: _getPrize.outCode,
        initialData: '',
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting)
            return Center(child: CircularProgressIndicator());
          if(snapshot.hasError){
            return Container(child: Text("Problemas al obtener el codigo"),);
          }
          if(snapshot.hasData && snapshot.data!=''){
            return ListView(
              children: <Widget>[qrCode(snapshot.data), textCode(snapshot.data)],
            );

          }else{
            return Container();
          }
        });
  }

  Widget qrCode(String code){
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: QrImage(
          data: code,
          version: QrVersions.auto,
          size: 250.0,
        ),
      ),
    );
  }

  Widget appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: new Text("Reclamar Premio", style: TextStyle(color: Colors.black)),
    );
  }

  textCode(String code) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: double.maxFinite,
          child: Stack(
            children: <Widget>[
              TextField(
                keyboardType: TextInputType.text,
                controller: TextEditingController()..text = " ",
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Codigo',
                ),
                maxLines: 1,
              ),
              Center(
                  child: FlatButton(
                      onPressed: null,
                      child: Text(code, style: TextStyle(fontSize: 14))))
            ],
          ),
        ),
      ),
    );
  }
}
