import 'package:Darhu/src/ui/pages/view_profile/v_view_profile.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:flutter/material.dart';

class WItemUserPopulate extends StatelessWidget {
  final MUser user;
  final double photoSize;
  final double margin;
  final double fontSize;

  const WItemUserPopulate(
      {Key key,
      @required this.user,
      @required this.photoSize,
      @required this.margin,
      @required this.fontSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        margin: EdgeInsets.all(margin),
        child: InkWell(
          onTap: () => _navigationToProfile(context),
          child: Column(
            children: <Widget>[
              PhotoThumbnail(user: user,size: 45),
              SizedBox(height: 8.0),
              Text(
                user.nameUser,
                overflow: TextOverflow.ellipsis,
                softWrap: false,
                style: StyleRes.text_populate_name_user,
              ),
            ],
          ),
        ),
      ),
    );
  }

  _navigationToProfile(BuildContext context) {
    Navigator.pushNamed(context, VViewProfile.routeName,arguments: user);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => PViewProfile(
                  user: user,
                )));

     */
  }
}
