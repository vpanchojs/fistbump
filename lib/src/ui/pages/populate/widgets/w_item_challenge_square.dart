import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WItemChallengeSquare extends StatelessWidget {
  final MChallenge challenge;

  const WItemChallengeSquare(this.challenge);

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      elevation: 3,
      child: InkWell(
        onTap: () => _navigationToChallengePage(context),
        child: Stack(
          children: <Widget>[
            _getImageContainer(),
            _getContainerNameChallenge()
          ],
        ),
      ),
    );
  }

  _getImageContainer() {
    return CachedNetworkImage(
      imageUrl: challenge.urlThumbnail,
      placeholder: (context, url) {
        return Container();
      },
      width: double.infinity,
      height: double.infinity,
      fit: BoxFit.cover,
      alignment: FractionalOffset.center,
      errorWidget: (context, url, error) {
        return new Center(
            child: Icon(
          Icons.error,
          color: Colors.black38,
        ));
      },
    );
  }

  _getContainerNameChallenge() {
    return Align(
      child: Container(
        width: double.infinity,
        color: Colors.black54,
        padding: EdgeInsets.all(4.0),
        child: Text(challenge.name,
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            //style: new TextStyle(fontSize: 13.0, color: Colors.white),
            style: GoogleFonts.montserrat(
                fontSize: 14,
                color: Colors.white,
                fontWeight: FontWeight.bold)),
      ),
      alignment: FractionalOffset.bottomLeft,
    );
  }

  _navigationToChallengePage(BuildContext context) async {
    Navigator.pushNamed(context, VViewChallenge.routeName,
        arguments: ArgViewChallenge()..challenge = challenge);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new PViewChallenge(
                challenge: challenge,
              ),
        ));

     */
  }
}
