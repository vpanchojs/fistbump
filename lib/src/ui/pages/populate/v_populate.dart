import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/bloc/populate/b_populate.dart';
import 'package:Darhu/src/ui/pages/populate/widgets/w_item_challenge_square.dart';
import 'package:Darhu/src/ui/pages/populate/widgets/w_item_user_populate.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/src/bloc/search/p_search.dart';
import 'package:Darhu/utils/share_widgets/list/GridSliverGenericWidget.dart';
import 'package:Darhu/utils/share_widgets/list/ListHorizontalGenericWigdet.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VPopulate extends StatefulWidget {
  static const routeName= "vpopulate";
  @override
  VPopulateState createState() => VPopulateState();
}

class VPopulateState extends State<VPopulate>
    with AutomaticKeepAliveClientMixin<VPopulate> {
  final scrollController = new ScrollController();
  BPopulate _populateBloc;
  final String path = 'images/mydares_ic.svg';

  @override
  void initState() {
    _populateBloc = BlocProvider.of<BPopulate>(context);

    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        _populateBloc.getMoreChallengePopulate();
      }
    });
    _populateBloc.outView.listen((data) {
      data.run(context:context,key: null);
    });
    super.initState();
  }

  Future<void> _refresh() async {
    await _populateBloc.getChallengePopulate();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(string_title_page_populate, style: StyleRes.text_appbar),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: _showSearchPage)
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: CustomScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          controller: scrollController,
          slivers: <Widget>[
            userPopulateListHorizontal(),
            challengePopulateGrid(),
            progressSliver()
          ],
        ),
      ),
    );
  }

  userPopulateListHorizontal() {
    return SliverToBoxAdapter(
        child: ListHorizontalGenericWidget(
      stream: _populateBloc.outUsers,
      items: setDataUsersPopulateStream,
      heightList: 100, //Se debe cambiar el valor, por algo mas dinamico
      noneData: string_message_none_users_populate,
    ));
  }

  challengePopulateGrid() {
    return GridSliverGenericWidget(
      noneData: string_message_none_challenges_populate,
      stream: _populateBloc.outChallenge,
      refresh: _refresh,
      items: setDataChallengeStream,
    );
  }

  setDataChallengeStream(List<MChallenge> list) {
    return list
        .map<Widget>((challenge) => WItemChallengeSquare(challenge))
        .toList();
  }

  setDataUsersPopulateStream(List<MUser> list) {
    return list
        .map<Widget>((user) => WItemUserPopulate(
            user: user, fontSize: 10, photoSize: 40, margin: 8))
        .toList();
  }

  @override
  bool get wantKeepAlive => true;

  progressSliver() {
    return StreamBuilder<bool>(
        stream: _populateBloc.outProgressMore,
        initialData: false,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          return SliverToBoxAdapter(
            child: Align(
              alignment: Alignment.center,
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 8),
                height: 24,
                width: 24,
                child: (snapshot.data)
                    ? CircularProgressIndicator(strokeWidth: 2)
                    : Container(),
              ),
            ),
          );
        });
  }

  void _showSearchPage() {
    showSearch(
        context: context,
        delegate: PSearch(
            hinText: string_hint_search_user));
  }
}
