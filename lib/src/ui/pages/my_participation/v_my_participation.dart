import 'package:Darhu/src/bloc/view_participation/p_view_participation.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

import '../../../bloc/my_participation/b_my_participation.dart';

class VMyParticipation extends StatefulWidget {
  @override
  _VMyParticipationState createState() =>
      _VMyParticipationState();
}

class _VMyParticipationState extends State<VMyParticipation> {
  BMyParticipation _myParticipationBloc;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MParticipation>(
        stream: _myParticipationBloc.outMyParticipation,
        builder: (context, snapshot) {
          if (snapshot.hasError) return Container();

          if (ConnectionState.waiting == snapshot.connectionState) {
            return Container();
          } else {
            if (snapshot.data.idParticipation != null) {
              return Column(
                children: <Widget>[
                  dividerSection("Mi participación"),
                  ViewParticipationProvider(
                    participation: snapshot.data,
                    challenge: _myParticipationBloc.challenge,
                    updateDateview: _myParticipationBloc.updateDataView,
                  ),
                ],
              );
            } else {
              return Container();
            }
          }
        });
  }

  dividerSection(String title) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      color: Colors.black12,
      height: 40,
      child: Align(child: Text(title), alignment: Alignment.centerLeft),
    );
  }

  @override
  void initState() {
    _myParticipationBloc = BlocProvider.of<BMyParticipation>(context);
    super.initState();
  }
}
