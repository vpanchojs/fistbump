import 'dart:async';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/bloc/manage_profile/b_manage_profile.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class VManageProfile extends StatefulWidget {
  static const routeName = "vmanangeprofile";
  @override
  _VManageProfileState createState() => new _VManageProfileState();
}

class _VManageProfileState extends State<VManageProfile> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  BManageProfile editProfileBloc;

  var nameCtl = TextEditingController();
  var nameUserCtl = TextEditingController();
  var cellPhoneCtl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(string_title_edit_profile, style: StyleRes.text_appbar),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          actions: <Widget>[
            actionButton(),
          ],
        ),
        body: GestureDetector(
            onTap: () {
              // call this method here to hide soft keyboard
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Container(
              margin: const EdgeInsets.all(16),
              child: profile(),
            )));
  }

  @override
  void initState() {
    editProfileBloc = BlocProvider.of<BManageProfile>(context);
    editProfileBloc.outView.listen((view) {
      view.run(context: context, key: scaffoldKey);
      // data.actionExecute(
      //     context,
      //     scaffoldKey,
      //     () => Navigator.pushAndRemoveUntil(
      //         context,
      //         MaterialPageRoute(builder: (context) => DashboardPage()),
      //         ModalRoute.withName("")));
    });
    super.initState();
  }

  Widget profile() {
    return StreamBuilder(
      stream: editProfileBloc.outShow,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return progress();
        }
        return formProfile();
      },
    );
  }

  Widget progress() {
    return Align(
      alignment: Alignment.center,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(string_message_load_info_user),
            Container(
              padding: const EdgeInsets.all(16.0),
              child: CircularProgressIndicator(),
            )
          ]),
    );
  }

  Widget formProfile() {
    return ListView(
      children: <Widget>[
        SizedBox(height: 16.0),
        textName(),
        SizedBox(height: 16.0),
        textCellPhone(),
        SizedBox(height: 16.0),
        textNameUser(),
        SizedBox(height: 16.0),
        selectBirthday(),
        SizedBox(height: 16.0),
        selectGender(),
      ],
    );
  }

  Widget textName() {
    return StreamBuilder(
      stream: editProfileBloc.outName,
      builder: (context, snapshot) {
        if (snapshot.hasData && nameCtl.text.length <= 0) {
          nameCtl.text = snapshot.data;
        }

        return TextField(
          controller: nameCtl,
          onChanged: editProfileBloc.inName,
          keyboardType: TextInputType.text,
          maxLines: 1,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: string_label_name_challenge,
            hintText: string_hint_name_person,
            errorText: snapshot.error,
          ),
        );
      },
    );
  }

  Widget textNameUser() {
    return StreamBuilder(
      stream: editProfileBloc.outNameUser,
      builder: (context, snapshot) {
        if (snapshot.hasData && nameUserCtl.text.length <= 0) {
          nameUserCtl.text = snapshot.data;
        }
        return TextField(
          style: TextStyle(color: Colors.black38),
          controller: nameUserCtl,
          onChanged: (val) => editProfileBloc.inNameUser(val.toLowerCase()),
          keyboardType: TextInputType.text,
          maxLines: 1,
          enabled: !editProfileBloc.exists,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: string_label_name_user,
            hintText: string_hint_name_user,
            errorText: snapshot.error,
          ),
          inputFormatters: <TextInputFormatter>[
            UpperCaseTextFormatter(),
          ],
        );
      },
    );
  }

  Widget textCellPhone() {
    return StreamBuilder(
      stream: editProfileBloc.outCellPhone,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          cellPhoneCtl.text = snapshot.data;
        }
        return TextField(
          style: TextStyle(color: Colors.black38),
          controller: cellPhoneCtl,
          onChanged: editProfileBloc.inCellPhone,
          enabled: false,
          keyboardType: TextInputType.number,
          maxLines: 1,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: string_label_phone,
            hintText: string_hint_phone,
            errorText: snapshot.error,
          ),
        );
      },
    );
  }

  Widget selectBirthday() {
    return StreamBuilder(
      stream: editProfileBloc.outBirthday,
      builder: (context, snapshot) {
        return Stack(children: <Widget>[
          TextField(
            enabled: false,
            style: TextStyle(fontSize: 16.0),
            controller: TextEditingController(text: " "),
            maxLines: 1,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: string_label_birthdate),
          ),
          Container(
              height: 60,
              child: FlatButton(
                onPressed: _selectDate,
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Text((snapshot.data == null)
                            ? string_label_select_date
                            : DateFormat.yMMMd().format(snapshot.data))),
                    Icon(
                      Icons.calendar_today,
                      size: 20,
                      color: Colors.white70,
                    )
                  ],
                ),
              )),
        ]);
      },
    );
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        firstDate: editProfileBloc.first,
        initialDate: editProfileBloc.last,
        lastDate: editProfileBloc.last);

    if (picked != null) editProfileBloc.inBirthday(picked);
  }

  Widget selectGender() {
    return StreamBuilder(
      stream: editProfileBloc.outGender,
      initialData: 1,
      builder: (context, snapshot) {
        return Stack(children: <Widget>[
          TextField(
            enabled: false,
            style: TextStyle(fontSize: 16.0),
            controller: TextEditingController(text: " "),
            maxLines: 2,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: string_label_select_gender),
          ),
          Container(
            height: 82,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FilterChip(
                  label: Text(string_label_men),
                  selected: snapshot.data == 1 ? true : false,
                  onSelected: (val) {
                    if (val) editProfileBloc.inGender(1);
                  },
                ),
                FilterChip(
                  label: Text(string_label_women),
                  selected: snapshot.data == 2 ? true : false,
                  onSelected: (val) {
                    if (val) editProfileBloc.inGender(2);
                  },
                ),
              ],
            ),
          ),
        ]);
      },
    );
  }

  Widget actionButton() {
    return StreamBuilder(
      stream: editProfileBloc.outProgressAction,
      initialData: false,
      builder: (context, snapshot) {
        if (snapshot.data) {
          return Align(
            alignment: FractionalOffset.center,
            child: Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.purple),
              ),
            ),
          );
        }
        return button();
      },
    );
  }

  Widget button() {
    return StreamBuilder(
        stream: editProfileBloc.active,
        builder: (context, snapshot) {
          print("data name ${snapshot.data}  ${snapshot.hasData}");
          return FlatButton(
            textColor: ColorsRes.light_blue_color,
            child: Text(
              "Actualizar",
              style: GoogleFonts.montserrat(
                  fontSize: 16, fontWeight: FontWeight.bold),
            ),
            onPressed:
                snapshot.hasData ? editProfileBloc.updateInfoProfile : null,
          );
        });
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text?.toLowerCase(),
      selection: newValue.selection,
    );
  }
}
