import 'dart:io';
import 'package:Darhu/src/bloc/menu/b_menu.dart';
import 'package:Darhu/src/ui/pages/login/v_login.dart';
import 'package:Darhu/utils/share_widgets/share_platforms.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class WListOptions extends StatelessWidget {
  final BMenu bloc;

  const WListOptions({Key key, this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> options = List();
    options.add(createOptionMenu(
        "Compartir Aplicación", () =>{
          if(Platform.isAndroid){
    SharePlatforms.shareOtherPlatformsDynamicLinks(context,"https://play.google.com/store/apps/details?id=site.darhu.challenge")
    }else{
        if(Platform.isIOS)
        SharePlatforms.shareOtherPlatformsDynamicLinks(context,"https://play.google.com/store/apps/details?id=site.darhu.challenge")
    }
    }));
    options.add(createOptionMenu(
        "Términos y Condiciones", () async {
      const urlTerms = 'https://darhu.site/termsAndConditions';
      if (await canLaunch(urlTerms)) {
      await launch(urlTerms);
      }
    }));
    options.add(createOptionMenu("Preguntas Frecuentes", () async {
      const urlQuestions = 'https://darhu.site/frequentQuestions';
      if (await canLaunch(urlQuestions)) {
      await launch(urlQuestions);
      }
    }));
    options.add(createOptionMenu("Cerrar Sesión", () => closeSession(context)));
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: ListView.separated(
        separatorBuilder: (context, index) =>
            Divider(
              color: Colors.black12,
            ),
        itemCount: 4,
        itemBuilder: (context, index) => options[index],
      ),
    );
  }

  createOptionMenu(String description, dynamic action) {
    return ListTile(
      contentPadding: const EdgeInsets.all(0),
      dense:true ,
      title: Text(description,style: TextStyle(fontSize: 16)),
      onTap: action,
    );
  }

  closeSession(BuildContext context) {
    bloc.closeSession().then((v) {

      /*
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => PLogin()),
          ModalRoute.withName(''));

       */
      Navigator.pushNamedAndRemoveUntil(context, VLogin.routeName, ModalRoute.withName(''));
    }).catchError((e) {
      final snackBar = SnackBar(content: Text(e));
      Scaffold.of(context).showSnackBar(snackBar);
    });
  }

}
