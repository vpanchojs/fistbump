import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/ui/pages/view_profile/v_view_profile.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/application_bloc.dart';
import 'package:Darhu/src/bloc/menu/b_menu.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'widgets/w_list_options.dart';

class Menu extends StatefulWidget {
  @override
  MenuState createState() => new MenuState();
}

class MenuState extends State<Menu> with AutomaticKeepAliveClientMixin<Menu> {
  BMenu _bloc;
  final scaffKey = new GlobalKey<ScaffoldState>();
  ApplicationBloc _applicationBloc;
  final String path = 'images/mydares_ic.svg';
  Size sizeScreen;

  @override
  void initState() {
    _bloc = BlocProvider.of<BMenu>(context);
    _applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    sizeScreen = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffKey,
      appBar: AppBar(
        title: Text("Menú", style: StyleRes.text_appbar),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          getImageProfile(_applicationBloc.user),
          Expanded(child: WListOptions(bloc: _bloc))
        ],
      ),
    );
  }

  Widget getImageProfile(MUser user) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Center(
              child: PhotoThumbnail(
                  size: 125,
                  round: 5,
                  width: 3,
                  borderRadiusOut: 35,
                  borderRadiusIn: 10,
                  user: user,
                  navigationProfile: false),
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(_applicationBloc.user.nameUser,
                    style: GoogleFonts.montserrat(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: ColorsRes.light_blue_color)),
                Container(
                  child: ActionChip(
                    labelStyle: StyleRes.text_button_chip_white,
                    labelPadding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    backgroundColor: ColorsRes.purple_color,
                    label: Text("Ver Perfil"),
                    onPressed: _navigationProfile,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  _navigationProfile() {
    Navigator.pushNamed(context, VViewProfile.routeName,
        arguments: _applicationBloc.user);
    /*
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => new PViewProfile(
          user: _applicationBloc.user,
        ),
      ),
    );

     */
  }

  @override
  bool get wantKeepAlive => true;
}
