import 'package:Darhu/src/bloc/my_challenges/p_my_challenges.dart';
import 'package:Darhu/src/bloc/menu/p_menu.dart';
import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/bloc/newsfeed/p_newsfeed.dart';
import 'package:Darhu/src/bloc/notifications/p_notifications.dart';
import 'package:Darhu/src/bloc/populate/p_populate.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/src/ui/pages/view_profile/v_view_profile.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/enums/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../application_bloc.dart';

class VInitSection extends StatefulWidget {
  static const routeName="vinitsection";
  @override
  VInitSectionState createState() {
    return new VInitSectionState();
  }
}

class VInitSectionState extends State<VInitSection>
    with AutomaticKeepAliveClientMixin<VInitSection> {
  final String homeIc = 'images/home_ic.svg';
  final String populateIc = 'images/populate_ic.svg';
  final String notificationIc = 'images/notificacion_ic.svg';
  final String mydaresIc = 'images/mydares_ic.svg';
  final String menuIc = 'images/menu_ic.svg';
  final scaffKey = new GlobalKey<ScaffoldState>();

  PageController pageController;
  PageView pageView;
  int _index;

  @override
  void initState() {
    _index = 0;
    pageController = PageController(initialPage: _index, keepPage: true);
    ApplicationBloc _applicationBloc =
        BlocProvider.of<ApplicationBloc>(context);
    _applicationBloc.saveTokenNotification();
    initListenNotificationsAndLinks(_applicationBloc);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return new Scaffold(
      key: scaffKey,
      body: PageView(
        physics: new NeverScrollableScrollPhysics(),
        controller: pageController,
        onPageChanged: (index) {
          setState(() {
            pageController.jumpToPage(index);
            _index = index;
          });
        },
        children: <Widget>[
          PNewsFeed(),
          PPopulate(),
          PMyChallenges(),
          PNotifications(),
          PMenu()
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedItemColor: ColorsRes.purple_color,
        onTap: (index) {
          setState(() {
            pageController.jumpToPage(index);
            _index = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              icon: createIcon(homeIc,28.0),
              title: Text("Inicio"),
              activeIcon: createIconActive(homeIc,28.0)),
          BottomNavigationBarItem(
              icon: createIcon(populateIc),
              title: Text("Populares"),
              activeIcon: createIconActive(populateIc)),
          BottomNavigationBarItem(
              icon: createIcon(mydaresIc),
              title: Text("Mis Retos"),
              activeIcon: createIconActive(mydaresIc)),
          BottomNavigationBarItem(
              icon: createIcon(notificationIc),
              title: Text("Notificaciones"),
              activeIcon: createIconActive(notificationIc)),
          BottomNavigationBarItem(
              icon: createIcon(menuIc),
              title: Text("Menú"),
              activeIcon: createIconActive(menuIc)),
        ],
      ),
    );
  }

  Widget createIcon(String path,[tam=24.0]) {
    return SvgPicture.asset(
      path,
      color: Colors.black38,
      height: tam,
    );
  }

  Widget createIconActive(String path,[tam=24.0]) {
    return SvgPicture.asset(
      path,
      color: ColorsRes.purple_color,
      height: tam,
    );
  }

  String getTitle(index) {
    switch (index) {
      case 0:
        return "Inicio";
      case 1:
        return "Populares";
      case 2:
        return "Mis Retos";
      case 3:
        return "Notificaciones";
      case 4:
        return "Menú";
      default:
        return "Darhu";
    }
  }

  Color getAppBarColor(index) {
    switch (index) {
      case 0:
        return Colors.transparent;
      case 1:
        return ColorsRes.purple_color;
      case 2:
        return Colors.transparent;
      case 3:
        return Colors.transparent;
      case 4:
        return Colors.transparent;
      default:
        return ColorsRes.purple_color;
    }
  }

  TextStyle getStyle(index) {
    switch (index) {
      case 0:
        return StyleRes.text_appbar;
      case 1:
        return StyleRes.text_appbar_white;
      case 2:
        return StyleRes.text_appbar;
      case 3:
        return StyleRes.text_appbar;
      case 4:
        return StyleRes.text_appbar;
      default:
        return StyleRes.text_appbar_white;
    }
  }

  initListenNotificationsAndLinks(ApplicationBloc _applicationBloc) {
    _applicationBloc.outNavigation.listen((data) {
      print("llegaron los parametros aqui $data");
      switch (data["navigation"]) {
        case NAVIGATION_LINKS_OR_NOTIFICATIONS.CHALLENGE:
          Navigator.pushNamed(context, VViewChallenge.routeName,arguments: ArgViewChallenge()..challenge=(MChallenge()..idChallenge = data["challenge"]));
          /*
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => PViewChallenge(
                      challenge: MChallenge()
                        ..idChallenge = data["challenge"])));

           */
          break;
        case NAVIGATION_LINKS_OR_NOTIFICATIONS.PROFILE:
          Navigator.pushNamed(context, VViewProfile.routeName,arguments: MUser()..idUser = data["user"]);
         /*
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      PViewProfile(user: MUser()..idUser = data["user"])));

          */
          break;
        case NAVIGATION_LINKS_OR_NOTIFICATIONS.PARTICIPATION:
          Navigator.pushNamed(context, VViewChallenge.routeName,arguments: ArgViewChallenge()..challenge=(MChallenge()..idChallenge = data["challenge"])..participation=(MParticipation()
            ..idParticipation = data["participation"]));

          /*
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => PViewChallenge(
                      challenge: MChallenge()..idChallenge = data["challenge"],
                      participation: MParticipation()
                        ..idParticipation = data["participation"])));

           */
          break;
      }
    });
  }

  @override
  bool get wantKeepAlive => true;
}
