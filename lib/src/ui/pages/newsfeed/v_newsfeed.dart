import 'package:Darhu/src/models/m_news.dart';
import 'package:Darhu/src/bloc/newsfeed/b_newsfeed.dart';
import 'package:Darhu/src/ui/pages/newsfeed/widgets/w_item_challenge_news.dart';
import 'package:Darhu/src/ui/pages/newsfeed/widgets/w_item_participation_news.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/share_widgets/list/ListGenericWidget.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';

class VNewsFeed extends StatefulWidget {
  static const routeName="vnewsfeed";

  @override
  VNewsFeedState createState() {
    return new VNewsFeedState();
  }
}

class VNewsFeedState extends State<VNewsFeed>
    with AutomaticKeepAliveClientMixin<VNewsFeed> {
  final scrollController = new ScrollController();
  BNewsFeed _newsFeedBloc;
  final String assetName = 'images/info_none.svg';

  Future<void> _refresData() async {
    return _newsFeedBloc.getNews();
  }

  @override
  void initState() {
    _newsFeedBloc = BlocProvider.of<BNewsFeed>(context);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        _newsFeedBloc.getMoreNews();
      }
    });

    _newsFeedBloc.outView.listen((view) {
      view.run(context: context, key: null);      
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(string_title_page_newsfeed, style: StyleRes.text_appbar),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
      ),
      body: ListGenericWidget(
        refresh: _refresData,
        stream: _newsFeedBloc.outNews,
        progress: _newsFeedBloc.outProgressMore,
        scrollController: scrollController,
        itemWidget: setDataNewsFeedStream,
        noneData: string_message_none_newsfeed,
        showDivider: true,
      ),
    );
  }

  setDataNewsFeedStream(MNews news) {
    if (news.type == 0) {
      return WItemChallengeNews(
          news: news, bloc: _newsFeedBloc);
    } else {
      return WItemParticipationNews(
          news: news, bloc: _newsFeedBloc);
    }
  }

  @override
  bool get wantKeepAlive => true;
}
