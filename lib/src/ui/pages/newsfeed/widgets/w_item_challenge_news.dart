import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/models/m_news.dart';
import 'package:Darhu/src/bloc/newsfeed/b_newsfeed.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/utils/share_widgets/date_format.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/utils/share_widgets/share_platforms.dart';
import 'package:Darhu/utils/share_widgets/multimedia/image/thumbnail_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class WItemChallengeNews extends StatelessWidget {
  final MNews news;
  final BNewsFeed bloc;

  const WItemChallengeNews({Key key, this.news, this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _navigationChallenge(context),
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            getContainerUser(context),
            _thumbnailWidget(),
            getContainerFooter()
          ],
        ),
      ),
    );
  }

  getContainerUser(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListTile(
        onTap: () {},
        contentPadding: EdgeInsets.symmetric(horizontal: 0),
        leading: PhotoThumbnail(user: news.challenge.user),
        title: titleUserChallenge(),
        subtitle: DatePostFull(datePost: news.challenge.datePost),
        trailing: actionShareOtherPlatforms(context),
      ),
    );
  }

  _shareOtherPlatforms(String url, BuildContext context) {
    SharePlatforms.shareOtherPlatformsDynamicLinks(context, url);
  }

  actionShareOtherPlatforms(BuildContext context) {
    return StreamBuilder<List<String>>(
        stream: bloc.outProgressActionList,
        initialData: List(),
        builder: (context, snapshot) {
          if (snapshot.data.contains(news.idNews)) {
            return Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor:
                    AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
              ),
            );
          }
          return IconButton(
              icon: Icon(Icons.share, color: Colors.black),
              onPressed: () => bloc.shareChallengePlatforms(
                  (url) => _shareOtherPlatforms(url, context), news));
        });
  }

  Widget titleUserChallenge() {
    return Text(
      news.challenge.user.nameUser,
      style: GoogleFonts.montserrat(fontSize: 16, fontWeight: FontWeight.bold),
    );
  }

  Widget _thumbnailWidget() {
    print("news.challenge.urlThumbnail ${news.challenge.urlThumbnail}");
    return ThumbnailAction(
      actionTap: _navigationChallenge,
      typeMultimedia: news.challenge.typeMultimedia,
      urlMultimedia: news.challenge.urlMultimedia,
      urlThumbnail: news.challenge.urlThumbnail,
    );
  }

  getContainerFooter() {
    return Container(
      margin: EdgeInsets.only(bottom: 8.0),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      alignment: FractionalOffset.center,
      child: Column(children: <Widget>[
        ListTile(
          contentPadding: const EdgeInsets.all(0),
          title: Text(
            news.challenge.name,
            style: GoogleFonts.montserrat(
                fontSize: 18, fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            news.challenge.description,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            style: GoogleFonts.montserrat(
                fontSize: 14, fontWeight: FontWeight.normal),
          ),
        )
      ]),
    );
  }

  Widget iconParticipant(String typeParticipation) {
    String assetName = "";

    switch (typeParticipation) {
      case 'image':
        assetName = 'images/image_ic.svg';
        break;
      case 'video':
        assetName = 'images/video_ic.svg';
        break;
      case 'text':
        assetName = 'images/texto_ic.svg';
        break;
    }

    return SvgPicture.asset(
      assetName,
      height: 14,
      color: Colors.black38,
    );
  }

  Widget iconWinnerBy(String typeParticipation) {
    String assetName = "";

    switch (typeParticipation) {
      case 'like':
        assetName = 'images/apoyos_line.svg';
        break;
      case 'random':
        assetName = 'images/aleatorio_line.svg';
        break;
      default:
        assetName = 'images/aleatorio_line.svg';
    }

    return SvgPicture.asset(
      assetName,
      height: 14,
      color: Colors.black38,
    );
  }

  void _navigationChallenge(BuildContext context) {
    Navigator.pushNamed(context, VViewChallenge.routeName,
        arguments: ArgViewChallenge()..challenge = news.challenge);

    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PViewChallenge(
            challenge: news.challenge,
          ),
        ));
     */
  }
}
