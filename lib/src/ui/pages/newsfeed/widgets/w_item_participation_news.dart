import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/models/m_news.dart';
import 'package:Darhu/src/bloc/newsfeed/b_newsfeed.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/utils/share_widgets/date_format.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/utils/share_widgets/share_platforms.dart';
import 'package:Darhu/utils/share_widgets/multimedia/image/thumbnail_action.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WItemParticipationNews extends StatelessWidget {
  final MNews news;
  final BNewsFeed bloc;

  const WItemParticipationNews({Key key, this.news, this.bloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _goViewParticipationPage(context),
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            getContainerUser(context),
            news.participation.type == 'text'
                ? getContainerText()
                : _thumbnailWidget(),
            //getContainerFooter()
          ],
        ),
      ),
    );
  }

  getContainerUser(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListTile(
        onTap: () {},
        contentPadding: EdgeInsets.symmetric(horizontal: 0),
        leading: PhotoThumbnail(user: news.participation.user),
        title: titleUserChallenge(context),
        subtitle: DatePostFull(datePost: news.participation.datePost),
        trailing: actionShareOtherPlatforms(context),
      ),
    );
  }

  _shareOtherPlatforms(String url, BuildContext context) {
    SharePlatforms.shareOtherPlatformsDynamicLinks(context, url);
  }

  actionShareOtherPlatforms(BuildContext context) {
    return StreamBuilder<List<String>>(
        stream: bloc.outProgressActionList,
        initialData: List(),
        builder: (context, snapshot) {
          if (snapshot.data.contains(news.idNews)) {
            return Container(
              height: 24,
              margin: EdgeInsets.only(right: 8),
              width: 24,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor:
                    AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
              ),
            );
          }
          return IconButton(
              icon: Icon(Icons.share, color: Colors.black),
              onPressed: () => bloc.shareParticipationPlatforms(
                  (url) => _shareOtherPlatforms(url, context), news));
        });
  }

  Widget titleUserChallenge(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          news.participation.user.nameUser,
          style:
              GoogleFonts.montserrat(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        Icon(
          Icons.label_important,
          color: Colors.black38,
          size: 16,
        ),
        Expanded(
            child: InkWell(
                onTap: () => _navigationChallenge(context),
                child: Text(
                  news.participation.challenge.name,
                  overflow: TextOverflow.ellipsis,
                  softWrap: false,
                  style: GoogleFonts.montserrat(
                      fontSize: 16, fontWeight: FontWeight.bold),
                )))
      ],
    );
  }

  getContainerText() {
    return Container(
      child: Column(
        children: <Widget>[
          Divider(color: Colors.black38),
          SizedBox(height: 8),
          Text(
            news.participation.response,
            style: GoogleFonts.montserrat(
                fontSize: 16, fontWeight: FontWeight.normal),
          ),
          Divider(color: Colors.black38),
          SizedBox(
            height: 8,
          )
        ],
      ),
    );
  }

  Widget _thumbnailWidget() {
    return ThumbnailAction(
      actionTap: _goViewParticipationPage,
      typeMultimedia: news.participation.type,
      urlMultimedia: news.participation.response,
      urlThumbnail: news.participation.thumbnail,
    );
  }

  /*
  getContainerFooter() {
    return Container(
      height: 40,
      margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      alignment: FractionalOffset.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Expanded(
            child: Text(
              "${widget.participation.numLikes} Apoyos",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 14,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          //    getActionButtom()
        ],
      ),
    );
  }
  */

  void _goViewParticipationPage(BuildContext context) async {
    Navigator.pushNamed(context, VViewChallenge.routeName,
        arguments: ArgViewChallenge()
          ..challenge = news.participation.challenge
          ..participation = news.participation);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PViewChallenge(
            challenge: news.participation.challenge,
            participation: news.participation,
          ),
        ));
     */
  }

  void _navigationChallenge(BuildContext context) {
    Navigator.pushNamed(context, VViewChallenge.routeName,
        arguments: ArgViewChallenge()
          ..challenge = news.participation.challenge);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PViewChallenge(
            challenge: news.participation.challenge,
          ),
        ));
     */
  }
}
