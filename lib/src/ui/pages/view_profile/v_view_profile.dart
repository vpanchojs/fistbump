import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/ui/pages/contacts/util/arg_contact.dart';
import 'package:Darhu/src/ui/pages/contacts/v_contacts.dart';
import 'package:Darhu/src/ui/pages/manage_profile/v_manage_profile.dart';
import 'package:Darhu/src/ui/pages/populate/widgets/w_item_challenge_square.dart';
import 'package:Darhu/src/ui/pages/view_profile/widgets/w_item_participation.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/button/button_reload.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/utils/share_widgets/list/SilverListGenericWigdet.dart';
import 'package:Darhu/utils/share_widgets/multimedia/get_multimedia.dart';
import 'package:Darhu/utils/share_widgets/progress/progress_custom.dart';
import 'package:Darhu/utils/share_widgets/share_platforms.dart';
import 'package:Darhu/utils/utils.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../bloc/view_profile/b_view_profile.dart';

class VViewProfile extends StatefulWidget {
  static const routeName = "vviewprofile";
  @override
  VViewProfileState createState() {
    return new VViewProfileState();
  }
}

class VViewProfileState extends State<VViewProfile> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  BViewProfile profileBloc;
  final scrollController = new ScrollController();
  List<Widget> profileWidgets = List();
  final String path = 'images/mydares_ic.svg';
  Size sizeScreen;

  @override
  void initState() {
    profileBloc = BlocProvider.of<BViewProfile>(context);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        profileBloc.getMoreMyParticipations();
      }
    });

    profileBloc.outView.listen((view) {
      view.run(context: context, key: scaffoldKey);
      //data.actionExecute(context, scaffoldKey);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    sizeScreen = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      body: containerMain(),
    );
  }

  sliverAppBar(MUser user, showActions) {
    return SliverAppBar(
      title: Text(user.nameUser ?? string_default_name_user,
          style: StyleRes.text_appbar),
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
      actions: <Widget>[(showActions) ? shareProfile() : Container()],
    );
  }

  shareProfile() {
    return StreamBuilder<bool>(
        stream: profileBloc.outProgressShare,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return Align(
              alignment: FractionalOffset.center,
              child: Container(
                height: 24,
                margin: EdgeInsets.only(right: 8),
                width: 24,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor:
                      AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
                ),
              ),
            );
          } else {
            return IconButton(
                icon: Icon(Icons.share),
                onPressed: () =>
                    profileBloc.shareProfilePlatforms(_shareOtherPlatforms));
          }
        });
  }

  containerMain() {
    return StreamBuilder<MUser>(
        stream: profileBloc.outUsers,
        builder: (context, snapshot) {
          if (snapshot.hasError)
            return containerInfoBasicError(
                profileBloc.user, snapshot.error.toString());

          if (snapshot.connectionState == ConnectionState.waiting ||
              !snapshot.hasData)
            return containerInfoBasicProgress(profileBloc.user);

          return containerInfoComplete(snapshot.data);
        });
  }

  /*
  * Metodo encargado de motrar la foto de perfil,
  * y el boton de reintentar, en caso de producirse un error
  * @ Param User, informacion del usuario obtenida localmente
  * */
  containerInfoBasicError(MUser user, String error) {
    return CustomScrollView(
      controller: scrollController,
      slivers: <Widget>[
        sliverAppBar(user, false),
        SliverToBoxAdapter(child: getImageProfileError(user.urlPhoto)),
        SliverToBoxAdapter(
          child: Column(
            children: <Widget>[
              ButtonReload(
                refresh: () {},
              ),
              Text(error)
            ],
          ),
        )
      ],
    );
  }

  /*
  * Metodo encargado de mostrar la informacion basica del usuario, junto
  * con un progress, indicando la espera al obtener los datos.
  * @ Param User, informacion del usuario obtenida localmente
  * */

  containerInfoBasicProgress(MUser user) {
    return CustomScrollView(
      controller: scrollController,
      slivers: <Widget>[
        sliverAppBar(user, false),
        SliverToBoxAdapter(child: getImageProfileError(user.urlPhoto)),
        SliverToBoxAdapter(
          child: Column(
            children: <Widget>[CircularProgressIndicator()],
          ),
        )
      ],
    );
  }

  /*
  * Metodo encargado de mostrar la informacion del usuario ya cuando
  * la consulta ha terminado.
  * @ Param User, informacion del usuario obtenida desde el servicio web
  * */

  containerInfoComplete(MUser user) {
    return RefreshIndicator(
      onRefresh: _refreshProfile,
      child: CustomScrollView(
        controller: scrollController,
        physics: const AlwaysScrollableScrollPhysics(),
        slivers: <Widget>[
          sliverAppBar(user, true),
          SliverToBoxAdapter(child: getImageProfileComplete(user)),
          sliverStatitics(user),
          SliverToBoxAdapter(
              child: dividerSection(string_section_challenges_actives)),
          challenges(user.challengesActive),
          SliverToBoxAdapter(
              child: dividerSection(string_section_participant_challenges)),
          participationsWidget(),
          progressSliver()
        ],
      ),
    );
  }

  Widget getImageProfileError(String url) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Center(
              child: PhotoThumbnail(
                  size: 125,
                  round: 5,
                  width: 3,
                  borderRadiusOut: 35,
                  borderRadiusIn: 10,
                  navigationProfile: false,
                  user: MUser()..urlPhoto = url),
            ),
          ),
          Expanded(child: Container(), flex: 1)
        ],
      ),
    );
  }

  Widget getImageProfileComplete(MUser user) {
    print("presentado todo completo");
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                PhotoThumbnail(
                    size: 125,
                    round: 5,
                    width: 3,
                    borderRadiusOut: 35,
                    borderRadiusIn: 10,
                    user: user,
                    navigationProfile: false),
                updatePhotoButton(user.type)
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                isVerifiedIcon(user),
                Text(user.name ?? string_default_name_complete,
                    style: GoogleFonts.montserrat(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: ColorsRes.light_blue_color),
                    textAlign: TextAlign.center),
                Container(
                  child: _getButtonTypeProfile(user.type),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  sliverStatitics(MUser user) {
    return SliverToBoxAdapter(
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _getButtonStadists(string_label_followers,
                user.numFollowers.toString(), _navigationFollowers),
            _getButtonStadists(string_label_followings,
                user.numFolloweds.toString(), _navigationFolloweds),
            _getButtonStadists(
                string_label_challenges, user.numChallenges.toString(), () {}),
          ],
        ),
      ),
    );
  }

  _navigationEditProfile() {
    Navigator.pushNamed(context, VManageProfile.routeName).then((result) {
      if (result != null) {
        profileBloc.updateInfoBasicUser();
        Utils.showSnackBar(string_success_update_data_profile, scaffoldKey);
      }
    });
  }

  _getActionChipTypeProfile(String label, dynamic) {
    return StreamBuilder<bool>(
        stream: profileBloc.outProgressAction,
        initialData: false,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.data) {
            return ProgressCustom(show: true);
          }
          return ActionChip(
            labelStyle: StyleRes.text_button_chip_white,
            labelPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
            backgroundColor: ColorsRes.purple_color,
            label: Text(label),
            onPressed: dynamic,
          );
        });
  }

  Widget _getButtonTypeProfile(int type) {
    switch (type) {
      case 0:
        return _getActionChipTypeProfile(
            string_label_button_edit_profile, _navigationEditProfile);
      case -1:
        return _getActionChipTypeProfile(
            string_label_button_follower, profileBloc.followingUser);
      case 1:
        return _getActionChipTypeProfile(
            string_label_button_follower, profileBloc.followingUser);
      case 2:
        return _getActionChipTypeProfile(
            string_label_button_unfollower, profileBloc.unFollowingUser);
      case 3:
        return _getActionChipTypeProfile(
            string_label_button_unfollower, profileBloc.unFollowingUser);
      default:
        return Container();
    }
  }

  _getButtonStadists(String label, String number, dynamic) {
    return Expanded(
      child: FlatButton(
        onPressed: dynamic,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                number,
                style: GoogleFonts.montserrat(
                    fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Text(
                label,
                overflow: TextOverflow.ellipsis,
                softWrap: false,
                style: GoogleFonts.montserrat(fontSize: 14),
              )
            ],
          ),
        ),
      ),
    );
  }

  ///Navegar page Seguidores
  /*
  _navigationFollowers() => Navigator.push(
      context,
      MaterialPageRoute(
          builder: (BuildContext context) => new PContact(
                user: profileBloc.user,
                type: 0,
              )));
*/
  _navigationFollowers() {
    Navigator.pushNamed(context, VContacts.routeName,
        arguments: ArgContact()
          ..user = profileBloc.user
          ..type = 0);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
            new PContact(
              user: profileBloc.user,
              type: 0,
            )));

     */
  }

  ///Navegar page Seguidos
  /*
  _navigationFolloweds() => Navigator.push(
      context,
      MaterialPageRoute(
          builder: (BuildContext context) => new PContact(
                user: profileBloc.user,
                type: 1,
              )));

   */
  _navigationFolloweds() {
    Navigator.pushNamed(context, VContacts.routeName,
        arguments: ArgContact()
          ..user = profileBloc.user
          ..type = 1);
  }

  Widget challenges(List<MChallenge> challenges) {
    if (challenges.length <= 0) {
      return noneData(50, string_message_none_challenges_active);
    }
    return SliverGrid.extent(
        maxCrossAxisExtent: 200,
        mainAxisSpacing: 3.0,
        crossAxisSpacing: 3.0,
        children: setDataChallengeStream(challenges));
  }

  setDataChallengeStream(List<MChallenge> list) {
    return list
        .map<Widget>((challenge) => WItemChallengeSquare(challenge))
        .toList();
  }

  setDataParticipationStream(List<MParticipation> list) {
    return list
        .map<Widget>(
          (item) => WItemParticipation(
            participation: item,
          ),
        )
        .toList();
  }

  participationsWidget() {
    return SilverListGenericWidget(
      stream: profileBloc.outParticipations,
      refresh: _refreshProfile,
      noneData: string_message_none_participations,
      items: setItems,
    );
  }

  setItems(MParticipation item) {
    return WItemParticipation(
      participation: item,
    );
  }

  Future<void> _refreshProfile() async {
    await profileBloc.getInfoUser();
  }

  //Mostar la camara o el cargando para subir la foto
  Widget updatePhotoButton(int type) {
    if (type == 0) {
      return StreamBuilder(
          initialData: false,
          stream: profileBloc.outProgressActionAux,
          builder: (context, snapshot) {
            if (!snapshot.data) {
              return Positioned(
                right: 30,
                bottom: 10,
                child: FloatingActionButton(
                  backgroundColor: Colors.white,
                  onPressed: null,
                  mini: true,
                  child: SizedBox(
                    width: 20,
                    height: 20,
                    child: Align(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                      ),
                    ),
                  ),
                ),
              );
            } else {
              return Positioned(
                right: 30,
                bottom: 10,
                child: FloatingActionButton(
                  backgroundColor: Colors.white,
                  onPressed: _mainBottomSheet,
                  mini: true,
                  child:
                      Icon(Icons.photo_camera, color: ColorsRes.purple_color),
                ),
              );
            }
          });
    } else {
      return Container();
    }
  }

  _actionTakePhoto() async {
    GetMultimedia.actionTakePictureCamera().then((response) {
      if (response.success) {
        profileBloc.updatePhotoProfile(response.multimedia);
      } else {
        //presentar en caso de error
      }
    }).catchError((error) {
      if (error != "") Utils.showSnackBar(error.toString(), scaffoldKey);
    });
  }

  _actionGetGalleryPhoto() async {
    GetMultimedia.actionGetGalleryPhoto().then((response) {
      if (response.success) {
        profileBloc.updatePhotoProfile(response.multimedia);
      } else {
        //presentar en caso de error
      }
    }).catchError((error) {
      if (error != "") Utils.showSnackBar(error.toString(), scaffoldKey);
    });
  }

  ListTile _createTitle(
      BuildContext context, String name, IconData icon, Function action) {
    return ListTile(
      leading: Icon(icon),
      title: Text(name),
      onTap: () {
        Navigator.pop(context);
        action();
      },
    );
  }

  _mainBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _createTitle(context, string_option_photo, Icons.photo_camera,
                  _actionTakePhoto),
              _createTitle(context, string_option_gallery_image,
                  Icons.photo_album, _actionGetGalleryPhoto),
            ],
          );
        });
  }

  dividerSection(String title) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      color: Colors.black12,
      height: 40,
      child: Align(child: Text(title), alignment: Alignment.centerLeft),
    );
  }

  /* Mensaje cuando no existen ningun reto activo*/
  noneData(double size, String message) {
    final Widget svg = SvgPicture.asset(
      'images/info_none.svg',
      height: size,
    );
    return SliverToBoxAdapter(
        child: Container(
      height: 200,
      width: double.infinity,
      child: Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            svg,
            SizedBox(height: 10.0),
            Text(message,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black26, fontSize: 16.0)),
          ],
        ),
      ),
    ));
  }

  _shareOtherPlatforms(String url) {
    SharePlatforms.shareOtherPlatformsDynamicLinks(context, url);
  }

  /*Progress para el cargar mas participaciones*/
  progressSliver() {
    return StreamBuilder<bool>(
        stream: profileBloc.outProgressMore,
        initialData: false,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          return SliverToBoxAdapter(
            child: Align(
              alignment: Alignment.center,
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 8),
                height: 14,
                width: 14,
                child: (snapshot.data)
                    ? CircularProgressIndicator(strokeWidth: 2)
                    : Container(),
              ),
            ),
          );
        });
  }

  isVerifiedIcon(MUser user) {
    if (user.verified)
      return Tooltip(
        child: Icon(Icons.verified_user, color: Colors.green),
        message: "Usuario verificado por Darhu",
      );
    else
      return Container();
  }
}
