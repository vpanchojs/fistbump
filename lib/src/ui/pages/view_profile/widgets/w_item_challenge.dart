import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:flutter/material.dart';

class WItemChallenge extends StatefulWidget {
  final MChallenge challenge;

  const WItemChallenge({Key key, this.challenge}) : super(key: key);

  @override
  _WItemChallengeState createState() => _WItemChallengeState();
}

class _WItemChallengeState extends State<WItemChallenge> with AutomaticKeepAliveClientMixin<WItemChallenge> {
  void _goViewChallengePage() async {
    Navigator.pushNamed(context, VViewChallenge.routeName,arguments: ArgViewChallenge()..challenge=widget.challenge);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PViewChallenge(
                challenge: widget.challenge,
              ),
        ));

     */
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return InkWell(
      onTap: _goViewChallengePage,
      child: Stack(
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: widget.challenge.urlMultimedia,
            height:double.maxFinite,
            width: double.maxFinite,
            placeholder: (context, url) {
              return new Center(
                  child: AspectRatio(
                      aspectRatio: 1,
                      child: SizedBox.expand(
                          child: Container(
                        color: Colors.white30,
                      ))));
            },
            fit: BoxFit.cover,
            errorWidget: (context, url, error) {
              return new AspectRatio(aspectRatio: 1, child: Icon(Icons.error));
            },
          ),
          isVideoOrImage(),
          Align(
            child: Container(
              width: double.maxFinite,
              color: Colors.black38,
              padding: EdgeInsets.all(4.0),
              child: Text(
                widget.challenge.name,
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: new TextStyle(
                  fontSize: 13.0,
                  color: Colors.white,
                ),
              ),
            ),
            alignment: FractionalOffset.bottomLeft,
          )
        ],
      ),
    );
  }

  Widget isVideoOrImage(){
    return widget.challenge.typeMultimedia == "video"
        ? Align(
      child: Container(
        margin: EdgeInsets.all(4.0),
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.black38,
        ),
        padding: EdgeInsets.all(3.0),
        child: Icon(
          Icons.videocam,
          color: Colors.white,
          size: 18.0,
        ),
      ),
      alignment: FractionalOffset.topRight,
    )
        : SizedBox(
      height: 0,
      width: 0,
    );
  }

  @override
  bool get wantKeepAlive => true;
}
