import 'package:Darhu/src/bloc/view_challenge/util/arg_view_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/utils/share_widgets/date_format.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/utils/share_widgets/multimedia/image/thumbnail_action.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class WItemParticipation extends StatelessWidget {
  final MParticipation participation;

  const WItemParticipation({Key key, this.participation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _goViewParticipationPage(context),
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            getContainerUser(context),
            participation.type == 'text'
                ? getContainerText()
                : _thumbnailWidget(),
            //getContainerFooter()
          ],
        ),
      ),
    );
  }

  getContainerUser(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListTile(
        onTap: () => () {},
        contentPadding: EdgeInsets.symmetric(horizontal: 0),
        leading: PhotoThumbnail(user: participation.user),
        title: titleUserChallenge(context),
        subtitle: DatePostFull(datePost: participation.datePost),
      ),
    );
  }

  Widget titleUserChallenge(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          participation.user.nameUser,
          style:
              GoogleFonts.montserrat(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        Icon(
          Icons.label_important,
          color: Colors.black38,
          size: 16,
        ),
        Expanded(
            child: InkWell(
                onTap: () => _navigationChallenge(context),
                child: Text(
                  participation.challenge.name,
                  overflow: TextOverflow.ellipsis,
                  softWrap: false,
                  style: GoogleFonts.montserrat(
                      fontSize: 16, fontWeight: FontWeight.bold),
                )))
      ],
    );
  }

  getContainerText() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Divider(color: Colors.black38),
            SizedBox(height: 8),
            Text(participation.response,
                style: GoogleFonts.montserrat(
                    fontSize: 16, fontWeight: FontWeight.normal)),
            Divider(color: Colors.black38),
            SizedBox(
              height: 8,
            )
          ],
        ),
      ),
    );
  }

  Widget _thumbnailWidget() {
    return ThumbnailAction(
      actionTap: _goViewParticipationPage,
      typeMultimedia: participation.type,
      urlMultimedia: participation.response,
      urlThumbnail: participation.thumbnail,
    );
  }

  getMedalButton() {
    if (participation.winner) {
      return FloatingActionButton(
          onPressed: () {},
          child: Icon(
            FontAwesomeIcons.medal,
            color: Colors.white,
          ));
    } else {
      return Container();
    }
  }

  getContainerFooter() {
    return Container(
      height: 40,
      margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      alignment: FractionalOffset.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Expanded(
            child: Text(
              "${participation.numLikes} Apoyos",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 14,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          //    getActionButtom()
        ],
      ),
    );
  }

  void _goViewParticipationPage(BuildContext context) async {
    Navigator.pushNamed(context, VViewChallenge.routeName,
        arguments: ArgViewChallenge()
          ..challenge = participation.challenge
          ..participation = participation);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PViewChallenge(
            challenge: participation.challenge,
            participation: participation,
          ),
        ));

     */
  }

  void _navigationChallenge(BuildContext context) {
    Navigator.pushNamed(context, VViewChallenge.routeName,
        arguments: ArgViewChallenge()..challenge = participation.challenge);
    /*
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PViewChallenge(
            challenge: participation.challenge,
          ),
        ));

     */
  }
}
