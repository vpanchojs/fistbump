import 'package:Darhu/src/bloc/suports_participation/b_support_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/src/ui/pages/search/widgets/w_item_user_search.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/list/ListGenericWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VSupportParticipation extends StatefulWidget{
  static const routeName="VSupportParticipation";
  @override
  _VSupportParticipationState createState() => _VSupportParticipationState();
}

class _VSupportParticipationState extends State<VSupportParticipation> {
  final scrollController = new ScrollController();
  BSupportParticipation _blSupportUser;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: appBar(),
      body: _listUserSupports(),
    );
  }

  Widget appBar() {
    return AppBar(
      title: Text("Apoyos (${_blSupportUser.numSupports})",
          style: StyleRes.text_appbar),
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
    );
  }
   
  @override
  void initState() {
    _blSupportUser = BlocProvider.of<BSupportParticipation>(context);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        _blSupportUser.getMoreSupportParticipations();
      }
    });
    _blSupportUser.outView.listen((data) {
      data.run(context: context, key: scaffoldKey);      
    });
    super.initState();
  }

  Future<void> _refresh() async {
    await _blSupportUser.getSupportParticipations();
  }

  setItems(MUser user) {
    return WItemUserSearch(
      user: user
    );
  }

  _listUserSupports() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 16.0),
      child: ListGenericWidget(
        progress: _blSupportUser.outProgressMore,
        stream: _blSupportUser.outUsersList,
        refresh: _refresh,
        itemWidget: setItems,
        noneData:
        "Sin apoyos",
        scrollController: scrollController,
      ),
    );
  }
}