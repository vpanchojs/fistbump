import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_prize.dart';
import 'package:Darhu/src/ui/pages/prize_detail/util/arg_prize_detail.dart';
import 'package:Darhu/src/ui/pages/prize_detail/v_prize_detail.dart';
import 'package:Darhu/src/ui/share_widgets/w_image_custom.dart';
import 'package:Darhu/utils/enums/enums.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PrizeChallenge extends StatelessWidget {
  final Prize prize;
  final bool withPrize;
  final MChallenge challenge;
  final USER_CHALLENGE userChallenge;

  const PrizeChallenge(
      {Key key, this.prize, this.withPrize, this.userChallenge, this.challenge})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (withPrize) {
      return SliverToBoxAdapter(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: ListTile(
            onTap: () => showDetailsPrize(context),
            contentPadding: EdgeInsets.symmetric(horizontal: 0),
            leading: (prize.urlThumbnail != null)
                ? WImageCustom(image: prize.urlThumbnail)
                : Icon(Icons.card_giftcard, size: 50),
            title: Text(
              "Premio a Ganar",
              style: GoogleFonts.montserrat(
                  fontSize: 16, fontWeight: FontWeight.bold),
            ),
            subtitle: Text("${prize.lot} ${prize.name}",
                style: GoogleFonts.montserrat(
                    fontSize: 14, fontWeight: FontWeight.normal)),
            trailing: Icon(Icons.navigate_next),
          ),
        ),
      );
    }
    return SliverToBoxAdapter();
  }

  showDetailsPrize(BuildContext context) async {
    var data = await Navigator.pushNamed(context, VPrizeDetail.routeName,
        arguments: ArgPrizeDetail()
          ..userChallenge = userChallenge
          ..idChallenge = challenge.idChallenge
          ..stateChallenge = challenge.state);

    /*
    var data=await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => PPrizeDetail(
            userChallenge: userChallenge,
            idChallenge: challenge.idChallenge,
            stateChallenge: challenge.state),
      ),
    );
     */

    if (data != null) {
      if (data is String) {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text(data.toString())));
      }
    }

    /* showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CachedNetworkImage(imageUrl: prize.urlThumbnail,height: 200,
                fit: BoxFit.cover),
              ListTile(title: Text("${prize.lot} ${prize.name}"),subtitle: Text(prize.termsConditions))
            ],
          );
        });*/
  }
}
