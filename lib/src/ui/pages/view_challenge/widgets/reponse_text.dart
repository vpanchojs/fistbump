import 'package:flutter/material.dart';

class ResponseText extends StatefulWidget {
  @override
  _ResponseTextState createState() => _ResponseTextState();
}

class _ResponseTextState extends State<ResponseText> {
  final _formKey = GlobalKey<FormState>();
  String response;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*  appBar: AppBar(
        actions: <Widget>[
          IconButton(icon: Icon(Icons.done), onPressed: _saveResponseText)
        ],
      ),*/
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
          title:
              new Text("Participación", style: TextStyle(color: Colors.black)),
          actions: <Widget>[
            FlatButton(child: Text("Participar"), onPressed: _saveResponseText)
          ]),
      body: Container(
        margin: const EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: TextFormField(
            validator: (val) =>
                val.length < 5 ? 'Respuesta demaciado corta' : null,
            keyboardType: TextInputType.text,
            style: TextStyle(fontSize: 16.0),
            onSaved: (val) {
              response = val;
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(), labelText: "Escribe tu respuesta"),
            maxLines: 3,
          ),
        ),
      ),
    );
  }

  void _saveResponseText() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      // If the form is valid, display a Snackbar.
      Navigator.of(context).pop(["text", response]);
    }
  }
}
