import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/utils/share_widgets/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class InfoChallenge extends StatelessWidget {
  final MChallenge challenge;

  const InfoChallenge({Key key, this.challenge}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.all(14.0),
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            getDescription(challenge.description),
            SizedBox(height: 20),
            Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        getTitleSection("Participa con"),
                        SizedBox(height: 8),
                        iconParticipant(challenge.typeParticipation),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        getTitleSection("Gana por"),
                        SizedBox(height: 8),
                        iconWinnerBy(challenge.winnerBy),
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 10),
            (challenge.state == "a")
                ? getTitleSection("Finaliza el")
                : Container(),
            SizedBox(height: 8),
            getSubtitle(challenge.dateFinish),
          ],
        ),
      ),
    );
  }

  getSubtitle(DateTime data) {
    if (challenge.state == "e") {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("RETO FINALIZADO",
            style: GoogleFonts.montserrat(
                color: Colors.red[900],
                fontSize: 18,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.center),
      );
    }
    return DatePostFull(
        datePost: data,
        style: GoogleFonts.montserrat(
            fontSize: 14, fontWeight: FontWeight.normal));
  }

  getTitleSection(String data) {
    return Text(
      data,
      style: GoogleFonts.montserrat(fontSize: 14, fontWeight: FontWeight.bold),
      textAlign: TextAlign.start,
    );
  }

  getDescription(String data) {
    return Text(
      data,
      style: GoogleFonts.montserrat(fontSize: 16),
      textAlign: TextAlign.left,
    );
  }

  Widget iconParticipant(String typeParticipation) {
    String assetName = "";
    String descripcion = "";

    switch (typeParticipation) {
      case 'image':
        assetName = 'images/image_ic.svg';
        descripcion = "Foto";
        break;
      case 'video':
        assetName = 'images/video_ic.svg';
        descripcion = "Video";
        break;
      case 'text':
        assetName = 'images/texto_ic.svg';
        descripcion = "Texto";
        break;
    }

    return Row(
      children: <Widget>[
        SvgPicture.asset(
          assetName,
          height: 24,
          color: ColorsRes.purple_color,
        ),
        SizedBox(width: 8),
        Text(descripcion, style: GoogleFonts.montserrat(fontSize: 14))
      ],
    );
  }

  Widget iconWinnerBy(String typeParticipation) {
    String assetName = "";
    String description = "";

    switch (typeParticipation) {
      case 'like':
        assetName = 'images/apoyos_line.svg';
        description = "Apoyos";
        break;
      case 'random':
        assetName = 'images/aleatorio_line.svg';
        description = "Aleatorio";
        break;
    }

    return Row(
      children: <Widget>[
        SvgPicture.asset(
          assetName,
          height: 24,
          color: ColorsRes.purple_color,
        ),
        SizedBox(width: 8),
        Text(description, style: GoogleFonts.montserrat(fontSize: 14))
      ],
    );
  }
}
