import 'package:Darhu/src/bloc/creation_participation/util/arg_creation_participation.dart';
import 'package:Darhu/src/bloc/view_challenge/b_view_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/ui/pages/create_participation/v_create_participation.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class ParticipantWith extends StatelessWidget {
  final BViewChallenge viewChallengeBloc;
  final GlobalKey<ScaffoldState> scallKey;

  const ParticipantWith({Key key, this.viewChallengeBloc, this.scallKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 35,
        child: FloatingActionButton.extended(
          label: Text("Participar"),
          onPressed: () => navigationCreateParticipation(context),
          icon: Icon(Icons.add),
        ));
  }

  navigationCreateParticipation(context) {
    Navigator.pushNamed(context, VCreateParticipation.routeName,
            arguments: ArgCreationParticipation()
              ..challenge = viewChallengeBloc.challenge
              ..typeParticipation = MParticipation.getTypeParticipation(
                  viewChallengeBloc.challenge.typeParticipation))
        .then((data) {
          if(data!=null){
            BlocProvider.of<BViewChallenge>(context).myParticipation(data);            
          }
        })
        .catchError((e) {

        });
  }
}
