import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class UserChallenger extends StatelessWidget {
  final MUser user;

  const UserChallenger({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (user != null) {
      return SliverToBoxAdapter(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 0),
            leading: PhotoThumbnail(user: user, size: 50),
            title: Text(
              user.nameUser ?? "Nombre Usuario..",
              style: GoogleFonts.montserrat(
                  fontSize: 16, fontWeight: FontWeight.bold),
            ),
            subtitle: Text(user.name ?? "Nombre Real...",
                style: GoogleFonts.montserrat(
                    fontSize: 14, fontWeight: FontWeight.normal)),
            trailing: isVerifiedIcon(),
          ),
        ),
      );
    }
    return SliverToBoxAdapter();
  }

  isVerifiedIcon() {
    if (user.verified)
      return Tooltip(
        child: Icon(Icons.verified_user, color: Colors.green),
        message: "Usuario verificado por Darhu",
      );
    else
      return Icon(Icons.verified_user);
  }
}
