import 'package:Darhu/src/bloc/view_challenge/b_view_challenge.dart';
import 'package:Darhu/src/bloc/view_participation/p_view_participation.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/utils/share_widgets/list/SilverListGenericWigdet.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';

class ListParticipations extends StatefulWidget {
  final ScrollController scrollController;
  final BViewChallenge viewChallengeBloc;

  const ListParticipations(
      {Key key, this.scrollController, this.viewChallengeBloc})
      : super(key: key);

  @override
  _ListParticipationsState createState() => _ListParticipationsState();
}

class _ListParticipationsState extends State<ListParticipations> {
  @override
  void initState() {
    widget.scrollController.addListener(() {
      if (widget.scrollController.position.pixels ==
          widget.scrollController.position.maxScrollExtent) {
        widget.viewChallengeBloc.getMoreParticipations();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SilverListGenericWidget(
      stream: widget.viewChallengeBloc.outParticipationList,
      refresh: _refreshParticipations,
      noneData: string_message_none_participants,
      items: setItems,
    );
  }

  _refreshParticipations() async {
    await widget.viewChallengeBloc.getParticipations();
  }

  setItems(MParticipation item) {
    return ViewParticipationProvider(
      participation: item,
      challenge: widget.viewChallengeBloc.challenge,
      updateDateview: widget.viewChallengeBloc.updateDataListParticipation,
    );
  }
}
