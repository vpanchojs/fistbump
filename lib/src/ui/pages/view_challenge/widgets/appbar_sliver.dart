import 'package:Darhu/src/bloc/view_challenge/b_view_challenge.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/utils/share_widgets/multimedia/image/thumbnail_action.dart';
import 'package:Darhu/utils/share_widgets/share_platforms.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';

class AppbarSliver extends StatelessWidget {
  final MChallenge challenge;
  final BViewChallenge viewChallengeBloc;
  final bool shareActionOptions;

  const AppbarSliver(
      {Key key,
      this.challenge,
      this.viewChallengeBloc,
      this.shareActionOptions})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      pinned: true,
      expandedHeight: MediaQuery.of(context).size.height * 0.4,
      flexibleSpace: FlexibleSpaceBar(
        title: Text(challenge.name ?? "Nombre Reto"),
        centerTitle: true,
        background: getThumbnail(context),
      ),
      actions: <Widget>[
        actionShareOtherPlatforms(context),
        actionReportChallenge(context),
      ],
    );
  }

  _shareOtherPlatforms(String url, BuildContext context) {
    SharePlatforms.shareOtherPlatformsDynamicLinks(context, url);
  }

  actionShareOtherPlatforms(BuildContext context) {
    if (shareActionOptions != null && shareActionOptions == true) {
      return shareChallenge();
    }
    return Container();
  }

  shareChallenge() {
    return StreamBuilder<bool>(
        stream: viewChallengeBloc.outProgressShare,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return Align(
              alignment: FractionalOffset.center,
              child: Container(
                height: 24,
                margin: EdgeInsets.only(right: 8),
                width: 24,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor:
                      AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
                ),
              ),
            );
          } else {
            return IconButton(
                icon: Icon(Icons.share),
                onPressed: () => viewChallengeBloc.shareChallengePlatforms(
                    (url) => _shareOtherPlatforms(url, context)));
          }
        });
  }

  actionReportChallenge(BuildContext context) {
    if (shareActionOptions != null &&
        shareActionOptions == true &&
        !viewChallengeBloc.isMyPost(viewChallengeBloc.challenge.user.idUser)) {
      return IconButton(
          icon: Icon(Icons.report),
          onPressed: () => _showDialogReport(context));
    }
    return Container();
  }

  void _showDialogReport(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: new Text(string_label_report_challenge),
          children: <Widget>[
            SimpleDialogOption(
              child: Text(string_option_report_content_inappropriate),
              onPressed: () => reportChallenge("ci", context),
            ),
            SimpleDialogOption(
              child: Text(string_option_report_challenge_false),
              onPressed: () => reportChallenge("cf", context),
            ),
            SimpleDialogOption(
              child: Text(string_option_report_not_delivery),
              onPressed: () => reportChallenge("nd", context),
            ),
          ],
        );
      },
    );
  }

  reportChallenge(String code, BuildContext context) {
    viewChallengeBloc.reportChallenge(code);
    Navigator.pop(context);
  }

  Widget getThumbnail(BuildContext context) {
    return Stack(
      children: <Widget>[
        ThumbnailAction(
          actionTap: null,
          alto: double.maxFinite,
          typeMultimedia: challenge.typeMultimedia,
          urlMultimedia: challenge.urlMultimedia,
          urlThumbnail: challenge.urlThumbnail,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            color: Colors.white70,
            height: 60,
            width: double.maxFinite,
          ),
        )
      ],
    );
  }
}
