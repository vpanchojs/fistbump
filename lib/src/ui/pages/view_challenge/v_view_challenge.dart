import 'package:Darhu/src/bloc/view_challenge/b_view_challenge.dart';
import 'package:Darhu/src/ui/pages/view_challenge/widgets/appbar_sliver.dart';
import 'package:Darhu/src/ui/pages/view_challenge/widgets/list_participations.dart';
import 'package:Darhu/src/ui/pages/view_challenge/widgets/participant_with.dart';
import 'package:Darhu/src/ui/pages/view_challenge/widgets/user_challeger.dart';
import 'package:Darhu/src/bloc/view_participation/p_view_participation.dart';
import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/utils/share_widgets/button/button_reload.dart';
import 'package:Darhu/utils/share_widgets/progress/progress_custom.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'widgets/info_challenge.dart';
import 'widgets/prize_challenge.dart';

class VViewChallenge extends StatefulWidget {
  static const routeName="VViewChallenge";
  @override
  VViewChallengeState createState() => new VViewChallengeState();
}

class VViewChallengeState extends State<VViewChallenge> {
  BViewChallenge _viewChallengeBloc;
  final scaffKey = new GlobalKey<ScaffoldState>();
  final snackBar = SnackBar(
      content: Container(
        height: 25,
        child: Row(
          children: <Widget>[
            Expanded(child: Text(string_message_post_participation)),
            ProgressCustom(show: true)
          ],
        ),
      ),
      duration: Duration(minutes: 5));

  ScrollController scrollController;

  @override
  void initState() {
    _viewChallengeBloc = BlocProvider.of<BViewChallenge>(context);
    scrollController = new PageController();

    _viewChallengeBloc.outProgressInfo.listen((data) {
      if (data)
        scaffKey.currentState.showSnackBar(snackBar);
      else
        scaffKey.currentState.removeCurrentSnackBar();
    });

    _viewChallengeBloc.outView.listen((view) {
      view.run(context: context, key: scaffKey);
    });
    super.initState();
  }

  @override
  void dispose() {
    scrollController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffKey,
        body: containerMain(),
        floatingActionButton: containerParticipantWith());
  }

  containerMain() {
    return StreamBuilder<MChallenge>(
        stream: _viewChallengeBloc.outChallenge,
        builder: (context, snapshot) {
          if (snapshot.hasError)
            return containerInfoBasicError(_viewChallengeBloc.challenge);

          if (snapshot.connectionState == ConnectionState.waiting)
            return containerInfoBasicProgress(_viewChallengeBloc.challenge);

          if (snapshot.data.state == "e")
            return containerInfoCompleteEndWinner(snapshot.data);
          else if (snapshot.hasData) {
            return containerInfoComplete(snapshot.data);
          }
          return containerInfoBasicProgress(_viewChallengeBloc.challenge);
        });
  }

  containerMyParticipation() {
    return StreamBuilder<MParticipation>(
        stream: _viewChallengeBloc.outMyParticipation,
        builder: (context, snapshot) {
          if (snapshot.hasError) return SliverToBoxAdapter();

          if (ConnectionState.waiting == snapshot.connectionState) {
            return SliverToBoxAdapter();
          } else {
            if (snapshot.data.idParticipation != null) {
              return SliverToBoxAdapter(
                child: Column(
                  children: <Widget>[
                    dividerSection(_viewChallengeBloc.labelMyParticipation),
                    ViewParticipationProvider(
                      participation: snapshot.data,
                      challenge: _viewChallengeBloc.challenge,
                      updateDateview:
                          _viewChallengeBloc.updateDataMyParticipationView,
                    ),
                  ],
                ),
              );
            } else {
              return SliverToBoxAdapter();
            }
          }
        });
  }

  containerSelectParticipation() {
    return StreamBuilder<MParticipation>(
        stream: _viewChallengeBloc.outSelectParticipation,
        builder: (context, snapshot) {
          if (snapshot.hasError) return SliverToBoxAdapter();

          if (ConnectionState.waiting == snapshot.connectionState) {
            return SliverToBoxAdapter();
          } else {
            if (snapshot.data.idParticipation != null) {
              return SliverToBoxAdapter(
                child: Column(
                  children: <Widget>[
                    dividerSection(string_section_participation_select),
                    ViewParticipationProvider(
                      participation: snapshot.data,
                      challenge: _viewChallengeBloc.challenge,
                      updateDateview:
                          _viewChallengeBloc.updateDateParticipationSelect,
                    ),
                  ],
                ),
              );
            } else {
              return SliverToBoxAdapter();
            }
          }
        });
  }

  containerWinnerParticipation(MParticipation participation) {
    if (participation.user == null) {
      return SliverToBoxAdapter(
        child: Column(
          children: <Widget>[
            dividerSection(string_message_post_winner),
            Container(
                padding: const EdgeInsets.all(16),
                child: Text(
                  string_message_none_participants_winner,
                  textAlign: TextAlign.center,
                )),
          ],
        ),
      );
    }
    return SliverToBoxAdapter(
      child: Column(
        children: <Widget>[
          dividerSection(string_message_post_winner),
          ViewParticipationProvider(
            participation: participation,
            challenge: _viewChallengeBloc.challenge,
            updateDateview: () {},
          ),
        ],
      ),
    );
  }

  /*
  * Solo muestra informacion basica del reto,  proveniente de publicaciones.
  *   Miniatura
  *   Nombre
  *   Usuario creador.
  * */
  containerInfoBasicError(MChallenge challenge) {
    return CustomScrollView(
      controller: scrollController,
      slivers: <Widget>[
        AppbarSliver(
            challenge: challenge, viewChallengeBloc: _viewChallengeBloc),
        UserChallenger(user: _viewChallengeBloc.challenge.user),
        SliverToBoxAdapter(
          child: Column(
            children: <Widget>[
              ButtonReload(refresh: _viewChallengeBloc.getFulChallenge),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(string_error_challenge_delete,
                    textAlign: TextAlign.center),
              )
            ],
          ),
        )
      ],
    );
  }

  /*
  * Solo muestra informacion basica del reto,  proveniente de publicaciones.
  *   Miniatura
  *   Nombre
  *   Usuario creador.
  * */
  containerInfoBasicProgress(MChallenge challenge) {
    return CustomScrollView(
      controller: scrollController,
      slivers: <Widget>[
        AppbarSliver(
            challenge: challenge, viewChallengeBloc: _viewChallengeBloc),
        UserChallenger(user: _viewChallengeBloc.challenge.user),
        SliverToBoxAdapter(
            child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: CircularProgressIndicator(),
            ),
          ],
        ))
      ],
    );
  }

  containerInfoComplete(MChallenge challenge) {
    return CustomScrollView(
      controller: scrollController,
      slivers: <Widget>[
        AppbarSliver(
          challenge: challenge,
          viewChallengeBloc: _viewChallengeBloc,
          shareActionOptions: true,
        ),
        UserChallenger(user: challenge.user),
        InfoChallenge(challenge: challenge),
        PrizeChallenge(
            prize: challenge.prize,
            withPrize: challenge.withPrize,
            userChallenge: _viewChallengeBloc.getRolUserInChallenge(),
            challenge: challenge),
        containerSelectParticipation(),
        containerMyParticipation(),
        SliverToBoxAdapter(
            child: dividerSection(string_section_participantions +
                " (${challenge.numParticipantions})")),
        ListParticipations(
            scrollController: scrollController,
            viewChallengeBloc: _viewChallengeBloc),
        /*    PParticipations(
          challenge: challenge,
          //progressInfo: _viewChallengeBloc.progress,
          scrollController: scrollController,
        ),*/
        progressSliver()
      ],
    );
  }

  containerInfoCompleteEndWinner(MChallenge challenge) {
    return CustomScrollView(
      controller: scrollController,
      slivers: <Widget>[
        AppbarSliver(
            challenge: challenge,
            viewChallengeBloc: _viewChallengeBloc,
            shareActionOptions: true),
        UserChallenger(user: challenge.user),
        InfoChallenge(challenge: challenge),
        PrizeChallenge(
            prize: challenge.prize,
            withPrize: challenge.withPrize,
            userChallenge: _viewChallengeBloc.getRolUserInChallenge(),
            challenge: challenge),
        containerWinnerParticipation(challenge.winner),
      ],
    );
  }

  dividerSection(String title) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      color: Colors.black12,
      height: 40,
      child: Align(child: Text(title), alignment: Alignment.centerLeft),
    );
  }

  containerParticipantWith() {
    return StreamBuilder<bool>(
        stream: _viewChallengeBloc.outShow,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return ParticipantWith(
                viewChallengeBloc: _viewChallengeBloc, scallKey: scaffKey);
          }
          return Container(height: 0);
        });
  }

  progressSliver() {
    return StreamBuilder<bool>(
        stream: _viewChallengeBloc.outProgressMore,
        initialData: false,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          return SliverToBoxAdapter(
            child: Align(
              alignment: Alignment.center,
              child: (snapshot.data)
                  ? SizedBox(
                      height: 14,
                      width: 14,
                      child: CircularProgressIndicator(strokeWidth: 2))
                  : Container(height: 60),
            ),
          );
        });
  }
}
