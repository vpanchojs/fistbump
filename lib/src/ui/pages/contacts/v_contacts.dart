import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/bloc/contacts/b_contacts.dart';
import 'package:Darhu/src/ui/pages/contacts/widgets/w_item_contact.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/utils/share_widgets/list/ListGenericWidget.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class VContacts extends StatefulWidget {
  static const routeName="vcontacts";
  @override
  VContactsState createState() {
    return new VContactsState();
  }
}

class VContactsState extends State<VContacts> {
  BContacts _contactsBloc;
  final scrollController = new ScrollController();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: appBar(),
      body: _listContacts(),
    );
  }

  Widget appBar() {
    return AppBar(
      title: Text((_contactsBloc.type == 0) ? "Seguidores" : "Siguiendo",
          style: StyleRes.text_appbar),
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
    );
  }

  @override
  void initState() {
    _contactsBloc = BlocProvider.of<BContacts>(context);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        _contactsBloc.getFollowMore();
      }
    });
    _contactsBloc.outView.listen((view) {
      view.run(context: context, key: scaffoldKey);      
    });
    super.initState();
  }

  Future<void> _refresh() async {
    await _contactsBloc.getContacts();
  }

  setItems(MUser user) {
    return WItemContact(
      user: user,
      isDelete: _contactsBloc.isDelete(),
      delete: _contactsBloc.removeContact,
      progressButton: _contactsBloc.outProgressActionList,
    );
  }

  _listContacts() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 16.0),
      child: ListGenericWidget(
        progress: _contactsBloc.outProgressMore,
        stream: _contactsBloc.outUsersList,
        refresh: _refresh,
        itemWidget: setItems,
        noneData:
            "Sin ${((_contactsBloc.type == 0) ? "Seguidores" : "Seguidos")}",
        scrollController: scrollController,
      ),
    );
  }
}
