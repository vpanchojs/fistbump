import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:Darhu/src/res/style_res.dart';
import 'package:Darhu/src/ui/pages/view_profile/v_view_profile.dart';
import 'package:Darhu/utils/share_widgets/item_user/photo_thumbnail.dart';
import 'package:Darhu/utils/share_widgets/progress/progress_custom.dart';
import 'package:flutter/material.dart';

class WItemContact extends StatelessWidget {
  final MUser user;
  final bool isDelete;
  final dynamic delete;
  final Stream progressButton;

  const WItemContact(
      {Key key, this.user, this.isDelete, this.delete, this.progressButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.blue,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _contact(context),
          _containerButton(),
        ],
      ),
    );
  }

  Widget _contact(BuildContext context) {
    return Expanded(
      child: ListTile(
        contentPadding: const EdgeInsets.all(0),
        title: Text(
          user.nameUser,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        subtitle: Text(
          user.name ?? "Nombre Completo",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        leading: PhotoThumbnail(user: user, size: 45),
        onTap: () {
          Navigator.pushNamed(context, VViewProfile.routeName,arguments: user);
          /*
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => PViewProfile(
                        user: user,
                      )));

           */
        },
      ),
    );
  }

  Widget _containerButton() {
    return Offstage(
      offstage: !isDelete,
      child: Container(
        margin: EdgeInsets.only(right: 8.0),
        height: 30,
        child: _button(),
      ),
    );
  }

  Widget _button() {
    return StreamBuilder<List<String>>(
        stream: progressButton,
        initialData: List(),
        builder: (context, AsyncSnapshot<List<String>> snapshot) {
          if (snapshot.data.contains(user.idUser)) {
            return ProgressCustom(show: true);
          }

          return ActionChip(
              labelStyle: StyleRes.text_button_chip_white,
              labelPadding:
                  const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              backgroundColor: ColorsRes.purple_color,
              label: Text("Eliminar"),
              onPressed: () {
                delete(user.idUser);
              });
        });
  }
}
