import 'package:Darhu/src/bloc/list_participations/b_list_participations.dart';
import 'package:Darhu/src/bloc/view_participation/p_view_participation.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/utils/share_widgets/list/SilverListGenericWigdet.dart';
import 'package:Darhu/utils/bloc/bloc_provider.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:flutter/material.dart';

class VListParticipations extends StatefulWidget {
  final ScrollController scrollController;

  const VListParticipations({Key key, this.scrollController}) : super(key: key);

  @override
  _VListParticipationsState createState() => _VListParticipationsState();
}

class _VListParticipationsState extends State<VListParticipations> {
  BListParticipations _bparticipations;

  @override
  void initState() {
    _bparticipations = BlocProvider.of<BListParticipations>(context);
    widget.scrollController.addListener(() {
      if (widget.scrollController.position.pixels ==
          widget.scrollController.position.maxScrollExtent) {
        _bparticipations.getMoreParticipations();
      }
    });

    _bparticipations.outView.listen((view) {
      view.run(context: context, key: null);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SilverListGenericWidget(
      stream: _bparticipations.outParticipationList,
      refresh: _refreshParticipations,
      noneData: string_message_none_participants,
      items: setItems,
    );
  }

  _refreshParticipations() async {
    await _bparticipations.getParticipations();
  }

  setItems(MParticipation item) {
    return ViewParticipationProvider(
      participation: item,
      challenge: _bparticipations.challenge,
      updateDateview: _bparticipations.updateDataListParticipation,
    );
  }
}
