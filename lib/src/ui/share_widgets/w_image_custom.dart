import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class WImageCustom extends StatelessWidget {
  final double heigth;
  final double width;
  final dynamic image;
  const WImageCustom(
      {Key key, this.heigth = 50, this.width = 50, @required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: containerImage(),
    );
  }

  containerImage() {
    if (image is String) {
      return ClipRRect(
        borderRadius: new BorderRadius.circular(12.0),
        child: CachedNetworkImage(
          imageUrl: image,
          placeholder: (context, url) {
            return Container(
              height: heigth,
              width: width,
            );
          },
          width: width,
          height: heigth,
          fit: BoxFit.cover,
          alignment: FractionalOffset.center,
          errorWidget: (context, url, error) {
            return Container(
              height: heigth,
              width: width,
              child: Center(
                  child: Icon(
                Icons.error,
                color: Colors.black38,
              )),
            );
          },
        ),
      );
    }
    return Container(
      height: heigth,
      width: width,
      child: ClipRRect(
          borderRadius: new BorderRadius.circular(12.0),
          child: Image.file(
            image,
            fit: BoxFit.cover,
            width: double.maxFinite,
          )),
    );
  }
}
