import 'package:flutter/material.dart';

class WInputTextStream extends StatelessWidget {
  final dynamic outData;
  final dynamic inData;
  final String hint;
  final String label;
  final int maxLines;
  final FocusNode myFocusNode;
  final TextEditingController controller = TextEditingController();

  WInputTextStream(
      {Key key,
      @required this.outData,
      @required this.inData,
      this.hint,
      @required this.label,
      this.maxLines = 1,
      this.myFocusNode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: outData,
      builder: (context, snapshot) {
        if (snapshot.hasData && controller.text.length <= 0) {
          controller.text = snapshot.data.toString();
        }
        return TextFormField(
          onChanged: inData,
          focusNode: myFocusNode,
          controller: controller,
          keyboardType: TextInputType.text,
          maxLines: maxLines,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: label,
            hintText: hint,
            errorText: snapshot.error,
          ),
        );
      },
    );
  }
}
