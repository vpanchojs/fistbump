import 'package:flutter/material.dart';

/*
* Clase para comunicacion de acciones de exito o error hacia la vista
* */
class MActionView {
  String message; //Mensaje
  ActionEnum action;
  dynamic data; // Otro conjunto de valores
  dynamic idAction;
  String routeName;
  dynamic arguments;

  MActionView();

  MActionView.push({this.routeName}) : action = ActionEnum.PUSH;

  MActionView.pushData({this.routeName, this.arguments})
      : action = ActionEnum.PUSHDATA;

  MActionView.pop({this.routeName}) : action = ActionEnum.POP;

  MActionView.popUntil({this.routeName}) : action = ActionEnum.POP_UNTIL;

  MActionView.popAndPush({this.routeName}) : action = ActionEnum.POP_PUSH;

  MActionView.popAndPushData({this.routeName, this.arguments})
      : action = ActionEnum.POP_PUSH_DATA;

  MActionView.popData({this.arguments})
      : action = ActionEnum.POPDATA;

  MActionView.error({this.message}) : action = ActionEnum.ERROR;

  MActionView.succes({this.message}) : action = ActionEnum.SUCCESS;

  static showSnackBar(String msg, GlobalKey<ScaffoldState> scaffoldKey) {
    try {
      scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(msg),
        duration: new Duration(seconds: 5),
      ));
    } catch (e) {}
  }

  run(
      {@required BuildContext context,
      @required GlobalKey<ScaffoldState> key}) {
    switch (action) {
      case ActionEnum.PUSHDATA:
        navigationPushArguments(context, routeName, arguments);
        break;
      case ActionEnum.PUSH:
        navigationPush(context, routeName);
        break;
      case ActionEnum.POP:
        navigationPop(context);
        break;
      case ActionEnum.ERROR:
        showMessageError(context, key, message);
        break;
      case ActionEnum.SUCCESS:
        showMessageSucces(context, key, message);
        break;
      case ActionEnum.POPDATA:
        navigationPopData(context, arguments);
        break;
      case ActionEnum.POP_PUSH:
        break;
      case ActionEnum.PUSH_REMPLACEMENT:
        navigationPushAndRemplace(context, routeName);
        break;
      case ActionEnum.PUSH_REMOVE:
        navigationPushAndRemplace(context, routeName);
        break;
      case ActionEnum.POP_PUSH_DATA:
        navigationPopAndPushData(context, routeName, arguments);
        break;
      case ActionEnum.POP_UNTIL:
        navigationPopUntil(context, routeName);
        break;
    }
  }

  navigationPopUntil(BuildContext context, String routeName) {
    Navigator.of(context).popUntil(ModalRoute.withName(routeName));
  }

  navigationPopAndPush(BuildContext context, String routeName) {
    Navigator.of(context).popAndPushNamed(routeName);
  }

  navigationPopAndPushData(
      BuildContext context, String routeName, dynamic arguments) {
    Navigator.of(context).popAndPushNamed(routeName, arguments: arguments);
  }

  navigationPushAndRemove(BuildContext context, String routeName) {
    Navigator.of(context).pushReplacementNamed(routeName);
  }

  navigationPushAndRemplace(BuildContext context, String routeName) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false);
  }

  navigationPush(BuildContext context, String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }

  navigationPushArguments(
      BuildContext context, String routeName, dynamic arguments) {
    Navigator.of(context).pushNamed(routeName, arguments: arguments);
  }

  navigationPop(BuildContext context) {
    Navigator.of(context).pop();
  }

  navigationPopData(BuildContext context, dynamic arguments) {
    Navigator.of(context).pop(arguments);
  }

  showMessageSucces(
      BuildContext context, GlobalKey<ScaffoldState> key, String message) {
    key.currentState.showSnackBar(new SnackBar(
      content: new Text(message),
      duration: new Duration(seconds: 3),
    ));
  }

  showMessageError(
      BuildContext context, GlobalKey<ScaffoldState> key, String message) {
    if (message != null && message.trim().isNotEmpty) {
      key.currentState.showSnackBar(new SnackBar(
        content: new Text(message),
        duration: new Duration(seconds: 3),
      ));
    }
  }
}

enum ActionEnum {
  PUSH,
  POP,
  POP_PUSH,
  ERROR,
  SUCCESS,
  POPDATA,
  PUSHDATA,
  PUSH_REMPLACEMENT,
  PUSH_REMOVE,
  POP_PUSH_DATA,
  POP_UNTIL
}
