import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_prize.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'm_multimedia.dart';

class MChallenge {
  String idChallenge;
  String name;
  String description;
  MMultimedia multimedia;
  String urlMultimedia;
  String pathMultimedia;
  String pathThumbnail;
  String typeMultimedia;
  bool withPrize=false;
  Prize prize;
  String state = 'a';
  String winnerBy;
  String typeParticipation;
  MParticipation winner;

  /*  List<String> _optionsParticipation = <String>['Foto', image 'Video', video 'Texto', text ];*/
  DateTime dateInit;
  List<dynamic> keyWords;
  DateTime dateFinish;
  DateTime datePost;
  int numParticipantions;
  bool available;
  String urlThumbnail;
  MUser user;

  ///
  ///
  /// Map del reto, para el envio de datos
  Map<String, dynamic> fullChallenge() {
    var map = Map<String, dynamic>();
    map['idChallenge'] = idChallenge;
    map['name'] = name;
    map['description'] = description;
    map['urlMultimedia'] = urlMultimedia;
    map['typeMultimedia'] = typeMultimedia;
    map['typeParticipation'] = typeParticipation;
    map['dateFinish'] = dateFinish.toString();
    map["pathMultimedia"]=pathMultimedia;
    map["withPrize"] = withPrize;
    map['state'] = state;
    map["winnerBy"] = winnerBy;
    if (withPrize) {
      map["prize"] = prize.prizeFull();
    }
    return map;
  }

  //Este metodo esta funcionado
  Map<String, dynamic> minChallengeMap() {
    var map = Map<String, dynamic>();
    map['idChallenge'] = idChallenge;
    map['name'] = name;
    return map;
  }

  //Metodo funcionando
  MChallenge getFullFromDateSnapshot(Map doc, String id) {
    idChallenge = id;
    name = doc['name'];
    description = doc['description'];
    urlMultimedia = doc['urlMultimedia'];
    urlThumbnail = doc["thumbail"];
    typeMultimedia = doc['typeMultimedia'];
    typeParticipation = doc['typeParticipation'];
    dateInit = doc['dateInit']?.toDate();
    dateFinish = doc['dateFinish']?.toDate();
    datePost = doc['datePost']?.toDate();
    numParticipantions = doc['numParticipantions'] ?? 0;
    user = MUser.min(doc['user']);
    state= doc["state"]??"f";
    withPrize=doc["withPrize"]??false;
    //prize = doc["prize"];
    winnerBy = doc["winnerBy"];
    return this;
  }

  MChallenge();

  /*
  * Constructor para crear un reto completo.
  * @Param doc, mapa con la información del reto.
  * @Param id, identificador del reto.
  * */
  MChallenge.complete(Map doc, String id) {
    idChallenge = id;
    name = doc['name'];
    description = doc['description'];
    urlMultimedia = doc['urlMultimedia'];
    urlThumbnail = doc["thumbail"];
    typeMultimedia = doc['typeMultimedia'];
    typeParticipation = doc['typeParticipation'];
    dateInit = doc['dateInit']?.toDate();
    dateFinish = doc['dateFinish']?.toDate();
    datePost = doc['datePost']?.toDate();
    numParticipantions = doc['numParticipantions'] ?? 0;
    user = MUser.min(doc['user']);
    state = doc["state"];
    withPrize = doc["withPrize"]??false;
    if (withPrize) prize = Prize.challenge(doc["prize"]);
    winnerBy = doc["winnerBy"];
    winner = MParticipation.winner(doc: doc["winner"]);
  }

  /*
  * Constructor para crear un reto que va dentro de la participacion
  * @Param doc, mapa con la información del reto
  * */
  MChallenge.inParticipation(Map doc) {
    idChallenge = doc["idChallenge"];
    name = doc['name'];
    urlThumbnail = doc["thumbail"];
    state = doc["state"] ?? "a";
  }

  /*
  * Constructor para crear un reto que sera mostrado en el newsFeed
  * @Param doc, mapa con la información del reto
  * */
  MChallenge.inNewsFeed(Map doc) {
    idChallenge = doc["idChallenge"];
    name = doc['name'];
    description = doc['description'];
    urlThumbnail = doc["thumbail"];
    typeMultimedia = doc['typeMultimedia'];
    urlMultimedia = doc['urlMultimedia'];
    dateFinish = doc['dateFinish']?.toDate();
    datePost = doc['datePost']?.toDate();
    user = MUser.minPost(doc['user']);
    winnerBy = doc["winnerBy"];
    state = doc["state"] ?? "a";
  }

  /*
  * Constructor para crear un reto que sera mostrado en el newsFeed
  * @Param doc, mapa con la información del reto
  * */
  MChallenge.inNotification(Map doc) {
    idChallenge = doc["idChallenge"];
    name = doc['name'];
    urlThumbnail = doc["thumbail"];
    typeMultimedia = doc['typeMultimedia'];
    urlMultimedia = doc['urlMultimedia'];
    state = doc["state"] ?? "a";
  }

/*
  getTypesParticipation(List<dynamic> typesParticipations) {
    var list = List<String>();
    typesParticipations.forEach((i) {
      list.add(i.toString());
    });
    return list;
  }
  */

  /*Constructor para iniciarle un reto para ser mostrado en el perfil de usuario*/
  MChallenge.profileUser(Map doc, MUser user) {
    idChallenge = doc["idChallenge"];
    name = doc['name'];
    description = doc['description'];
    urlMultimedia = doc['urlMultimedia'];
    urlThumbnail = doc["thumbail"];
    typeMultimedia = doc['typeMultimedia'];
    typeParticipation = doc["typeParticipation"];
    winnerBy = doc["winnerBy"];
    datePost = doc['datePost']?.toDate();
    dateFinish = doc["dateFinish"]?.toDate();
    this.user = user;
  }

  ///Reto cuando se crea el reto.
  MChallenge getMinFromDateSnapshotJson(Map doc, String id) {
    idChallenge = id;
    name = doc['name'];
    description = doc['description'];
    urlMultimedia = doc['urlMultimedia'];
    urlThumbnail = doc["thumbail"];
    typeMultimedia = doc['typeMultimedia'];
    typeParticipation = doc["typeParticipation"];
    winnerBy = doc["winnerBy"];
    withPrize = doc["withPrize"]??false;
    state = doc["state"] ?? "a";
    return this;
  }

  /*
  * Actualiza toda la informacion del reto.
  * */

  updateData(MChallenge c) {
    idChallenge = c.idChallenge;
    name = c.name;
    description = c.description;
    typeMultimedia = c.typeMultimedia;
    urlMultimedia = c.urlMultimedia;
    pathMultimedia = c.pathMultimedia;
    pathThumbnail = c.pathThumbnail;
    urlThumbnail = c.urlThumbnail;
    typeParticipation = c.typeParticipation;
    winnerBy = c.winnerBy;
    datePost = c.datePost;
    dateFinish = c.dateFinish;
    dateInit = c.dateInit;
    numParticipantions = c.numParticipantions;
    user = c.user;
    state = c.state;
    winner = c.winner;
    withPrize=c.withPrize;
    prize=c.prize;
  }

  Map<String, dynamic> getSearchMap(){
    return {
      'name':name,
      'description' : description,
      'thumbail':multimedia.thumbnail.item,
      'typeParticipation': typeParticipation,
      'withPrize':withPrize,
      'winnerBy':winnerBy,
      'numParticipantions':0,
      'datePost':FieldValue.serverTimestamp(),
      'state':"a"
    };
  }

  Map<String, dynamic> getFullMap(){
    return {
      'name':name,
      'description' : description,
      'urlMultimedia':multimedia.data.item,
      'thumbail':multimedia.thumbnail.item,
      'pathThumbnail':multimedia.thumbnail.pathOrigin,
      'pathMultimedia':multimedia.data.pathOrigin,
      'typeMultimedia': multimedia.data.type.toString(),
      'typeParticipation': typeParticipation,
      'withPrize':withPrize,
      'prize': withPrize? prize.getMinPrizeMap():null,
      'winnerBy':winnerBy,
      'numParticipantions':0,
      'dateFinish': dateFinish,
      'user':user.userMin(),
      'datePost':FieldValue.serverTimestamp(),
      'state':"a"
    };
  }
}
