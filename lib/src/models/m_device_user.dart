import 'package:cloud_firestore/cloud_firestore.dart';

class MDeviceUser {
  String token;
  String idDevice;
  String dateSignIn;
  String name;
  String so;

  Map<String, dynamic> deviceUserMap() {
    var map = Map<String, dynamic>();
    map['token'] = token;
  //  map['idDevice'] = idDevice;
    map['name'] = name;
    map['dateSignIn'] = FieldValue.serverTimestamp();
    map['so'] = so;
    return map;
  }
}