import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';

class MNews {
  String idNews;
  MUser user;
  int type; //indica si es de tipo nuevo reto (0) o participacion (1)
  MChallenge challenge;
  MParticipation participation;
  DateTime datePost;

  /*
  * Constructor para inicializar una noticia de tipo participacion
  * @Param doc, contiene toda la informacion
  * @Param id, identifcador de la noticia
  * */

  MNews.isParticipation(Map doc, String id) {
    idNews = id;
    participation =
        MParticipation.inNewsFeed(doc["participation"]);
    type = doc['type'];
    datePost = doc["datePost"].toDate();
  }

  /*
  * Constructor para inicializar una noticia de tipo nuevo reto
  * @Param doc, contiene toda la informacion
  * @Param id, identifcador de la noticia
  * */

  MNews.newChallenge(Map doc, String id) {
    idNews = id;
    challenge = MChallenge.inNewsFeed(doc["challenge"]);
    type = doc['type'];
    datePost = doc["datePost"].toDate();
  }
}
