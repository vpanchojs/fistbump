import 'package:Darhu/src/models/m_multimedia.dart';

class MItemMultimedia<T>{
  T item;
  String pathOrigin;
  TypeMultimedia type;

  MItemMultimedia.image({this.item}):type=TypeMultimedia.IMAGE;
  MItemMultimedia.video({this.item}):type=TypeMultimedia.VIDEO;

  MItemMultimedia.imageUrl({this.item,this.pathOrigin}):type=TypeMultimedia.IMAGE;
  MItemMultimedia.videoUrl({this.item,this.pathOrigin}):type=TypeMultimedia.VIDEO;

}