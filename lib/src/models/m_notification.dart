import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';

class MNotification {
  String idNotification;
  DateTime datePost;
  int type;
  MChallenge challenge;
  MUser user;
  MParticipation participation;

  MNotification.isNewParticipation(Map doc, String id) {
    idNotification = id;
    participation = MParticipation.inNotification(doc["participation"]);
    challenge = MChallenge.inNotification(doc["challenge"]);
    type = doc['type'];
    user = MUser.min(doc['user']);
    datePost = doc["datePost"].toDate();
  }

  MNotification.isNewChallenge(Map doc, String id) {
    idNotification = id;
    challenge = MChallenge.inNotification(doc["challenge"]);
    type = doc['type'];
    user = MUser.min(doc['user']);
    datePost = doc["datePost"].toDate();
  }

  MNotification();
}