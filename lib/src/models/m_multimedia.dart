import 'package:Darhu/src/models/m_item_multimedia.dart';

class MMultimedia {
  MItemMultimedia data;
  MItemMultimedia thumbnail;
  OriginMultimedia origin;

  MMultimedia.remote(this.data,this.thumbnail):origin=OriginMultimedia.REMOTE;
  MMultimedia.local(this.data,this.thumbnail):origin=OriginMultimedia.LOCAL;
  MMultimedia.empty():origin=OriginMultimedia.EMPTY;
}

enum TypeMultimedia { IMAGE, VIDEO, EMPTY, ALL }
enum OriginMultimedia { LOCAL, REMOTE,EMPTY}
