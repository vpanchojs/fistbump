import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_user.dart';

import 'm_challenge.dart';

class MParticipation {
  String idParticipation;
  String type;

  /// text description or URL multimedia, depends on "typeParticipation"
  String response;
  String description; //Descripcion
  DateTime datePost;
  int numLikes;
  MUser user;
  bool liked;
  bool winner = false;
  String pathMultimedia;
  MChallenge challenge;
  String thumbnail;
  TypeParticipation typeParticipation;
  MMultimedia multimedia;

  MParticipation();

  /*
  * Constructor para inicializar my participacion en un reto.
  * @param doc, contiene toda la data.
  * @param id, identificador de la participacion
  * */
  MParticipation.myParticipation(Map doc, String id) {
    idParticipation = id;
    type = doc['type'];
    response = doc['response'];
    datePost =
        (doc['datePost'] == null) ? DateTime.now() : doc['datePost'].toDate();
    thumbnail = doc["thumbnail"] ?? "";
    numLikes = (doc['numLikes'] == null) ? 0 : doc['numLikes'];
    user = MUser.min(doc['user']);
    winner = doc["winner"] ?? false;
  }

  /*
  * Constructor para inicializar una participacion para ser visualizada en el perfil
  * @param doc, contiene toda la data.
  * @param id, identificador de la participacion
  * */

  MParticipation.inProfile(Map doc, String id) {
    idParticipation = id;
    type = doc['type'];
    response = doc['response'];
    datePost = doc['datePost']?.toDate();
    thumbnail = doc["thumbnail"] ?? "";
    numLikes = (doc['numLikes'] == null) ? 0 : doc['numLikes'];
    user = MUser.min(doc['user']);
    winner = doc["winner"] ?? false;
    challenge = MChallenge.inParticipation(doc["challenge"]);
  }

  /*
  * Constructor para inicializar una participacion para ser visualizada en el newsFeed
  * @param doc, contiene toda la data.
  * */

  MParticipation.inNewsFeed(Map doc) {
    idParticipation = doc["idParticipation"];
    type = doc['type'];
    response = doc['response'];
    thumbnail = doc["thumbnail"] ?? "";
    datePost = doc['datePost']?.toDate();
    user = MUser.min(doc['user']);
    challenge = MChallenge.inParticipation(doc["challenge"]);
    winner = doc["winner"] ?? false;
  }

  /*
  * Constructor para inicializar una participacion para ser visualizada en el newsFeed
  * @param doc, contiene toda la data.
  * */

  MParticipation.inNotification(Map doc) {
    idParticipation = doc["idParticipation"];
    winner = doc["winner"] ?? false;
  }

/*
  * Constructor para inicializar una participacion y asignar el apoyo.
  * @param doc, contiene toda la data.
  * @param id, identificador de la participacion
  * @param liked, bandera que indica si tiene o no mi apoyo.
  * */

  MParticipation.completeIsLiked(Map doc, String id, bool liked) {
    idParticipation = id;
    type = doc['type'];
    response = doc['response'];
    datePost = doc['datePost']?.toDate();
    thumbnail = doc["thumbnail"] ?? "";
    numLikes = (doc['numLikes'] == null) ? 0 : doc['numLikes'];
    user = MUser.min(doc['user']);
    winner = doc["winner"] ?? false;
    this.liked = liked;
  }

  //Metodo encargado  de retornar una mapa con datos basicos de la participacion
  Map<String, dynamic> minParticipationMap() {
    var map = Map<String, dynamic>();
    map['idParticipation'] = idParticipation;
    map['type'] = getTypeParticipationValue();
    map['response'] = multimedia.data.item;
    map['pathMultimedia'] = multimedia.data.pathOrigin;
    map['pathThumbnail'] = multimedia.thumbnail.pathOrigin;
    map['thumbnail'] = multimedia.thumbnail.item;
    map['description'] = description ?? "";
    return map;
  }

  /*
  * Constructor para instanciar la participacion ganadora de un reto
  * @param doc, contiene toda la data.
  * */
  MParticipation.winner({Map doc}) {
    if (doc != null) {
      idParticipation = doc['idParticipation'];
      type = doc['type'];
      numLikes = doc["numLikes"];
      response = doc['response'];
      thumbnail = doc["thumbnail"] ?? "";
      datePost = doc['datePost']?.toDate();
      user = MUser.min(doc['user']);
    }
  }

  static getTypeParticipation(String type) {
    switch (type) {
      case 'image':
        return TypeParticipation.IMAGE;
      case 'video':
        return TypeParticipation.VIDEO;
      case 'text':
        return TypeParticipation.TEXT;
    }
  }

  getTypeParticipationValue() {
    switch (typeParticipation) {
      case TypeParticipation.IMAGE:
        return "image";
      case TypeParticipation.VIDEO:
        return 'video';
      case TypeParticipation.TEXT:
        return "text";
    }
  }
}

enum TypeParticipation { IMAGE, VIDEO, TEXT }
