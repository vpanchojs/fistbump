import 'package:Darhu/src/models/m_user.dart';

import 'm_multimedia.dart';

class Prize {
  String idPrize;
  String urlMultimedia = "";
  String urlThumbnail;
  MMultimedia multimedia;
  String name;
  int lot;
  String termsConditions;
  String pathMultimedia;
  WinnerCode winnerCode;
  bool delivery;

  Prize();

  Prize.complete(Map data, this.idPrize) {
    urlMultimedia = data["urlMultimedia"];
    urlThumbnail = data["urlThumbnail"];
    name = data["name"];
    lot = data["lot"];
    termsConditions = data["termsConditions"];
    winnerCode = WinnerCode.complete(data["winnerCode"]);
    delivery = data["delivery"] ?? false;
  }

  Prize.challenge(Map data) {
    urlThumbnail = data["urlThumbnail"];
    name = data["name"];
    lot = data["lot"];
  }

  Map<String, dynamic> prizeFull() {
    var map = Map<String, dynamic>();
    map['urlMultimedia'] = urlMultimedia;
    map['name'] = name;
    map['lot'] = lot;
    map['termsConditions'] = termsConditions;
    map["pathMultimedia"] = pathMultimedia;
    return map;
  }

  Map<String,dynamic> getPrizeMap(){
    if(multimedia!=null && multimedia.data!=null && multimedia.origin!=OriginMultimedia.EMPTY) {
      return {
        'name':name,
        'lot':lot,
        'termsConditions':termsConditions,
        'urlMultimedia':multimedia.data.item,
        'pathMultimedia':multimedia.data.pathOrigin,
        'urlThumbnail':multimedia.thumbnail.item,
        'pathThumbnail':multimedia.thumbnail.pathOrigin,
        'position':1, // considera enviar la posicion para futuras actualizaciones cuando existan varios premios y posiciones.
      };
    }else{
      return {
        'name':name,
        'lot':lot,
        'termsConditions':termsConditions,
        'position':1, // considera enviar la posicion para futuras actualizaciones cuando existan varios premios y posiciones.
      };
    }
  }
  Map<String,dynamic> getMinPrizeMap(){
    return {
      'name':name,
      'lot':lot,      
      'urlThumbnail':multimedia.thumbnail.item      
    };
  }
}




class WinnerCode {
  String code;
  dynamic refParticipation;
  MUser user;

  WinnerCode.complete(Map data) {
    print("winner code ${data.toString()}");
    if (data != null) {
      code = data["code"] ?? "";
      refParticipation = data["refParticipation"];
    }
  }
}
