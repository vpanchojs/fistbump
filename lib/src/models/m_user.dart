import 'package:firebase_auth/firebase_auth.dart';
import 'package:Darhu/src/models/m_challenge.dart';

class MUser {
  String name;
  String urlPhoto;
  String idUser;
  String nameUser;
  String cellPhone;
  DateTime birthday;
  int gender = 0;
  int numParticipations = 0;
  int numChallenges = 0;
  int numFollowers = 0;
  int numFolloweds = 0;
  bool verified = false;
  List<MChallenge> challengesActive = List();
  int type = 0;
  List<String> keywords;

  @override
  String toString() {
    return "nombre: $name idUser: $idUser";
  }

  ///Constructor para inicializar un usuario con todos los datos.
  ///@Param data, contiene la informacion completa del usuario a excepcion del identificador
  ///@Param id, identificador del usuario
  MUser.completeId(Map<String, dynamic> data, String id) {
    idUser = id;
    nameUser = data["nameUser"] ?? "";
    urlPhoto = data["thumbnail"];
    name = data["name"] ?? "";
    cellPhone = data["cellPhone"];
    gender = data["gender"] ?? 1;
    birthday = data["birthday"]?.toDate();
    verified=data["verified"]??false;
  }

  //esta funcionando correctamente
  MUser.firebaseUser(FirebaseUser user) {
    nameUser = user.displayName;
    cellPhone = user.phoneNumber;
    idUser = user.uid;
    urlPhoto = user.photoUrl;
  }

  //esta funcionando
  Map<String, dynamic> userMapUpdate() {
    var map = Map<String, dynamic>();
    map['name'] = name;
    map['gender'] = gender;
    map['birthday'] = birthday;
    map['cellPhone'] = cellPhone;
    map['nameUser'] = nameUser;
    return map;
  }

  ///Metodo encargado de retornar el mapa con los datos de usuario, necesarios para realizar busquedas
  Map<String, dynamic> userMapSearch() {
    getKeywords();
    var map = Map<String, dynamic>();
    map['nameUser'] = nameUser;
    map['name']= name;
    map['urlPhoto'] = urlPhoto;
    map["numChallenges"] = numChallenges;
    map["keywords"] = keywords;
    return map;
  }

  MUser();

  /*
  * Constructor para inicilizar un usuario con sus datos basicos
  * @Param doc, contiene la informacion del usuario.
  * */
  MUser.min(Map doc) {
    idUser = doc['idUser'];
    nameUser = doc['nameUser'];
    urlPhoto = doc["urlPhoto"];
    name = doc["name"];
    verified = doc["verified"]??false;
  }

  /*
  * Constructor para inicilizar un usuario con sus datos basicos
  * @Param doc, contiene la informacion del usuario a excepcion del identificador
  * @Param idUser, identificador del usuario
  * */
  MUser.minId(Map doc, String id) {
    idUser = id;
    nameUser = doc['nameUser'];
    urlPhoto = doc["urlPhoto"];
    name = doc["name"];
  }

  /*
  * Constructor para inicilizar un usuario con sus datos basicos para mostrar en sus publicaciones.
  * @Param doc, contiene la informacion del usuario.
  * */
  MUser.minPost(Map doc) {
    idUser = doc['idUser'];
    nameUser = doc['nameUser'];
    urlPhoto = doc["urlPhoto"];
  }

  //Metodo encargado de retornar un mapa sobre datos basicos del usuario
  Map<String, dynamic> userMin() {
    var map = Map<String, dynamic>();
    map['idUser'] = idUser;
    map['nameUser'] = nameUser;
    map['name'] = name;
    map['urlPhoto'] = urlPhoto;
    map["verified"]=verified??false;
    return map;
  }

  List<MChallenge> getChallengesActives(listChallenges) {
    List<MChallenge> challenges = List();
    if (listChallenges != null) {
      listChallenges.forEach((c) {
        challenges.add(MChallenge.profileUser(c, this));
      });
    }
    return challenges;
  }

  /*
  * Metodo para actualizar los datos completos para el perfil
  * @Param data, contiene la informacion completa para visualizar en el perfil de usuario
  * */
  void updateDataProfile(Map<String, dynamic> data) {
    nameUser = data["nameUser"];
    name = data['name'];
    urlPhoto = data['thumbnail'];
    gender = data["gender"];
    challengesActive = getChallengesActives(data["challengesActive"]);
    numChallenges = data["numChallenges"] ?? 0;
    numFolloweds = data["numFolloweds"] ?? 0;
    numFollowers = data["numFollowers"] ?? 0;
    numParticipations = data["numParticipations"] ?? 0;
    verified=data["verified"]??false;
  }

  ///Se encarga de obtener cada uno de los nombres y nameUser para realizar
  ///busquedas
  getKeywords() {
    keywords = List<String>();
    keywords.addAll(name.toLowerCase().split(" "));
    keywords.add(nameUser);
  }
}
