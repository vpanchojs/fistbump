import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_item_multimedia.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/utils/responses/response_api.dart';

class RProfile {
  static final RProfile _instance = RProfile._internal();

  factory RProfile() => _instance;

  RProfile._internal();

  Future<ResponseApi> updatePhotoProfile(
      {MMultimedia multimedia, String idUser}) async {
    ResponseApi<MItemMultimedia> responseMultimedia = await api.storageApi
        .uploadThumbnail(multimedia.thumbnail,
            api.storageApi.getStorageReferencePhotoUser(idUser: idUser));
    if (responseMultimedia.success) {
      ResponseApi responseApi = await api.firestoreApi
          .updatePhotoPerfil(idUser, responseMultimedia.data);

      if (responseApi.success) {
        ResponseApi responseApi = await api.authApi
            .updatePhotoProfile(url: responseMultimedia.data.item);

        if (responseApi.success) {
          return ResponseApi.success(responseMultimedia.data.item);
        } else {
          return responseApi;
        }
      } else {
        return responseApi;
      }
    } else {
      return responseMultimedia;
    }
  }
}
