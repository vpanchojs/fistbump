import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/src/models/m_item_multimedia.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/utils/responses/response_api.dart';

class RChallenge {
  Future<ResponseApi> createChallenge(MChallenge challenge, MUser user) async {
    String idChallenge = api.firestoreApi.firestore
        .collection(Api.path_challenges)
        .document()
        .documentID;

    challenge.idChallenge = idChallenge;
    var refChallenge = api.storageApi.getStorageReferenceChallenge(
        idChallenge: idChallenge, idUser: user.idUser);
    //1. Subimos la imagen de portada del reto
    ResponseApi<MItemMultimedia> multimediaChallenge = await api.storageApi
        .uploadMultimedia(challenge.multimedia.data, refChallenge);
    if (multimediaChallenge.success) {
      //2. Subimos la miniatura del reto
      ResponseApi<MItemMultimedia> thumbnailChallenge = await api.storageApi
          .uploadThumbnail(challenge.multimedia.thumbnail, refChallenge);
      if (thumbnailChallenge.success) {
        //Agregamos la informa multimedia al reto(portada)
        challenge.multimedia = MMultimedia.remote(
            multimediaChallenge.data, thumbnailChallenge.data);

        ///Verificamos que exista un premio
        if (challenge.withPrize &&
            challenge.prize.multimedia != null &&
            challenge.prize.multimedia.origin != OriginMultimedia.EMPTY) {
          var refPrize = api.storageApi.getStorageReferencePrize(
              idUser: user.idUser, idChallenge: idChallenge);
          //3. Subimos la foto del premio
          ResponseApi<MItemMultimedia> multimediaPrize = await api.storageApi
              .uploadMultimedia(challenge.prize.multimedia.data, refPrize);

          if (multimediaPrize.success) {
            //4 subirmos la miniatura del premio;
            ResponseApi<MItemMultimedia> thumbnailPrize = await api.storageApi
                .uploadThumbnail(
                    challenge.prize.multimedia.thumbnail, refPrize);
            if (thumbnailPrize.success) {
              challenge.prize.multimedia =
                  MMultimedia.remote(multimediaPrize.data, thumbnailPrize.data);
            } else {
              return thumbnailPrize;
              //No se subio el thumbnail de la foto del premio
            }
          } else {
            return multimediaPrize;
            //No se subio el la foto del premio
          }
        }
        //Una vez subido  lo multimedia procedemos a crear
        return await api.firestoreApi.createChallenge(challenge);
      } else {
        return thumbnailChallenge;
        //No se subio el multimedia de portada del reto
      }
    } else {
      return multimediaChallenge;
      // No se subio el thumbnail de la portada del reto
    }
  }

  Future<ResponseApi> isVerifiedUser({String idUser}) async {
    ResponseApi response = await api.firestoreApi.getMinUser(idUser);
    if (response.success) {
      return ResponseApi.success(response.data.data["verified"] ?? false);
    } else {
      return response;
    }
  }
}
