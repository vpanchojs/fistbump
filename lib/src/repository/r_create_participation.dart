import 'package:Darhu/src/models/m_challenge.dart';
import 'package:Darhu/src/models/m_item_multimedia.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/src/models/m_participation.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/servicies/remote/api.dart';
import 'package:Darhu/utils/responses/response_api.dart';

class RCreateParticipation {
  Future<ResponseApi> createParticipation(MParticipation participation,
      MChallenge challenge, MUser userSession) async {
    var idParticipacion = userSession.idUser + challenge.idChallenge;
    participation.idParticipation = idParticipacion;
    if (participation.typeParticipation != TypeParticipation.TEXT) {
      var refParticipation = api.storageApi.getStorageReferenceParticipation(
          idParticipation: idParticipacion, idUser: userSession.idUser);

      ResponseApi<MItemMultimedia> multimediaParticipation = await api
          .storageApi
          .uploadMultimedia(participation.multimedia.data, refParticipation);
      if (multimediaParticipation.success) {
        //2. Subimos la miniatura de la participacion˝
        ResponseApi<MItemMultimedia> thumbnailParticipation =
            await api.storageApi.uploadThumbnail(
                participation.multimedia.thumbnail, refParticipation);
        if (thumbnailParticipation.success) {
          participation.multimedia = MMultimedia.remote(
              multimediaParticipation.data, thumbnailParticipation.data);
        } else {
          return thumbnailParticipation;
        }
      } else {
        return multimediaParticipation;
      }
    }

    ResponseApi response = await api.functionsApi
        .createParticipation(participation, challenge, userSession);

    if (response.success) {
      MParticipation participation = MParticipation.myParticipation(
          response.data, response.data["idParticipation"]);
      return ResponseApi.success(participation);
    } else {
      return response;
    }
  }
}
