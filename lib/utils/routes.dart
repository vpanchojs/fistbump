import 'package:Darhu/src/bloc/add_prize/p_add_prize.dart';
import 'package:Darhu/src/bloc/claim_prize/p_claim_prize.dart';
import 'package:Darhu/src/bloc/contacts/p_contact.dart';
import 'package:Darhu/src/bloc/creation_participation/p_create_participation.dart';
import 'package:Darhu/src/bloc/login/p_login.dart';
import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_config/p_manage_challenge_config.dart';
import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_info/p_manange_challenge_info.dart';
import 'package:Darhu/src/bloc/manage_challenge/manage_challenge_resume/p_manage_challenge_resume.dart';
import 'package:Darhu/src/bloc/manage_profile/p_manage_profile.dart';
import 'package:Darhu/src/bloc/prize_deliver/p_prize_deliver.dart';
import 'package:Darhu/src/bloc/prize_detail/p_prize_detail.dart';
import 'package:Darhu/src/bloc/suports_participation/p_support_participation.dart';
import 'package:Darhu/src/bloc/view_challenge/p_view_challenge.dart';
import 'package:Darhu/src/bloc/view_profile/p_view_profile.dart';
import 'package:Darhu/src/ui/pages/add_prize/v_add_prize.dart';
import 'package:Darhu/src/ui/pages/claim_prize/v_claim_prize.dart';
import 'package:Darhu/src/ui/pages/contacts/v_contacts.dart';
import 'package:Darhu/src/ui/pages/create_participation/v_create_participation.dart';
import 'package:Darhu/src/ui/pages/init_section/v_init_section.dart';
import 'package:Darhu/src/ui/pages/login/v_login.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_config/v_manage_challenge_config.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_info/v_manage_challenge_info.dart';
import 'package:Darhu/src/ui/pages/manage_challenge/manage_challenge_resume/v_manage_challenge_resume.dart';
import 'package:Darhu/src/ui/pages/manage_profile/v_manage_profile.dart';
import 'package:Darhu/src/ui/pages/prize_deliver/v_deliver_prize.dart';
import 'package:Darhu/src/ui/pages/prize_detail/v_prize_detail.dart';
import 'package:Darhu/src/ui/pages/supports_participation/v_support_participation.dart';
import 'package:Darhu/src/ui/pages/view_challenge/v_view_challenge.dart';
import 'package:Darhu/src/ui/pages/view_profile/v_view_profile.dart';
import 'package:flutter/material.dart';

getRoutes() {
  return <String, WidgetBuilder>{
    VLogin.routeName: (context) => PLogin(),
    VManageProfile.routeName: (context) => PManageProfile(),
    VInitSection.routeName: (context) => VInitSection(),
    VManageChallengeInfo.routeName: (context) => PManageChallengeInfo(
        idChallenge: ModalRoute.of(context).settings.arguments),
    VManageChallengeConfig.routeName: (context) => PManageChallengeConfig(),
    VViewChallenge.routeName: (context) =>
        PViewChallenge(arguments: ModalRoute.of(context).settings.arguments),
    VViewProfile.routeName: (context) =>
        PViewProfile(user: ModalRoute.of(context).settings.arguments),
    VContacts.routeName: (context) =>
        PContact(argContact: ModalRoute.of(context).settings.arguments),
    VPrizeDetail.routeName: (context) =>
        PPrizeDetail(argPrizeDetail: ModalRoute.of(context).settings.arguments),
    VSupportParticipation.routeName: (context) => PSupportParticipation(
        argSupportParticipation: ModalRoute.of(context).settings.arguments),
    VDeliverPrize.routeName: (context) => PPrizeDeliver(
        argDeliverPrize: ModalRoute.of(context).settings.arguments),
    VClaimPrize.routeName: (context) =>
        PClaimPrize(argClaimPrize: ModalRoute.of(context).settings.arguments),
    VManageChallengeResume.routeName: (context) => PManageChalleResume(),
    VAddPrize.routeName: (context) => PAddPrize(),
    VCreateParticipation.routeName: (context) => PCreateParticipation(
        argCreationParticipation: ModalRoute.of(context).settings.arguments)
  };
}
