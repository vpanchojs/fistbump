import 'package:Darhu/utils/share_widgets/button/button_reload.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class GridSliverGenericWidget extends StatelessWidget {
  final stream;
  final dynamic items;
  final dynamic refresh;
  final String noneData;

  const GridSliverGenericWidget(
      {Key key, this.stream, this.items, this.refresh, this.noneData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Widget svg = SvgPicture.asset(
      'images/info_none.svg',
      height: 150,
    );
    return StreamBuilder(
      stream: stream,
      initialData: List(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.hasError) return sliverHasError();

        if (snapshot.connectionState == ConnectionState.waiting)
          return sliverWaiting();

        if (!snapshot.hasData || snapshot.data.length <= 0)
          return sliverNoneData(svg);

        return SliverGrid.extent(
          maxCrossAxisExtent: 220.0,
          crossAxisSpacing: 3,
          mainAxisSpacing: 1.5,
          childAspectRatio: 1,
          children: items(snapshot.data),
        );
      },
    );
  }

  sliverHasError() {
    return SliverToBoxAdapter(
      child: Container(
        height: 200,
        width: double.infinity,
        child: ButtonReload(refresh: refresh),
      ),
    );
  }

  sliverWaiting() {
    return SliverToBoxAdapter(
      child: Container(
        height: 200,
        width: double.infinity,
        child: Align(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  sliverNoneData(Widget svg) {
    return SliverToBoxAdapter(
        child: Container(
      height: 200,
      width: double.infinity,
      child: Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            svg,
            SizedBox(height: 4.0),
            Text(noneData,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black26, fontSize: 16.0)),
          ],
        ),
      ),
    ));
  }
}
