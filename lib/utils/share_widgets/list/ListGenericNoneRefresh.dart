import 'package:Darhu/utils/share_widgets/button/button_reload.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ListGenericWidget extends StatelessWidget {
  final Stream stream;
  final Stream progress;
  final dynamic itemWidget;
  final dynamic refresh;
  final ScrollController scrollController;
  final String noneData;

  const ListGenericWidget(
      {Key key,
        this.stream,
        this.progress,
        this.refresh,
        this.scrollController,
        this.itemWidget,
        this.noneData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Widget svg = SvgPicture.asset(
      'images/info_none.svg',
      height: 150,
    );

    return Column(
      children: <Widget>[
        Expanded(
          child: StreamBuilder<List<dynamic>>(
              stream: stream,
              initialData: List(),
              builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
                if (snapshot.hasError) return simpleHasError();

                if (snapshot.connectionState == ConnectionState.waiting)
                  return simpleWaiting();

                if (!snapshot.hasData || snapshot.data.length <= 0)
                  return simpleNoneData(svg);

                return ListView.builder(
                    physics: const AlwaysScrollableScrollPhysics(),
                    controller: scrollController,
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return itemWidget(snapshot.data[index]);
                    });
              }),
        ),
        StreamBuilder<bool>(
            stream: progress,
            initialData: true,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              return Align(
                alignment: Alignment.center,
                child: Container(
                  margin: const EdgeInsets.symmetric(vertical: 8),
                  height: 24,
                  width: 24,
                  child: (snapshot.data)
                      ? Container()
                      : CircularProgressIndicator(strokeWidth: 2),
                ),
              );
            })
      ],
    );
  }

  simpleHasError() {
    return Container(
        height: 200,
        width: double.infinity,
        child: ButtonReload(refresh: refresh));
  }

  simpleWaiting() {
    return Container(
      height: 200,
      width: double.infinity,
      child: Align(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  simpleNoneData(Widget svg) {
    return Stack(
      children: <Widget>[
        Container(
          height: double.maxFinite,
          width: double.infinity,
          child: Align(
            alignment: Alignment.center,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  svg,
                  SizedBox(height: 4.0),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(noneData,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black26, fontSize: 16.0)),
                  )
                ],
              ),
            ),
          ),
        ),
        RefreshIndicator(
            onRefresh: refresh,
            child: SingleChildScrollView(
              child: Container(height: 600),
            )),
      ],
    );
  }
}
