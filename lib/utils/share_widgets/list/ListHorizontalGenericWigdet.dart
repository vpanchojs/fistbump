import 'package:Darhu/utils/share_widgets/button/button_reload.dart';
import 'package:flutter/material.dart';

class ListHorizontalGenericWidget extends StatelessWidget {
  final Stream stream;
  final String noneData;
  final dynamic items;
  final double heightList;

  const ListHorizontalGenericWidget(
      {Key key,
      @required this.stream,
      @required this.noneData,
      @required this.items,
      @required this.heightList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: stream,
        initialData: List(),
        builder: (context, AsyncSnapshot snapshot) {
          // if (snapshot.hasError) return listHasError();
          if (snapshot.hasError) return Container();

          if (snapshot.connectionState == ConnectionState.waiting)
            //return listWaiting();
            return Container();

          if (!snapshot.hasData || snapshot.data.length <= 0)
            return Container();
          //return listNoneData();

          return Container(
            height: heightList,
            child: ListView(
                addAutomaticKeepAlives: true,
                scrollDirection: Axis.horizontal,
                children: items(snapshot.data)),
          );
        });
  }

  listHasError() {
    return Container(
      height: 200,
      width: double.infinity,
      child: ButtonReload(refresh: (){}),
    );
  }

  listWaiting() {
    return Container(
      height: 200,
      width: double.infinity,
      child: Align(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  listNoneData() {
    return Container(
      height: 200,
      width: double.infinity,
      child: Align(
    alignment: Alignment.center,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Icon(Icons.person),
        SizedBox(height: 10.0),
        Text(noneData,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black26, fontSize: 16.0)),
      ],
    ),
      ),
    );
  }
}
