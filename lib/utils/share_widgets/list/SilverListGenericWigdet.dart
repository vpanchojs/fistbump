import 'package:Darhu/utils/share_widgets/button/button_reload.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SilverListGenericWidget extends StatelessWidget {
  final Stream stream;
  final dynamic items;
  final dynamic refresh;
  final String noneData;

  const SilverListGenericWidget(
      {Key key, this.stream, this.items, this.refresh, this.noneData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Widget svg = SvgPicture.asset(
      'images/info_none.svg',
      height: 150,
    );

    return StreamBuilder(
        stream: stream,
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) return sliverHasError();

          if (snapshot.connectionState == ConnectionState.waiting)
            return sliverWaiting();

          if (!snapshot.hasData || snapshot.data.length <= 0)
            return sliverNoneData(svg);
          
          return SliverList(
            delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  if (index.isEven) {
                    return items(snapshot.data[index]);
                  } else {
                    return Divider(
                      color: Colors.black12,
                      thickness: 5,
                    );
                  }
                },
                childCount: snapshot.data.length,
                semanticIndexCallback: (Widget widget, int localIndex) {
                  if (localIndex.isEven) {
                    return localIndex ~/ 2;
                  }
                  return null;
                }),
          );
        });
  }

  sliverHasError() {
    return SliverToBoxAdapter(
      child: Container(
        height: 200,
        width: double.infinity,
        child: ButtonReload(refresh: refresh),
      ),
    );
  }

  sliverWaiting() {
    return SliverToBoxAdapter(
      child: Container(
        height: 200,
        width: double.infinity,
        child: Align(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  sliverNoneData(Widget svg) {
    return SliverToBoxAdapter(
        child: Container(
      height: 200,
      width: double.infinity,
      child: Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            svg,
            SizedBox(height: 4.0),
            Text(noneData,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black26, fontSize: 16.0)),
          ],
        ),
      ),
    ));
  }
}
