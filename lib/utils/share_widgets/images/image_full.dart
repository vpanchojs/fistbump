import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ImageFull extends StatelessWidget {
  final String urlImage;

  const ImageFull({Key key, this.urlImage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragEnd: (velocity) {
        if (velocity.velocity.pixelsPerSecond.distance > 500) {
          Navigator.of(context).pop();
        }
      },
      child: Container(
          child: PhotoView(
              loadFailedChild:Icon(Icons.error),
              imageProvider: CachedNetworkImageProvider(urlImage),
              minScale: PhotoViewComputedScale.contained,
              maxScale: PhotoViewComputedScale.covered * 2)),
    );
  }
}
