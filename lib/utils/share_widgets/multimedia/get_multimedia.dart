import 'dart:io';
import 'package:Darhu/src/models/m_item_multimedia.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/utils/share_widgets/multimedia/camera_page.dart';
import 'package:Darhu/utils/share_widgets/multimedia/response_multimedia.dart';
import 'package:Darhu/src/res/strings.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'camera_read_time_real.dart';

///Clase se encarga de la seleccion y captura de archivos multimedia(images,video)
class GetMultimedia {
  static const limitSizeVideo = 80000000;
  static const limitSizeImage = 80000000;
  static const quality = 30;

  /*
  * Toma una fotografia desde la camara
  * @Param (opcional) limitSize, indica el tamano maximo en MB de la foto
  * @return Array [type, file],  type -> image | file -> archivo foto
  * */
  static Future<ResponseMultimedia> actionTakePictureCamera(
      [int limitSize = limitSizeImage]) async {
    try {
      var image = await ImagePicker.pickImage(source: ImageSource.camera);
      if (image != null) {
        return ResponseMultimedia.success(MMultimedia.local(
            MItemMultimedia.image(item: image),
            MItemMultimedia.image(item: await getThumbnailImage(image))));
      } else {
        return ResponseMultimedia.success(MMultimedia.empty());
      }
    } catch (ex) {
      if (ex is PlatformException) {
        if (ex.code == "camera_access_denied") {
          return ResponseMultimedia.error(
              string_error_camera_permission_required);
        }
      }
      return ResponseMultimedia.error(string_error_image_path_read);
    }
  }

  /*
  * Toma una fotografia desde la galeria
  * @Param (opcional) limitSize, indica el tamano maximo en MB de la foto
  * @return Array [type, file],  type -> image | file -> archivo foto
  * */

  static Future<ResponseMultimedia> actionGetGalleryPhoto(
      [int limitSize = limitSizeImage]) async {
    try {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (image != null) {
        var size = File(image.resolveSymbolicLinksSync()).lengthSync();
        if (size > limitSize) {
          return ResponseMultimedia.error(string_error_image_exceed_size);
        } else {
          return ResponseMultimedia.success(MMultimedia.local(
              MItemMultimedia.image(item: image),
              MItemMultimedia.image(item: await getThumbnailImage(image))));
        }
      } else {
        return ResponseMultimedia.success(MMultimedia.empty());
      }
    } catch (ex) {
      //print(ex);
      if (ex is PlatformException) {
        if (ex.code == "photo_access_denied") {
          return Future.error(string_error_read_permission_required);
        }
      }
      return Future.error(string_error_image_path_read);
    }
  }

  /*
  * Toma una fotografia desde la galeria
  * @Param (opcional) limitSize, indica el tamano maximo en MB de la foto
  * @return Array [type, file],  type -> video | file -> archivo foto
  * */
  static Future<ResponseMultimedia> actionGetGalleryVideo(
      [int limitSize = limitSizeVideo]) async {
    try {
      // final _flutterVideoCompress = FlutterVideoCompress();
      var video = await ImagePicker.pickVideo(source: ImageSource.gallery);
      if (video != null) {
        var size = video.lengthSync();
        if (size > limitSize) {
          return ResponseMultimedia.error(string_error_video_exceed_size);
        } else {
          //Se almacena el path de la imagen o video seleccionado.
          return ResponseMultimedia.success(MMultimedia.local(
              MItemMultimedia.video(item: video),
              MItemMultimedia.image(item: await getThumbnailVideo(video))));
        }
      } else {
        return ResponseMultimedia.success(MMultimedia.empty());
      }
    } catch (ex) {
      if (ex is PlatformException) {
        if (ex.code == "photo_access_denied") {
          return ResponseMultimedia.error(
              string_error_read_permission_required);
        }
      }
      return ResponseMultimedia.error(string_error_video_path_read);
    }
  }

  static Future<ResponseMultimedia> openCameraView(
      TypeMultimedia typeMultimedia, BuildContext context,
      [int limitSize = limitSizeImage]) async {
    var cameras;

    try {
      cameras = await availableCameras();
    } on CameraException catch (e) {
      return ResponseMultimedia.error(
          "No se ha encontrado ninguna camara ${e.toString()}");
    }

    try {
      ResponseMultimedia response = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CameraExampleHome(
                  typeMultimediaParticipation: typeMultimedia,
                  cameras: cameras,
                )),
      );

      if (response != null) {
        if (response.success) {
          if (response.multimedia.data.type == TypeMultimedia.IMAGE) {
            response.multimedia.thumbnail = MItemMultimedia.image(
                item: await getThumbnailImage(response.multimedia.data.item));
          } else if (response.multimedia.data.type == TypeMultimedia.VIDEO) {
            response.multimedia.thumbnail = MItemMultimedia.image(
                item: await getThumbnailVideo(response.multimedia.data.item));
          }
        }
        return response;
      } else {
        return ResponseMultimedia.error("");
      }
    } catch (error) {
      return ResponseMultimedia.error(
          "Problemas al procesar el archivo multimedia");
    }
  }

  //Se obtiene una miniatura de la imagen
  static Future<File> getThumbnailImage(File file) async {
    // ffmpeg -i input.jpg -vf scale=320:240 output_320x240.png
    var name = DateTime.now().millisecondsSinceEpoch.toString();
    final FlutterFFmpeg _flutterFFmpeg = new FlutterFFmpeg();
    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path + "/thumb_$name.jpg";
    var arguments = [
      "-i",
      file.path,
      "-vf",
      "scale=-1:600",
      "-q:v",
      "5",
      path,
    ];
    int rc = await _flutterFFmpeg.executeWithArguments(arguments);
    if (rc == 0) {
      print("Se obtuvo la miniatura");
      return file = File(path);
    } else {
      print("No se obtuvo la miniatura");
      return Future.error("Problemas al obtener la miniatura");
    }
  }

  /// Se obtiene una miniatura del video.
  static Future<File> getThumbnailVideo(File file) async {
    var name = DateTime.now().millisecondsSinceEpoch.toString();
    final FlutterFFmpeg _flutterFFmpeg = new FlutterFFmpeg();
    final directory = await getApplicationDocumentsDirectory();
    var path = directory.path + "/thumb_$name.jpg";
    //-q:v 1
    var arguments = [
      "-i",
      file.path,
      "-ss",
      "00:00:02",
      "-vframes",
      "1",
      "-q:v",
      "5",
      path
    ];

    int rc = await _flutterFFmpeg.executeWithArguments(arguments);
    if (rc == 0) {
      print("Se obtuvo la miniatura");
      return file = File(path);
    } else {
      print("No se obtuvo la miniatura");
      return Future.error("Problemas al obtener la miniatura");
    }
  }

  static Future actionReadQr(context) async {
    var cameras;
    try {
      cameras = await availableCameras();
    } on CameraException catch (e) {
      return Future.error("No se ha encontrado ninguna camara ${e.toString()}");
    }

    String result = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => CameraReadRealTime(
                cameras: cameras,
              )),
    );

    if (result != null) {
      print(" codigo $result");
      return (result is String) ? result : null;
    } else {
      return Future.error("Problemas al obtener el codigo");
    }
  }
}
