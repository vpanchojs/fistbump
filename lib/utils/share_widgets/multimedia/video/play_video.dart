import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:video_player/video_player.dart';

import 'aspect_ratio_video.dart';
import 'player_life_cycle.dart';

class PlayVideo extends StatefulWidget {
  final String urlVideo;

  const PlayVideo({Key key, this.urlVideo}) : super(key: key);

  @override
  _PlayVideoState createState() => _PlayVideoState();
}

class _PlayVideoState extends State<PlayVideo> {
  int state = 1;
  File file;

  @override
  void initState() {
    //getVideo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragEnd: (velocity) {
        if (velocity.velocity.pixelsPerSecond.distance > 500) {
          Navigator.of(context).pop();
        }
      },
      child: selectView(),
    );
  }

  selectView() {
    switch (state) {
      case 0:
        return Center(
            child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white)));
      case 1:
        return NetworkPlayerLifeCycle(
          widget.urlVideo,
          (BuildContext context, VideoPlayerController controller) =>
              AspectRatioVideo(controller),
        );
      case 2:
        return Center(
          child: Text("problemas al cargar el video",
              style: TextStyle(color: Colors.white)),
        );
      default:
        return Container();
    }
  }

  getVideo() {
    DefaultCacheManager().getSingleFile(widget.urlVideo).then((file) {
      this.file = file;
      state = 1;
      setState(() {});
    }).catchError((error) {
      state = 0;
      setState(() {});
    });
  }
}

/// A widget connecting its life cycle to a [VideoPlayerController] using
/// a data source from the network.
class NetworkPlayerLifeCycle extends PlayerLifeCycle {
  NetworkPlayerLifeCycle(dynamic dataSource, VideoWidgetBuilder childBuilder)
      : super(dataSource, childBuilder);

  @override
  _NetworkPlayerLifeCycleState createState() => _NetworkPlayerLifeCycleState();
}

class _NetworkPlayerLifeCycleState extends PlayerLifeCycleState {
  @override
  VideoPlayerController createVideoPlayerController() {
    //return VideoPlayerController.file(widget.dataSource);
    return VideoPlayerController.network(widget.dataSource);
  }
}
