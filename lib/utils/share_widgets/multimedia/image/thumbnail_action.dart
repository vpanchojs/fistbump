import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../images/image_full.dart';
import '../video/play_video.dart';

class ThumbnailAction extends StatelessWidget {
  final String urlThumbnail;
  final String typeMultimedia;
  final String urlMultimedia;
  final dynamic actionTap;
  final double alto;

  const ThumbnailAction(
      {Key key,
      this.urlThumbnail,
      this.typeMultimedia,
      this.urlMultimedia,
      this.actionTap,
      this.alto = 300})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (urlThumbnail == null) {
      return Center(child: Icon(Icons.image, size: alto, color: Colors.white));
    }

    return InkWell(
      onTap: () => (actionTap == null)
          ? navigationToViewMultimedia(context)
          : actionTap(context),
      onLongPress: () => navigationToViewMultimedia(context),
      child: Container(
        height: alto,
        width: double.infinity,
        color: Colors.white,
        child: CachedNetworkImage(
          imageUrl: urlThumbnail,
          placeholder: (context, url) {
            return Center(
              child: CircularProgressIndicator(),
            );
          },
          fit: BoxFit.cover,
          alignment: FractionalOffset.center,
          errorWidget: (context, url, error) {
            return Icon(Icons.error);
          },
        ),
      ),
    );
  }

  navigationToViewMultimedia(BuildContext context) {
    if (urlMultimedia != null) {
      if (typeMultimedia == 'image') {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) =>
                ImageFull(urlImage: urlMultimedia??""),
          ),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) =>
                PlayVideo(urlVideo: urlMultimedia??""),
          ),
        ).then((result) {
          if (result != null) {
            Scaffold.of(context).showSnackBar(SnackBar(content: Text(result)));
          }
        });
      }
    }
  }
}
