import 'package:Darhu/src/models/m_multimedia.dart';

class ResponseMultimedia {
  bool success;
  MMultimedia multimedia;
  String error;

  ResponseMultimedia.success(this.multimedia) : success = true;
  ResponseMultimedia.error(this.error) : success = false;
}
