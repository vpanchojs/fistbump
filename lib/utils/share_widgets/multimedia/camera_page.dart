import 'dart:async';
import 'dart:io';
import 'package:Darhu/src/models/m_item_multimedia.dart';
import 'package:Darhu/src/models/m_multimedia.dart';
import 'package:Darhu/utils/share_widgets/multimedia/response_multimedia.dart';
import 'package:camera/camera.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:quiver/async.dart';
import 'package:video_player/video_player.dart';

class CameraExampleHome extends StatefulWidget {
  final TypeMultimedia typeMultimediaParticipation;
  final List<CameraDescription> cameras;

  CameraExampleHome({Key key, this.typeMultimediaParticipation, this.cameras})
      : super(key: key);

  @override
  _CameraExampleHomeState createState() {
    return _CameraExampleHomeState();
  }
}

class _CameraExampleHomeState extends State<CameraExampleHome>
    with WidgetsBindingObserver {
  CameraController controller;
  String imagePath;
  String videoPath;
  VideoPlayerController videoController;
  VoidCallback videoPlayerListener;
  bool toggleCamera;
  bool toggleMultimedia;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final timeOutInSeconds = 30;
  final stepInSeconds = 1;
  int currentNumber = 30;
  var listenerCountDown;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    toggleCamera = false;
    toggleMultimedia = false;
    onNewCameraSelected(widget.cameras.first);
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller?.dispose();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    videoController?.dispose();
    listenerCountDown?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.black,
          key: _scaffoldKey,
          body: Stack(
            children: <Widget>[
              getContainerCameraPreview(),
              getAppBar(),
            ],
          )),
    );
  }

  getAppBar() {
    return Container(
      height: 50,
      child: AppBar(
        title: _getCountAppBar(),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.black26,
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            if (mounted) {
              setState(() {
                videoController?.pause();
                if (videoController != null || imagePath != null) {
                  videoController = null;
                  imagePath = null;
                } else
                  Navigator.of(context).pop();
              });
            }
          },
        ),
      ),
    );
  }

  _onPressedTakeMultimedia() {
    currentNumber = 60; // reset downtime
    if (controller != null &&
        controller.value.isInitialized &&
        controller.value.isRecordingVideo) {
      listenerCountDown?.cancel();
      return onStopButtonPressed();
    }
    if (controller != null) {
      switch (widget.typeMultimediaParticipation) {
        case TypeMultimedia.IMAGE:
          print("FOTO");
          return onTakePictureButtonPressed();
        case TypeMultimedia.VIDEO:
          print("VIDEO");
          startTimeout();
          return onVideoRecordButtonPressed();
        case TypeMultimedia.ALL:
          if (!toggleMultimedia) {
            print("FOTO");
            return onTakePictureButtonPressed();
          }
          print("VIDEO");
          startTimeout();
          return onVideoRecordButtonPressed();
        case TypeMultimedia.EMPTY:
          return null;
      }
    } else
      return null;
  }

  void onTimerTick(int currentNumber) {
    if (mounted) {
      setState(() {
        currentNumber = currentNumber;
      });
    }
  }

  startTimeout([int milliseconds]) {
    CountdownTimer countDownTimer = new CountdownTimer(
        new Duration(seconds: timeOutInSeconds),
        new Duration(seconds: stepInSeconds));

    listenerCountDown = countDownTimer.listen(null);
    listenerCountDown?.onData((duration) {
      currentNumber -= stepInSeconds;
      onTimerTick(currentNumber);
      print('Your message here: $currentNumber');
    });

    listenerCountDown?.onDone(() {
      print("I'm done");
      listenerCountDown?.cancel();
      onStopButtonPressed();
    });
  }

  Widget _getCountAppBar() {
    return controller != null && controller.value.isRecordingVideo
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  right: 6.0,
                ),
                width: 14.0,
                height: 14.0,
                decoration: new BoxDecoration(
                  color: Colors.red,
                  shape: BoxShape.circle,
                ),
              ),
              Text(
                '00:${currentNumber.toString().length == 1 ? "0" : ""}$currentNumber',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
            ],
          )
        : null;
  }

  void popScreen() async {
    MMultimedia multimedia;
    if (videoPath != null && videoPath.isNotEmpty) {
      multimedia =
          MMultimedia.local(MItemMultimedia.video(item: File(videoPath)), null);
    } else if (imagePath != null && imagePath.isNotEmpty) {
      multimedia =
          MMultimedia.local(MItemMultimedia.image(item: File(imagePath)), null);
    } else {
      multimedia = MMultimedia.empty();
    }
    ResponseMultimedia responseMultimedia =
        ResponseMultimedia.success(multimedia);
    Navigator.pop(context, responseMultimedia);
  }

  getContainerCameraPreview() {
    if (videoController == null && imagePath == null) {
      return Stack(alignment: Alignment.center, children: <Widget>[
        _cameraPreviewWidget(),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            color: Colors.black26,
            height: 100,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _getButtonToggleMultimedia(),
                  _getButtonToggleTakeMultimedia(),
                  _getButtonToggleCamera()
                ]),
          ),
        )
      ]);
    } else {
      return Stack(alignment: Alignment.center, children: <Widget>[
        _thumbnailWidget(),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            color: Colors.black26,
            height: 100,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FloatingActionButton(
                  onPressed: popScreen,
                  child: new Icon(
                    Icons.check,
                    color: ColorsRes.purple_color,
                    size: 40.0,
                  ),
                  backgroundColor: Colors.white,
                  elevation: 0,
                )
              ],
            ),
          ),
        )
      ]);
    }
  }

  _getButtonToggleTakeMultimedia() {
    return Container(
      child: Container(
        height: 65,
        child: RawMaterialButton(
          onPressed: _onPressedTakeMultimedia,
          child: controller != null && !controller.value.isRecordingVideo
              ? Container()
              : Icon(Icons.stop, color: Colors.red),
          shape: new CircleBorder(
            side: BorderSide(color: ColorsRes.purple_color, width: 2.0),
          ),
          fillColor: Colors.white,
        ),
      ),
    );
  }

  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    } else {
      return Container(
          child: AspectRatio(
              child: CameraPreview(controller),
              aspectRatio: controller.value.aspectRatio));
    }
  }

  /// Display the thumbnail of the captured image or video.
  Widget _thumbnailWidget() {
    return videoController == null && imagePath == null
        ? Container()
        : (videoController == null)
            ? AspectRatio(
                aspectRatio: controller.value.aspectRatio,
                child: Image.file(
                  File(imagePath),
                ),
              )
            : VideoPlayer(videoController);
  }

  _toggleCamera() {
    if (!toggleCamera)
      onNewCameraSelected(widget.cameras.last);
    else
      onNewCameraSelected(widget.cameras.first);
    toggleCamera = !toggleCamera;
  }

  _toggleMultimedia() {
    if (mounted) {
      setState(() {
        toggleMultimedia = !toggleMultimedia;
      });
    }
  }

  Widget _getButtonToggleMultimedia() {
    if (controller != null && !controller.value.isRecordingVideo) {
      if (widget.typeMultimediaParticipation == TypeMultimedia.ALL) {
        return IconButton(
          onPressed: _toggleMultimedia,
          icon: Icon(toggleMultimedia ? Icons.videocam : Icons.camera_alt,
              size: 26.0, color: Colors.white),
        );
      } else {
        if (widget.typeMultimediaParticipation == TypeMultimedia.IMAGE) {
          return IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.camera_alt,
              size: 26.0,
              color: Colors.white,
            ),
          );
        } else {
          return IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.videocam,
              size: 26.0,
              color: Colors.white,
            ),
          );
        }
      }
    } else {
      return Container();
    }
  }

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _getButtonToggleCamera() {
    if (widget.cameras.isEmpty) {
      return const Text('Algo salío mal al abrir la cámara');
    } else if (widget.cameras.length == 2 &&
        controller != null &&
        !controller.value.isRecordingVideo) {
      return IconButton(
        onPressed: _toggleCamera,
        icon: Icon(
          toggleCamera ? Icons.camera_front : Icons.camera_rear,
          size: 26.0,
          color: Colors.white,
        ),
      );
    } else
      return Container();
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.ultraHigh,
      enableAudio: (widget.typeMultimediaParticipation == TypeMultimedia.VIDEO)
          ? true
          : false,
    );
    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        //showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
          videoController?.dispose();
          videoController = null;
        });
      }
    }).catchError((error) {
      print("Error capturar foto: $error");
    });
  }

  void onVideoRecordButtonPressed() {
    startVideoRecording().then((String filePath) {
      if (mounted) setState(() {});
      //if (filePath != null) showInSnackBar('Saving video to $filePath');
    });
  }

  void onStopButtonPressed() {
    stopVideoRecording().then((_) {
      if (mounted) setState(() {});
      //showInSnackBar('Video recorded to: $videoPath');
    });
  }

  Future<String> startVideoRecording() async {
    if (!controller.value.isInitialized) {
      print('Error: select a camera first.');
      return null;
    }

    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Movies/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.mp4';

    if (controller.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return null;
    }

    try {
      videoPath = filePath;
      await controller.startVideoRecording(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  Future<void> stopVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.stopVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }

    await _startVideoPlayer();
  }

  Future<void> _startVideoPlayer() async {
    final VideoPlayerController vcontroller =
        VideoPlayerController.file(File(videoPath));
    videoPlayerListener = () {
      if (videoController != null && videoController.value.size != null) {
        // Refreshing the state to update video player with the correct ratio.
        if (mounted) setState(() {});
        videoController.removeListener(videoPlayerListener);
      }
    };
    vcontroller.addListener(videoPlayerListener);
    await vcontroller.setLooping(true);
    await vcontroller.initialize();
    await videoController?.dispose();
    if (mounted) {
      setState(() {
        imagePath = null;
        videoController = vcontroller;
      });
    }
    await vcontroller.play();
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      print('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  void _showCameraException(CameraException e) {
    logError(e.code, e.description);

    if (e.code == "cameraPermission") {
      Navigator.pop(
          context,
          ResponseMultimedia.error(
              "Permisos no concedidos, agregue los permisos manualmente"));
    } else {
      Navigator.pop(context, ResponseMultimedia.error(e.toString()));
    }
  }
}

void logError(String code, String message) =>
    print('Error camara: $code\nError Message: $message');
