import 'package:Darhu/src/servicies/local/qr_api.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:video_player/video_player.dart';

class CameraReadRealTime extends StatefulWidget {
  final List<CameraDescription> cameras;

  CameraReadRealTime({Key key, this.cameras})
      : super(key: key);

  @override
  _CameraReadRealTimeState createState() {
    return _CameraReadRealTimeState();
  }
}

class _CameraReadRealTimeState extends State<CameraReadRealTime>
    with WidgetsBindingObserver {
  CameraController controller;
  String imagePath;
  String videoPath;
  String code;
  VideoPlayerController videoController;
  VoidCallback videoPlayerListener;
  bool toggleCamera;
  bool toggleMultimedia;
  final qrRead=QrApi();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final timeOutInSeconds = 10;
  final stepInSeconds = 1;
  int currentNumber = 10;
  var listenerCountDown;

  @override
  void initState() {
    controller = CameraController(widget.cameras.first, ResolutionPreset.medium,enableAudio: false);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      controller.startImageStream((CameraImage availableImage) {
        print("Estoy transmitiendo imagenes${controller.value.isStreamingImages}");
        qrRead.readCameraImage(availableImage,setCode);
      });

      setState(() {});
    });
    super.initState();
  }

  setCode(String code) async {
    if(code.isNotEmpty){
      this.code=code;
      print("El codigo es $code");
      if(controller.value.isStreamingImages) {
        controller.stopImageStream();
      }else{
        Navigator.pop(context,this.code);
      }
    }
  }


  @override
  void dispose() {
   // controller?.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(key: _scaffoldKey, body: getContainerCameraPreview());
  }

  getContainerCameraPreview() {
    return Column(
      children: <Widget>[
        Expanded(flex: 4, child: _cameraPreviewWidget()),
        Expanded(flex: 1, child: Text("Enfoque el codigo QR",textAlign: TextAlign.center))
      ],
    );
  }


  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    } else {
      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller),
      );
    }
  }
}

void logError(String code, String message) =>
    print('Error camara: $code\nError Message: $message');
