import 'package:flutter/material.dart';
import 'package:share/share.dart';

class SharePlatforms {
  static Future shareOtherPlatformsDynamicLinks(BuildContext context, dynamic url) {
    return Share.share((url is String) ? url : url.toString());
  }
}
