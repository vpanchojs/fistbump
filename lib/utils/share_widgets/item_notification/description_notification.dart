import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DescriptionNotification extends StatelessWidget {
  final String nameUser; //Nombre del Usuario
  final String action; //Nombre de la accion ya sea participacion / nuevo reto
  final String description; // union entre el nombre de usuario y la accion.

  const DescriptionNotification(
      {Key key, this.nameUser, this.action, this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: nameUser,
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        children: <TextSpan>[
          TextSpan(
              text: ', $description ',
              style: TextStyle(fontWeight: FontWeight.normal)),
          TextSpan(text: action),
        ],
      ),
    );
  }
}

class DatePostNotification extends StatelessWidget {
  final formatDate = DateFormat("dd 'de' MMMM 'del' yyyy  hh:mm", 'es');
  final DateTime datePost;

  DatePostNotification({Key key, this.datePost})
      : super(key: key); //Fecha de publicacion

  @override
  Widget build(BuildContext context) {
    return Text(formatDate.format(datePost));
  }
}
