import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class DatePostFull extends StatelessWidget {
  final formatDate = DateFormat("dd 'de' MMMM 'del' yyyy  HH:mm", 'es');
  final DateTime datePost;
  final TextStyle style;

  DatePostFull({Key key, this.datePost, this.style})
      : super(key: key); //Fecha de publicacion

  @override
  Widget build(BuildContext context) {
    if (style != null) {
      return Text(formatDate.format(datePost), style: style);
    }
    return Text(
      formatDate.format(datePost),
      style:
          GoogleFonts.montserrat(fontSize: 14, fontWeight: FontWeight.normal),
    );
  }
}

class DateFormatCustom {
  static final formatDate = DateFormat("dd 'de' MMMM 'del' yyyy  HH:mm", 'es');
  static String getDateFormatted(DateTime date) => formatDate.format(date);
}
