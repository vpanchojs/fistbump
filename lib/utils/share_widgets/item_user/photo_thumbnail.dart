import 'dart:math';
import 'package:Darhu/src/ui/pages/view_profile/v_view_profile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:Darhu/src/models/m_user.dart';
import 'package:Darhu/src/res/colors_res.dart';
import 'package:flutter/material.dart';

class PhotoThumbnail extends StatelessWidget {
  final MUser user;
  final double size;
  final double round;
  final double width;
  final double borderRadiusOut;
  final double borderRadiusIn;
  final navigationProfile;

  const PhotoThumbnail(
      {Key key,
      this.user,
      this.size = 35,
      this.round = 4,
      this.width = 2,
      this.borderRadiusOut = 8,
      this.borderRadiusIn = 6,
      this.navigationProfile = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _navigationToProfile(context),
      child: getImageProfile(user.urlPhoto),
    );
  }

  Widget getImageProfile(String urlPhoto) {
    if (urlPhoto == null || urlPhoto.trim() == "") {
      return decorationNonePopulate();
    } else {
      return decorationPopulate(urlPhoto);
    }
  }

  Widget decorationPopulate(String url) {
    double L = size; // image side length
    double R = round; // rounding radius
    double k = sqrt(2) - R / L * 2 * (sqrt(2) - 1);

    return Transform.rotate(
      angle: pi / 4,
      child: Container(
        child: Transform.scale(
          scale: 1 / k,
          child: Stack(
            children: <Widget>[
              Container(
                decoration: new BoxDecoration(
                    color: Colors.white30,
                    border: new Border.all(color: Colors.black12, width: width),
                    borderRadius: BorderRadius.circular(borderRadiusIn)),
                child: ClipRRect(
                  borderRadius: new BorderRadius.circular(R),
                  child: Transform.rotate(
                    angle: -pi / 4,
                    child: Transform.scale(
                      scale: k,
                      child: CachedNetworkImage(
                        imageUrl: url,
                        width: L,
                        height: L,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget decorationNonePopulate() {
    double L = size; // image side length
    double R = round; // rounding radius
    double k = sqrt(2) - R / L * 2 * (sqrt(2) - 1);

    return Transform.rotate(
      angle: pi / 4,
      child: Container(
        child: Transform.scale(
          scale: 1 / k,
          child: Stack(
            children: <Widget>[
              Container(
                decoration: new BoxDecoration(
                    color: Colors.white30,
                    border: new Border.all(color: Colors.black12, width: width),
                    borderRadius: BorderRadius.circular(borderRadiusIn)),
                child: ClipRRect(
                  borderRadius: new BorderRadius.circular(R),
                  child: Transform.rotate(
                    angle: -pi / 4,
                    child: Transform.scale(
                      scale: k,
                      child: Icon(
                        Icons.person,
                        color: Colors.black45,
                        size: L,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _navigationToProfile(BuildContext context) {
    if (navigationProfile) {
      Navigator.pushNamed(context, VViewProfile.routeName, arguments: user);
      /*
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => PViewProfile(
                    user: user,
                  )));

       */
    }
  }
}
