import 'package:flutter/material.dart';

class ProgressCustom extends StatelessWidget {
  final bool show;

  const ProgressCustom({Key key, this.show}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        height: 24,
        width: 24,
        child: show ? CircularProgressIndicator(strokeWidth: 2) : Container(),
      ),
    );
  }
}
