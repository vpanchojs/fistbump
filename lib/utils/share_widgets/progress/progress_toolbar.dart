import 'package:Darhu/src/res/colors_res.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProgressToolbar extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24,
      margin: EdgeInsets.only(right: 8),
      width: 24,
      child: CircularProgressIndicator(
        strokeWidth: 2,
        valueColor:
        AlwaysStoppedAnimation<Color>(ColorsRes.purple_color),
      ),
    );
  }

}