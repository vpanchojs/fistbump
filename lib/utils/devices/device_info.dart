import 'dart:io';

import 'package:Darhu/src/models/m_device_user.dart';
import 'package:device_info/device_info.dart';

class  DeviceInfo{


   static Future<MDeviceUser> getDevicesInfo() async {
     DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
     MDeviceUser deviceUser= new MDeviceUser();
     if (Platform.isAndroid) {
       AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
       deviceUser.idDevice= androidInfo.androidId;
       deviceUser.name=androidInfo.id;
       deviceUser.so="Android ${androidInfo.version.sdkInt}";
     } else if (Platform.isIOS) {
       IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
       deviceUser.idDevice= iosInfo.identifierForVendor;
       deviceUser.name=iosInfo.name;
       deviceUser.so="iOS";
     }
     return deviceUser;
  }
}
