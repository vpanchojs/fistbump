import 'package:flutter/material.dart';

class Utils {
  static const int QUALITY = 20;

  static showSnackBar(String msg, GlobalKey<ScaffoldState> scaffoldKey) {
    try{
      scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(msg),
        duration: new Duration(seconds: 5),
      ));
    }catch(e){

    }
  }

  static Future<bool> showLoadingDialog(context, msg) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(20.0),
            children: <Widget>[
              Row(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(width: 20.0),
                  Text(
                    msg,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                ],
              )
            ],
          );
        });
  }

}
