class ValidateFields {

  ///@Author: Doris Guaman
  ///@Modify: Victor Jumbo
  ///@Date : 5/01/2020
  ///@cause: Se agrega minimo y maximo, ademas de permiter caracteres especiales
  /// como : @ # , .
  static String validDescription(String data, [int size=30, int limit=800]) {
    if (data.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      if (data.length < size) {
        return 'Debe contener mas de $size caracteres';
      } else if (data.length > limit){
        return 'Debe ser inferior a $limit caracteres';
      }else{
        return null;
      }
      /*
      if (evaluateExp(r"^([a-zA-Z0-9 ñÑáéíóúÁÉÍÓÚ.,@#%]+[\s]*)$", data)) {
        if (data.length < size) {
          return 'Debe contener mas de $size caracteres';
        } else if (data.length > limit){
          return 'Debe ser inferior a $limit caracteres';
        }else{
          return null;
        }
      } else {
        return "Solo: letras números . @ # % , ";
      }
      */
    }
  }

  ///@Author: Victor Jumbo
  ///@Date : 5/01/2020
  ///El metodo valida:
  ///Caracteres Alfanumericos ya sea mayusculas o minisculas.
  ///Maximo y minimo de caracteres.
  static String nameChallenge(String data,[int size=3, int limit=30]){
    if (data.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      if (evaluateExp(r"^([a-zA-Z0-9 ñÑáéíóúÁÉÍÓÚ]+[\s]*)$", data)) {
        if (data.length < size) {
          return 'Debe contener mas de $size caracteres';
        } else if (data.length > limit){
          return 'Debe ser inferior a $limit caracteres';
        }else{
          return null;
        }
      } else {
        return "Solo: letras números";
      }
    }
  }

  ///@Author: Victor Jumbo
  ///@Date : 5/01/2020
  static String namePrize(String data,[int size=3, int limit=100]){
    if (data.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      if (evaluateExp(r"^([a-zA-Z0-9 ñÑáéíóúÁÉÍÓÚ]+[\s]*)$", data)) {
        if (data.length < size) {
          return 'Debe contener mas de $size caracteres';
        } else if (data.length > limit){
          return 'Debe ser inferior a $limit caracteres';
        }else{
          return null;
        }
      } else {
        return "Solo: letras y números";
      }
    }
  }

  ///@Author: Victor Jumbo
  ///@Date : 5/01/2020
  static String codePrize(String data,[int size=15, int limit=200]){
    if (data.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      if (evaluateExp(r"^([a-zA-Z0-9-]+[\s]*)$", data)) {
        if (data.length < size) {
          return 'Debe contener mas de $size caracteres';
        } else if (data.length > limit){
          return 'Debe ser inferior a $limit caracteres';
        }else{
          return null;
        }
      } else {
        return "Solo: letras numeros _";
      }
    }
  }


  // Validar letras, numeros y _ , -
  static String validNameUser(String data) {
    print(data);
    if (data.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      if (evaluateExp(r"^[a-z0-9_]+$", data)) {
        if (data.length < 3) {
          return 'El nombre de usuario es demaciado corto';
        } else {
          return null;
        }
      } else {
        return "Solo letras minusculas numeros  _";
      }
    }
  }

  //Valida nobre
  static String validateName(String name) {
    if (name.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      if (!evaluateExp(r"^[a-zA-ZñÑáéíóúÁÉÍÓÚ]$", name[0])) {
        return "Debe iniciar con letra";
      } else {
        if (evaluateExp(
            r"^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+([a-zA-ZñÑáéíóúÁÉÍÓÚ ]*)$", name)) {
          if (name.length < 3) {
            return 'El nombre es demaciado corto';
          } else {
            return null;
          }
        } else {
          return "Solo se acepta solo letras";
        }
      }
    }
  }

  static String validateUrl(String name) {
    if (name.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      return null;
    }
  }

  //Valida el email
  static String validateEmail(String email) {
    if (email.trim().isEmpty) {
      return null;
    } else {
      if (evaluateExp(
          r"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$", email)) {
        return null;
      } else {
        return "Email invalido";
      }
    }
  }

  //Valida el codigo enviado por el msj de texto
  static String validateCode(String number) {
    if (number.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      if (evaluateExp(r"(^[0-9]*$)", number)) {
        if (number.length != 6) {
          return "El código esta mal ingresado";
        } else {
          return null;
        }
      } else {
        return "Solo se acepta números";
      }
    }
  }

  //Valida numero de celular
  static String validatePhone(String number) {
    if (number.trim().isEmpty) {
      return "El campo es requerido";
    } else {
      if (evaluateExp(r"(^[0-9]*$)", number)) {
        //if (number.length <11 &&number.length>8) {
        if (number.length == 9 || number.length == 10) {
          return null;
        } else {
          return "Celular mal ingresado";
        }
      } else {
        return "Solo se acepta números";
      }
    }
  }

//Valida la expresion enviada con el valor a compar
  static bool evaluateExp(expre, valor) {
    RegExp expression = new RegExp(
      expre,
      caseSensitive: true,
      multiLine: false,
    );
    return expression.hasMatch(valor);
  }

  static validateListEmpty(List<String> data) {
    if (data.isEmpty) {
      return "El campo es requerido";
    } else {
      return null;
    }
  }
}
