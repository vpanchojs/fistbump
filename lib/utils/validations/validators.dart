import 'dart:async';

import 'package:Darhu/utils/validations/validate_fields.dart';

class Validators {
  final validNameUser = StreamTransformer<String, String>.fromHandlers(
      handleData: (numContrac, skin) {
    var message = ValidateFields.validNameUser(numContrac);
    (message == null) ? skin.add(numContrac) : skin.addError(message);
  });


  final validName = StreamTransformer<String, String>.fromHandlers(
      handleData: (nameProject, skin) {
    var message = ValidateFields.validateName(nameProject);
    (message == null) ? skin.add(nameProject) : skin.addError(message);
  });

  final validDescription=StreamTransformer<String, String>.fromHandlers(
      handleData: (texto, skin) {
        var message = ValidateFields.validDescription(texto);
        (message == null) ? skin.add(texto) : skin.addError(message);
      });

  final validNamePrize =StreamTransformer<String, String>.fromHandlers(
      handleData: (data, skin) {
        var message = ValidateFields.namePrize(data);
        (message == null) ? skin.add(data) : skin.addError(message);
      });

  final validCodePrize =StreamTransformer<String, String>.fromHandlers(
      handleData: (data, skin) {
        var message = ValidateFields.codePrize(data);
        (message == null) ? skin.add(data) : skin.addError(message);
      });


  final validateNameChallenge = StreamTransformer<String, String>.fromHandlers(
      handleData: (data, skin) {
        var message = ValidateFields.nameChallenge(data);
        (message == null) ? skin.add(data) : skin.addError(message);
      });


  final validUrl =
      StreamTransformer<String, String>.fromHandlers(handleData: (data, skin) {
    var message = ValidateFields.validateUrl(data);
    (message == null) ? skin.add(data) : skin.addError(message);
  });

  final validParticipants =
      StreamTransformer<List<String>, List<String>>.fromHandlers(
          handleData: (data, skin) {
    var message = ValidateFields.validateListEmpty(data);
    (message == null) ? skin.add(data) : skin.addError(message);
  });
}
