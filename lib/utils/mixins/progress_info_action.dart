import 'package:rxdart/rxdart.dart';

///Mixin encargada de proporcionar un BehaviorSubject para controlar la ejecucion de una accion,
///que su carga se mostrara en  un progress con mensaje ya sea en un snackbar o dialog.
mixin ProgressInfoAction {
  final progressInfo = BehaviorSubject<bool>();

  Function(bool) get inProgressInfo => progressInfo.sink.add;

  Stream<bool> get outProgressInfo => progressInfo.stream;

  closeProgressInfoAction() {
    progressInfo.close();
  }
}
