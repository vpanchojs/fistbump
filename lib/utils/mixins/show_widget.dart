import 'package:rxdart/rxdart.dart';

mixin ShowWidget {
  final _showWidgetCtrl = BehaviorSubject<bool>();

  Function(bool) get inShow => _showWidgetCtrl.sink.add;

  Stream<bool> get outShow => _showWidgetCtrl.stream;

  closeShowWidget() {
    _showWidgetCtrl.close();
  }
}
