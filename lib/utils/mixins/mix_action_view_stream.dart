import 'dart:async';

import 'package:Darhu/src/models/m_action_view.dart';
import 'package:rxdart/rxdart.dart';

///Mixin encargada de proporcionar un BehaviorSubject para notificar a la vista de realizar determinas acciones
///como se observar en la clase DataView.
mixin MixActionViewStream {
  final viewCtrl = PublishSubject<MActionView>();

  Function(MActionView) get inView => viewCtrl.sink.add;

  Stream<MActionView> get outView => viewCtrl.stream;

  StreamSubscription listenView;

  initActionView({context, key}) {
    listenView = outView.listen((event) {
      event.run(context: context, key: key);
    });
  }

  closeDataView() {
    viewCtrl.close();
    listenView?.cancel();
  }
}
