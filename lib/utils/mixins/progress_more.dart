import 'package:rxdart/rxdart.dart';

///Mixin encargado de brindar un BehaviorSubject para controlar el progress de
///cargar mas elementos en lista o grilla.
mixin ProgressMore {
  //Variable que indica si se debe consultar mas elementos, ademas controla el spam.
  bool gettingMore = true;

  final _progressMoreCtrl = BehaviorSubject<bool>();

  Function(bool) get inProgressMore => _progressMoreCtrl.sink.add;

  Stream<bool> get outProgressMore => _progressMoreCtrl.stream;

  closeProgressMoreList() {
    _progressMoreCtrl.close();
  }
}
