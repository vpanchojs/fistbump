import 'package:rxdart/rxdart.dart';

///Mixin se encarga de proporcionar un BehaviorSubject de una lista,
/// donde se contiene los identificadores de los elementos de la lista
/// en base al identificador se puede mostrar/ocultar el progress de accion
mixin ProgressActionList<T> {
  List<T> idList = List();
  final _progressActionListCtrl = BehaviorSubject<List<T>>();

  Function(List<T>) get inProgressActionList =>
      _progressActionListCtrl.sink.add;

  Stream<List<T>> get outProgressActionList => _progressActionListCtrl.stream;

  closeProgressActionList() {
    _progressActionListCtrl.close();
  }
}
