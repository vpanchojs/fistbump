import 'package:rxdart/rxdart.dart';

///Tiene el mismo comportamiento que <ProgressAction>,
///solo que actualmente no conosco como utilizar 2 veces el mismo mixin en una clase
mixin ProgressActionAux {
  final progressActionAuxCtrl = BehaviorSubject<bool>();

  Function(bool) get inProgressActionAux => progressActionAuxCtrl.sink.add;

  Stream<bool> get outProgressActionAux => progressActionAuxCtrl.stream;

  closeProgressActionAux() {
    progressActionAuxCtrl.close();
  }
}
