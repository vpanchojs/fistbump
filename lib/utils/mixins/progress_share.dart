import 'package:rxdart/rxdart.dart';

///Mixin encargado de brindar un BehaviorSubject para controlar el progress de compartir enlances dinamicos
mixin ProgressShare {

  final _progressShareCtrl = BehaviorSubject<bool>();

  Function(bool) get inProgressShare => _progressShareCtrl.sink.add;

  Stream<bool> get outProgressShare => _progressShareCtrl.stream;

  closeProgressShare() {
    _progressShareCtrl.close();
  }
}
