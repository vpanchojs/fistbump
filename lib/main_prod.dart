import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'application_bloc.dart';
import 'main.dart';
import 'utils/bloc/bloc_provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Crashlytics.instance.enableInDevMode = true;
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
  String titleFlavor = "Darhu";
  //SystemChrome.setEnabledSystemUIOverlays([]);
  runApp(
    BlocProvider<ApplicationBloc>(
        blocBuilder: () => ApplicationBloc(),
        child: MyApp(titleApp: titleFlavor)),
  );
}

// /Users/victor/development/flutter/bin/flutter build appbundle --flavor prod  -t lib/main_prod.dart
