import 'package:Darhu/utils/FlavorFlutter.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'application_bloc.dart';
import 'main.dart';
import 'utils/bloc/bloc_provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Crashlytics.instance.enableInDevMode = true;
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
  String titleFlavor = "Darhu - Dev";
  FlavorFlutter.debug = true;
  SystemChrome.setEnabledSystemUIOverlays([
    SystemUiOverlay.top,
    SystemUiOverlay.bottom
  ]);

  runApp(
    BlocProvider<ApplicationBloc>(
        blocBuilder: () => ApplicationBloc(),
        child: MyApp(titleApp: titleFlavor)),
  );
}
