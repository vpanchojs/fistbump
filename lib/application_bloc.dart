import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:Darhu/src/models/m_device_user.dart';
import 'package:Darhu/utils/devices/device_info.dart';
import 'package:Darhu/utils/enums/enums.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'src/servicies/remote/api.dart';
import 'src/models/m_challenge.dart';
import 'src/models/m_user.dart';
import 'utils/bloc/bloc_provider.dart';

class ApplicationBloc extends BlocBase {
  final List<ConnectivityHandler> _delegates = List();
  StreamSubscription<ConnectivityResult> _connectivity;
  ConnectivityResult _currentConnectivity;

  List<MUser> usersPopulate = List();
  List<MChallenge> challengesPopulate = List();
  List<MChallenge> myListChallenge = List();
  List<MUser> resultUserSearch = List();
  MChallenge challenge;
  MUser user = MUser();
  
  final dylinks = FirebaseDynamicLinks.instance;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final navigationCtrl = BehaviorSubject<Map<dynamic, dynamic>>();

  Function(Map<dynamic, dynamic>) get inNavigation => navigationCtrl.sink.add;

  Stream<Map<dynamic, dynamic>> get outNavigation => navigationCtrl.stream;

  @override
  void dispose() {
    navigationCtrl.close();
    _connectivity.cancel();
  }

  ApplicationBloc() {
    initDynamicLinks();
    initMessaging();
    _initConnectivity();
  }

  _initConnectivity() {
    _connectivity = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (_currentConnectivity != null) {
        for (var delegate in _delegates) {
          delegate.onConnectivityChanged(result);
        }
      }
      _currentConnectivity = result;
    });
  }

  void listenConnectivity(ConnectivityHandler handler) =>
      _delegates.add(handler);
  ConnectivityResult get currentConnectivity => _currentConnectivity;

  void initDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;
    processDataDynamicLink(deepLink);

    dylinks.onLink(onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;
      processDataDynamicLink(deepLink);
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError (al procesar el enlance dinamico)');
      print(e.message);
    });
  }

  processDataDynamicLink(deepLink) {
    if (deepLink != null) {
      Map<String, dynamic> params = Map();
      params.addAll(deepLink.queryParameters);
      if (params.length > 0) {
        if (params.containsKey("challenge")) {
          //Reto
          if (params.containsKey("participation")) {
            //Participacion en Reto
            params["navigation"] =
                NAVIGATION_LINKS_OR_NOTIFICATIONS.PARTICIPATION;
            inNavigation(params);
          } else {
            params["navigation"] = NAVIGATION_LINKS_OR_NOTIFICATIONS.CHALLENGE;
            inNavigation(params);
          }
        } else {
          if (params.containsKey("user")) {
            //usuario
            params["navigation"] = NAVIGATION_LINKS_OR_NOTIFICATIONS.PROFILE;
            inNavigation(params);
          }
        }
      }
    }
  }

  void initMessaging() {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        new FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        var data = message["data"] ?? message;

        try {
          var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
              '9999',
              'darhuChallenges',
              'Es una canal para notificaciones de challenges darhu');

          var iOSPlatformChannelSpecifics =
              new IOSNotificationDetails(sound: "slow_spring_board.aiff");
          var platformChannelSpecifics = new NotificationDetails(
              androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
          if (Platform.isAndroid) {
            await flutterLocalNotificationsPlugin.show(
              0,
              message["notification"]["title"],
              message["notification"]["body"],
              platformChannelSpecifics,
              payload: json.encode(data),
            );
          }
        } catch (error) {
          print("Error en las notificaciones $error");
        }
      },
      onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        var data = message["data"] ?? message;
        processDataNotifications(data);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume fl: $message");
        var data = message["data"] ?? message;
        print("la data que llego es $data");
        try {
          processDataNotifications(data);
        } catch (ex) {
          print(ex);
        }
      },
    );
  }

  Future onSelectNotification(payload) async {
    try {
      if (payload != null) {
        print('notification payload: ' + payload);
        Map data = json.decode(payload);
        processDataNotifications(data);
      }
    } catch (ex) {
      print("No se pudo procesar la info de la notificacion $ex");
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {}

  processDataNotifications(Map<dynamic, dynamic> params) {
    print("data $params");
    //Se debe revisar este metodo
    if (params.length > 0) {
      print("data si tiene data");
      if (params.containsKey("challenge")) {
        print("data tiene reto");
        //Reto
        if (params.containsKey("participation")) {
          //Participacion en Reto
          print("data tiene participacion");
          params["navigation"] =
              NAVIGATION_LINKS_OR_NOTIFICATIONS.PARTICIPATION;
          inNavigation(params);
        } else {
          params["navigation"] = NAVIGATION_LINKS_OR_NOTIFICATIONS.CHALLENGE;
          inNavigation(params);
        }
      } else {
        if (params.containsKey("user")) {
          //usuario
          print("si tiene el user");
          params["navigation"] = NAVIGATION_LINKS_OR_NOTIFICATIONS.PROFILE;
          inNavigation(params);
        }
      }
    }
  }

  void saveTokenNotification() async {
    api.notificationsApi.getToken().then((token) async {
      print("El token es $token");
      if (await api.preferencesApi.isTokenExists(token)) {
      } else {
        await api.preferencesApi.saveTokenNotification(token);
        try {
          MDeviceUser deviceUser = await DeviceInfo.getDevicesInfo()
            ..token = token;
          await api.firestoreApi
              .updateTokenNotification(user.idUser, deviceUser);
        } catch (e) {
          print("Problema al sincronizar el token" + e.toString());
        }
      }
    });
  }

  getChallenge() {
    challenge.urlThumbnail = challenge.multimedia.thumbnail.item.toString();
    challenge.datePost = DateTime.now();
    return challenge;
  }

  resetChallenge() {
    challenge = null;
  }
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  print(" notifcacion en background");
  if (message.containsKey('data')) {
    //final dynamic data = message['data'] ?? message;
  }

  if (message.containsKey('notification')) {
    //final dynamic notification = message['notification'];
  }
}

abstract class ConnectivityHandler {
  void onError(String error);
  void onConnectivityChanged(ConnectivityResult connectivity);
}
