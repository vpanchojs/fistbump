import 'package:Darhu/src/ui/pages/login/v_login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'src/res/colors_res.dart';
import 'src/res/style_res.dart';
import 'utils/routes.dart';

class MyApp extends StatelessWidget {
  final titleApp;

  const MyApp({Key key, this.titleApp}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          fontFamily: GoogleFonts.montserrat().fontFamily,
          iconTheme: new IconThemeData(color: Colors.black, opacity: 1.0),
          primaryIconTheme:
              new IconThemeData(color: Colors.black, opacity: 1.0),
          primaryTextTheme: TextTheme(headline6: StyleRes.text_appbar),
          accentIconTheme: new IconThemeData(color: Colors.white, opacity: 1.0),
          primaryColor: Colors.white,                    
          accentColor: ColorsRes.purple_color),
      title: titleApp,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      routes: getRoutes(),
      initialRoute: VLogin.routeName,
      supportedLocales: [
        const Locale('en'),
        const Locale('es'),
      ],
    );
  }
}
